package com.knight.plat.demo.controller;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private TestService testService;


    @RequestMapping(path = "/test/consume", method = RequestMethod.GET)
    public ResultDTO<String> consumeInventory(@RequestParam(required = false, name = "money") Integer money) {
        Integer inventory = testService.consumeInventory(money);
        return ResultDTO.ok("剩余余额：" + inventory);
    }

}
