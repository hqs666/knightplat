package com.knight.plat.demo.service.impl;

import com.knight.plat.api.eventbus.EventBus;
import com.knight.plat.api.exceptions.BusinessException;
import com.knight.plat.demo.controller.TestController;
import com.knight.plat.demo.service.TestService;
import com.knight.plat.redis.lock.annoation.RedisLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service
public class TestServiceImpl implements TestService {

    private final static Logger log = LoggerFactory.getLogger(TestController.class);

    /**
     * 模拟个人钱包账户 并发读写问题
     */
    public static int balance = 10;

    private EventBus eventBus;

    @RedisLock(isFixedKey = true, synKey = "TestServiceImpl#consumeInventory")
    @Override
    public Integer consumeInventory(Integer extractMoney) {
        if (extractMoney > balance) {
            throw new BusinessException("余额不足,提现失败");
        }
        balance = balance - extractMoney;
        log.info("提现成功，剩余余额：{}", balance);
        return balance;
    }
}
