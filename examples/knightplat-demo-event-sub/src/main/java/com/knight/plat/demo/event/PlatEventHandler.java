package com.knight.plat.demo.event;

import com.knight.plat.api.annotations.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PlatEventHandler {

    private static final Logger log = LoggerFactory.getLogger(PlatEventHandler.class);

    @EventListener("${event.redisTopic}")
    public void PlatEventHandler(String eventParam) {
        log.info("EVENT-SUB：redisTopic 消费进入 参数：{}", eventParam);
    }

}
