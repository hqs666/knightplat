package com.knight.plat.demo.event.redis;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.eventbus.EventBus;
import com.knight.plat.commons.JsonObject;
import com.knight.plat.commons.JsonRoot;
import com.knight.plat.timer.delayed.DelayBusPersistenceHandler;
import com.knight.plat.timer.delayed.DelayedBus;
import com.knight.plat.timer.delayed.StandaloneDelayedBus;
import com.knight.plat.timer.delayed.TimerManager;
import com.knight.plat.utils.JsonUtils;
import com.knight.plat.utils.KeyGenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@RestController
public class RedisEventController {

    @Autowired
    private EventBus eventBus;
    @Autowired
    private DelayedBus delayedBus;

    @Value("${event.redisTopic}")
    private String redisTopic;

    @GetMapping(path = "/test/redis")
    public ResultDTO<Boolean> createRedisEvent(@RequestParam(name = "param", required = false) String eventParam) {
        //广播推送
        eventBus.publish(redisTopic, String.class, eventParam);
        return ResultDTO.ok(Boolean.TRUE);
    }

    @GetMapping(path = "/test/redis/delay")
    public ResultDTO<Boolean> createDelayRedisEvent(@RequestParam(name = "param", required = false) String eventParam,
                                                    @RequestParam(name = "time",required = false)Integer count) {
        //延迟广播推送
        CompletableFuture<String> str = delayedBus.delayPublish(1000 * count, TimeUnit.MILLISECONDS, redisTopic, String.class, eventParam);
        try {
            String s = str.get();
            System.out.println(s);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
        return ResultDTO.ok(Boolean.TRUE);
    }

}
