package com.knight.plat.demo.event.rabbitmq;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RabbitEventController {

    @Autowired
    private EventBus eventBus;

    @Value("${event.rabbitTopic}")
    private String rabbitTopic;

    @GetMapping(path = "/test/rabbit")
    public ResultDTO<Boolean> createEvent(@RequestParam(name = "param", required = false) String eventParam) {
        //事件推送
        eventBus.send(rabbitTopic, String.class, eventParam);
        return ResultDTO.ok(Boolean.TRUE);
    }

}
