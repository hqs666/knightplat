package com.knight.plat.demo.handler;

import com.knight.plat.api.annotations.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class EventHandler {

    private final static Logger log = LoggerFactory.getLogger(EventHandler.class);

    @EventListener("${event.localTopic}")
    public void LocalEventHandle(String eventParam) {
        log.info("localEvent 消费进入 参数：{}", eventParam);
    }

    @EventListener(router = "${event.redisTopic}")
    public void redisEventHandle(String eventParam) {
        log.info("redisTopic 消费进入 参数：{}", eventParam);
    }

}
