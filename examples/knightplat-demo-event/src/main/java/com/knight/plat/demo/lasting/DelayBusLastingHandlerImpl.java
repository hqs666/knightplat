package com.knight.plat.demo.lasting;

import com.knight.plat.redis.RedisUtils;
import com.knight.plat.timer.delayed.DelayBusPersistenceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 持久化任务处理
 * 这里的持久化借助了redis
 */
@Component
public class DelayBusLastingHandlerImpl implements DelayBusPersistenceHandler {

    private final static Logger log = LoggerFactory.getLogger(DelayBusLastingHandlerImpl.class);

    private final static String LASTING_KEY = "Lasting_Redis";
    private static Map<String, TaskMeta> TASK_METAS = new HashMap<>();

    static {
        TASK_METAS = new HashMap<>();
    }

    @Override
    public <T> boolean saveTaskMeta(TaskMeta<T> meta) {
        TASK_METAS.put(meta.getId(), meta);
        RedisUtils.put(LASTING_KEY, TASK_METAS);
        return true;
    }

    @Override
    public <T> void onTaskFinish(TaskMeta<T> meta) {
        TASK_METAS.remove(meta.getId());
        RedisUtils.put(LASTING_KEY, TASK_METAS);
    }

    @Override
    public <T> void onTaskFail(TaskMeta<T> meta, Throwable e) {
        log.error("事件重启异常：{}",e);
        TASK_METAS.remove(meta.getId());
        RedisUtils.put(LASTING_KEY, TASK_METAS);
    }

    @Override
    public <T> boolean onTaskCancel(TaskMeta<T> meta) {
        TASK_METAS.remove(meta.getId());
        RedisUtils.put(LASTING_KEY, TASK_METAS);
        return true;
    }

    @Override
    public List<TaskMeta<?>> loadTaskMeta() {
        TASK_METAS = RedisUtils.get(LASTING_KEY, Map.class);
        List<TaskMeta<?>> data = new ArrayList<>();
        if (null == TASK_METAS || TASK_METAS.isEmpty()) {
            return data;
        }

        TASK_METAS.forEach((s, taskMeta) -> {
            data.add(taskMeta);
        });
        return data;
    }
}
