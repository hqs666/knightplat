package com.knight.plat.demo.event.local;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LocalEventController {

    @Autowired
    private EventBus eventBus;

    @Value("${event.localTopic}")
    private String localTopic;

    @RequestMapping(path = "/test/local", method = RequestMethod.GET)
    public ResultDTO<Boolean> createLocalEvent(@RequestParam(name = "param", required = false) String eventParam) {
        //事件推送
        eventBus.send(localTopic, String.class, eventParam);
        return ResultDTO.ok(Boolean.TRUE);
    }

}
