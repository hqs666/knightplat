package com.knight.plat.demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("enc")
public class ConfigTest {

    private final static Logger log = LoggerFactory.getLogger(ConfigTest.class);

    private String testPwd;

    public String getTestPwd() {
        return testPwd;
    }

    public void setTestPwd(String testPwd) {
        log.info("读取到加密文件内容 testPwd = {}", testPwd);
        this.testPwd = testPwd;
    }

    @Override
    public String toString() {
        return "{"
                + "\"testPwd\":\""
                + testPwd + '\"'

                + "}";
    }


}
