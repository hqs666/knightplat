package com.knight.plat.demo;

import com.knight.plat.utils.EncryptUtils;

import java.security.spec.InvalidKeySpecException;

public class TestEnc {

    public static void main(String[] args) {
        String key = "ceshiencjiami";
        String pwd = "解密后的明文";
        try {
            String entryStr = EncryptUtils.aesEncryptToString(pwd, key).toString();
            System.out.println("nacos要配置的密文 = " + entryStr);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

}
