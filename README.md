# knightplat

#### 介绍

Knightplat是一款web应用开发的自用开发框架，参照springboot源码风格 集成了主流的web框架及自行扩展的组件与工具类。

此项目包含的组件内容，主要选自Spring、Spring Cloud Alibaba 系列套件，但也不局限与这些产品

如果您对此作品有更好的建议或想法，欢迎在issues中或通过社区其他渠道提出，共同讨论。

#### 软件架构

软件除了以java进行代码编写以外，引用了kotlin和rust进行代码配合。

项目构建工具使用的是gradle，使用版本为gradle-7.2

java支持版本 java8

kotlin建议版本 1.8

rust建议版本 1.70.0

#### 安装教程

以maven项目为例，将pom引入如下

```
    <parent>
        <groupId>com.knight.plat</groupId>
        <artifactId>knightplat-parent</artifactId>
        <version>${knightplat.version}</version>
    </parent>
```
除此之外可以引用更多的框架组件例如eureka套件可以像如下引用
```
        <dependency>
            <groupId>com.knight.plat</groupId>
            <artifactId>knightplat-supporter-eureka</artifactId>
        </dependency>
```

然后可以像如下图进行应用后启动此项目这里和springboot项目的操作是一致的
![img.png](img.png)


