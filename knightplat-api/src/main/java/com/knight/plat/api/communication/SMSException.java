/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.communication;

/**
 * Created on 2016/11/23.
 */
public class SMSException extends Exception {

    public SMSException() {
    }

    public SMSException(String message) {
        super(message);
    }

    public SMSException(Throwable cause) {
        super(cause);
    }
}
