package com.knight.plat.api.commons;

@FunctionalInterface
public interface Callable<R> {
    R call() throws Throwable;
}
