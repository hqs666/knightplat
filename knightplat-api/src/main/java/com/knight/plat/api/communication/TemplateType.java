/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.communication;

/**
 * Created on 16/8/18.
 */
public enum TemplateType {
    MUSTACHE,
    FREEMARKER,
    THYMELEAF,
    VELOCITY
}
