package com.knight.plat.api.runtime;

import com.knight.plat.api.commons.Cookie;

import java.util.List;
import java.util.Map;

/**
 * 代表一个function部署实例，通常对应一次脚本源文件提交和载入的完整生命周期。
 */
public interface Deployment extends AutoCloseable {

    /**
     * 启动function实例
     * @return 本对象方便链式调用
     */
    Deployment start(Map<String, Object> globalMap);

    /**
     * 关闭实例，清理上下文
     */
    void shutdown();

    /**
     * 调用function逻辑得到处理后的context对象
     * @param context 传入function的上下文
     * @return 处理后的上下文
     */
    Context call(final Context context) throws FunctionException;

    /**
     * 获取当前function部署源文件的字符串
     * @return 源文件字符串
     */
    String readString();

    /**
     * 获取本部署的唯一id
     * @return id
     */
    long getId();

    default Object fetchResponseBody(Context context) {
        return context.getResponseBody();
    }

    default Map<String, List<String>> fetchSetHeaders(Context context) {
        return context.getSetHeaders();
    }

    default List<Cookie> fetchSetCookies(Context context) {
        return context.getSetCookies();
    }
}
