/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.mq;


public interface Handler<T> {

    String getTopic();

    String getTag();

    Class<T> getType();

    void handle(T input) throws Exception;
}
