package com.knight.plat.api.job;

/**
 * @author neo
 *   xxlJob开发步骤：
 *      1、任务开发：在Spring Bean实例中，开发Job方法；
 *      2、注解配置：继承IJobHandler并实现JobExecute, 使用@Component标记，系统将自动进行注册
 *      3、执行日志：需要通过 "XxlJobHelper.log" 打印执行日志；
 *      4、任务结果：默认任务结果为 "成功" 状态，不需要主动设置；如有诉求，比如设置任务结果为失败，可以通过 "XxlJobHelper.handleFail/handleSuccess" 自主设置任务结果；
 *      5、执行参数  通过XxlJobHelper.getJobParam()获取
 */
@FunctionalInterface
public interface JobHandler {

    ThreadLocal<JobLogger> loggerContainer = new ThreadLocal<>();

    void execute(String params, JobLogger logger) throws Exception;

    default void execute(String params) throws Exception {
        execute(params, getLogger());
    }

    default JobLogger getLogger() {
        final JobLogger logger = loggerContainer.get();
        if (logger != null) {
            return logger;
        }
        return JobLogger.SIMPLE_LOGGER;
    }
}
