/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.log;

import java.util.Date;


public interface LogProvider {

    /**
     * Send a log with ip
     *
     * @param ip         remote user ip
     * @param createTime time
     * @param content    log body
     * @deprecated use the one with more info params.
     */
    @Deprecated
    void log(String ip, Date createTime, LogContent content);

    /**
     * Send a log with ip
     *
     * @param ip         remote user ip
     * @param createTime time
     * @param content    log body
     * @param platform   log platform
     * @param userAgent  user-agent info.
     */
    void log(String platform, String userAgent, String ip, Date createTime, LogContent content);
}
