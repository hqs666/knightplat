package com.knight.plat.api.executor;

public interface RunnableFilter {
    Runnable filter(Runnable origin);
}
