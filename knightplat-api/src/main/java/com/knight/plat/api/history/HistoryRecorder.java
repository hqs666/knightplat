package com.knight.plat.api.history;

@FunctionalInterface
public interface HistoryRecorder {
    void log(BaseHistoryLog log);
}
