package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;

public interface SM2Engine {
    ByteBuffer sm2Encode(byte[] data, byte[] publicKey, boolean isBase64) throws InvalidKeySpecException;

    ByteBuffer sm2Decode(byte[] data, byte[] privateKey, boolean isBase64) throws InvalidKeySpecException;

    default SM2Engine asSM2Engine() {
        return this;
    }
}
