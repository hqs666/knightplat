package com.knight.plat.api.convertible;

public interface Copiable<T> {

    T copy();
}
