/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.exceptions;

import java.util.Map;


public interface ExceptionCodeManager {

    void registerCode(String code, Object message);

    default void registerCodes(Map<String, Object> codeMap) {
        if (null != codeMap && !codeMap.isEmpty()) {
            for (Map.Entry<String, Object> entry : codeMap.entrySet()) {
                registerCode(entry.getKey(), entry.getValue());
            }
        }
    }

    Object getMessage(String code);
}
