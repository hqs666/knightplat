package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;

public interface SM4Engine {
    ByteBuffer sm4Encode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException;

    ByteBuffer sm4Decode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException;

    default SM4Engine asSM4Engine() {
        return this;
    }
}
