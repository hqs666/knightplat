/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.log;

public enum Level {
    TRACE, DEBUG, INFO, WARN, ERROR, FATAL
}
