package com.knight.plat.api.filesystem;

public final class FileChannelPair {
    private final FileManager fileManager;
    private final String channel;

    public FileChannelPair(FileManager fileManager, String channel) {
        this.fileManager = fileManager;
        this.channel = channel;
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    public String getChannel() {
        return channel;
    }
}
