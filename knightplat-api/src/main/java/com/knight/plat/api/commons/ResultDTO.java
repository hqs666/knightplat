/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.commons;

import com.knight.plat.api.exceptions.BusinessException;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 16/8/10.
 */
@Schema(name = "请求结果", description = "对接口请求返回的结果进行封装")
public class ResultDTO<T> implements Serializable {

    @Schema(name = "是否成功", description = "是否成功")
    private Boolean success = Boolean.TRUE;

    @Schema(name = "返回代码", description = "返回代码")
    private String code = "OK";

    @Schema(name = "返回描述", description = "返回描述")
    private String message = "OK";

    @Schema(name = "页码", description = "页码")
    private Integer pageNum;

    @Schema(name = "每页条数", description = "每页条数")
    private Integer pageSize;

    @Schema(name = "总页数", description = "总页数")
    private Integer pageCount;

    @Schema(name = "总条数", description = "总条数")
    private Long total;

    @Schema(name = "返回的实际数据", description = "返回的实际数据")
    private T data;

    @ArraySchema(schema = @Schema(name = "字段验证错误信息", description = "字段验证错误信息"))
    private List<FieldErrorInfo> fieldErrors;

    @Schema(name = "元数据", description = "元数据")
    private Map<String, Object> meta;

    public ResultDTO() {
        // Do nothing.
    }

    public ResultDTO(T data) {
        this.data = data;
    }

    public ResultDTO(T data, Map<String, Object> meta) {
        this.data = data;
        this.meta = meta;
    }

    /**
     * 使用ResultDTO.empty()方法
     * @param <T> 数据类型
     * @return 返回对象
     */
    @Deprecated
    public static <T> ResultDTO<T> newResult() {
        return empty();
    }

    /**
     * 使用ResultDTO.ok(T data)方法
     * @param data 包装的对象
     * @param <T> 数据类型
     * @return 返回对象
     */
    @Deprecated
    public static <T> ResultDTO<T> newResult(T data) {
        return ok(data);
    }

    /**
     * 使用ResultDTO.ok(T data, Map&lt;String, Object&gt; meta)方法
     * @param data 包装的对象
     * @param meta 结果元数据
     * @param <T> 数据类型
     * @return 返回对象
     */
    @Deprecated
    public static <T> ResultDTO<T> newResult(T data, Map<String, Object> meta) {
        return ok(data, meta);
    }

    /**
     * 创建空的返回对象
     * @param <T> 数据类型
     * @return 返回对象
     */
    public static <T> ResultDTO<T> empty() {
        return new ResultDTO<>();
    }

    /**
     * 创建表示成功的返回对象
     * @param data 数据对象
     * @param <T> 数据类型
     * @return 返回对象
     */
    public static <T> ResultDTO<T> ok(T data) {
        return new ResultDTO<>(data);
    }

    /**
     * 创建表示成功的返回对象
     * @param data 数据对象
     * @param meta 元数据map
     * @param <T> 数据类型
     * @return 返回对象
     */
    public static <T> ResultDTO<T> ok(T data, Map<String, Object> meta) {
        return new ResultDTO<>(data, meta);
    }

    public static <T> ResultDTO<T> err(String code, String msg) {
        ResultDTO<T> inst = new ResultDTO<>();
        inst.code = code;
        inst.message = msg;
        inst.success = false;
        return inst;
    }

    public static <T> ResultDTO<T> err(String msg) {
        return err("RY00", msg);
    }

    public static <T> ResultDTO<T> err(BusinessException e) {
        ResultDTO<T> inst = new ResultDTO<>();
        inst.code = e.getCode();
        inst.message = e.getMessage();
        inst.success = false;
        return inst;
    }

    /**
     * 根据提供的mapper函数自动转换集合中的每个对象，并生成保存转换后列表的返回对象
     * @param origin 原始集合
     * @param mapper 转换函数
     * @param <R> 原数据类型
     * @param <D> 转换后数据类型
     * @return 返回对象
     */
    public static <R, D> ResultDTO<List<D>> map(Collection<R> origin, Function<R, D> mapper) {
        List<D> list = origin.stream().map(mapper).collect(Collectors.toList());
        return new ResultDTO<>(list);
    }

    /**
     * 根据提供的mapper函数自动转换集合中的每个对象，并生成保存转换后列表的返回对象
     * @param origin 原始集合
     * @param mapper 转换函数
     * @param meta 元数据map
     * @param <R> 原数据类型
     * @param <D> 转换后数据类型
     * @return 返回对象
     */
    public static <R, D> ResultDTO<List<D>> map(Collection<R> origin, Function<R, D> mapper, Map<String, Object> meta) {
        List<D> list = origin.stream().map(mapper).collect(Collectors.toList());
        return new ResultDTO<>(list, meta);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Map<String, Object> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, Object> meta) {
        this.meta = meta;
    }

    public Object getMetaValue(String key) {
        if (meta == null) {
            return null;
        }
        return meta.get(key);
    }

    public void setMetaValue(String key, Object value) {
        if (meta == null) {
            meta = new HashMap<>();
        }
        meta.put(key, value);
    }

    public List<FieldErrorInfo> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<FieldErrorInfo> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ResultDTO.class.getSimpleName() + "[", "]")
                .add("success=" + success)
                .add("code='" + code + "'")
                .add("message='" + message + "'")
                .add("pageNum=" + pageNum)
                .add("pageSize=" + pageSize)
                .add("pageCount=" + pageCount)
                .add("total=" + total)
                .add("data=" + data)
                .add("fieldErrors=" + fieldErrors)
                .add("meta=" + meta)
                .toString();
    }
}
