package com.knight.plat.api.security;

public interface PasswordProvider {

    byte[] password(String userId);
}
