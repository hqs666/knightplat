/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.communication.domain;

/**
 * Created on 2016/11/28.
 */
public enum MessageStatus {
    READ, UNREAD, OTHER
}
