package com.knight.plat.api.commons;

@FunctionalInterface
public interface ParametizedCallable<IN, R> {
    R call(IN input) throws Exception;
}
