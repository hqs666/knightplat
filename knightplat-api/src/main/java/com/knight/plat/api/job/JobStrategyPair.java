package com.knight.plat.api.job;

public final class JobStrategyPair {
    private final JobStrategy strategy;
    private final JobLogger logger;
    private final String schema;

    public JobStrategyPair(String schema, JobStrategy strategy, JobLogger logger) {
        this.strategy = strategy;
        this.schema = schema;
        this.logger = logger;
    }

    public JobStrategy getStrategy() {
        return strategy;
    }

    public String getSchema() {
        return schema;
    }

    public JobLogger getLogger() {
        return logger;
    }
}
