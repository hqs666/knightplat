/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.commons;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Created on 16/8/11.
 */
@Schema(name = "分页信息对象", description = "对分页信息进行封装")
public class SimplePageInfo implements Serializable {

    @Schema(name = "页码", description = "页码")
    private Integer pageNum = 1;

    @Schema(name = "每页条数", description = "每页条数")
    private Integer pageSize = 10;

    @Schema(name = "是否返回总数", description = "是否返回总数")
    private Boolean count = Boolean.TRUE;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Boolean getCount() {
        return count;
    }

    public void setCount(Boolean count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SimplePageInfo.class.getSimpleName() + "[", "]")
                .add("pageNum=" + pageNum)
                .add("pageSize=" + pageSize)
                .add("isCount=" + count)
                .toString();
    }
}
