/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.filesystem;

import com.knight.plat.api.commons.Callback;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public interface FileManager {

    /**
     * 存储文件
     *
     * @param data 文件内容
     * @param bucket 文件所属组(bucket)
     * @param path 文件路径
     * @return 文件最终访问路径
     * @throws IOException 需要处理IO异常情况
     */
    @Deprecated
    String save(InputStream data, String bucket, String path) throws IOException;

    /**
     * 存储文件
     *
     * @param data 文件内容
     * @param contentLength 文件大小(bytes)
     * @param bucket 文件所属组(bucket)
     * @param path 文件路径
     * @return 文件最终访问路径
     * @throws IOException 需要处理IO异常情况
     */
    String save(InputStream data, long contentLength, String bucket, String path) throws IOException;

    /**
     * 异步存储文件
     *
     * @param data 文件内容
     * @param bucket 文件所属组(bucket)
     * @param path 文件路径
     * @param callback 存储完成后回调
     */
    @Deprecated
    void saveAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback);

    /**
     * 异步存储文件
     *
     * @param data 文件内容
     * @param contentLength 文件大小(bytes)
     * @param bucket 文件所属组(bucket)
     * @param path 文件路径
     * @param callback 存储完成后回调
     */
    void saveAsync(InputStream data, long contentLength, String bucket, String path, Callback<String, IOException> callback);

    /**
     * 追加文件内容
     *
     * @param data   文件内容
     * @param bucket 文件所属组(bucket)
     * @param path   文件路径
     * @return 文件最终访问路径
     * @throws IOException 需要处理IO异常情况
     */
    String append(InputStream data, String bucket, String path) throws IOException;

    /**
     * 异步追加文件内容
     *
     * @param data     文件内容
     * @param bucket   文件所属组(bucket)
     * @param path     文件路径
     * @param callback 存储完成后回调
     */
    void appendAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback);

    /**
     * 读取文件
     *
     * @param bucket 文件所属组(bucket)
     * @param path   文件路径
     * @return 文件内容
     * @throws IOException 需要处理IO异常情况
     */
    byte[] read(String bucket, String path) throws IOException;

    /**
     * 获取文件大小
     *
     * @param bucket 文件所属组(bucket)
     * @param path 文件路径
     * @return 文件大小
     * @throws IOException 需要处理IO异常情况
     */
    long size(String bucket, String path) throws IOException;

    /**
     * 读取文件
     *
     * @param bucket 文件所属组(bucket)
     * @param path   文件路径
     * @return 文件内容
     * @throws IOException 需要处理IO异常情况
     */
    InputStream readAsInputStream(String bucket, String path) throws IOException;

    /**
     * 异步读取文件
     *
     * @param bucket   文件所属组(bucket)
     * @param path     文件路径
     * @param callback 存储完成后回调
     */
    void readAsync(String bucket, String path, Callback<byte[], IOException> callback);

    /**
     * 异步读取文件
     *
     * @param bucket   文件所属组(bucket)
     * @param path     文件路径
     * @param callback 存储完成后回调
     */
    void readAsInputStreamAsync(String bucket, String path, Callback<InputStream, IOException> callback);

    /**
     * 读取文件
     *
     * @param bucket 文件所属组(bucket)
     * @param path   文件路径
     * @param params 额外处理参数
     * @return 文件最终访问路径
     * @throws IOException 需要处理IO异常情况
     */
    byte[] read(String bucket, String path, String params) throws IOException;

    /**
     * 异步读取文件
     *
     * @param bucket   文件所属组(bucket)
     * @param path     文件路径
     * @param params   额外处理参数
     * @param callback 存储完成后回调
     */
    void readAsync(String bucket, String path, String params, Callback<byte[], IOException> callback);

    /**
     * 创建文件上传请求
     *
     * @param bucket 文件所属组(bucket)
     * @param path   文件路径
     * @return
     */
    String createMultipartUpload(String bucket, String path);

    /**
     * @param bucket   文件所属组(bucket)
     * @param path     文件路径
     * @param chunk    文件上传 分片片数
     * @param data     分片文件流
     * @param uploadId 分片上传请求ID
     * @return
     * @throws IOException
     */
    Part multipartUpload(String bucket, String path, Integer chunk, InputStream data, String uploadId)
            throws IOException;

    /**
     * @param bucket   文件所属组(bucket)
     * @param path     文件路径
     * @param parts    分片文件流集
     * @param uploadId 分片上传请求ID
     * @return
     * @throws IOException
     */
    String completeMultipartUpload(String bucket, String path, List<Part> parts, String uploadId) throws IOException;


    /**
     *  获取期限url
     * @param bucketName 文件所属组(bucket)
     * @param key 文件key
     * @param expireMillisecond 有效时间  单位 毫秒
     * @return 文件url
     */
    String getExpireUrl(String bucketName, String key, Long expireMillisecond);


    /**
     * 删除文件
     * @param bucket 文件路径（组）
     * @param path 文件名
     * @return 删除结果
     * @throws IOException 需要处理IO异常情况
     */
    boolean dropFile(String bucket, String path) throws IOException;


    /**
     * 异步删除文件
     * @param bucket 文件路径（组）
     * @param path 文件名
     * @param callback 返回结果
     */
    void dropFileAsync(String bucket, String path, Callback<Boolean, IOException> callback);
}
