package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;

public interface Base64Engine {
    ByteBuffer base64Encode(byte[] data, boolean isUrl);

    ByteBuffer base64Decode(byte[] data, boolean isUrl);

    default Base64Engine asBase64Engine() {
        return this;
    }
}
