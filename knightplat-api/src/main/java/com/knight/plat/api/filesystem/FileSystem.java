package com.knight.plat.api.filesystem;

public interface FileSystem {

    public void registerFileManager(String channel, FileManager fileManager);

    public FileManager channel(String channel);

    public FileManager channel(FileChannel channel);
}
