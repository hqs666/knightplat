/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.search.domain;

/**
 * Created on 2016/11/11.
 */
public enum Status {
    OK, FAIL
}
