package com.knight.plat.api.eventbus;

import com.knight.plat.api.commons.Callback;

import java.nio.charset.StandardCharsets;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

/**
 * 异步事件处理策略接口
 * Add on 2021/01/20
 *
 * @author knightplat
 * @since 1.0.12
 */
public interface EventStrategy {

    // 操作全集
    Set<Actions> allActions = EnumSet.allOf(Actions.class);

    /**
     * 注册事件消费订阅处理器
     * @param router 事件路由地址
     * @param clazz 事件数据类型对象
     * @param handler 事件消费处理器
     * @param <T> 事件数据类型
     */
    <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler);

    /**
     * 注册事件消费订阅处理器 - 同步处理
     * @param router 事件路由地址
     * @param clazz 事件数据类型对象
     * @param handler 事件消费处理器
     * @param <T> 事件数据类型
     */
    <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler);

    /**
     * 注册事件消费订阅处理器
     * @param router 事件路由地址
     * @param clazz 事件数据类型对象
     * @param handler 事件消费处理器
     * @param <T> 事件数据类型
     */
    <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler);

    /**
     * 注册事件消费订阅处理器 - 同步处理
     * @param router 事件路由地址
     * @param clazz 事件数据类型对象
     * @param handler 事件消费处理器
     * @param <T> 事件数据类型
     */
    <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler);

    /**
     * 发送异步事件到单个订阅者
     * @param router 事件路由地址
     * @param clazz 事件数据类型对象
     * @param payload 事件数据对象
     * @param result 处理完的回调方法
     * @param <T> 事件数据类型
     */
    <T> void send(String router, Class<T> clazz, T payload, Callback<T, Throwable> result);

    /**
     * 发送异步事件到所有订阅者
     * @param router 事件路由地址
     * @param clazz 事件数据类型对象
     * @param payload 事件数据对象
     * @param result 处理完的回调方法
     * @param <T> 事件数据类型
     */
    <T> void publish(String router, Class<T> clazz, T payload, Callback<T, Throwable> result);

    default <T, R> void request(String router, Class<T> clazz, T payload, Callback<R, Exception> callback) {

    }

    /**
     * 返回支持的动作
     * @return 支持的动作
     */
    default Set<Actions> supportedActions() {
        return allActions;
    }

    /**
     * 关闭处理通道
     */
    void close();

    default <T> byte[] encodeValue(T value, Class<T> clazz, ValueEncoder<T> encoder) throws Throwable {
        byte[] bytes;
        if (byte[].class == clazz && byte[].class == value.getClass()) {
            bytes = (byte[]) value;
        } else if (String.class == clazz && String.class == value.getClass()) {
            bytes = ((String) value).getBytes(StandardCharsets.UTF_8);
        } else {
            bytes = encoder.encode(value);
        }
        return bytes;
    }

    default <T> T decodeValue(byte[] bytes, Class<T> clazz, ValueDecoder<T> decoder) throws Throwable {
        T value;
        if (byte[].class == clazz) {
            value = (T) bytes;
        } else if (String.class == clazz) {
            @SuppressWarnings("unchecked")
            T unchecked = (T) new String(bytes, StandardCharsets.UTF_8);
            value = unchecked;
        } else {
            value = decoder.decode(bytes, clazz);
        }
        return value;
    }

    // 封装处理器和数据类型
    class ConsumerMeta<T> {
        Class<T> clazz;
        ConsumerHandler<T> handler;
        ErrorHandler errorHandler;

        public void handle(T value) {
            try {
                handler.handle(value);
            } catch (Throwable e) {
                errorHandler.handle(e);
            }
        }

        public static <T> ConsumerMeta<T> of(Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
            ConsumerMeta<T> meta = new ConsumerMeta<>();
            meta.clazz = clazz;
            meta.handler = handler;
            meta.errorHandler = errorHandler;
            return meta;
        }

        public void fail(Throwable e) {
            errorHandler.handle(e);
        }

        public Class<T> valueType() {
            return this.clazz;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            ConsumerMeta<?> that = (ConsumerMeta<?>) o;

            if (!clazz.equals(that.clazz)) {
                return false;
            }
            if (!handler.equals(that.handler)) {
                return false;
            }
            return Objects.equals(errorHandler, that.errorHandler);
        }

        @Override
        public int hashCode() {
            int result = clazz.hashCode();
            result = 31 * result + handler.hashCode();
            result = 31 * result + (errorHandler != null ? errorHandler.hashCode() : 0);
            return result;
        }
    }
}
