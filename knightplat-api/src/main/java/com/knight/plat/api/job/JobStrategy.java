package com.knight.plat.api.job;

@FunctionalInterface
public interface JobStrategy {
    void registerJob(String name, JobHandler jobHandler, JobLogger logger);
}
