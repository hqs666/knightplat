package com.knight.plat.api.commons;

public final class Constants {
    private Constants() {
        // do nothing.
    }

    public static final String RY_REQUEST_KEY = "knight-request";
}
