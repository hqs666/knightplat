package com.knight.plat.api.history;

import java.util.HashMap;
import java.util.Map;

public class History {
    private final String appName;
    private Map<Class<? extends HistoryRecorder>, HistoryRecorder> recorders;

    public History(String appName) {
        this.appName = appName;
    }

    public void registerRecorder(HistoryRecorder recorder) {
        if (recorders == null) {
            recorders = new HashMap<>();
        }
        recorders.put(recorder.getClass(), recorder);
    }

    public void log(BaseHistoryLog log) {
        log.setApp(appName);
        recorders.forEach((clazz, recorder) -> recorder.log(log));
    }
}
