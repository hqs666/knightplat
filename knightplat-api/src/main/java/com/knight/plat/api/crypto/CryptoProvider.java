package com.knight.plat.api.crypto;

public interface CryptoProvider extends AESEngine, DESEngine, HashEngine, RSAEngine, SM2Engine, SM4Engine, HMACEngine, Base64Engine {

}
