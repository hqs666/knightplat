/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.log;

import java.util.Date;
import java.util.StringJoiner;


public class LogContent {

    private Level level = Level.INFO;
    private Exception exception = null;

    private String type;

    private String module;
    private String operating;
    private String operator;
    private String content;
    private Date operateDate;

    private LogContent() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public Level getLevel() {
        return level;
    }

    public Exception getException() {
        return exception;
    }

    public String getType() {
        return type;
    }

    public String getModule() {
        return module;
    }

    public String getOperating() {
        return operating;
    }

    public String getOperator() {
        return operator;
    }

    public String getContent() {
        return content;
    }

    public Date getOperateDate() {
        return operateDate;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", LogContent.class.getSimpleName() + "[", "]")
                .add("level=" + level)
                .add("exception=" + exception)
                .add("type='" + type + "'")
                .add("module='" + module + "'")
                .add("operating='" + operating + "'")
                .add("operator='" + operator + "'")
                .add("content='" + content + "'")
                .add("operateDate=" + operateDate)
                .toString();
    }

    public static class Builder {

        private Level level = Level.INFO;
        private Exception exception = null;

        private String type;

        private String module;
        private String operating;
        private String operator;
        private String content;
        private Date operateDate;

        private Builder() {
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder module(String module) {
            this.module = module;
            return this;
        }

        public Builder operating(String operating) {
            this.operating = operating;
            return this;
        }

        public Builder operator(String operator) {
            this.operator = operator;
            return this;
        }

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Builder operateDate(Date operateDate) {
            this.operateDate = operateDate;
            return this;
        }

        public Builder level(Level level) {
            this.level = level;
            return this;
        }

        public Builder exception(Exception exception) {
            this.exception = exception;
            return this;
        }

        public LogContent build() {
            LogContent logContent = new LogContent();

            logContent.level = level;
            logContent.exception = exception;
            logContent.type = type;
            logContent.module = module;
            logContent.operating = operating;
            logContent.operator = operator;
            logContent.content = content;
            logContent.operateDate = operateDate;

            return logContent;
        }
    }
}
