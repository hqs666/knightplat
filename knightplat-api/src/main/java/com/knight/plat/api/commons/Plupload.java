package com.knight.plat.api.commons;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * /** Plupload是一个上传插件。 这是一个bean类,主要存储Plupload插件上传时需要的参数。 属性名不可随意改动. 这里主要使用MultipartFile文件上传方法
 */
public class Plupload {

    private final boolean enabled;

    public Plupload() {
        boolean tempValue = true;
        try {
            Class.forName("javax.servlet.http.HttpServletRequest");
            Class.forName("org.springframework.web.multipart.MultipartFile");
        } catch (ClassNotFoundException e) {
            tempValue = false;
        }
        enabled = tempValue;
    }

    /**
     * 文件临时名(大文件被分解时)
     */
    private String name;
    /**
     * 保存的文件名
     */
    private String fileName;
    /**
     * 总的块数
     */
    private int chunks = -1;
    /**
     * 当前块数（从0开始计数）
     */
    private int chunk = -1;
    /**
     * HttpServletRequest对象，不能直接传入进来，需要手动传入
     */
    private HttpServletRequest request;
    /**
     * 保存文件上传信息，不能直接传入进来，需要手动传入
     */
    private MultipartFile multipartFile;

    /**
     * 文件压缩大小 单位KB
     */
    private Long compressSize;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getChunks() {
        return chunks;
    }

    public void setChunks(int chunks) {
        this.chunks = chunks;
    }

    public int getChunk() {
        return chunk;
    }

    public void setChunk(int chunk) {
        this.chunk = chunk;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public MultipartFile getMultipartFile() {
        if (!enabled) {
            throw new IllegalStateException("没有引入spring-web依赖包，Plupload功能无法使用");
        }
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        if (!enabled) {
            throw new IllegalStateException("没有引入spring-web依赖包，Plupload功能无法使用");
        }
        this.multipartFile = multipartFile;
    }

    public Long getCompressSize() {
        return compressSize;
    }

    public void setCompressSize(Long compressSize) {
        this.compressSize = compressSize;
    }
}
