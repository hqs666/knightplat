/*
 * Copyright (c) 2017. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.utils.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 2017/2/9. config object for keyword filter
 */
public class Config {

    private final List<Option> defaultOptions = new ArrayList<>();
    private final List<Option> commonOptions = new ArrayList<>();
    private final Map<String, List<Option>> levelOptions = new HashMap<>();

    public void addDefaultOption(Option... options) {
        Collections.addAll(defaultOptions, cloneOptions(options));
    }

    public void addCommonOpetion(Option... options) {
        Collections.addAll(commonOptions, cloneOptions(options));
    }

    public void addOptionForLevel(String level, Option... options) {
        if (options.length > 0) {
            if (levelOptions.containsKey(level)) {
                Collections.addAll(levelOptions.get(level), cloneOptions(options));
            } else {
                levelOptions.put(level,  Arrays.asList((cloneOptions(options))));
            }
        }
    }

    public List<Option> getDefaultOptions() {
        return defaultOptions;
    }

    public List<Option> getCommonOptions() {
        return commonOptions;
    }

    public Map<String, List<Option>> getLevelOptions() {
        return levelOptions;
    }

    private Option[] cloneOptions(Option[] options) {
        Option[] dist = new Option[options.length];
        for (int i = 0; i < options.length; i++) {
            dist[i] = options[i].cloneOption();
        }

        return dist;
    }
}
