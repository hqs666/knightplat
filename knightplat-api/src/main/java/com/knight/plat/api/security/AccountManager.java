/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.security;

import java.util.Collection;

/**
 * Created on 16/9/2. 供权限控制框架使用的权限信息管理接口, 需要在应用中进行实现
 */
public interface AccountManager {

    /**
     * 根据用户标志获取数据源中的用户对象
     *
     * @param userName 用户唯一标识符
     * @return 用户对象
     */
    Authorizable findUserByUserName(String userName);

    /**
     * 根据用户标志获取数据源中的用户角色信息
     *
     * @param userName 用户唯一标识
     * @return 角色字符串集合(可为空)
     */
    Collection<String> getRolesByUserName(String userName);

    /**
     * 根据用户标志获取数据源中的用户权限信息
     *
     * @param userName 用户唯一标识
     * @return 权限字符串集合(可为空)
     */
    Collection<String> getStringPermissionsByUserName(String userName);

    /**
     * 密码加密算法的实现, 用户输入的密码传入时不要再做额外的加解密操作, 此处统一处理了
     *
     * @param password 原始密码
     * @return 加密后密码
     */
    String encryptPassword(char[] password);

}
