package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;

public interface DESEngine {
    ByteBuffer desEncode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException;

    ByteBuffer desDecode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException;

    default DESEngine asDESEngine() {
        return this;
    }
}
