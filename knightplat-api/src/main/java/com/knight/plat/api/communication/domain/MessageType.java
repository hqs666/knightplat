/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.communication.domain;


public enum MessageType {
    EMAIL, LOCAL, MOBILE
}
