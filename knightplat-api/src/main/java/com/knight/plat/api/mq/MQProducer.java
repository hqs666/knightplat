/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.mq;

import com.knight.plat.api.commons.Callback;


public interface MQProducer {

    String send(Object message, String topic, String tag);

    void sendAsync(Object message, Callback<String, Exception> callback, String topic, String tag);

    void sendOneWay(Object message, String topic, String tag);

    void start();

    void shutdown();

}
