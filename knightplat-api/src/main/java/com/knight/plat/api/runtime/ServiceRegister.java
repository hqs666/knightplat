package com.knight.plat.api.runtime;

import com.knight.plat.api.commons.HttpMethod;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * function服务注册器接口
 */
public interface ServiceRegister extends AutoCloseable {

    /**
     * 添加服务
     * @param path 服务请求path
     * @param method 请求方法
     * @param deployment 映射到的部署实例
     */
    void addService(String path, HttpMethod method, Deployment deployment, Map<String, Object> globalMap);

    void removeService(String path, HttpMethod method);

    default RequestMethod convertMethod(HttpMethod httpMethod) {
        switch (httpMethod) {
            case OPTIONS:
                return RequestMethod.OPTIONS;
            case DELETE:
                return RequestMethod.DELETE;
            case TRACE:
                return RequestMethod.TRACE;
            case PATCH:
                return RequestMethod.PATCH;
            case HEAD:
                return RequestMethod.HEAD;
            case PUT:
                return RequestMethod.PUT;
            case POST:
                return RequestMethod.POST;
            case GET:
                return RequestMethod.GET;
            default:
                throw new IllegalArgumentException(httpMethod.name() + " is not a valid http method");
        }
    }
}
