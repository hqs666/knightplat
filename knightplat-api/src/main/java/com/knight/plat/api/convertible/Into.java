package com.knight.plat.api.convertible;

public interface Into<T> {
    T to(T dist);
}
