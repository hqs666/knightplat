/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.commons;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Created on 16/8/10.
 */
@Schema(name = "输入传输对象", description = "传入接口的对象的封装")
public class InputDTO<T> implements Serializable {

    @Schema(name = "操作人编号", description = "操作人编号")
    private Serializable operateId;

    @Schema(name = "操作人姓名", description = "操作人姓名")
    private String operateName;

    @Schema(name = "会员编号", description = "会员编号")
    private Serializable customerId;

    @Schema(name = "会员名称", description = "会员名称")
    private String customerName;

    @Schema(name = "语言类型", description = "语言类型（ZH-CN：中文，EN-US：英文）")
    private String langVer;

    @Schema(name = "数据对象", description = "数据对象")
    private T data;

    @Schema(name = "元数据", description = "元数据")
    private Map<String, Object> meta;

    private InputDTO() {
        // Do nothing.
    }

    public InputDTO(T data) {
        this.data = data;
    }

    public InputDTO(T data, Map<String, Object> meta) {
        this.data = data;
        this.meta = meta;
    }

    public static <T> InputDTO<T> newInput() {
        return new InputDTO<>();
    }

    public static <T> InputDTO<T> newInput(T data) {
        return new InputDTO<>(data);
    }

    public static <T> InputDTO<T> newInput(T data, Map<String, Object> meta) {
        return new InputDTO<>(data, meta);
    }

    public Serializable getOperateId() {
        return operateId;
    }

    public void setOperateId(Serializable operateId) {
        this.operateId = operateId;
    }

    public String getOperateName() {
        return operateName;
    }

    public void setOperateName(String operateName) {
        this.operateName = operateName;
    }

    public Serializable getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Serializable customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLangVer() {
        return langVer;
    }

    public void setLangVer(String langVer) {
        this.langVer = langVer;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Map<String, Object> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, Object> meta) {
        this.meta = meta;
    }

    public Object getMetaValue(String key) {
        if (meta == null) {
            return null;
        }
        return meta.get(key);
    }

    public void setMetaValue(String key, Object value) {
        if (meta == null) {
            meta = new HashMap<>();
        }
        meta.put(key, value);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", InputDTO.class.getSimpleName() + "[", "]")
                .add("operateId=" + operateId)
                .add("operateName='" + operateName + "'")
                .add("customerId=" + customerId)
                .add("customerName='" + customerName + "'")
                .add("langVer='" + langVer + "'")
                .add("data=" + data)
                .add("meta=" + meta)
                .toString();
    }
}
