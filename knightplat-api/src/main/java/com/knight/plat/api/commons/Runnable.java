package com.knight.plat.api.commons;

@FunctionalInterface
public interface Runnable {
    void run() throws Throwable;
}
