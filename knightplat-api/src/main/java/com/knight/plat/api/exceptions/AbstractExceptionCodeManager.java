/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.exceptions;

/**
 * Created on 2016/10/17.
 * @deprecated use {@link ExceptionCodeManager} instead
 */
@Deprecated
public abstract class AbstractExceptionCodeManager implements ExceptionCodeManager {

}
