package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;

public interface HashEngine {

    ByteBuffer md5(byte[] data);

    ByteBuffer sha1(byte[] data);

    ByteBuffer sha256(byte[] data);

    ByteBuffer sha512(byte[] data);

    ByteBuffer sm3(byte[] data);

    default HashEngine asHashEngine() {
        return this;
    }
}
