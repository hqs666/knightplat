package com.knight.plat.api.runtime;

import com.knight.plat.api.eventbus.EventBus;

import java.util.Map;

/**
 * function执行引擎的构造器接口
 * @param <E> 执行引擎具体类型
 */
public interface EngineBuilder<E extends FunctionEngine, BUS extends FunctionEventBus> {

    /**
     * 创建一个执行引擎实例
     * @param source function部署实例用以获取本次执行引擎使用的源文件数据
     * @return 执行引擎实例
     */
    E build(String source, Map<String, Object> globalMap);

    /**
     * 提供EventBus包装为引擎专有的EventBus对象的逻辑
     * @param eventBus 原始的框架eventBus对象
     * @return 封装后的EventBus对象
     */
    BUS decorateEventBus(EventBus eventBus);
}
