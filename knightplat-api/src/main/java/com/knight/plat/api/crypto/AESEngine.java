package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;

public interface AESEngine {
    ByteBuffer aesEncode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException;

    ByteBuffer aesDecode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException;

    default AESEngine asAESEngine() {
        return this;
    }
}
