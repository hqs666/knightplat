/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.utils;

import java.util.Calendar;

/**
 * Created on 2016/10/19. 节假日管理
 */
public interface HolidayManager {

    /**
     * 将一个节假日日期加入列表
     *
     * @param holiday 添加的节假日
     */
    void addHoliday(Calendar holiday);

    /**
     * 将一个工作日日期加入排除列表中
     *
     * @param workday 要排除的工作日
     */
    void addWorkday(Calendar workday);

    /**
     * 初始化一个节假日列表
     */
    void initHolidayList();

    /**
     * 判断给定的日期是否是节假日
     *
     * @param date 给定的日期
     * @return 判断结果
     */
    boolean isHoliday(Calendar date);
}
