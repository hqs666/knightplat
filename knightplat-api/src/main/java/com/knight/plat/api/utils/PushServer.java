package com.knight.plat.api.utils;

import com.knight.plat.api.utils.dto.SensitivePushDTO;

public interface PushServer {
    void push(SensitivePushDTO dto);
}
