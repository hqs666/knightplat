package com.knight.plat.api.runtime;

import java.util.Map;

/**
 * function事件注册器接口
 */
public interface EventRegister extends AutoCloseable {

    /**
     * 添加事件消费逻辑
     * @param router 消费地址
     * @param clazz 消费内容类型
     * @param deployment 处理逻辑的部署实例
     */
    void addConsumer(String router, Class<?> clazz, Deployment deployment, Map<String, Object> globalMap);
}
