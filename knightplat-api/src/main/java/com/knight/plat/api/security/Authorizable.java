/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.security;

import java.util.Map;

/**
 * Created on 16/9/2. 可授权对象标志, 权限控制框架使用
 */
public interface Authorizable {

    /**
     * 获取用户标志
     * @return 用户标识
     */
    String getUserName();

    /**
     * 获取密码信息
     * @return 密码
     */
    default String getPassword() {
        return "";
    }

    /**
     * 清空密码属性
     */
    default void cleanPassword() {
    }

    /**
     * 获取密码验证器
     * @return 密码验证器
     */
    boolean passwordValidate();

    /**
     * 获取其他用户自定义元信息
     * @return 元信息Map
     */
    Map<String, Object> getMetaData();

    /**
     * 获取某个自定义元信息
     * @param key 元信息key
     * @return 元信息值
     */
    Object getMetaValue(String key);

    /**
     * 设置一条自定义元信息数据
     * @param key   元信息key
     * @param value 元信息值
     */
    void putMetaValue(String key, Object value);

}
