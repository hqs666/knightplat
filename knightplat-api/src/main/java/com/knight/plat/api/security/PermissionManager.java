/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.security;

/**
 * Created on 2016/11/10. 权限验证管理
 */
public interface PermissionManager {

    /**
     * 获取资源访问需要的权限
     *
     * @param assessor 请求资源描述
     * @return 所需权限
     * @since 0.7.8.RELEASE
     */
    String getRequiredPermission(PermissionAssessor assessor);

    /**
     * 判断是否拥有访问权限
     *
     * @param assessor 请求资源描述
     * @return 判断结果
     */
    boolean isPermitted(PermissionAssessor assessor);
}
