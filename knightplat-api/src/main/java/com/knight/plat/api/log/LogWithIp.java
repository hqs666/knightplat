/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.log;

import java.util.StringJoiner;


public class LogWithIp {

    private String ip;
    private String userAgent;
    private String platform;
    private LogContent content;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public LogContent getContent() {
        return content;
    }

    public void setContent(LogContent content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", LogWithIp.class.getSimpleName() + "[", "]")
                .add("ip='" + ip + "'")
                .add("userAgent='" + userAgent + "'")
                .add("platform='" + platform + "'")
                .add("content=" + content)
                .toString();
    }
}
