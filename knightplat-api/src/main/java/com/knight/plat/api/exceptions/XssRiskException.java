package com.knight.plat.api.exceptions;

public class XssRiskException extends RuntimeException {

    public XssRiskException() {
        super();
    }

    public XssRiskException(String msg) {
        super(msg);
    }

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
