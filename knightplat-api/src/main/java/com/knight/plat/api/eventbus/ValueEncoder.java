package com.knight.plat.api.eventbus;

/**
 * 消息体值处理器接口 - 编码
 * Add on 2022/10/18
 *
 * @author knightplat
 * @since 1.0.19
 */
@FunctionalInterface
public interface ValueEncoder<T> {

    /**
     * 消息体值编码逻辑
     * @param value 原始值
     * @return 编码后的值
     */
    byte[] encode(T value) throws Throwable;
}
