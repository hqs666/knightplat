package com.knight.plat.api.image;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author knightplat
 * @version 1.0
 * @description: 缩略图
 * @date 2021/8/6 13:34
 */
public interface ThumbnailsManager<T> {


    /**
     * 顺时针旋转
     *
     * @param angle 旋转°
     * @return
     */
    public T rotate(double angle);

    /**
     * 缩略图
     *
     * @param width  宽
     * @param height 高
     * @return
     */
    public T size(int width, int height);


    /**
     * 指定因子缩略图
     *
     * @param scale 指定因子
     * @return
     */
    public T scale(double scale);

    /**
     * 指定因子缩略图
     *
     * @param scaleWidth  指定因子宽
     * @param scaleHeight 指定因子高
     * @return
     */
    public T scale(double scaleWidth, double scaleHeight);

    /**
     * 水印
     *
     * @param file    水印图片地址
     * @param opacity 透明度
     * @return
     */
    public T watermark(String file, float opacity) throws IOException;

    /**
     * 水印
     *
     * @param file    水印图片
     * @param opacity 透明度
     * @return
     */
    public T watermark(File file, float opacity) throws IOException;

    /**
     * 压缩质量把控 最高是1.0
     *
     * @param quality 压缩质量把控值 最高是1.0
     * @return
     */
    public T outputQuality(float quality);

    /**
     * 文件处理转换格式
     *
     * @param format 指定格式
     * @return
     */
    public T outputFormat(String format);

    /**
     * 输出文件
     *
     * @param path 输出地址
     * @throws IOException
     */
    public void toFile(String path) throws IOException;

    /**
     * 输出流
     *
     * @param stream
     * @throws IOException
     */
    public void toOutputStream(OutputStream stream) throws IOException;

    /**
     * 图片转为base64编码返回
     *
     * @return
     * @throws IOException
     */
    public String toBase64() throws IOException;
}
