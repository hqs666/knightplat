package com.knight.plat.api.eventbus;

public enum Actions {
    CONSUME, SEND, PUBLISH
}
