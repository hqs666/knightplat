/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.utils;

import com.knight.plat.api.utils.config.Config;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created on 2016/11/1.
 */
public interface KeywordManager {

    void load();

    Collection<String> loadKeywords();


    String filter(String src, char replace, String traceId);

    String filter(String src, Config config);

    boolean isSensitive(String src);

    List<String> keyWordLevels(String src);

    Set<String> countKeyWords(String src);
}
