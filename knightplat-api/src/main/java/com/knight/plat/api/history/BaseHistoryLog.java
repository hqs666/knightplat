package com.knight.plat.api.history;

import java.util.Date;
import java.util.Map;

public interface BaseHistoryLog {
    String getUuid();
    Date getDateTime();
    String getUserId();
    String getUserName();
    String getBizType();
    String getBizId();
    String getModule();
    String getSystem();
    String getDevice();
    String getCompany();
    String getEvent();
    String getOrganization();
    String getRequestUrl();
    String getRequestMethod();
    Map<String, Object> getData();

    String getApp();
    void setApp(String app);

    String serialize();
}
