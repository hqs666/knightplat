package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;

public interface HMACEngine {
    ByteBuffer hmacMd5(byte[] data, byte[] key, boolean isBase64);

    ByteBuffer hmacSha1(byte[] data, byte[] key, boolean isBase64);

    ByteBuffer hmacSha256(byte[] data, byte[] key, boolean isBase64);

    default HMACEngine asHMACEngine() {
        return this;
    }
}
