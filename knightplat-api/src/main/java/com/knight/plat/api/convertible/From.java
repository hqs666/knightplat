package com.knight.plat.api.convertible;

public interface From<T> {
    void from(T src);
}
