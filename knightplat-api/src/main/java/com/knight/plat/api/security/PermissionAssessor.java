/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.security;

import java.util.Collection;
import java.util.StringJoiner;

/**
 * Created on 2016/11/26.
 */
public class PermissionAssessor {

    private String resource;
    private Collection<String> permissions;
    private Collection<String> roles;

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public Collection<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(Collection<String> permissions) {
        this.permissions = permissions;
    }

    public Collection<String> getRoles() {
        return roles;
    }

    public void setRoles(Collection<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PermissionAssessor.class.getSimpleName() + "[", "]")
                .add("resource='" + resource + "'")
                .add("permissions=" + permissions)
                .add("roles=" + roles)
                .toString();
    }
}
