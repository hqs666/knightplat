package com.knight.plat.api.crypto;

import java.nio.ByteBuffer;
import java.security.spec.InvalidKeySpecException;

public interface RSAEngine {
    ByteBuffer rsaEncode(byte[] data, byte[] publicKey, boolean isBase64) throws InvalidKeySpecException;

    ByteBuffer rsaDecode(byte[] data, byte[] privateKey, boolean isBase64) throws InvalidKeySpecException;

    default RSAEngine asRSAEngine() {
        return this;
    }
}
