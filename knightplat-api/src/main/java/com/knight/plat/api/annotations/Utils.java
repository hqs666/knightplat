package com.knight.plat.api.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface Utils {
    String value() default "";

    boolean lib() default false;

    @Retention(RetentionPolicy.CLASS)
    @Target(ElementType.METHOD)
    @interface Ignore {
        String value() default "";
    }

    @Retention(RetentionPolicy.CLASS)
    @Target(ElementType.METHOD)
    @interface ThrowException {
        Class<? extends Throwable>[] value() default { Throwable.class };
    }
}
