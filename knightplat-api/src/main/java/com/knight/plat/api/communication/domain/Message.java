/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.communication.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Created on 2016/11/28. Message domain object.
 */
public class Message implements Serializable {

    private String id;
    private String title;
    private String content;
    // 存放信息内容Map数据，用于渲染模板
    private Map<String, Object> contentMap;
    // 业务组编号
    private String bizGroupCode;
    // 会员id
    private String memberId;
    // 事件编号
    private String eventCode;
    // TODO: 2016/12/10 sender和receiver改成一个用户对象来表示用户信息
    private String senderId;
    // 站内信接收人id
    private String[] receiverId;
    // 短信接收手机号
    private String[] receiverPhoneNumber;
    // 邮件接收地址
    private String[] receiverEmail;
    private String[] receiverNickName; // TODO: 2016/12/10 用一个更通用的命名来替换
    private MessageStatus status;
    private MessageType[] messageTypes;
    private String bizCode;
    private String bizType;
    private String bizUrl;
    private Date sendDate;
    private String jdTemplateId;
    private String jdSignId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String[] getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String... receiverId) {
        this.receiverId = receiverId;
    }

    public String[] getReceiverPhoneNumber() {
        return receiverPhoneNumber;
    }

    public void setReceiverPhoneNumber(String... receiverPhoneNumber) {
        this.receiverPhoneNumber = receiverPhoneNumber;
    }

    public String[] getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String... receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String[] getReceiverNickName() {
        return receiverNickName;
    }

    public void setReceiverNickName(String... receiverNickName) {
        this.receiverNickName = receiverNickName;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(MessageStatus status) {
        this.status = status;
    }

    public MessageType[] getMessageTypes() {
        return messageTypes;
    }

    public void setMessageTypes(MessageType... messageTypes) {
        this.messageTypes = messageTypes;
    }

    public String getBizCode() {
        return bizCode;
    }

    public void setBizCode(String bizCode) {
        this.bizCode = bizCode;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getBizUrl() {
        return bizUrl;
    }

    public void setBizUrl(String bizUrl) {
        this.bizUrl = bizUrl;
    }

    public Map<String, Object> getContentMap() {
        return contentMap;
    }

    public void setContentMap(Map<String, Object> contentMap) {
        this.contentMap = contentMap;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getBizGroupCode() {
        return bizGroupCode;
    }

    public void setBizGroupCode(String bizGroupCode) {
        this.bizGroupCode = bizGroupCode;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getJdTemplateId() {
        return jdTemplateId;
    }

    public void setJdTemplateId(String jdTemplateId) {
        this.jdTemplateId = jdTemplateId;
    }

    public String getJdSignId() {
        return jdSignId;
    }

    public void setJdSignId(String jdSignId) {
        this.jdSignId = jdSignId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Message.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("title='" + title + "'")
                .add("content='" + content + "'")
                .add("contentMap=" + contentMap)
                .add("bizGroupCode='" + bizGroupCode + "'")
                .add("memberId='" + memberId + "'")
                .add("eventCode='" + eventCode + "'")
                .add("senderId='" + senderId + "'")
                .add("receiverId=" + Arrays.toString(receiverId))
                .add("receiverPhoneNumber=" + Arrays.toString(receiverPhoneNumber))
                .add("receiverEmail=" + Arrays.toString(receiverEmail))
                .add("receiverNickName=" + Arrays.toString(receiverNickName))
                .add("status=" + status)
                .add("messageTypes=" + Arrays.toString(messageTypes))
                .add("bizCode='" + bizCode + "'")
                .add("bizType='" + bizType + "'")
                .add("bizUrl='" + bizUrl + "'")
                .add("sendDate=" + sendDate)
                .add("jdTemplateId='" + jdTemplateId + "'")
                .add("jdSignId='" + jdSignId + "'")
                .toString();
    }
}
