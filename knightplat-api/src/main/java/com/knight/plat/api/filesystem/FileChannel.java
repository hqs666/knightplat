package com.knight.plat.api.filesystem;

@FunctionalInterface
public interface FileChannel {
    String getName();
}
