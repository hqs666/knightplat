package com.knight.plat.api.job;

public interface JobManager {

    void registerStrategy(final String schema, final JobStrategy strategy, final JobLogger logger);

    void registerJob(String executor, String name, JobHandler jobHandler);

    boolean hasExecutor(String executor);
}
