package com.knight.plat.api.runtime;

import com.knight.plat.api.utils.Tools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * function执行引擎接口
 */
public interface FunctionEngine extends AutoCloseable {

    String UTILS_OBJECT_NAME = "utils";
    String EVENT_BUS_OBJECT_NAME = "event";

    /**
     * 获取源文件数据
     * @return 源文件数据
     */
    String getSource();

    /**
     * 执行具体的function逻辑
     * @param context 传入的上下文对象
     */
    void call(Context context, FunctionErrorContext errorContext);

    static String loadResource(String resourceName) {
        final String runnerStr;

        // 载入脚本资源
        try(ByteArrayOutputStream output = new ByteArrayOutputStream();
                InputStream runnerIs = FunctionEngine.class.getClassLoader().getResourceAsStream(resourceName)) {
            if (runnerIs == null) {
                return "";
            }
            byte[] b = new byte[1024];
            int n;
            while ((n = runnerIs.read(b)) != -1) {
                output.write(b, 0, n);
            }
            runnerStr = new String(output.toByteArray(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException(resourceName + " load fail.");
        }
        return runnerStr;
    }

    static String loadResource(Map<String, Object> argsMap, String resourceName) {
        // 载入脚本资源
        final String runnerStr = loadResource(resourceName);

        // 替换脚本中的变量
        return Tools.patternReplace(runnerStr, Pattern.compile("\\{\\{(.*?)}}"), argsMap::get);
    }
}
