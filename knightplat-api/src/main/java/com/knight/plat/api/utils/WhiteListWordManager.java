package com.knight.plat.api.utils;

import java.util.Collection;

public interface WhiteListWordManager {
    void load();

    Collection<String> loadKeywords();

    boolean belong(String src);
}
