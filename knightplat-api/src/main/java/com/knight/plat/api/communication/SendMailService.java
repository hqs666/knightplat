/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.communication;

import java.util.Map;


public interface SendMailService {

    /**
     * 直接发送邮件内容
     *
     * @param subject 主题
     * @param bizType 业务代码
     * @param content 邮件内容
     * @param to      收件人列表
     * @throws SendMailException 发送失败返回错误
     */
    void send(String subject, String bizType, Object content, String... to)
            throws SendMailException;

    /**
     * 按照模板ID来解析邮件内容并发送
     *
     * @param subject    主题
     * @param bizType    业务代码
     * @param data       模板参数
     * @param templateId 模板ID
     * @param to         收件人列表
     * @throws SendMailException 发送失败返回错误
     */
    void send(String subject, String bizType, Map<String, Object> data, int templateId, String... to)
            throws SendMailException;

    /**
     * 按照模板类型和模板代码来解析邮件内容并发送
     *
     * @param subject      主题
     * @param bizType      业务代码
     * @param data         模板参数
     * @param template     模板内容
     * @param templateType 模板类型
     * @param to           收件人列表
     * @throws SendMailException 发送失败返回错误
     */
    void send(String subject, String bizType, Map<String, Object> data, String template,
            TemplateType templateType, String... to)
            throws SendMailException;
}
