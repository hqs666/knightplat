/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.communication;

/**
 * Created on 2016/11/23. Send sms service interface MUST implement it if use sms.
 */

public interface SendSmsService {

    void send(String phoneNum, String content);
}
