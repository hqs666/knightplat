package com.knight.plat.api.task;

public interface TaskStrategy {

    void start(Task<?> task);
    void syncStart(Task<?> task);
    void loop(Task<?> task);

    <R> Task<R> getTask(String id);

    void remove(String id);

    void clear();
}
