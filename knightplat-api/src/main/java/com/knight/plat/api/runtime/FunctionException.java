package com.knight.plat.api.runtime;

public class FunctionException extends RuntimeException {
    private final String name;
    private final String stack;

    public FunctionException(String name, String message, String stack) {
        super(message);
        this.name = name;
        this.stack = stack;
    }

    public FunctionException(String name, String message, String stack, Throwable cause) {
        super(message, cause);
        this.name = name;
        this.stack = stack;
    }

    public String getName() {
        return name;
    }

    public String getStack() {
        return stack;
    }
}
