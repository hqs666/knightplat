package com.knight.plat.api.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * aot org.springframework.aot.hint.RuntimeHintsRegistrar
 *
 * @author JiaLe
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface AotRuntimeHintsRegistrar {
}
