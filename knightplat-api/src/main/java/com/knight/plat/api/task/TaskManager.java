package com.knight.plat.api.task;

import com.knight.plat.api.commons.Callable;
import com.knight.plat.api.commons.ParametizedCallable;

public interface TaskManager {

    <R> Task<R> runTask(final Callable<R> task);

    <R> Task<R> runTask(final ParametizedCallable<String, R> task);

    <R> Task<R> syncRunTask(final Callable<R> task);

    <R> Task<R> syncRunTask(final ParametizedCallable<String, R> task);

    Task<?> loopTask(final Runnable task);

    TaskStatus taskStatus(final String id);

    <R> R getResult(final String id);

    <R> R waitAndGetResult(final String id);

    void cancel(final String id);

    void cancel(final Task<?> task);

    void close();
}
