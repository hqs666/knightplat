/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.mq;


public interface MQConsumer {

    <T> void subscribe(final Handler<T> handler);

    void start();

    void shutdown();
}
