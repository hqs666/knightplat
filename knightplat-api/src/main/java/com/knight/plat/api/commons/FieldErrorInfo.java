package com.knight.plat.api.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 字段错误信息详情
 */
@Schema(name = "字段验证错误信息", description = "字段验证错误信息")
@JsonInclude(Include.NON_NULL)
public class FieldErrorInfo {

    @Schema(name = "字段名", description = "字段名")
    private String field;

    @Schema(name = "字段错误信息", description = "字段错误信息")
    private String message;

    public FieldErrorInfo() {
        this.field = null;
        this.message = null;
    }

    public FieldErrorInfo(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
