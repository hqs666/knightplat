package com.knight.plat.api.filesystem;

import java.io.Serializable;

/**
 * @author knightplat
 * @version 1.0
 */
public class Part implements Serializable {

    private static final long serialVersionUID = 5229979925100528068L;

    private String etag;

    private Integer partNumber;

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public Integer getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(Integer partNumber) {
        this.partNumber = partNumber;
    }
}
