package com.knight.plat.api.eventbus;

public final class SchemaStrategyPair {
    private final EventStrategy strategy;
    private final String schema;

    public SchemaStrategyPair(String schema, EventStrategy strategy) {
        this.strategy = strategy;
        this.schema = schema;
    }

    public EventStrategy getStrategy() {
        return strategy;
    }

    public String getSchema() {
        return schema;
    }
}
