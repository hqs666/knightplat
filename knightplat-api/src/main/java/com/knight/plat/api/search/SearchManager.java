/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.search;

import com.knight.plat.api.commons.SimplePageInfo;
import com.knight.plat.api.search.domain.SearchResponse;

import java.io.IOException;
import java.util.Map;

/**
 * Created  on 2016/11/11. Search engine support.
 * 以数据库为例
 * indexes = AppName = dataBaseName
 * index = tableName
 * doc = table line
 */
public interface SearchManager {

    QueryExecutor query(String... indexes);

    QueryExecutor query(SimplePageInfo pageInfo, String... indexes);

    boolean newIndex(String index);

    boolean deleteIndex(String index);

    boolean newDoc(String index, Map<String, Object> source);

    boolean updateDoc(String index, String id, Map<String, Object> source);

    boolean removeDoc(String index, Map<String, Object> source);

    SearchResponse getDoc(String index, String idField, String id);

    /**
     * Do query.
     */
    interface QueryExecutor {

        SearchResponse execute() throws IOException;

        QueryExecutor range(String field, Object from, Object to);

        QueryExecutor where(String field, String value);

        QueryExecutor and(String field, String value);

        QueryExecutor or(String field, String value);

        QueryExecutor andNot(String field, String value);

        QueryExecutor filterWithAnd(String expression);

        QueryExecutor filterWithOr(String expression);

        QueryExecutor sort(String field, Sort sort);

        QueryExecutor types(String... types);

        QueryExecutor more(Map<String, Object> opts);
    }
}
