package com.knight.plat.api.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface JobLogger {

    Logger log = LoggerFactory.getLogger(JobLogger.class);

    JobLogger SIMPLE_LOGGER = new JobLogger() {

        @Override
        public void log(String pattern, Object... args) {
            log.info(pattern, args);
        }

        @Override
        public void log(Throwable throwable) {
            log.error("Job exception", throwable);
        }
    };

    void log(String pattern, Object... args);
    void log(Throwable throwable);
}
