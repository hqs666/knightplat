/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.search;

/**
 * Created on 2016/11/11.
 */
public enum Sort {
    DESC("-"), ASC("+");

    String sortChar;

    Sort(String sortChar) {
        this.sortChar = sortChar;
    }

    public String getChar() {
        return this.sortChar;
    }
}
