/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.api.search.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Created on 2016/11/11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SingleResponse {

    private Status status;

    @JsonProperty("request_id")
    private String requestId;

    private Map<String, Object> result;

    private List<ErrorItem> errors;

    @JsonIgnore
    private String src;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    public List<ErrorItem> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorItem> errors) {
        this.errors = errors;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SingleResponse.class.getSimpleName() + "[", "]")
                .add("status=" + status)
                .add("requestId='" + requestId + "'")
                .add("result=" + result)
                .add("errors=" + errors)
                .add("src='" + src + "'")
                .toString();
    }
}
