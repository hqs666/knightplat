package com.knight.plat.api.runtime;

import com.knight.plat.api.commons.Cookie;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

public class Context {

    private URI uri;
    private Map<String, List<String>> headers;
    private List<Cookie> cookies;
    private Object requestBody;
    private Object responseBody;

    private final Map<String, List<String>> setHeaders = new HashMap<>();
    private final List<Cookie> setCookies = new ArrayList<>();

    /**
     * 创建构造器方便构建Context对象
     * @return 构造器
     */
    public static Builder builder() {
        return new Builder();
    }

    // 下面是Function部分使用的方法

    /**
     * 脚本中放入请求返回体
     * @param body 返回体
     * @return 本对象方便链式调用
     */
    public Context succeed(Object body) {
        responseBody = body;
        return this;
    }

    /**
     * 设置返回头
     * @param name 返回头名称
     * @param value 返回头值
     * @return 本对象方便链式调用
     */
    public Context setHeader(String name, String value) {
        List<String> values = setHeaders.computeIfAbsent(name, k -> new ArrayList<>());
        values.add(value);
        return this;
    }

    /**
     * 设置返回Cookie
     * @param name Cookie名称
     * @param value Cookie值
     * @return 本对象方便链式调用
     */
    public Context setCookie(String name, String value) {
        return setCookie(name, value, false, -1);
    }

    /**
     * 设置返回Cookie
     * @param name Cookie名称
     * @param value Cookie值
     * @param httpOnly 是否httpOnly
     * @return 本对象方便链式调用
     */
    public Context setCookie(String name, String value, boolean httpOnly) {
        return setCookie(name, value, httpOnly, -1);
    }

    /**
     * 设置返回Cookie
     * @param name Cookie名称
     * @param value Cookie值
     * @param maxAge 有效期
     * @return 本对象方便链式调用
     */
    public Context setCookie(String name, String value, int maxAge) {
        return setCookie(name, value, false, maxAge);
    }

    /**
     * 设置返回Cookie
     * @param name Cookie名称
     * @param value Cookie值
     * @param httpOnly 是否httpOnly
     * @param maxAge 有效期
     * @return 本对象方便链式调用
     */
    public Context setCookie(String name, String value, boolean httpOnly, int maxAge) {
        final Cookie cookie = new Cookie();
        cookie.setName(name);
        cookie.setValue(value);
        cookie.setHttpOnly(httpOnly);
        cookie.setMaxAge(maxAge);
        setCookies.add(cookie);
        return this;
    }

    /**
     * 脚本中获取本次请求的uri
     * @return uri对象
     */
    public URI uri() {
        return uri;
    }

    /**
     * 脚本中获取请求头
     * @return 请求头
     */
    public Map<String, List<String>> requestHeaders() {
        return headers;
    }

    /**
     * 脚本中获取请求cookies
     * @return 请求cookies
     */
    public List<Cookie> requestCookies() {
        return cookies;
    }

    /**
     * 脚本中获取请求体
     * @return 请求体
     */
    public Object requestBody() {
        return requestBody;
    }

    // 下面是Java部分使用的方法

    Object getResponseBody() {
        return responseBody;
    }

    Map<String, List<String>> getSetHeaders() {
        return setHeaders;
    }

    List<Cookie> getSetCookies() {
        return setCookies;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Context.class.getSimpleName() + "[", "]")
                .add("uri=" + uri)
                .add("headers=" + headers)
                .add("cookies=" + cookies)
                .add("requestBody=" + requestBody)
                .add("responseBody=" + responseBody)
                .toString();
    }

    public static class Builder {
        private URI uri;
        private Map<String, List<String>> headers;
        private List<Cookie> cookies;
        private Object requestBody;

        private Builder() {}

        public Builder uri(URI uri) {
            this.uri = uri;
            return this;
        }

        public Builder headers(Map<String, List<String>> headers) {
            this.headers = headers;
            return this;
        }

        public Builder header(String name, String value) {
            if (this.headers == null) {
                this.headers = new HashMap<>();
            }
            List<String> values = headers.computeIfAbsent(name, k -> new ArrayList<>());
            values.add(value);
            return this;
        }

        public Builder cookies(List<Cookie> cookies) {
            this.cookies = cookies;
            return this;
        }

        public Builder cookie(Cookie cookie) {
            if (this.cookies == null) {
                this.cookies = new ArrayList<>();
            }
            this.cookies.add(cookie);
            return this;
        }

        public Builder body(Object requestBody) {
            this.requestBody = requestBody;
            return this;
        }

        public Context build() {
            final Context context = new Context();
            context.headers = headers;
            context.requestBody = requestBody;
            context.uri = uri;
            context.cookies = cookies;
            return context;
        }
    }
}
