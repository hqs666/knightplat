package com.knight.plat.api.utils.dto;

import java.util.List;
import java.util.Set;

public class SensitivePushDTO {
    /**
     * 追踪id 用于跟踪敏感词过滤结果
     */
    private String traceId;

    /**
     * 原句子
     */
    private String originStr;

    /**
     * 过滤后句子
     */
    private String filteredStr;

    /**
     * 敏感词库编码
     */
    private String sysCode;
    /**
     * 分词结果
     */
    private List<String> segmentationList;

    /**
     * 白名单命中
     */
    private Set<String> whiteListHits;


    /**
     * 敏感词命中
     */
    private Set<String> sensitiveHits;

    public SensitivePushDTO() {
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getTraceId() {
        return this.traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getOriginStr() {
        return this.originStr;
    }

    public void setOriginStr(String originStr) {
        this.originStr = originStr;
    }

    public String getFilteredStr() {
        return this.filteredStr;
    }

    public void setFilteredStr(String filteredStr) {
        this.filteredStr = filteredStr;
    }

    public List<String> getSegmentationList() {
        return this.segmentationList;
    }

    public void setSegmentationList(List<String> segmentationList) {
        this.segmentationList = segmentationList;
    }

    public Set<String> getWhiteListHits() {
        return this.whiteListHits;
    }

    public void setWhiteListHits(Set<String> whiteListHits) {
        this.whiteListHits = whiteListHits;
    }

    public Set<String> getSensitiveHits() {
        return this.sensitiveHits;
    }

    public void setSensitiveHits(Set<String> sensitiveHits) {
        this.sensitiveHits = sensitiveHits;
    }

    public String toString() {
        return "SensitivePushDTO{traceId='" + this.traceId + '\'' + ", originStr='" + this.originStr + '\'' + ", filteredStr='" + this.filteredStr + '\'' + ", segmentationList=" + this.segmentationList + ", whiteListHits=" + this.whiteListHits + ", sensitiveHits=" + this.sensitiveHits + '}';
    }
}
