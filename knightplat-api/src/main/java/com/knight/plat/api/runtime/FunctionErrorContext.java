package com.knight.plat.api.runtime;

public class FunctionErrorContext {

    private String errName;
    private String errMsg;
    private String errStack;

    private boolean hasErr = false;

    public void handle(String errName, String errMsg, String errStack) {
        this.errName = errName;
        this.errMsg = errMsg;
        this.errStack = errStack;
        this.hasErr = true;
    }

    public boolean hasErr() {
        return this.hasErr;
    }

    public FunctionException makeException() {
        return new FunctionException(errName, errMsg, errStack);
    }
}
