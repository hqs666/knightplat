/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.token.exceptions;


public enum TokenExceptionType {

    DEFAULT("TK00"), DATA_PARSE("TK01"), PERIOD("TK02"), VALID("TK03"), USER("TK04");

    private final String code;

    TokenExceptionType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
