package com.knight.plat.token;

import com.knight.plat.token.exceptions.TokenException;
import com.knight.plat.token.internal.TokenParser;
import io.jsonwebtoken.Claims;

import javax.crypto.SecretKey;
import java.util.Map;

@SuppressWarnings("unused")
public class TokenManager {

    private final TokenParser parser = TokenParser.INSTANCE;

    public String create(String tokenId, String userId, Map<String, Object> data) {
        return parser.createToken(tokenId, userId, data);
    }

    public String create(String tokenId, String userId, Map<String, Object> data, SecretKey key) {
        return parser.createToken(tokenId, userId, data, 300000, key);
    }

    public String create(String tokenId, String userId, Map<String, Object> data, long expMillis) {
        return parser.createToken(tokenId, userId, data, expMillis);
    }

    public String create(String tokenId, String userId, Map<String, Object> data, SecretKey key, long expMillis) {
        return parser.createToken(tokenId, userId, data, expMillis, key);
    }

    public Claims checkAndRead(String token) throws TokenException {
        return parser.parseToken(token);
    }

    public Claims checkAndRead(String token, SecretKey key) throws TokenException {
        return parser.parseToken(token, key);
    }

    public Claims readWithoutCheck(String token) throws TokenException {
        return parser.parseTokenIgnoreTtl(token);
    }
}
