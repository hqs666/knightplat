/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.token.exceptions;


public class IncorrectSignatureException extends TokenException {

    private final TokenExceptionType type = TokenExceptionType.VALID;

    public IncorrectSignatureException() {
        // create exception with no event.
    }

    public IncorrectSignatureException(String message) {
        super(message);
    }

    public IncorrectSignatureException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectSignatureException(Throwable cause) {
        super(cause);
    }

    @Override
    public TokenExceptionType getType() {
        return type;
    }

}
