/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.token.exceptions;


public class DataParseFailException extends TokenException {

    private final TokenExceptionType type = TokenExceptionType.DATA_PARSE;

    public DataParseFailException() {
        // create the exception with put event.
    }

    public DataParseFailException(String message) {
        super(message);
    }

    public DataParseFailException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataParseFailException(Throwable cause) {
        super(cause);
    }

    @Override
    public TokenExceptionType getType() {
        return type;
    }

}
