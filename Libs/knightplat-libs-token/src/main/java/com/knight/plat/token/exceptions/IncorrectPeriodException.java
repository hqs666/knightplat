/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.token.exceptions;


public class IncorrectPeriodException extends TokenException {

    private final TokenExceptionType type = TokenExceptionType.PERIOD;

    public IncorrectPeriodException() {
        // create exception with no event.
    }

    public IncorrectPeriodException(String message) {
        super(message);
    }

    public IncorrectPeriodException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectPeriodException(Throwable cause) {
        super(cause);
    }

    @Override
    public TokenExceptionType getType() {
        return type;
    }

}
