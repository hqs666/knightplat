/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.token.exceptions;


public class TokenException extends Exception {

    private final TokenExceptionType type = TokenExceptionType.DEFAULT;

    public TokenException() {
    }

    public TokenException(String message) {
        super(message);
    }

    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenException(Throwable cause) {
        super(cause);
    }

    public TokenExceptionType getType() {
        return type;
    }

}
