package com.knight.plat.token.internal

import com.fasterxml.jackson.databind.ObjectMapper
import com.knight.plat.token.exceptions.DataParseFailException
import com.knight.plat.token.exceptions.IncorrectPeriodException
import com.knight.plat.token.exceptions.IncorrectSignatureException
import com.knight.plat.token.exceptions.TokenException
import io.jsonwebtoken.*
import io.jsonwebtoken.impl.DefaultClaims
import io.jsonwebtoken.impl.DefaultHeader
import io.jsonwebtoken.impl.DefaultJwsHeader
import io.jsonwebtoken.impl.TextCodec
import io.jsonwebtoken.impl.compression.DefaultCompressionCodecResolver
import io.jsonwebtoken.lang.Strings
import java.io.IOException
import java.util.*
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

/**
 * token管理器
 */
internal object TokenParser {

    private val objectMapper = ObjectMapper()
    private val compressionCodecResolver: CompressionCodecResolver = DefaultCompressionCodecResolver()

    /**
     * 解析token字符串[token]，返回解析后的[Claims]对象
     */
    @JvmOverloads
    @Throws(TokenException::class)
    fun parseToken(token: String, key: SecretKey = generateKey()): Claims {
        return try {
            Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token).body
        } catch (e: ExpiredJwtException) {
            throw IncorrectPeriodException(e)
        } catch (e: UnsupportedJwtException) {
            throw DataParseFailException(e)
        } catch (e: MalformedJwtException) {
            throw DataParseFailException(e)
        } catch (e: SignatureException) {
            throw IncorrectSignatureException(e)
        } catch (e: IllegalArgumentException) {
            throw TokenException(e)
        }
    }

    /**
     * 忽略检测，解析token字符串[jwt]，返回解析后的[Claims]对象
     */
    @Throws(TokenException::class)
    fun parseTokenIgnoreTtl(jwt: String): Claims? {
        var base64UrlEncodedHeader: String? = null
        var base64UrlEncodedPayload: String? = null
        var base64UrlEncodedDigest: String? = null

        var delimiterCount = 0

        val sb = StringBuilder(128)

        for (c in jwt.toCharArray()) {

            if (c == JwtParser.SEPARATOR_CHAR) {

                val tokenSeq = Strings.clean(sb)
                val token = tokenSeq?.toString()

                if (delimiterCount == 0) {
                    base64UrlEncodedHeader = token
                } else if (delimiterCount == 1) {
                    base64UrlEncodedPayload = token
                }

                delimiterCount++
                sb.setLength(0)
            } else {
                sb.append(c)
            }
        }

        if (delimiterCount != 2) {
            val msg = "JWT strings must contain exactly 2 period characters. Found: $delimiterCount"
            throw DataParseFailException(msg)
        }
        if (sb.isNotEmpty()) {
            base64UrlEncodedDigest = sb.toString()
        }

        if (base64UrlEncodedPayload == null) {
            throw DataParseFailException("JWT string '$jwt' is missing a body/payload.")
        }

        // =============== Header =================
        val header: Header<*>?
        var compressionCodec: CompressionCodec? = null

        if (base64UrlEncodedHeader != null) {
            val bytes: ByteArray = TextCodec.BASE64URL.decode(base64UrlEncodedHeader)
            val origValue = String(bytes, Strings.UTF_8)
            val m = readValue(origValue)
            header = if (base64UrlEncodedDigest != null) {
                DefaultJwsHeader(m)
            } else {
                DefaultHeader(m)
            }
            compressionCodec = compressionCodecResolver.resolveCompressionCodec(header)
        }

        // =============== Body =================
        val payload: String = if (compressionCodec != null) {
            val decompressed = compressionCodec.decompress(TextCodec.BASE64URL.decode(base64UrlEncodedPayload))
            String(decompressed, Strings.UTF_8)
        } else {
            String(TextCodec.BASE64URL.decode(base64UrlEncodedPayload), Strings.UTF_8)
        }

        var claims: Claims? = null

        if (payload[0] == '{' && payload[payload.length - 1] == '}') { //likely to be json, parse it:
            val claimsMap = readValue(payload)
            claims = DefaultClaims(claimsMap)
        }
        return claims
    }

    /**
     * 创建一个新的token
     */
    @JvmOverloads
    fun createToken(
        id: String,
        subject: String,
        data: Map<String, Any>,
        ttl: Long = 300000,
        key: SecretKey = generateKey()
    ): String {
        val nowMillis = System.currentTimeMillis()
        return Jwts.builder()
            .setId(id)
            .setIssuedAt(Date(nowMillis))
            .setSubject(subject)
            .claim("data", data)
            .signWith(SignatureAlgorithm.HS512, key)
            .also {
                if (ttl >= 0) {
                    val expMillis = nowMillis + ttl
                    it.setExpiration(Date(expMillis))
                }
            }.compact()
    }

    private fun generateKey(): SecretKey = Base64.getEncoder().encode("jwt-key+${C.JWT_SECRET}".toByteArray()).let {
        SecretKeySpec(it, 0, it.size, "AES")
    }

    private fun readValue(value: String): Map<String, Any> {
        try {
            @Suppress("UNCHECKED_CAST")
            return objectMapper.readValue(value, Map::class.java) as Map<String, Any>
        } catch (e: IOException) {
            throw MalformedJwtException("Unable to read value: $value as JSON", e)
        }

    }

}