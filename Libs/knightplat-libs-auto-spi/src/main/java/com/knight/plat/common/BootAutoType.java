package com.knight.plat.common;

import com.knight.plat.api.annotations.AutoConfigDataLoader;
import com.knight.plat.api.annotations.AutoConfigDataLocationResolver;
import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.api.annotations.AutoContextInitializer;
import com.knight.plat.api.annotations.AutoDatabaseInitializerDetector;
import com.knight.plat.api.annotations.AutoDependsOnDatabaseInitializationDetector;
import com.knight.plat.api.annotations.AutoEnableCircuitBreaker;
import com.knight.plat.api.annotations.AutoEnvPostProcessor;
import com.knight.plat.api.annotations.AutoFailureAnalyzer;
import com.knight.plat.api.annotations.AutoListener;
import com.knight.plat.api.annotations.AutoLoggingSystemFactory;
import com.knight.plat.api.annotations.AutoRunListener;
import com.knight.plat.api.annotations.AutoTemplateProvider;

/**
 * 注解类型
 */

public enum BootAutoType {

    /**
     * Component，组合注解，添加到 spring.factories
     */
    COMPONENT(BootAutoType.COMPONENT_ANNOTATION, "org.springframework.boot.autoconfigure.EnableAutoConfiguration"),
    /**
     * ApplicationContextInitializer 添加到 spring.factories
     */
    CONTEXT_INITIALIZER(AutoContextInitializer.class.getName(), "org.springframework.context.ApplicationContextInitializer"),
    /**
     * ApplicationListener 添加到 spring.factories
     */
    LISTENER(AutoListener.class.getName(), "org.springframework.context.ApplicationListener"),
    /**
     * SpringApplicationRunListener 添加到 spring.factories
     */
    RUN_LISTENER(AutoRunListener.class.getName(), "org.springframework.boot.SpringApplicationRunListener"),
    /**
     * EnvironmentPostProcessor 添加到 spring.factories
     */
    ENV_POST_PROCESSOR(AutoEnvPostProcessor.class.getName(), "org.springframework.boot.env.EnvironmentPostProcessor"),
    /**
     * FailureAnalyzer 添加到 spring.factories
     */
    FAILURE_ANALYZER(AutoFailureAnalyzer.class.getName(), "org.springframework.boot.diagnostics.FailureAnalyzer"),
    /**
     * AutoConfigurationImportFilter spring.factories
     */
    AUTO_CONFIGURATION_IMPORT_FILTER(AutoConfigImportFilter.class.getName(), "org.springframework.boot.autoconfigure.AutoConfigurationImportFilter"),
    /**
     * TemplateAvailabilityProvider 添加到 spring.factories
     */
    TEMPLATE_AVAILABILITY_PROVIDER(AutoTemplateProvider.class.getName(), "org.springframework.boot.autoconfigure.template.TemplateAvailabilityProvider"),
    /**
     * auto EnableCircuitBreaker
     */
    AUTO_ENABLE_CIRCUIT_BREAKER(AutoEnableCircuitBreaker.class.getName(), "org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker"),
    /**
     * auto ConfigDataLocationResolver
     */
    AUTO_CONFIG_DATA_LOCATION_RESOLVER(AutoConfigDataLocationResolver.class.getName(), "org.springframework.boot.context.config.ConfigDataLocationResolver"),
    /**
     * auto ConfigDataLoader
     */
    AUTO_CONFIG_DATA_LOADER(AutoConfigDataLoader.class.getName(), "org.springframework.boot.context.config.ConfigDataLoader"),
    /**
     * auto DatabaseInitializerDetector
     */
    AUTO_DATABASE_INITIALIZER_DETECTOR(AutoDatabaseInitializerDetector.class.getName(), "org.springframework.boot.sql.init.dependency.DatabaseInitializerDetector"),
    /**
     * auto DependsOnDatabaseInitializationDetector
     */
    AUTO_DEPENDS_ON_DATABASE_INITIALIZATION_DETECTOR(AutoDependsOnDatabaseInitializationDetector.class.getName(), "org.springframework.boot.sql.init.dependency.DependsOnDatabaseInitializationDetector"),
    /**
     * auto LoggingSystemFactory
     */
    AUTO_LOGGING_SYSTEM(AutoLoggingSystemFactory.class.getName(), "org.springframework.boot.logging.LoggingSystemFactory"),
    ;

    BootAutoType(String annotation, String configureKey) {
        this.annotation = annotation;
        this.configureKey = configureKey;
    }

    private final String annotation;
    private final String configureKey;

    public static final String COMPONENT_ANNOTATION = "org.springframework.stereotype.Component";

    public String getAnnotation() {
        return annotation;
    }

    public String getConfigureKey() {
        return configureKey;
    }
}
