package com.knight.plat.common;

import com.knight.plat.api.annotations.AotBeanFactoryInitialization;
import com.knight.plat.api.annotations.AotBeanRegistration;
import com.knight.plat.api.annotations.AotRuntimeHintsRegistrar;

public enum AotAutoType {

    /**
     * RuntimeHintsRegistrar 添加到 aot.factories
     */
    RUNTIME_HINTS_REGISTRAR(AotRuntimeHintsRegistrar.class.getName(), "org.springframework.aot.hint.RuntimeHintsRegistrar"),
    /**
     * BeanRegistrationAotProcessor 添加到 aot.factories
     */
    BEAN_REGISTRATION(AotBeanRegistration.class.getName(), "org.springframework.beans.factory.aot.BeanRegistrationAotProcessor"),
    /**
     * BeanFactoryInitializationAotProcessor 添加到 aot.factories
     */
    BEAN_FACTORY_INITIALIZATION(AotBeanFactoryInitialization.class.getName(), "org.springframework.beans.factory.aot.BeanFactoryInitializationAotProcessor");

    private final String annotation;
    private final String configureKey;

    AotAutoType(String annotation, String configureKey) {
        this.annotation = annotation;
        this.configureKey = configureKey;
    }

    public String getAnnotation() {
        return annotation;
    }

    public String getConfigureKey() {
        return configureKey;
    }
}
