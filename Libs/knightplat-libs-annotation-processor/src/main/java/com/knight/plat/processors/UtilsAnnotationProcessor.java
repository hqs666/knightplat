package com.knight.plat.processors;


import com.google.auto.service.AutoService;
import com.knight.plat.api.annotations.Utils;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypesException;
import javax.lang.model.type.TypeMirror;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("com.knight.plat.api.annotations.Utils")
@AutoService(Processor.class)
public class UtilsAnnotationProcessor extends AbstractProcessor {

    private Filer filer;
    private List<FieldSpec> existsFields;
    private boolean isLib = false;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnv.getFiler(); // for creating file
        TypeElement typeElement = processingEnvironment.getElementUtils().getTypeElement("com.knight.plat.Utils");
        if (typeElement != null) {
            List<? extends Element> existsElements = typeElement.getEnclosedElements();
            if (!existsElements.isEmpty()) {
                existsFields = existsElements.stream()
                        .filter(e -> e.getKind().isField())
                        .map(this::getSpecFromElement)
                        .collect(Collectors.toList());
            }
        }
    }

    private FieldSpec getSpecFromElement(Element element) {
        return FieldSpec
                .builder(ClassName.get(element.asType()), element.getSimpleName().toString(), Modifier.PUBLIC,
                        Modifier.FINAL, Modifier.STATIC)
                .initializer("new $T()", ClassName.get(element.asType()))
                .build();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        final Map<CharSequence, TypeMirror> utils = new HashMap<>();
        final Map<CharSequence, List<ExecutableElement>> methodMap = new HashMap<>();
        for (TypeElement te : annotations) {
            for (Element e : roundEnv.getElementsAnnotatedWith(te)) {
                Utils annotation = e.getAnnotation(Utils.class);
                boolean lib = annotation.lib();
                if (lib) {
                    isLib = true;
                }
                CharSequence utilName = annotation.value();
                if (utilName.length() == 0) {
                    utilName = e.getSimpleName();
                }
                utils.put(utilName, e.asType());
                List<ExecutableElement> methods = new ArrayList<>();
                for (Element m : e.getEnclosedElements()) {
                    if (m instanceof ExecutableElement) {
                        if (!m.getSimpleName().contentEquals("<init>")) {
                            methods.add((ExecutableElement) m);
                        }
                    }
                }
                methodMap.put(utilName, methods);
            }
        }
        if (!utils.isEmpty()) {
            productUtilsClass(utils, methodMap);
        }
        return true;
    }

    private void productUtilsClass(final Map<CharSequence, TypeMirror> utils,
                                   final Map<CharSequence, List<ExecutableElement>> methodMap) {
        final Map<String, TypeSpec> innerTypes = new HashMap<>();
        utils.forEach((name, type) -> {
            String nameStr = name.toString();
            String typeName = nameStr.substring(0, 1).toUpperCase() + nameStr.substring(1) + "Delegate";
            TypeSpec.Builder utilType = TypeSpec.classBuilder(typeName)
                    .addModifiers(Modifier.FINAL, Modifier.PUBLIC);
            List<ExecutableElement> methods = methodMap.get(name);
            methods.stream()
                    .filter(m -> m.getModifiers().contains(Modifier.PUBLIC) && m.getModifiers()
                            .contains(Modifier.STATIC))
                    .filter(m -> m.getAnnotationsByType(Utils.Ignore.class).length == 0)
                    .map(m -> {
                        List<ParameterSpec> params = m.getParameters().stream()
                                .map(p -> ParameterSpec.builder(TypeName.get(p.asType()), p.getSimpleName().toString())
                                        .build()).collect(Collectors.toList());
                        List<TypeVariableName> typeParams = m.getTypeParameters().stream().map(TypeVariableName::get)
                                .collect(Collectors.toList());
                        CodeBlock innerParams = CodeBlock.join(params.stream().map(p -> CodeBlock.of("$N", p.name))
                                .collect(Collectors.toList()), ", ");
                        Set<TypeName> throwList = m.getThrownTypes().stream().map(TypeName::get)
                                .collect(Collectors.toSet());
                        Utils.ThrowException throwAnnotation = m.getAnnotation(Utils.ThrowException.class);
                        if (throwAnnotation != null) {
                            try {
                                Class<? extends Throwable>[] clazz = throwAnnotation.value();
                                Stream.of(clazz).map(ClassName::get).forEach(throwList::add);
                            } catch (MirroredTypesException e) {
                                e.getTypeMirrors().stream().map(TypeName::get).forEach(throwList::add);
                            }
                        }

                        TypeName returnType = TypeName.get(m.getReturnType());
                        boolean isReturnVoid = returnType.equals(TypeName.VOID);
                        String returnHead = isReturnVoid ? "" : "return ";
                        return MethodSpec.methodBuilder(m.getSimpleName().toString())
                                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                                .addTypeVariables(typeParams)
                                .addExceptions(throwList)
                                .returns(returnType)
                                .addParameters(params)
                                .addStatement(
                                        CodeBlock.of(returnHead + "$T.$N(", type, m.getSimpleName())
                                                .toBuilder().add(innerParams).add(")").build()
                                )
                                .build();
                    }).forEach(utilType::addMethod);
            innerTypes.put(nameStr, utilType.build());
        });
        final String utilName = isLib ? "Utils" : "U";
        TypeSpec.Builder ryUtilsBuilder = TypeSpec.classBuilder(utilName)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addJavadoc("Tool methods collection.");
        innerTypes.forEach((name, type) -> {
            JavaFile delegateFile = JavaFile.builder("com.knight.plat.delegate", type)
                    .build();
            try {
                delegateFile.writeTo(filer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FieldSpec field = FieldSpec
                    .builder(ClassName.get("com.knight.plat.delegate", type.name), name, Modifier.PUBLIC, Modifier.FINAL, Modifier.STATIC)
                    .initializer("new $T()", ClassName.get("", type.name)).build();
            ryUtilsBuilder.addField(field);
        });

        if (existsFields != null && !existsFields.isEmpty()) {
            ryUtilsBuilder.addFields(existsFields);
        }

        try {
            // build com.knight.plat.U.java
            JavaFile javaFile = JavaFile.builder("com.knight.plat", ryUtilsBuilder.build())
                    .build();
            // write to file
            javaFile.writeTo(filer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
