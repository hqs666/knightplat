package com.knight.plat.migration.content.impl;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;
import com.knight.plat.migration.content.ContentConverter;
import com.knight.plat.migration.content.ConvertResult;
import com.knight.plat.migration.content.impl.visitor.AnnotationVisitor;


import java.io.BufferedReader;

public class ParserContentConverter implements ContentConverter {

    final AnnotationVisitor annotationVisitor = new AnnotationVisitor();

    @Override
    public ConvertResult convert(BufferedReader reader, String fileName) {
        CompilationUnit compilationUnit = StaticJavaParser.parse(reader);
        LexicalPreservingPrinter.setup(compilationUnit);
        annotationVisitor.reset(fileName.substring(0, fileName.lastIndexOf(".")));
        try {
            annotationVisitor.visit(compilationUnit, null);
        } catch (Exception e) {
            throw new RuntimeException("Error while parsing file: " + fileName, e);
        }
        if (annotationVisitor.isModified()) {
            return ConvertResult.modified(compilationUnit.toString());
        }
        return ConvertResult.unmodified();
    }
}
