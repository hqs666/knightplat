package com.knight.plat.migration.dir;

import java.io.File;

/**
 * 处理路径信息，用于在转换文件时存储遍历文件的路径
 */
public class PathResolver {
    private final String base;

    private final String path;

    private PathResolver(String basePath, String relativePath) {
        base = basePath;
        path = relativePath;
    }

    public static PathResolver of(String basePath, String relativePath) {
        return new PathResolver(basePath, relativePath);
    }

    public String getRelativePath() {
        StringBuilder rp = new StringBuilder(path);
        while (rp.length() > 0 && rp.charAt(0) == File.separatorChar) {
            rp.deleteCharAt(0);
        }
        return rp.toString();
    }

    public String getAbsolutePath() {
        return getResolvedPath(base);
    }

    public String getResolvedPath(String basePath) {
        final StringBuilder bp = new StringBuilder(basePath), rp = new StringBuilder(path);
        while (bp.length() > 0 && bp.charAt(bp.length() - 1) == File.separatorChar) {
            bp.deleteCharAt(bp.length() - 1);
        }
        while (rp.length() > 0 && rp.charAt(0) == File.separatorChar) {
            rp.deleteCharAt(0);
        }
        return bp.append(File.separatorChar).append(rp).toString();
    }

    public File toFile(String basePath) {
        return new File(getResolvedPath(basePath));
    }

    public File toFile() {
        return new File(getAbsolutePath());
    }
}
