package com.knight.plat.migration.config;

import java.util.HashMap;
import java.util.Map;

public class ReplaceConfig {
    private String replacement;
    private Map<String, String> params = new HashMap<>();

    public String getReplacement() {
        return replacement;
    }

    public void setReplacement(String replacement) {
        this.replacement = replacement;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }
}
