package com.knight.plat.migration.config;

import com.google.common.base.Strings;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Config {

    private Set<String> needName = new HashSet<>();
    private Map<String, String> imports = new HashMap<>();
    private Map<String, ReplaceConfig> config = new HashMap<>();

    public Map<String, ReplaceConfig> getConfig() {
        return config;
    }

    public void setConfig(Map<String, ReplaceConfig> config) {
        this.config = config;
    }

    public Map<String, String> getImports() {
        return imports;
    }

    public void setImports(Map<String, String> imports) {
        this.imports = imports;
    }

    public Set<String> getNeedName() {
        return needName;
    }

    public void setNeedName(Set<String> needName) {
        this.needName = needName;
    }

    public boolean needName(String origin) {
        return needName.contains(origin);
    }

    public boolean noConfig(String origin) {
        return !config.containsKey(origin);
    }

    public String importReplacement(String origin) {
        return imports.get(origin);
    }

    public String replacement(String origin) {
        final ReplaceConfig replaceConfig = config.get(origin);
        if (replaceConfig == null) {
            return origin;
        }
        final String replacement = replaceConfig.getReplacement();
        if (Strings.isNullOrEmpty(replacement)) {
            return origin;
        }
        return replacement;
    }

    public Map<String, String> paramMap(String origin) {
        final ReplaceConfig replaceConfig = config.get(origin);
        if (replaceConfig == null) {
            return Collections.emptyMap();
        }
        return replaceConfig.getParams();
    }
}
