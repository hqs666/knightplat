package com.knight.plat.migration.content.impl.visitor;

import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.google.common.base.Strings;
import com.knight.plat.migration.config.Config;
import com.knight.plat.migration.config.ConfigReader;

import java.util.Map;

public class AnnotationVisitor extends VoidVisitorAdapter<Map<String, String>> {

    private final Config config = ConfigReader.get();
    private String className;
    private boolean modified = false;

    public void reset(String className) {
        this.className = className;
        modified = false;
    }

    @Override
    public void visit(SingleMemberAnnotationExpr n, Map<String, String> params) {

        final String selfName = n.getNameAsString();
        if (config.noConfig(selfName)) {
            super.visit(n, params);
            return;
        }

        String replacement = config.replacement(selfName);
        final Map<String, String> realParams = config.paramMap(selfName);

        final String singleKey = realParams.get("");
        // 如果子节点是字符串或表达式，则进行节点替换，修改为转换后的节点
        for (Node node : n.getChildNodes()) {
            if (node instanceof StringLiteralExpr || node instanceof BinaryExpr) {
                if (Strings.isNullOrEmpty(singleKey)) {
                    break;
                }
                final NodeList<MemberValuePair> newPairs = new NodeList<>();
                if (config.needName(selfName)) {
                    newPairs.add(new MemberValuePair("name", new StringLiteralExpr(className)));
                }
                newPairs.add(new MemberValuePair(singleKey, (Expression) node));
                boolean replace = n.replace(new NormalAnnotationExpr(new Name(replacement), newPairs));
                modified = modified || replace;
                break;
            }
        }
        n.setName(replacement);
        super.visit(n, realParams);
    }

    @Override
    public void visit(NormalAnnotationExpr n, Map<String, String> params) {
        final String selfName = n.getNameAsString();
        if (config.noConfig(selfName)) {
            super.visit(n, params);
            return;
        }

        String replacement = config.replacement(selfName);
        final Map<String, String> realParams = config.paramMap(selfName);
        final NodeList<MemberValuePair> pairs = n.getPairs();
        final NodeList<MemberValuePair> newPairs = new NodeList<>();
        boolean hasNameKey = false;
        for (MemberValuePair pair : pairs) {
            final String paramReplacement = realParams.get(pair.getNameAsString());
            if (Strings.isNullOrEmpty(paramReplacement)) {
                continue;
            }
            pair.setName(paramReplacement);
            newPairs.add(pair);
            if ("name".equals(paramReplacement)) {
                hasNameKey = true;
            }
            modified = true;
        }
        if (!hasNameKey && config.needName(selfName)) {
            newPairs.add(0, new MemberValuePair("name", new StringLiteralExpr(className)));
        }
        n.setName(replacement);
        n.setPairs(newPairs);
        super.visit(n, realParams);
    }

    @Override
    public void visit(MarkerAnnotationExpr n, Map<String, String> params) {
        final String selfName = n.getNameAsString();
        if (config.noConfig(selfName)) {
            super.visit(n, params);
            return;
        }

        String replacement = config.replacement(selfName);
        n.replace(new MarkerAnnotationExpr(replacement));
        modified = true;
        final Map<String, String> realParams = config.paramMap(selfName);
        super.visit(n, realParams);
    }

    @Override
    public void visit(ImportDeclaration n, Map<String, String> params) {
        String replacement = config.importReplacement(n.getNameAsString());
        if (!Strings.isNullOrEmpty(replacement)) {
            n.setName(replacement);
            modified = true;
        }
        super.visit(n, params);
    }

    public boolean isModified() {
        return modified;
    }
}
