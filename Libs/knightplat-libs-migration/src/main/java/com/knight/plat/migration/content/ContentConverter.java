package com.knight.plat.migration.content;

import java.io.BufferedReader;
import java.io.IOException;

public interface ContentConverter {
    ConvertResult convert(BufferedReader reader, String fileName) throws IOException;
}
