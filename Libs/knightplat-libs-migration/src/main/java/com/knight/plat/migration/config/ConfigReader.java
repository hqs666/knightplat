package com.knight.plat.migration.config;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

public class ConfigReader {

    private static final Config config;

    static {
        InputStream is = ConfigReader.class.getResourceAsStream("/config.yml");
        config = new Yaml().loadAs(is, Config.class);
    }

    public static Config get() {
        return config;
    }
}
