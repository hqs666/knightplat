package com.knight.plat.migration;

public class SwaggerAnnotationUpgradeConfig {

    private String[] skipDirectory;

    private String[] skipFile;

    public SwaggerAnnotationUpgradeConfig() {
        this.skipDirectory = new String[]{};
        // 指定跳过当前文件（工具类所在的项目要跳过这两个文件）
        this.skipFile = new String[]{"SwaggerAnnotationUpgradeConfig.java", "SwaggerAnnotationUpgradeManager.java"};
    }

    public String[] getSkipFile() {
        return skipFile;
    }

    public void setSkipFile(String[] skipFile) {
        this.skipFile = skipFile;
    }

    public String[] getSkipDirectory() {
        return skipDirectory;
    }

    public void setSkipDirectory(String[] skipDirectory) {
        this.skipDirectory = skipDirectory;
    }
}
