package com.knight.plat.migration;



import com.knight.plat.migration.content.ContentConverter;
import com.knight.plat.migration.content.ConvertResult;
import com.knight.plat.migration.content.impl.ParserContentConverter;
import com.knight.plat.migration.dir.FileHandler;
import com.knight.plat.migration.dir.PathResolver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class SwaggerAnnotationUpgradeManager {

    private final SwaggerAnnotationUpgradeConfig config = new SwaggerAnnotationUpgradeConfig();
    private final ContentConverter converter = new ParserContentConverter();

    public void scanAndDo(Path path, FileHandler handler) throws IOException {
        // 指定要遍历的文件夹
        final File directory = path.toFile();
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException("Path: " + path.toAbsolutePath().normalize() + "is not a directory.");
        }
        traverse(directory, directory.getAbsolutePath(), config.getSkipDirectory(), config.getSkipFile(), handler);
    }

    /**
     * Traverse a given file, skipping any directories and files in the given arrays.
     *
     * @param file the file to traverse
     * @param basePath the base path from which the traversal is started
     * @param skipDirectory an array of directories to skip
     * @param skipFile an array of files to skip
     * @param handler a handler for a File
     * @throws IOException if an I/O exception occurs
     */
    private void traverse(File file, String basePath, String[] skipDirectory, String[] skipFile, FileHandler handler) throws IOException {
        if (!file.exists()) {
            return;
        }
        List<String> skipDirectoryList = null;
        if (skipDirectory != null && skipDirectory.length > 0) {
            // array转化为list
            skipDirectoryList = Arrays.asList(skipDirectory);
        }
        List<String> skipFileList = null;
        if (skipFile != null && skipFile.length > 0) {
            skipFileList = Arrays.asList(skipFile);
        }
        // 如果是文件夹
        if (file.isDirectory()) {
            // 要跳过的文件夹
            if (skipDirectoryList != null && skipDirectoryList.contains(file.getPath())) {
                return;
            }
            // 获取文件夹下所有文件
            File[] files = file.listFiles();
            if (files == null || files.length == 0) {
                return;
            }
            // 遍历所有文件
            for (File subFile : files) {
                // 递归遍历子文件夹
                traverse(subFile, basePath, skipDirectory, skipFile, handler);
            }
            // 如果是文件
        } else if (file.isFile()) {
            String fileName = file.getName();
            // 跳过指定文件
            if (skipFileList != null && skipFileList.contains(fileName)) {
                return;
            }
            if (file.exists() && fileName.endsWith(".java")) {
                try(Reader fr = new FileReader(file);BufferedReader br = new BufferedReader(fr)) {
                    ConvertResult result = converter.convert(br, fileName);
                    if (result.isModified()) {
                        final String filePath = file.getAbsolutePath();
                        final String relativePath = filePath.substring(basePath.length());
                        handler.handle(
                                PathResolver.of(basePath, relativePath),
                                result.getResult()
                        );
                    }
                }
            }
        }
    }
}