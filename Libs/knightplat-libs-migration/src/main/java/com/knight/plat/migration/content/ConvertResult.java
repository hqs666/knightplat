package com.knight.plat.migration.content;

public class ConvertResult {

    private ConvertResult(boolean modified, String result) {
        this.modified = modified;
        this.result = result;
    }

    public static ConvertResult modified(String result) {
        return new ConvertResult(true, result);
    }

    public static ConvertResult unmodified() {
        return new ConvertResult(false, null);
    }

    private final boolean modified;
    private final String result;

    public boolean isModified() {
        return modified;
    }

    public String getResult() {
        return result;
    }
}
