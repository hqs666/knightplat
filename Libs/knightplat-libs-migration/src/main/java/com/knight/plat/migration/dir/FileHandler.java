package com.knight.plat.migration.dir;

import java.io.IOException;

@FunctionalInterface
public interface FileHandler {
    void handle(PathResolver pathResolver, String newContent) throws IOException;
}
