package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.api.crypto.AESEngine;
import com.knight.plat.api.crypto.Base64Engine;
import com.knight.plat.api.crypto.DESEngine;
import com.knight.plat.api.crypto.RSAEngine;
import com.knight.plat.api.crypto.SM2Engine;
import com.knight.plat.api.crypto.SM4Engine;
import com.knight.plat.utils.helper.CryptoEngineSelector;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

@Utils("enc")
public final class EncryptUtils {
    private static final Base64Engine base64Engine = CryptoEngineSelector.NATIVE.getBase64Engine();
    private static final AESEngine aesEngine = CryptoEngineSelector.NATIVE.getAESEngine();
    private static final DESEngine desEngine = CryptoEngineSelector.NATIVE.getDESEngine();
    private static final SM4Engine sm4Engine = CryptoEngineSelector.NATIVE.getSM4Engine();
    private static final SM2Engine sm2Engine = CryptoEngineSelector.NATIVE.getSM2Engine();
    private static final RSAEngine rsaEngine = CryptoEngineSelector.JVM.getRSAEngine();

    /**
     * md5编码
     *
     * @param source 原始数据
     * @return 编码后数据
     * @deprecated 使用 {@link DigestUtils#md5(String)} DigestUtils.md5(String)} 代替
     */
    @Deprecated
    @Utils.Ignore
    public static String md5Encrypt(String source) {
        return DigestUtils.md5(source);
    }

    /**
     * sha256编码
     *
     * @param source 原始数据
     * @return 编码后数据
     * @deprecated 使用 {@link DigestUtils#sha256(String)} DigestUtils.sha256(String)} 代替
     */
    @Deprecated
    @Utils.Ignore
    public static String sha256Encrypt(String source) {
        return DigestUtils.sha256(source);
    }

    /**
     * sha512编码
     *
     * @param source 原始数据
     * @return 编码后数据
     * @deprecated 使用 {@link DigestUtils#sha512(String)} DigestUtils.sha512(String)} 代替
     */
    @Deprecated
    @Utils.Ignore
    public static String sha512Encrypt(String source) {
        return DigestUtils.sha512(source);
    }

    /**
     * HMACSHA256编码
     *
     * @param source   原始数据
     * @param secret   秘钥
     * @param isBase64 是否返回base64编码后数据
     * @return 编码后数据
     * @deprecated 使用 {@link DigestUtils#hmacSha256(String, String, boolean) DigestUtils.hmacSha256(String, String, boolean)} 代替
     */
    @Deprecated
    @Utils.Ignore
    public static String hmacSha256Encrypt(String source, String secret, boolean isBase64) {
        return DigestUtils.hmacSha256(source, secret, isBase64);
    }

    /**
     * base64编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    public static String base64Encrypt(String source) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return base64Encrypt(source.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * base64编码
     *
     * @param source 原始数据字节数组
     * @return 编码后的数据
     */
    public static String base64Encrypt(byte[] source) {
        byte[] bytes = base64EncryptToBytes(source);
        return bytesToString(bytes);
    }

    /**
     * base64编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    public static byte[] base64EncryptToBytes(String source) {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return base64EncryptToBytes(source.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * base64编码
     *
     * @param source 原始数据字节数组
     * @return 编码后的数据
     */
    public static byte[] base64EncryptToBytes(byte[] source) {
        ByteBuffer buffer = base64Engine.base64Encode(source, true);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * base64解码
     *
     * @param origin 原始数据
     * @return 解码后数据
     */
    public static String base64Decode(String origin) {
        if (origin == null || origin.isEmpty()) {
            return "";
        }
        return base64Decode(origin.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * base64解码
     *
     * @param origin 原始数据
     * @return 解码后数据
     */
    public static String base64Decode(byte[] origin) {
        byte[] bytes = base64DecodeBytes(origin);
        return bytesToString(bytes);
    }

    /**
     * base64解码
     *
     * @param origin 原始数据
     * @return 解码后数据字节数组
     */
    public static byte[] base64DecodeBytes(String origin) {
        if (origin == null || origin.isEmpty()) {
            return new byte[0];
        }
        return base64DecodeBytes(origin.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * base64解码
     *
     * @param origin 原始数据
     * @return 解码后数据字节数组
     */
    public static byte[] base64DecodeBytes(byte[] origin) {
        ByteBuffer buffer = base64Engine.base64Decode(origin, true);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * SM4加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static byte[] sm4Encrypt(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = sm4Engine.sm4Encode(source, keyStr.getBytes(StandardCharsets.UTF_8), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * SM4加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static String sm4EncryptToString(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = sm4Engine.sm4Encode(source, keyStr.getBytes(StandardCharsets.UTF_8), true);
        return ByteBufferUtils.getString(buffer);
    }

    /**
     * SM4加密 - 字符串版
     *
     * @param source 原始数据字符串
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static byte[] sm4Encrypt(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return sm4Encrypt(source.getBytes(StandardCharsets.UTF_8), keyStr);
    }

    /**
     * SM4加密 - 字符串版
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static String sm4EncryptToString(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return sm4EncryptToString(source.getBytes(StandardCharsets.UTF_8), keyStr);
    }

    public static byte[] sm4Decrypt(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = sm4Engine.sm4Decode(source, keyStr.getBytes(StandardCharsets.UTF_8), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    public static byte[] sm4Decrypt(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = sm4Engine.sm4Decode(source.getBytes(StandardCharsets.UTF_8), keyStr.getBytes(StandardCharsets.UTF_8), true);
        return ByteBufferUtils.getBytes(buffer);
    }

    public static String sm4DecryptToString(byte[] source, String keyStr) throws InvalidKeySpecException {
        byte[] bytes = sm4Decrypt(source, keyStr);
        return bytesToString(bytes);
    }

    public static String sm4DecryptToString(String source, String keyStr) throws InvalidKeySpecException {
        byte[] bytes = sm4Decrypt(source, keyStr);
        return bytesToString(bytes);
    }

    /**
     * DES加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static byte[] desEncrypt(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = desEngine.desEncode(source, keyStr.getBytes(StandardCharsets.UTF_8), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * DES加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static String desEncryptToString(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = desEngine.desEncode(source, keyStr.getBytes(StandardCharsets.UTF_8), true);
        return ByteBufferUtils.getString(buffer);
    }

    /**
     * DES加密 - 字符串版
     *
     * @param source 原始数据字符串
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static byte[] desEncrypt(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return desEncrypt(source.getBytes(StandardCharsets.UTF_8), keyStr);
    }

    /**
     * DES加密 - 字符串版
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static String desEncryptToString(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return desEncryptToString(source.getBytes(StandardCharsets.UTF_8), keyStr);
    }

    public static byte[] desDecrypt(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = desEngine.desDecode(source, keyStr.getBytes(StandardCharsets.UTF_8), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    public static byte[] desDecrypt(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = desEngine.desDecode(source.getBytes(StandardCharsets.UTF_8), keyStr.getBytes(StandardCharsets.UTF_8), true);
        return ByteBufferUtils.getBytes(buffer);
    }

    public static String desDecryptToString(byte[] source, String keyStr) throws InvalidKeySpecException {
        byte[] bytes = desDecrypt(source, keyStr);
        return bytesToString(bytes);
    }

    public static String desDecryptToString(String source, String keyStr) throws InvalidKeySpecException {
        byte[] bytes = desDecrypt(source, keyStr);
        return bytesToString(bytes);
    }

    /**
     * AES加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static byte[] aesEncrypt(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = aesEngine.aesEncode(source, keyStr.getBytes(StandardCharsets.UTF_8), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * AES加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static String aesEncryptToString(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = aesEngine.aesEncode(source, keyStr.getBytes(StandardCharsets.UTF_8), true);
        return ByteBufferUtils.getString(buffer);
    }

    /**
     * AES加密 - 字符串版
     *
     * @param source 原始数据字符串
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static byte[] aesEncrypt(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return aesEncrypt(source.getBytes(StandardCharsets.UTF_8), keyStr);
    }

    /**
     * AES加密 - 字符串版
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    public static String aesEncryptToString(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return aesEncryptToString(source.getBytes(StandardCharsets.UTF_8), keyStr);
    }

    public static byte[] aesDecrypt(byte[] source, String keyStr) throws InvalidKeySpecException {
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = aesEngine.aesDecode(source, keyStr.getBytes(StandardCharsets.UTF_8), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    public static byte[] aesDecrypt(String source, String keyStr) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        if (keyStr == null || keyStr.isEmpty()) {
            throw new InvalidKeySpecException("keyStr can not be empty");
        }
        ByteBuffer buffer = aesEngine.aesDecode(source.getBytes(StandardCharsets.UTF_8), keyStr.getBytes(StandardCharsets.UTF_8), true);
        return ByteBufferUtils.getBytes(buffer);
    }

    public static String aesDecryptToString(byte[] source, String keyStr) throws InvalidKeySpecException {
        byte[] bytes = aesDecrypt(source, keyStr);
        return bytesToString(bytes);
    }

    public static String aesDecryptToString(String source, String keyStr) throws InvalidKeySpecException {
        byte[] bytes = aesDecrypt(source, keyStr);
        return bytesToString(bytes);
    }

    /**
     * 生成RSA加密公钥/私钥对
     *
     * @return 公钥/私钥对
     */
    public static KeyResolver generateRSAKeyPair() {
        return CryptoCommonKt.generateRSAKeyPair();
    }

    /**
     * 生成RSA加密公钥/私钥对
     *
     * @return 公钥/私钥对
     */
    public static KeyResolver generateSM2KeyPair() {
        return CryptoCommonKt.generateSM2KeyPair();
    }

// RSA加解密byte数组部分

    /**
     * RSA公钥加密 - 字节版 加密的最终处理方法
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static byte[] rsaEncrypt(byte[] source, PublicKey publicKey) {
        try {
            if (publicKey == null) {
                throw new InvalidKeySpecException("publicKey can not be empty");
            }
            ByteBuffer buffer = rsaEngine.rsaEncode(source, publicKey.getEncoded(), false);
            return ByteBufferUtils.getBytes(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * RSA公钥加密 - 字节版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static byte[] rsaEncrypt(byte[] source, String publicKey) throws InvalidKeySpecException {
        if (publicKey == null || publicKey.isEmpty()) {
            throw new InvalidKeySpecException("publicKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(publicKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = rsaEngine.rsaEncode(source, ByteBufferUtils.getBytes(keyBuffer), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * RSA公钥加密 - 字符串版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static String rsaEncryptToString(byte[] source, PublicKey publicKey) {
        try {
            if (publicKey == null) {
                throw new InvalidKeySpecException("publicKey can not be empty");
            }
            ByteBuffer buffer = rsaEngine.rsaEncode(source, publicKey.getEncoded(), true);
            return ByteBufferUtils.getString(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * RSA公钥加密 - 字符串版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static String rsaEncryptToString(byte[] source, String publicKey) throws InvalidKeySpecException {
        if (publicKey == null || publicKey.isEmpty()) {
            throw new InvalidKeySpecException("publicKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(publicKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = rsaEngine.rsaEncode(source, ByteBufferUtils.getBytes(keyBuffer), true);
        return ByteBufferUtils.getString(buffer);
    }

    /**
     * RSA私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static byte[] rsaDecrypt(byte[] source, PrivateKey privateKey) {
        try {
            if (privateKey == null) {
                throw new InvalidKeySpecException("privateKey can not be empty");
            }
            ByteBuffer buffer = rsaEngine.rsaDecode(source, privateKey.getEncoded(), false);
            return ByteBufferUtils.getBytes(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * RSA私钥解密 - 字节版
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static byte[] rsaDecrypt(byte[] source, String privateKey) throws InvalidKeySpecException {
        if (privateKey == null || privateKey.isEmpty()) {
            throw new InvalidKeySpecException("privateKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(privateKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = rsaEngine.rsaDecode(source, ByteBufferUtils.getBytes(keyBuffer), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * RSA私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static String rsaDecryptToString(byte[] source, PrivateKey privateKey) {
        byte[] bytes = rsaDecrypt(source, privateKey);
        return bytesToString(bytes);
    }

    /**
     * RSA私钥解密 - 字节版
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static String rsaDecryptToString(byte[] source, String privateKey) throws InvalidKeySpecException {
        byte[] bytes = rsaDecrypt(source, privateKey);
        return bytesToString(bytes);
    }

// RSA加解密字符串部分

    /**
     * RSA公钥加密 - 字节版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static byte[] rsaEncrypt(String source, PublicKey publicKey) {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return rsaEncrypt(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * RSA公钥加密 - 字节版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static byte[] rsaEncrypt(String source, String publicKey) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return rsaEncrypt(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * RSA公钥加密 - 字符串版
     *
     * @param source    原始数据字符串
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static String rsaEncryptToString(String source, PublicKey publicKey) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return rsaEncryptToString(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * RSA公钥加密 - 字符串版
     *
     * @param source    原始数据字符串
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static String rsaEncryptToString(String source, String publicKey) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return rsaEncryptToString(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * RSA私钥解密 - 字符串版
     *
     * @param source     原始数据
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static byte[] rsaDecrypt(String source, PrivateKey privateKey) {
        try {
            if (source == null || source.isEmpty()) {
                return new byte[0];
            }
            if (privateKey == null) {
                throw new InvalidKeySpecException("privateKey can not be empty");
            }
            ByteBuffer buffer = rsaEngine.rsaDecode(source.getBytes(StandardCharsets.UTF_8), privateKey.getEncoded(), true);
            return ByteBufferUtils.getBytes(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * RSA私钥解密 - 字符串版
     *
     * @param source     原始数据
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static byte[] rsaDecrypt(String source, String privateKey) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        if (privateKey == null || privateKey.isEmpty()) {
            throw new InvalidKeySpecException("privateKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(privateKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = rsaEngine.rsaDecode(source.getBytes(StandardCharsets.UTF_8), ByteBufferUtils.getBytes(keyBuffer), true);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * RSA私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static String rsaDecryptToString(String source, PrivateKey privateKey) {
        byte[] bytes = rsaDecrypt(source, privateKey);
        return bytesToString(bytes);
    }

    /**
     * RSA私钥解密 - 字节版
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static String rsaDecryptToString(String source, String privateKey) throws InvalidKeySpecException {
        byte[] bytes = rsaDecrypt(source, privateKey);
        return bytesToString(bytes);
    }

// SM2加解密byte数组部分

    /**
     * SM2公钥加密 - 字节版 加密的最终处理方法
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static byte[] sm2Encrypt(byte[] source, PublicKey publicKey) {
        try {
            if (publicKey == null) {
                throw new InvalidKeySpecException("publicKey can not be empty");
            }
            ByteBuffer buffer = sm2Engine.sm2Encode(source, publicKey.getEncoded(), false);
            return ByteBufferUtils.getBytes(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * SM2公钥加密 - 字节版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static byte[] sm2Encrypt(byte[] source, String publicKey) throws InvalidKeySpecException {
        if (publicKey == null || publicKey.isEmpty()) {
            throw new InvalidKeySpecException("publicKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(publicKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = sm2Engine.sm2Encode(source, ByteBufferUtils.getBytes(keyBuffer), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * SM2公钥加密 - 字符串版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static String sm2EncryptToString(byte[] source, PublicKey publicKey) {
        try {
            if (publicKey == null) {
                throw new InvalidKeySpecException("publicKey can not be empty");
            }
            ByteBuffer buffer = sm2Engine.sm2Encode(source, publicKey.getEncoded(), true);
            return ByteBufferUtils.getString(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * SM2公钥加密 - 字符串版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static String sm2EncryptToString(byte[] source, String publicKey) throws InvalidKeySpecException {
        if (publicKey == null || publicKey.isEmpty()) {
            throw new InvalidKeySpecException("publicKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(publicKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = sm2Engine.sm2Encode(source, ByteBufferUtils.getBytes(keyBuffer), true);
        return ByteBufferUtils.getString(buffer);
    }

    /**
     * SM2私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static byte[] sm2Decrypt(byte[] source, PrivateKey privateKey) {
        try {
            if (privateKey == null) {
                throw new InvalidKeySpecException("privateKey can not be empty");
            }
            ByteBuffer buffer = sm2Engine.sm2Decode(source, privateKey.getEncoded(), false);
            return ByteBufferUtils.getBytes(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * SM2私钥解密 - 字节版
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static byte[] sm2Decrypt(byte[] source, String privateKey) throws InvalidKeySpecException {
        if (privateKey == null || privateKey.isEmpty()) {
            throw new InvalidKeySpecException("privateKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(privateKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = sm2Engine.sm2Decode(source, ByteBufferUtils.getBytes(keyBuffer), false);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * SM2私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static String sm2DecryptToString(byte[] source, PrivateKey privateKey) {
        byte[] bytes = sm2Decrypt(source, privateKey);
        return bytesToString(bytes);
    }

    /**
     * SM2私钥解密 - 字节版
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static String sm2DecryptToString(byte[] source, String privateKey) throws InvalidKeySpecException {
        byte[] bytes = sm2Decrypt(source, privateKey);
        return bytesToString(bytes);
    }

// RSA加解密字符串部分

    /**
     * SM2公钥加密 - 字节版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static byte[] sm2Encrypt(String source, PublicKey publicKey) {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return sm2Encrypt(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * SM2公钥加密 - 字节版
     *
     * @param source    原始数据字节数组
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static byte[] sm2Encrypt(String source, String publicKey) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        return sm2Encrypt(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * SM2公钥加密 - 字符串版
     *
     * @param source    原始数据字符串
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    public static String sm2EncryptToString(String source, PublicKey publicKey) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return sm2EncryptToString(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * SM2公钥加密 - 字符串版
     *
     * @param source    原始数据字符串
     * @param publicKey 秘钥字符串
     * @return 加密结果
     */
    public static String sm2EncryptToString(String source, String publicKey) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return "";
        }
        return sm2EncryptToString(source.getBytes(StandardCharsets.UTF_8), publicKey);
    }

    /**
     * SM2私钥解密 - 字符串版
     *
     * @param source     原始数据
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static byte[] sm2Decrypt(String source, PrivateKey privateKey) {
        try {
            if (source == null || source.isEmpty()) {
                return new byte[0];
            }
            if (privateKey == null) {
                throw new InvalidKeySpecException("privateKey can not be empty");
            }
            ByteBuffer buffer = sm2Engine.sm2Decode(source.getBytes(StandardCharsets.UTF_8), privateKey.getEncoded(), true);
            return ByteBufferUtils.getBytes(buffer);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * SM2私钥解密 - 字符串版
     *
     * @param source     原始数据
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static byte[] sm2Decrypt(String source, String privateKey) throws InvalidKeySpecException {
        if (source == null || source.isEmpty()) {
            return new byte[0];
        }
        if (privateKey == null || privateKey.isEmpty()) {
            throw new InvalidKeySpecException("privateKey can not be empty");
        }
        ByteBuffer keyBuffer = base64Engine.base64Decode(privateKey.getBytes(StandardCharsets.UTF_8), false);
        ByteBuffer buffer = sm2Engine.sm2Decode(source.getBytes(StandardCharsets.UTF_8), ByteBufferUtils.getBytes(keyBuffer), true);
        return ByteBufferUtils.getBytes(buffer);
    }

    /**
     * SM2私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    public static String sm2DecryptToString(String source, PrivateKey privateKey) {
        byte[] bytes = sm2Decrypt(source, privateKey);
        return bytesToString(bytes);
    }

    /**
     * SM2私钥解密 - 字节版
     *
     * @param source     原始数据字节数组
     * @param privateKey 私钥字符串
     * @return 解密结果
     */
    public static String sm2DecryptToString(String source, String privateKey) throws InvalidKeySpecException {
        byte[] bytes = sm2Decrypt(source, privateKey);
        return bytesToString(bytes);
    }

    private static String bytesToString(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return "";
        }
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
