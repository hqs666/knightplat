package com.knight.plat.utils.helper;

import com.knight.plat.api.crypto.CryptoProvider;
import com.knight.plat.api.crypto.RSAEngine;
import com.knight.plat.crypto.NativeRSAEngine;

public class NativeCryptoProvider extends JvmCryptoProvider {

    private final NativeRSAEngine nativeRSAEngine = new NativeRSAEngine();

    @Override
    public RSAEngine asRSAEngine() {
        return nativeRSAEngine;
    }

}
