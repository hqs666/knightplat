package com.knight.plat.utils;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class ByteBufferUtils {

    public static byte[] getBytes(ByteBuffer buffer) {
        if (buffer == null) {
            return new byte[0];
        }
        int lim = buffer.limit();
        int cap = buffer.capacity();
        byte[] bytes;
        if (buffer.hasArray() && lim == cap) {
            bytes = buffer.array();
        } else {
            bytes = new byte[lim];
            buffer.get(bytes);
        }
        return bytes;
    }

    public static String getString(ByteBuffer buffer) {
        byte[] bytes = getBytes(buffer);
        if (bytes.length == 0) {
            return "";
        }
        return new String(bytes, StandardCharsets.UTF_8);
    }

}
