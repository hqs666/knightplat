package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;

@Utils("image")
public final class ImageFileUtils {

    /**
     * 获取图片的格式类型
     *
     * @param data 图片二进制数据
     *
     * @return 格式
     */
    public static ImageType getImageType(byte[] data) {
        return ImageFile.getImageType(data);
    }

    /**
     * 获取图片的格式类型
     *
     * @param inputStream 图片二进制流
     *
     * @return 格式
     */
    public static ImageType getImageType(InputStream inputStream) {
        return ImageFile.getImageType(inputStream);
    }

    /**
     * 获取图片的格式类型
     *
     * @param file 图片文件
     *
     * @return 格式
     */
    public static ImageType getImageType(File file) {
        return ImageFile.getImageType(file);
    }

    /**
     * 获取图片的格式类型
     *
     * @param filePath 图片文件路径
     *
     * @return 格式
     */
    public static ImageType getImageType(Path filePath) {
        return ImageFile.getImageType(filePath);
    }

    /**
     * 获取base64编码后的图片的格式类型
     *
     * @param base64 base64编码后的图片数据
     *
     * @return 格式
     */
    public static ImageType getImageTypeFromBase64(String base64) {
        return ImageFile.getImageTypeFromBase64(base64);
    }
}
