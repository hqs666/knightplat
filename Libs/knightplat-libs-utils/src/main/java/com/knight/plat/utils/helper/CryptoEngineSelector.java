package com.knight.plat.utils.helper;

import com.knight.plat.api.crypto.AESEngine;
import com.knight.plat.api.crypto.Base64Engine;
import com.knight.plat.api.crypto.CryptoProvider;
import com.knight.plat.api.crypto.DESEngine;
import com.knight.plat.api.crypto.HMACEngine;
import com.knight.plat.api.crypto.HashEngine;
import com.knight.plat.api.crypto.RSAEngine;
import com.knight.plat.api.crypto.SM2Engine;
import com.knight.plat.api.crypto.SM4Engine;

public enum CryptoEngineSelector {

    JVM(new JvmCryptoProvider()),
    NATIVE(new NativeCryptoProvider());

    private final CryptoProvider provider;

    CryptoEngineSelector(CryptoProvider provider) {
        this.provider = provider;
    }

    public AESEngine getAESEngine() {
        return provider.asAESEngine();
    }

    public Base64Engine getBase64Engine() {
        return provider.asBase64Engine();
    }

    public DESEngine getDESEngine() {
        return provider.asDESEngine();
    }

    public HashEngine getHashEngine() {
        return provider.asHashEngine();
    }

    public HMACEngine getHMACEngine() {
        return provider.asHMACEngine();
    }

    public RSAEngine getRSAEngine() {
        return provider.asRSAEngine();
    }

    public SM2Engine getSM2Engine() {
        return provider.asSM2Engine();
    }

    public SM4Engine getSM4Engine() {
        return provider.asSM4Engine();
    }

}
