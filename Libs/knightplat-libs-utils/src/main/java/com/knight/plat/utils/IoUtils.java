package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

@Utils("io")
public final class IoUtils {
    private static int BUFFER_SIZE = 1024 * 8;
    private static final Logger log = LoggerFactory.getLogger(IoUtils.class);

    // just for test
    static void resetBufferSize(int size) {
        BUFFER_SIZE = size;
    }

    public static byte[] toByteArray(InputStream is) throws IOException {
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            byte[] b = new byte[BUFFER_SIZE];
            int n;
            while ((n = is.read(b)) != -1) {
                output.write(b, 0, n);
            }
            return output.toByteArray();
        }
    }

    public static String toString(InputStream is, Charset charset) throws IOException {
        return new String(toByteArray(is), charset);
    }

    public static String toUtf8String(InputStream is) throws IOException {
        return toString(is, StandardCharsets.UTF_8);
    }

    public static long copy(InputStream in, OutputStream out, long readLimit) throws IOException {
        if (in instanceof FileInputStream && out instanceof FileOutputStream) {
            final FileChannel fis = ((FileInputStream) in).getChannel();
            final FileChannel fos = ((FileOutputStream) out).getChannel();
            long size = fis.size();
            if (readLimit != -1 && size >= readLimit) {
                size = readLimit;
            }
            return fis.transferTo(0, size, fos);
        }

        byte[] buf = new byte[BUFFER_SIZE];
        long count = 0;
        int n;
        while ((n = in.read(buf)) > -1) {
            final long nextSize = count + n;
            final long fill;
            if (readLimit == -1 || nextSize < readLimit) {
                fill = n;
            } else {
                fill = readLimit - count;
                if (fill <= 0) {
                    break;
                }
            }
            out.write(buf, 0, (int)fill);
            count += fill;
        }
        return count;
    }

    public static void write(OutputStream out, CharSequence content) throws IOException {
        if (content == null || out == null || content.length() == 0) {
            return;
        }
        out.write(content.toString().getBytes(StandardCharsets.UTF_8));
    }

    public static long copy(InputStream in, OutputStream out) throws IOException {
        return copy(in, out, Long.MAX_VALUE);
    }

    public static void closeQuietly(AutoCloseable is) {
        if (is == null) {
            return;
        }
        try {
            is.close();
        } catch (Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Ignore failure when closing the Closeable", ex);
            }
        }
    }

    public static Flux<ByteBuffer> toFlux(InputStream in) {
        return Flux.push(fluxSink -> {
            byte[] buf = new byte[BUFFER_SIZE];
            int n;
            while (true) {
                try {
                    if (!((n = in.read(buf)) > -1)) {
                        break;
                    }
                } catch (IOException e) {
                    fluxSink.error(e);
                    return; // 跳出整个消费处理方法
                }
                fluxSink.next(ByteBuffer.allocate(n).put(buf, 0, n));
            }
            fluxSink.complete();
        });
    }
}
