package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.util.List;
import java.util.Map;

@Utils("sign")
public final class SignUtils {

    // RSA签名验证部分

    /**
     * 使用私钥生成签名
     *
     * @param content 需要签名的内容
     * @param privateKey 私钥
     * @return 签名
     */
    public static String sign(String content, String privateKey) {
        return Sign.sign(content, privateKey);
    }

    /**
     * 对字符串map对象进行签名
     *
     * @param map 需要签名的map
     * @param privateKey 私钥
     * @return 签名
     */
    public static String signStringMap(Map<String, String> map, String privateKey) {
        return Sign.signStringMap(map, privateKey);
    }

    /**
     * 对对象map进行签名
     *
     * @param map 需要签名的map
     * @param privateKey privateKey 私钥
     * @return 签名
     */
    public static String signObjectMap(Map<String, Object> map, String privateKey) {
        return Sign.signObjectMap(map, privateKey);
    }

    /**
     * 对list进行签名
     *
     * @param timestamp 时间戳
     * @param list 需签名的list
     * @param privateKey 私钥
     * @return 签名
     */
    public static String signList(long timestamp, List<?> list, String privateKey) {
        return Sign.signList(timestamp, list, privateKey);
    }

    /**
     * 使用公钥验证签名
     *
     * @param content 签名的内容
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean verify(String content, String sign, String publicKey) {
        return Sign.verify(content, sign, publicKey);
    }

    /**
     * 对字符串map进行签名验证
     *
     * @param map 签名的map
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean verifyStringMap(Map<String, String> map, String sign, String publicKey) {
        return Sign.verifyStringMap(map, sign, publicKey);
    }

    /**
     * 对字符串map进行签名验证（签名包含在map中）
     *
     * @param map 签名的map
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean verifyStringMap(Map<String, String> map, String publicKey) {
        return Sign.verifyStringMap(map, publicKey);
    }

    /**
     * 对对象map进行签名验证（签名包含在map中）
     *
     * @param map 签名的map
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean verifyObjectMap(Map<String, Object> map, String publicKey) {
        return Sign.verifyObjectMap(map, publicKey);
    }

    /**
     * 对对象map进行签名验证
     *
     * @param map 签名的map
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean verifyObjectMap(Map<String, Object> map, String sign, String publicKey) {
        return Sign.verifyObjectMap(map, sign, publicKey);
    }

    /**
     * 对list进行签名验证
     *
     * @param timestamp 时间戳
     * @param list 签名的list
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean verifyList(long timestamp, List<?> list, String sign, String publicKey) {
        return Sign.verifyList(timestamp, list, sign, publicKey);
    }

    // SM2签名验证部分

    /**
     * 使用私钥生成签名（SM2算法）
     *
     * @param content 需要签名的内容
     * @param privateKey 私钥
     * @return 签名
     */
    public static String sm2Sign(String content, String privateKey) {
        return Sign.sm2Sign(content, privateKey);
    }

    /**
     * 对字符串map对象进行签名（SM2算法）
     *
     * @param map 需要签名的map
     * @param privateKey 私钥
     * @return 签名
     */
    public static String sm2SignStringMap(Map<String, String> map, String privateKey) {
        return Sign.sm2SignStringMap(map, privateKey);
    }

    /**
     * 对对象map进行签名（SM2算法）
     *
     * @param map 需要签名的map
     * @param privateKey privateKey 私钥
     * @return 签名
     */
    public static String sm2SignObjectMap(Map<String, Object> map, String privateKey) {
        return Sign.sm2SignObjectMap(map, privateKey);
    }

    /**
     * 对list进行签名（SM2算法）
     *
     * @param timestamp 时间戳
     * @param list 需签名的list
     * @param privateKey 私钥
     * @return 签名
     */
    public static String sm2SignList(long timestamp, List<?> list, String privateKey) {
        return Sign.sm2SignList(timestamp, list, privateKey);
    }

    /**
     * 使用公钥验证签名（SM2算法）
     *
     * @param content 签名的内容
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean sm2Verify(String content, String sign, String publicKey) {
        return Sign.sm2Verify(content, sign, publicKey);
    }

    /**
     * 对字符串map进行签名验证（SM2算法）
     *
     * @param map 签名的map
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean sm2VerifyStringMap(Map<String, String> map, String sign, String publicKey) {
        return Sign.sm2VerifyStringMap(map, sign, publicKey);
    }

    /**
     * 对字符串map进行签名验证（签名包含在map中，SM2算法）
     *
     * @param map 签名的map
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean sm2VerifyStringMap(Map<String, String> map, String publicKey) {
        return Sign.sm2VerifyStringMap(map, publicKey);
    }

    /**
     * 对对象map进行签名验证（签名包含在map中，SM2算法）
     *
     * @param map 签名的map
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean sm2VerifyObjectMap(Map<String, Object> map, String publicKey) {
        return Sign.sm2VerifyObjectMap(map, publicKey);
    }

    /**
     * 对对象map进行签名验证（SM2算法）
     *
     * @param map 签名的map
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean sm2VerifyObjectMap(Map<String, Object> map, String sign, String publicKey) {
        return Sign.sm2VerifyObjectMap(map, sign, publicKey);
    }

    /**
     * 对list进行签名验证（SM2算法）
     *
     * @param timestamp 时间戳
     * @param list 签名的list
     * @param sign 签名
     * @param publicKey 公钥
     * @return 签名是否有效
     */
    public static boolean sm2VerifyList(long timestamp, List<?> list, String sign, String publicKey) {
        return Sign.sm2VerifyList(timestamp, list, sign, publicKey);
    }

    /**
     * 生成密钥对(RSA算法)
     *
     * @return 密钥对
     *
     * @deprecated 使用 {@link SignUtils#generateRSAKeyPair()} SignUtils.generateRSAKeyPair()} 代替
     */
    @Deprecated
    @Utils.Ignore
    public static KeyResolver generateKeyPair() {
        return CryptoCommonKt.generateRSAKeyPair();
    }

    /**
     * 生成RSA密钥对
     *
     * @return 密钥对
     */
    public static KeyResolver generateRSAKeyPair() {
        return CryptoCommonKt.generateRSAKeyPair();
    }

    /**
     * 生成SM2密钥对
     *
     * @return 密钥对
     */
    public static KeyResolver generateSM2KeyPair() {
        return CryptoCommonKt.generateSM2KeyPair();
    }
}
