package com.knight.plat.utils;

public enum ImageType {

    PNG("png"), GIF("gif"), JPEG("jpeg"), BMP("bmp"), WEBP("webp"), NONE("none");

    ImageType(String ext) {
        this.ext = ext;
    }

    private final String ext;

    public String getExt() {
        return ext;
    }
}
