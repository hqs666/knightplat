/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils.helper;

import com.knight.plat.api.log.Level;
import com.knight.plat.api.log.LogContent;
import com.knight.plat.api.log.LogProvider;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 2016/11/17. Simple implement of LogProvider, just print them var system log component.
 *
 * @see com.knight.plat.api.log.LogProvider
 */
public class SimpleLogProvider implements LogProvider {

    private final Logger log = LoggerFactory.getLogger(LogProvider.class);

    @Override
    @SuppressWarnings("deprecated")
    public void log(String ip, Date createTime, LogContent content) {
        Level level = content.getLevel();
        switch (level) {
            case FATAL:
            case ERROR:
                if (log.isErrorEnabled()) {
                    log.error(content.getContent() + " from " + ip + " at " + createTime, content.getException());
                }
                break;
            case WARN:
                if (log.isWarnEnabled()) {
                    log.warn(content.getContent() + " from " + ip + " at " + createTime, content.getException());
                }
                break;
            case INFO:
                if (log.isInfoEnabled()) {
                    log.info(content.getContent() + " from " + ip + " at " + createTime, content.getException());
                }
                break;
            case DEBUG:
                if (log.isDebugEnabled()) {
                    log.debug(content.getContent() + " from " + ip + " at " + createTime, content.getException());
                }
                break;
            case TRACE:
                if (log.isTraceEnabled()) {
                    log.trace(content.getContent() + " from " + ip + " at " + createTime, content.getException());
                }
                break;
        }
    }

    @Override
    public void log(String platform, String userAgent, String ip, Date createTime, LogContent content) {
        log(ip, createTime, content);
    }
}
