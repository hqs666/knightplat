package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.utils.enums.Patterns;

@Utils("regular")
public final class RegularUtils {

    /**
     * 验证字符串是否为type类型的格式
     *
     * @param string  输入号码
     * @param patterns 枚举类选择验证字符串类型
     * @return true为是，false为不是
     */
    public static boolean match(String string, Patterns patterns) {
        return patterns.matches(string);
    }


    /**
     * 验证字符串是否包含type类型的格式
     *
     * @param string  输入号码
     * @param patterns 枚举类选择验证字符串类型
     * @return true为是，false为不是
     */
    public static boolean find(String string, Patterns patterns) {
        return patterns.find(string);
    }

    /**
     * 替换字符串
     *
     * @param src 输入字符串
     * @param patterns 匹配模式
     * @param replacement 替换字符串
     * @return 替换结果
     */
    public static String replace(String src, Patterns patterns, String replacement) {
        return patterns.replaces(src, replacement);
    }

    /**
     * 判断字段是否为空 符合返回ture
     *
     * @param str 输入字符串
     * @return boolean
     */
    public static  boolean strIsNull(String str) {
        return null == str || str.trim().length() <= 0;
    }

    /**
     * 判断字段是非空 符合返回ture
     *
     * @param str 输入字符串
     * @return boolean
     */
    public static boolean strNotNull(String str) {
        return !strIsNull(str);
    }

    /**
     * 字符串null转空
     *
     * @param str 输入字符串
     * @return boolean
     */
    public static String nullToStr(String str) {
        return strIsNull(str) ? "" : str;
    }

    /**
     * 字符串null赋值默认值
     *
     * @param str        输入字符串
     * @param defaultStr 默认值
     * @return String
     */
    public static String nullToStr(String str, String defaultStr) {
        return strIsNull(str) ? defaultStr : str;
    }

    /**
     * 判断字段是否超长
     * 字串为空返回false, 超过长度{long}返回ture 反之返回false
     *
     * @param str    输入字符串
     * @param length 长度
     * @return boolean
     */
    public static boolean isLongOut(String str, int length) {
        return !strIsNull(str) && str.trim().length() > length;
    }

}
