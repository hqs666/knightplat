package com.knight.plat.utils.helper;

import com.knight.plat.api.crypto.CryptoProvider;
import com.knight.plat.utils.CryptoCommonKt;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

public class JvmCryptoProvider implements CryptoProvider {

    static {
        Security.setProperty("crypto.policy", "unlimited");
    }

    private static final JvmEncryptManager manager = JvmEncryptManager.INSTANCE;

    @Override
    public ByteBuffer aesEncode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException {
        byte[] bytes = manager.aesEncrypt$knightplat_libs_utils(data, new String(key, StandardCharsets.UTF_8));
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer aesDecode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException {
        if (isBase64) {
            data = CryptoCommonKt.base64Engine(false).decode(data);
        }
        byte[] result = manager.aesDecrypt$knightplat_libs_utils(data, new String(key, StandardCharsets.UTF_8));
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer desEncode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException {
        byte[] bytes = manager.desEncrypt$knightplat_libs_utils(data, new String(key, StandardCharsets.UTF_8));
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer desDecode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException {
        if (isBase64) {
            data = CryptoCommonKt.base64Engine(false).decode(data);
        }
        byte[] result = manager.desDecrypt$knightplat_libs_utils(data, new String(key, StandardCharsets.UTF_8));
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer md5(byte[] data) {
        byte[] result = manager.md5Encrypt$knightplat_libs_utils(data);
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer sha1(byte[] data) {
        byte[] result = manager.sha1Encrypt$knightplat_libs_utils(data);
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer sha256(byte[] data) {
        byte[] result = manager.sha256Encrypt$knightplat_libs_utils(data);
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer sha512(byte[] data) {
        byte[] result = manager.sha512Encrypt$knightplat_libs_utils(data);
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer sm3(byte[] data) {
        byte[] result = manager.sm3Encrypt$knightplat_libs_utils(data);
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer hmacMd5(byte[] data, byte[] key, boolean isBase64) {
        byte[] bytes = manager.hmacEncrypt$knightplat_libs_utils("HmacMD5", data, key);
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer hmacSha1(byte[] data, byte[] key, boolean isBase64) {
        byte[] bytes = manager.hmacEncrypt$knightplat_libs_utils("HmacSHA1", data, key);
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer hmacSha256(byte[] data, byte[] key, boolean isBase64) {
        byte[] bytes = manager.hmacEncrypt$knightplat_libs_utils("HmacSHA256", data, key);
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer rsaEncode(byte[] data, byte[] publicKey, boolean isBase64) throws InvalidKeySpecException {
        byte[] bytes = manager.rsaEncrypt$knightplat_libs_utils(data, CryptoCommonKt.readRSAPublicKey(publicKey));
        if (bytes == null) {
            return ByteBuffer.allocate(0);
        }
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer rsaDecode(byte[] data, byte[] privateKey, boolean isBase64) throws InvalidKeySpecException {
        if (isBase64) {
            data = CryptoCommonKt.base64Engine(false).decode(data);
        }
        byte[] bytes = manager.rsaDecrypt$knightplat_libs_utils(data, CryptoCommonKt.readRSAPrivateKey(privateKey));
        if (bytes == null) {
            return ByteBuffer.allocate(0);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer sm2Encode(byte[] data, byte[] publicKey, boolean isBase64) throws InvalidKeySpecException {
        byte[] bytes = manager.sm2Encrypt$knightplat_libs_utils(data, CryptoCommonKt.readSM2PublicKey(publicKey));
        if (bytes == null) {
            return ByteBuffer.allocate(0);
        }
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer sm2Decode(byte[] data, byte[] privateKey, boolean isBase64) throws InvalidKeySpecException {
        if (isBase64) {
            data = CryptoCommonKt.base64Engine(false).decode(data);
        }
        byte[] bytes = manager.sm2Decrypt$knightplat_libs_utils(data, CryptoCommonKt.readSM2PrivateKey(privateKey));
        if (bytes == null) {
            return ByteBuffer.allocate(0);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer sm4Encode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException {
        byte[] bytes = manager.sm4Encrypt$knightplat_libs_utils(data, new String(key, StandardCharsets.UTF_8));
        if (isBase64) {
            bytes = CryptoCommonKt.base64Engine(false).encode(bytes);
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer sm4Decode(byte[] data, byte[] key, boolean isBase64) throws InvalidKeySpecException {
        if (isBase64) {
            data = CryptoCommonKt.base64Engine(false).decode(data);
        }
        byte[] result = manager.sm4Decrypt$knightplat_libs_utils(data, new String(key, StandardCharsets.UTF_8));
        return ByteBuffer.wrap(result);
    }

    @Override
    public ByteBuffer base64Encode(byte[] data, boolean isUrl) {
        if (data == null || data.length == 0) {
            return ByteBuffer.allocate(0);
        }
        byte[] encode = CryptoCommonKt.base64Engine(isUrl).encode(data);
        return ByteBuffer.wrap(encode);
    }

    @Override
    public ByteBuffer base64Decode(byte[] data, boolean isUrl) {
        if (data == null || data.length == 0) {
            return ByteBuffer.allocate(0);
        }
        byte[] decode = CryptoCommonKt.base64Engine(isUrl).decode(data);
        return ByteBuffer.wrap(decode);
    }
}
