package com.knight.plat.utils;

import com.knight.plat.utils.enums.Patterns;
import com.knight.plat.api.annotations.Utils;

@Utils("xss")
public final class XssUtils {

    private static final String REPLACEMENT = "*";

    public static String stripXss(final String src) {
        if (src == null || src.isEmpty()) {
            return src;
        }

        String result = src;
        result = Patterns.XSS_SCRIPT_TAG.replaces(result, REPLACEMENT);
        result = Patterns.XSS_SRC_TAG.replaces(result, REPLACEMENT);
        result = Patterns.XSS_SCRIPT_END.replaces(result, REPLACEMENT);
        result = Patterns.XSS_SCRIPT_START.replaces(result, REPLACEMENT);
        result = Patterns.XSS_EVAL.replaces(result, REPLACEMENT);
        result = Patterns.XSS_EXPRESSION.replaces(result, REPLACEMENT);
        result = Patterns.XSS_JAVASCRIPT_WORD.replaces(result, REPLACEMENT);
        result = Patterns.XSS_VBSCRIPT_WORD.replaces(result, REPLACEMENT);
        result = Patterns.XSS_ON_LOAD_TAG.replaces(result, REPLACEMENT);
        result = Patterns.XSS_RETURN_BACK.replaces(result, REPLACEMENT);
        result = Patterns.XSS_EVENT.replaces(result, REPLACEMENT);

        return stripTags(result);
    }

    public static String stripTags(String content) {
        String result = content;
        //TODO: 优化方案
//        result = result.replaceAll("<","&lt;");
//        result = result.replaceAll(">","&gt;");

        return result;
    }

    public static boolean hasXssRisk(String content) {
        if (content == null || content.isEmpty()) {
            return false;
        }
        content = content.replaceAll(" ", "");
        return Patterns.XSS_SCRIPT_TAG.find(content) ||
                Patterns.XSS_SRC_TAG.find(content) ||
                Patterns.XSS_SCRIPT_END.find(content) ||
                Patterns.XSS_SCRIPT_START.find(content) ||
                Patterns.XSS_EVAL.find(content) ||
                Patterns.XSS_EXPRESSION.find(content) ||
                Patterns.XSS_JAVASCRIPT_WORD.find(content) ||
                Patterns.XSS_VBSCRIPT_WORD.find(content) ||
                Patterns.XSS_ON_LOAD_TAG.find(content) ||
                Patterns.XSS_EVENT.find(content);
    }
}
