package com.knight.plat.utils.enums;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Patterns {

    /**
     * 手机号 ^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$
     */
    PHONE("^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$", 0),

    /**
     * 身份证号 ^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$
     */
    ID_CARD("^[1-9]\\d{5}(18|19|20|(3\\d))\\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$", 0),

    /**
     * 邮箱 ^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$
     */
    EMAIL("^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$", 0),

    /**
     * 域名 ^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$
     */
    DOMAIN("[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\\.?", 0),

    /**
     * 邮编 ^[1-9]\d{5}$
     */
    ZIP_CODE("^[1-9]\\d{5}$", 0),

    /**
     * IP地址 ^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$
     */
    IP("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$", 0),

    /**
     * Integer正则表达式 ^-?(([1-9]\d*$)|0)
     */
    INTEGER("^-?(([1-9]\\d*$)|0)", 0),

    /**
     * 正整数正则表达式 &gt;=0 ^[1-9]\d*|0$
     */
    INTEGER_NEGATIVE("^[1-9]\\d*|0$", 0),

    /**
     * 负整数正则表达式 &lt;=0 ^-[1-9]\d*|0$
     */
    INTEGER_POSITIVE("^-[1-9]\\d*|0$", 0),

    /**
     * Double正则表达式 ^-?([1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0)$
     */
    DOUBLE("^-?([1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*|0?\\.0+|0)$", 0),

    /**
     * 正Double正则表达式 &gt;=0 ^[1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0$
     */
    DOUBLE_NEGATIVE("^[1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*|0?\\.0+|0$", 0),

    /**
     * 负Double正则表达式 &lt;= 0 ^(-([1-9]\d*\.\d*|0\.\d*[1-9]\d*))|0?\.0+|0$
     */
    DOUBLE_POSITIVE("^(-([1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*))|0?\\.0+|0$", 0),

    /**
     * 年龄正则表达式 ^(?:[1-9][0-9]?|1[01][0-9]|120)$ 匹配0-120岁
     */
    AGE("^(?:[1-9][0-9]?|1[01][0-9]|120)$", 0),

    /**
     * 匹配由数字、26个英文字母或者下划线组成的字符串 ^\w+$
     */
    STR_ENG_NUM_("^\\w+$", 0),

    /**
     * 匹配由数字和26个英文字母组成的字符串 ^[A-Za-z0-9]+$
     */
    STR_ENG_NUM("^[A-Za-z0-9]+", 0),

    /**
     * 匹配由26个英文字母组成的字符串 ^[A-Za-z]+$
     */
    STR_ENG("^[A-Za-z]+$", 0),

    /**
     * 过滤特殊字符串正则
     * regEx="[`~!@#$%^&amp;*()+=|{}':;',\\[\\].&lt;&gt;/?~！@#￥%……&amp;*（）——+|{}【】‘；：”“'。，、？]"),
     */
    STR_SPECIAL("[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“'。，、？]+", 0),

    /***
     * 日期正则 支持：
     * YYYY-MM-DD
     * YYYY/MM/DD
     * YYYY_MM_DD
     * YYYYMMDD
     * YYYY.MM.DD的形式
     */
    DATE_ALL("((^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._]?)(10|12|0?[13578])([-\\/\\._]?)(3[01]|[12][0-9]|0?[1-9])$)" +
            "|(^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._]?)(11|0?[469])([-\\/\\._]?)(30|[12][0-9]|0?[1-9])$)" +
            "|(^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._]?)(0?2)([-\\/\\._]?)(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\\/\\._]?)(0?2)([-\\/\\._]?)(29)$)|(^([3579][26]00)" +
            "([-\\/\\._]?)(0?2)([-\\/\\._]?)(29)$)" +
            "|(^([1][89][0][48])([-\\/\\._]?)(0?2)([-\\/\\._]?)(29)$)|(^([2-9][0-9][0][48])([-\\/\\._]?)" +
            "(0?2)([-\\/\\._]?)(29)$)" +
            "|(^([1][89][2468][048])([-\\/\\._]?)(0?2)([-\\/\\._]?)(29)$)|(^([2-9][0-9][2468][048])([-\\/\\._]?)(0?2)" +
            "([-\\/\\._]?)(29)$)|(^([1][89][13579][26])([-\\/\\._]?)(0?2)([-\\/\\._]?)(29)$)|" +
            "(^([2-9][0-9][13579][26])([-\\/\\._]?)(0?2)([-\\/\\._]?)(29)$))", 0),


    /**
     * URL正则表达式
     * 匹配 http www ftp
     */
    URL("^(http|www|ftp|)?(://)?(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*((:\\d+)?)(/(\\w+(-\\w+)*))*(\\.?(\\w)*)(\\?)?" +
            "(((\\w*%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*(\\w*%)*(\\w*\\?)*" +
            "(\\w*:)*(\\w*\\+)*(\\w*\\.)*" +
            "(\\w*&)*(\\w*-)*(\\w*=)*)*(\\w*)*)$", 0),


    /**
     * 匹配数字组成的字符串 ^[0-9]+$
     */
    STR_NUM("^[0-9]+$", 0),

    /**
     * 强密码(必须包含大小写字母和数字的组合，不能使用特殊字符，长度在 8-10 之间)：^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,10}$
     */
    STRONG_PASSWORD_WITHOUT_SPECIAL("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,10}$", 0),

    /**
     * 强密码(必须包含大小写字母和数字的组合，可以使用特殊字符，长度在8-10之间)：^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,10}$
     */
    STRONG_PASSWORD_WITH_SPECIAL("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,10}$", 0),

    XSS_SCRIPT_TAG("<[\r\n| | ]*script[\r\n| | ]*>(.*?)</[\r\n| | ]*script[\r\n| | ]*>", Pattern.CASE_INSENSITIVE),

    XSS_SRC_TAG("src[\r\n| | ]*=[\r\n| | ]*[\\\"|\\\'](.*?)[\\\"|\\\']", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

    XSS_SCRIPT_END("</[\r\n| | ]*script[\r\n| | ]*>", Pattern.CASE_INSENSITIVE),

    XSS_SCRIPT_START("<[\r\n| | ]*script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

    XSS_EVAL("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

    XSS_EXPRESSION("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

    XSS_JAVASCRIPT_WORD("javascript[\r\n| | ]*:[\r\n| | ]*", Pattern.CASE_INSENSITIVE),

    XSS_VBSCRIPT_WORD("vbscript[\r\n| | ]*:[\r\n| | ]*", Pattern.CASE_INSENSITIVE),

    XSS_ON_LOAD_TAG("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

    XSS_RETURN_BACK("\"\\\\s*|\\t|\\r|\\n\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),

    XSS_EVENT("(?i)<.*?\\s+on.*?>.*?</.*?>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    private final Pattern compiled;

    Patterns(String pattern, int flag) {
        this.compiled = Pattern.compile(pattern, flag);
    }

    public Pattern getPattern() {
        return compiled;
    }

    public boolean matches(String str) {
        if (null == str || str.trim().isEmpty()) {
            return false;
        }
        final Matcher m = this.getPattern().matcher(str);
        return m.matches();
    }

    public boolean find(String str) {
        if (null == str || str.trim().isEmpty()) {
            return false;
        }
        final Matcher m = this.getPattern().matcher(str);
        return m.find();
    }

    public String replaces(String src, String replacement) {
        if (null == src || src.trim().isEmpty()) {
            return src;
        }
        final Matcher m = this.getPattern().matcher(src);
        return m.replaceAll(replacement);
    }
}
