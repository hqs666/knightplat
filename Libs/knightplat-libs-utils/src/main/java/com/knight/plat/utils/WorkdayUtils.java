/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.api.utils.HolidayManager;
import java.util.Calendar;
import java.util.Date;


@Utils("workday")
public final class WorkdayUtils {

    private static HolidayManager holidayManager;

    private WorkdayUtils() {
    }

    static void init(HolidayManager holidayManager) {
        WorkdayUtils.holidayManager = holidayManager;
    }

    private static HolidayManager getHolidayManager() {
        if (null == holidayManager) {
            throw new IllegalStateException("WorkdayUtils is not bean initiated.");
        }
        return holidayManager;
    }

    public static Calendar addWorkDays(Calendar start, long days) {
        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(start.getTimeInMillis());
        for (int i = 0; i < days; i++) {
            end.add(Calendar.DAY_OF_MONTH, 1);
            if (getHolidayManager().isHoliday(end)) {
                i--;
            }
        }
        return end;
    }

    public static long workDaysBetween(Calendar start, Calendar end) {
        int isSorted = start.compareTo(end);
        long startTime = start.getTimeInMillis();
        long endTime = end.getTimeInMillis();
        Calendar s, e;
        boolean minus;
        if (isSorted == 0) {
            return 0;
        } else if (isSorted < 0) {
            s = start;
            e = end;
            minus = false;
        } else {
            s = end;
            e = start;
            minus = true;
        }

        long gap = 0;
        // 循环判断中间日期是否是假期
        for (; !isSameDay(s, e);
                s.add(Calendar.DAY_OF_MONTH, 1),
                        gap += getHolidayManager().isHoliday(s) ? 0 : 1) {
            // do nothing.
        }

        start.setTimeInMillis(startTime);
        end.setTimeInMillis(endTime);
        return minus ? -gap : gap;
    }

    public static boolean isHoliday(Calendar calendar) {
        return getHolidayManager().isHoliday(calendar);
    }

    public static boolean isHoliday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return getHolidayManager().isHoliday(cal);
    }

    private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) &&
                (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)) &&
                (cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH));
    }
}
