/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.nio.ByteBuffer;


@Utils(value = "hex", lib = true)
public final class HexUtils {

    private HexUtils() {
    }

    /**
     * Convert byte[] to hex string.
     *
     * @param src byte[] data
     * @return hex string
     */
    public static String bytesToHexString(byte[] src) {
        if (src == null || src.length == 0) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : src) {
            String hex = Integer.toHexString(b & 0xFF);
            if (hex.length() == 1) {
                stringBuilder.append('0');
            }
            stringBuilder.append(hex);
        }
        return stringBuilder.toString();
    }

    /**
     * Convert ByteBuffer to hex string.
     *
     * @param buffer ByteBuffer data
     * @return hex string
     */
    public static String byteBufferToHexString(ByteBuffer buffer) {
        if (buffer == null || !buffer.hasRemaining()) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        while (buffer.hasRemaining()) {
            byte b = buffer.get();
            String hex = Integer.toHexString(b & 0xFF);
            if (hex.length() == 1) {
                stringBuilder.append('0');
            }
            stringBuilder.append(hex);
        }
        return stringBuilder.toString();
    }

    /**
     * Convert hex string to byte[]
     *
     * @param hexString the hex string
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || "".equals(hexString)) {
            return new byte[0];
        }
        int length = hexString.length() / 2;
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            char c1 = Character.toUpperCase(hexString.charAt(pos));
            char c2 = Character.toUpperCase(hexString.charAt(pos + 1));
            d[i] = (byte) (charToByte(c1) << 4 | (charToByte(c2) & 0xff));
        }
        return d;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }
}
