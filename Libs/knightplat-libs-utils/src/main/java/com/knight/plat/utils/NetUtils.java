package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;

@Utils("netInfo")
public class NetUtils {

    private static final Pattern IP_PATTERN = Pattern.compile("((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)");

    /**
     * 获取客户端IP地址

     * @param request 客户端请求对象
     * *
     * @return 客户端IP地址
     */
    public static String getRemoteIp(HttpServletRequest request) {
        return Net.getRemoteIp(request);
    }

    public static String getClientIp(HttpServletRequest request) {
        final String remoteIp = getRemoteIp(request);
        return analyseClientIp(remoteIp);
    }

    /**
     * 获取user-agent值

     * @param request 请求对象
     * *
     * @return user-agent值
     */
    public static String getUserAgent(HttpServletRequest request) {
        return Net.getUserAgent(request);
    }

    /**
     * 获取平台信息

     * @param request 请求对象
     * *
     * @return 平台代码
     */
    public static String getPlatform(HttpServletRequest request) {
        return Net.getPlatform(request);
    }

    static String analyseClientIp(String remoteIp) {
        Matcher matcher = IP_PATTERN.matcher(remoteIp);
        if (matcher.find()) {
            return matcher.group();
        }
        return "";
    }
}
