package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.api.crypto.Base64Engine;
import com.knight.plat.api.crypto.HMACEngine;
import com.knight.plat.api.crypto.HashEngine;
import com.knight.plat.utils.helper.CryptoEngineSelector;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.zip.CRC32;

@Utils("digest")
public final class DigestUtils {

    private static final HashEngine hashEngine = CryptoEngineSelector.JVM.getHashEngine();
    private static final HMACEngine hmacEngine = CryptoEngineSelector.JVM.getHMACEngine();

    /**
     * md5编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    public static String md5(String source) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        ByteBuffer buffer = hashEngine.md5(source.getBytes(StandardCharsets.UTF_8));
        return HexUtils.byteBufferToHexString(buffer);
    }


    /**
     * sha1编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    public static String sha1(String source) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        ByteBuffer buffer = hashEngine.sha1(source.getBytes(StandardCharsets.UTF_8));
        return HexUtils.byteBufferToHexString(buffer);
    }

    /**
     * sha256编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    public static String sha256(String source) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        ByteBuffer buffer = hashEngine.sha256(source.getBytes(StandardCharsets.UTF_8));
        return HexUtils.byteBufferToHexString(buffer);
    }

    /**
     * sha512编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    public static String sha512(String source) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        ByteBuffer buffer = hashEngine.sha512(source.getBytes(StandardCharsets.UTF_8));
        return HexUtils.byteBufferToHexString(buffer);
    }

    /**
     * sm3编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    public static String sm3(String source) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        ByteBuffer buffer = hashEngine.sm3(source.getBytes(StandardCharsets.UTF_8));
        return HexUtils.byteBufferToHexString(buffer);
    }

    /**
     * HMACSHA256编码
     *
     * @param source 原始数据
     * @param secret 秘钥
     * @param isBase64 是否返回base64编码后数据
     * @return 编码后数据
     */
    public static String hmacSha256(String source, String secret, boolean isBase64) {
        if (source == null || source.isEmpty()) {
            return "";
        }
        if (secret == null || secret.isEmpty()) {
            throw new IllegalArgumentException("secret is null");
        }
        ByteBuffer buffer = hmacEngine.hmacSha256(source.getBytes(StandardCharsets.UTF_8),
                secret.getBytes(StandardCharsets.UTF_8), isBase64);
        if (isBase64) {
            return ByteBufferUtils.getString(buffer);
        }
        return HexUtils.byteBufferToHexString(buffer);
    }

    public static long crc32(String source) {
        CRC32 crc32 = new CRC32();
        crc32.update(source.getBytes(StandardCharsets.UTF_8));
        return crc32.getValue();
    }
}
