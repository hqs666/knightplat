package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.awt.image.BufferedImage;

@Utils("barCode")
public final class BarCodeUtils {
    public static BufferedImage renderEAN8(String content, int width, int height) {
        return BarCode.renderEAN8(content, width, height);
    }

    public static BufferedImage renderEAN13(String content, int width, int height) {
        return BarCode.renderEAN13(content, width, height);
    }

    public static BufferedImage renderCODABAR(String content, int width, int height) {
        return BarCode.renderCODABAR(content, width, height);
    }

    public static BufferedImage renderCODE39(String content, int width, int height) {
        return BarCode.renderCODE39(content, width, height);
    }

    public static BufferedImage renderCODE93(String content, int width, int height) {
        return BarCode.renderCODE93(content, width, height);
    }

    public static BufferedImage renderCODE128(String content, int width, int height) {
        return BarCode.renderCODE128(content, width, height);
    }

    public static BufferedImage renderITF(String content, int width, int height) {
        return BarCode.renderITF(content, width, height);
    }

    public static BufferedImage renderUPCA(String content, int width, int height) {
        return BarCode.renderUPCA(content, width, height);
    }

    public static BufferedImage renderUPCE(String content, int width, int height) {
        return BarCode.renderUPCE(content, width, height);
    }
}
