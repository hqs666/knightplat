package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;

@Utils("key")
public final class KeyGenUtils {

    public static String newUuid(String separator) {
        return KeyGen.newUuid(separator);
    }

    public static String newUuid() {
        return KeyGen.newUuid();
    }

    public static long random(long from, long to) {
        return KeyGen.random(from, to);
    }

    public static String randomString(long from, long to, int length) {
        return KeyGen.randomString(from, to, length);
    }

    public static int secureRandom(int bound) {
        return KeyGen.secureRandom(bound);
    }

    public static String nonce(int len) {
        return KeyGen.nonce(len);
    }

    public static long snowFlake() {
        return KeyGen.snowFlake();
    }

    /**
     * 生成RSA密钥对
     *
     * @return 密钥对
     */
    public static KeyResolver generateRSAKeyPair() {
        return CryptoCommonKt.generateRSAKeyPair();
    }

    /**
     * 生成RSA密钥对 - 2048位
     *
     * @return 密钥对
     */
    public static KeyResolver generateStrongRSAKeyPair() {
        return CryptoCommonKt.generateStrongRSAKeyPair();
    }

    /**
     * 生成RSA密钥对 - 4096位
     *
     * @return 密钥对
     */
    public static KeyResolver generateBestRSAKeyPair() {
        return CryptoCommonKt.generateBestRSAKeyPair();
    }

    /**
     * 生成SM2密钥对
     *
     * @return 密钥对
     */
    public static KeyResolver generateSM2KeyPair() {
        return CryptoCommonKt.generateSM2KeyPair();
    }
}
