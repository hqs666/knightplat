package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;

@Utils("threadLocal")
public final class ThreadLocalUtils {
    public static <T> T getValue(Object key, Class<T> clazz) {
        return ThreadLocal.getValue(key, clazz);
    }

    public static void setValue(Object key, Object value) {
        ThreadLocal.setValue(key, value);
    }
    public static void clear(Object key) {
        ThreadLocal.clear(key);
    }
}
