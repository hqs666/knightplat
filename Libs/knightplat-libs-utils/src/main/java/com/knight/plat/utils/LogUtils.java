/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.api.log.LogContent;
import com.knight.plat.api.log.LogProvider;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;


@Utils("log")
public final class LogUtils {

    private static LogProvider logProvider;

    private LogUtils() {
    }

    static void init(LogProvider logProvider) {
        LogUtils.logProvider = logProvider;
    }

    private static LogProvider getProvider() {
        if (null == logProvider) {
            throw new IllegalStateException("LogUtils is not bean initiated.");
        }
        return logProvider;
    }

    public static void collect(LogContent content, HttpServletRequest request) {
        getProvider().log(
                NetUtils.getPlatform(request),
                NetUtils.getUserAgent(request),
                NetUtils.getRemoteIp(request),
                new Date(), content);
    }
}
