package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.awt.image.BufferedImage;

@Utils("qr")
public final class QRCodeUtils {
    public static BufferedImage renderQRImage(String content, int width, int height) {
        return QRCode.renderQRImage(content, width, height);
    }
}
