/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils.helper;

import com.google.common.collect.Maps;
import com.knight.plat.api.utils.SequenceManager;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created on 2016/11/17. Simple implement of SequenceManager, based on ConcurrentMap and not safe with
 * cluster.
 *
 * @see com.knight.plat.api.utils.SequenceManager
 */
public class SimpleSequenceManager implements SequenceManager {

    private final Map<String, AtomicLong> container = Maps.newConcurrentMap();

    @Override
    public boolean contains(String key) {
        return container.containsKey(key);
    }

    @Override
    public long increment(String key) {
        return container.get(key).incrementAndGet();
    }

    @Override
    public void save(String key, long value) {
        container.put(key, new AtomicLong(value));
    }
}
