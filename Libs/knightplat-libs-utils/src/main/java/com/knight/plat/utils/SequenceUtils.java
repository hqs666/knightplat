/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.api.utils.SequenceManager;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Utils("sequence")
public final class SequenceUtils {

    private static final Logger log = LoggerFactory.getLogger(SequenceUtils.class);
    private static SequenceManager sequenceManager;
    // record last tick.
    private static final AtomicLong lastTick = new AtomicLong(-1L);

    private SequenceUtils() { }

    static void init(SequenceManager sequenceManager) {
        SequenceUtils.sequenceManager = sequenceManager;
        log.info("load SequenceManager implement: " + sequenceManager.getClass().getName());
    }

    private static SequenceManager getSequenceManager() {
        if (null == sequenceManager) {
            throw new IllegalStateException("SequenceUtils is not bean initiated.");
        }
        return sequenceManager;
    }

    public static synchronized String nextSequence(String key) {
        return nextSequenceAll(key, "yyMMddHHmm");
    }

    public static synchronized String nextSequenceToDay(String key) {

        return nextSequenceAll(key, "yyMMdd");
    }

    private static String nextSequenceAll(String key, String DateFormat) {

        long tick = getTicks();
        if (tick < lastTick.get()) {
            throw new RuntimeException("Clock moved backwards.");
        }
        long next;
        if (tick == lastTick.get()) {
            if (getSequenceManager().contains(key)) {
                next = getSequenceManager().increment(key) % 100;
                if (next == 0) {
                    tick = tillNextTick(lastTick.get());
                }
            } else {
                getSequenceManager().save(key, 1);
                next = 1L;
            }
        } else {
            getSequenceManager().save(key, 0);
            next = 0L;
        }
        lastTick.getAndSet(tick);
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
        NumberFormat nf = new DecimalFormat("#####");
        nf.setMinimumIntegerDigits(5);
        long seq = ((tick * 100 % 60000 + next + 5678) * 22137) % 100000;
        StringBuilder number = new StringBuilder(nf.format(seq));
        char digit3 = number.charAt(3);
        char digit4 = number.charAt(4);
        number.setLength(3);
        number.insert(1, digit3).insert(3, digit4);
        return key + sdf.format(new Date(tick * 100)) + number;
    }

    private static long tillNextTick(long lastTick) {
        long tick = getTicks();
        while (tick <= lastTick) {
            tick = getTicks();
        }
        return tick;
    }

    private static long getTicks() {
        return System.currentTimeMillis() / 100;
    }
}
