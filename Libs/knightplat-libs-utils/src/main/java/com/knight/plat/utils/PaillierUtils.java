package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Utils(value = "paillier", lib = true)
public class PaillierUtils {

    private static final Logger log = LoggerFactory.getLogger(PaillierUtils.class);

    public static String encryptToString(BigInteger m, PublicKey publicKey) {
        return HexUtils.bytesToHexString(encrypt(m, publicKey));
    }

    public static String encryptToString(BigInteger m, String publicKey) throws InvalidKeySpecException {
        return encryptToString(m, CryptoCommonKt.readRSAPublicKey(publicKey));
    }

    public static byte[] encrypt(BigInteger m, PublicKey publicKey) {
        RSAPublicKey rsaPubKey = (RSAPublicKey) publicKey;
        BigInteger n = rsaPubKey.getModulus();
        BigInteger g = n.add(BigInteger.ONE);

        BigInteger random;
        do {
            random = new BigInteger(n.bitLength(), new Random());
        } while (random.signum() != 1);

        if (m.signum() == -1) {
            m = m.mod(n);
        }

        BigInteger nSquare = n.multiply(n);
        BigInteger ciphertext =
                g.modPow(m, nSquare).multiply(random.modPow(n, nSquare)).mod(nSquare);

        byte[] nBytes = BinaryUtils.asUnsignedByteArray(n);
        byte[] nLenBytes = BinaryUtils.unsignedShortToByte2(nBytes.length);
        byte[] cipherBytes = BinaryUtils.asUnsignedByteArray(ciphertext, n.bitLength() / 4);
        byte[] data = new byte[nLenBytes.length + nBytes.length + cipherBytes.length];
        System.arraycopy(nLenBytes, 0, data, 0, nLenBytes.length);
        System.arraycopy(nBytes, 0, data, nLenBytes.length, nBytes.length);
        System.arraycopy(
                cipherBytes, 0, data, nLenBytes.length + nBytes.length, cipherBytes.length);
        return data;
    }

    public static byte[] encrypt(BigInteger m, String publicKey) throws InvalidKeySpecException {
        return encrypt(m, CryptoCommonKt.readRSAPublicKey(publicKey));
    }

    public static BigInteger decrypt(String ciphertext, PrivateKey privateKey) {
        return decrypt(HexUtils.hexStringToBytes(ciphertext), privateKey);
    }

    public static BigInteger decrypt(String ciphertext, String privateKey) throws InvalidKeySpecException {
        return decrypt(ciphertext, CryptoCommonKt.readRSAPrivateKey(privateKey));
    }

    public static BigInteger decrypt(byte[] ciphertext, PrivateKey privateKey) {
        RSAPrivateCrtKey rsaPriKey = (RSAPrivateCrtKey) privateKey;
        BigInteger n = rsaPriKey.getModulus();
        BigInteger lambda = rsaPriKey
                .getPrimeP()
                .subtract(BigInteger.ONE)
                .multiply(rsaPriKey.getPrimeQ().subtract(BigInteger.ONE));

        int nLen = BinaryUtils.byte2ToUnsignedShort(ciphertext);
        byte[] nBytes = new byte[nLen];
        System.arraycopy(ciphertext, 2, nBytes, 0, nLen);
        BigInteger n1 = BinaryUtils.fromUnsignedByteArray(nBytes);
        if (n1.compareTo(n) != 0) {
            log.warn("Invalid ciphertext, cannot match n parameter");
            return null;
        }

        byte[] data = new byte[ciphertext.length - nLen - 2];
        System.arraycopy(ciphertext, 2 + nLen, data, 0, ciphertext.length - nLen - 2);
        BigInteger intCiphertext = BinaryUtils.fromUnsignedByteArray(data);

        BigInteger mu = lambda.modInverse(n);
        BigInteger nSquare = n.multiply(n);
        BigInteger message =
                intCiphertext
                        .modPow(lambda, nSquare)
                        .subtract(BigInteger.ONE)
                        .divide(n)
                        .multiply(mu)
                        .mod(n);
        BigInteger maxValue = BigInteger.ONE.shiftLeft(n.bitLength() / 2);
        if (message.compareTo(maxValue) > 0) {
            return message.subtract(n);
        } else {
            return message;
        }
    }

    public static BigInteger decrypt(byte[] ciphertext, String privateKey) throws InvalidKeySpecException {
        return decrypt(ciphertext, CryptoCommonKt.readRSAPrivateKey(privateKey));
    }

    public static String ciphertextAdd(String ciphertext1, String ciphertext2) {
        return HexUtils.bytesToHexString(
                ciphertextAdd(
                        HexUtils.hexStringToBytes(ciphertext1),
                        HexUtils.hexStringToBytes(ciphertext2)));
    }

    private static byte[] ciphertextAdd(byte[] ciphertext1, byte[] ciphertext2) {
        int nLen1 = BinaryUtils.byte2ToUnsignedShort(ciphertext1);
        byte[] nBytes1 = new byte[nLen1];
        System.arraycopy(ciphertext1, 2, nBytes1, 0, nLen1);
        BigInteger n1 = BinaryUtils.fromUnsignedByteArray(nBytes1);
        byte[] data1 = new byte[ciphertext1.length - nLen1 - 2];
        System.arraycopy(ciphertext1, 2 + nLen1, data1, 0, ciphertext1.length - nLen1 - 2);

        int nLen2 = BinaryUtils.byte2ToUnsignedShort(ciphertext2);
        byte[] nBytes2 = new byte[nLen2];
        System.arraycopy(ciphertext2, 2, nBytes2, 0, nLen2);
        BigInteger n2 = BinaryUtils.fromUnsignedByteArray(nBytes2);
        if (n2.compareTo(n1) != 0) {
            log.warn("Adding ciphertexts that encrypt using different keys.");
            return null;
        }

        byte[] data2 = new byte[ciphertext2.length - nLen2 - 2];
        System.arraycopy(ciphertext2, 2 + nLen2, data2, 0, ciphertext2.length - nLen2 - 2);

        BigInteger ct1 = BinaryUtils.fromUnsignedByteArray(data1);
        BigInteger ct2 = BinaryUtils.fromUnsignedByteArray(data2);
        BigInteger nSquare = n1.multiply(n1);
        BigInteger ct = ct1.multiply(ct2).mod(nSquare);

        byte[] nLenBytes = BinaryUtils.unsignedShortToByte2(nBytes1.length);
        byte[] cipherBytes = BinaryUtils.asUnsignedByteArray(ct, n1.bitLength() / 4);
        byte[] data = new byte[nLenBytes.length + nBytes1.length + cipherBytes.length];
        System.arraycopy(nLenBytes, 0, data, 0, nLenBytes.length);
        System.arraycopy(nBytes1, 0, data, nLenBytes.length, nBytes1.length);
        System.arraycopy(
                cipherBytes, 0, data, nLenBytes.length + nBytes1.length, cipherBytes.length);
        return data;
    }

}
