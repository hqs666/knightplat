package com.knight.plat.utils.helper;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;


public class SnowFlakeManager {

    /**
     * 开始时间截 (2020-01-01)
     */
    private final long timeStart = 1577808000000L;

    /**
     * 机器id所占的位数
     */
    private final long workerIdBits = 6L;

    /**
     * 数据标识id所占的位数
     */
    private final long dataCenterIdBits = 4L;

    /**
     * 支持的最大机器id，结果是63 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数)
     */
    private final long maxWorkerId = ~(-1L << workerIdBits);

    /**
     * 支持的最大数据标识id，结果是15
     */
    private final long maxDataCenterId = ~(-1L << dataCenterIdBits);

    /**
     * 序列在id中占的位数
     */
    private final long sequenceBits = 12L;

    /**
     * 机器ID向左移12位
     */
    private final long workerIdShift = sequenceBits;

    /**
     * 数据标识id向左移18位(12+6)
     */
    private final long dataCenterIdShift = sequenceBits + workerIdBits;

    /**
     * 时间截向左移22位(6+4+12)
     */
    private final long timestampLeftShift = sequenceBits + workerIdBits + dataCenterIdBits;

    /**
     * 生成序列的掩码，这里为4095 (0b111111111111=0xfff=4095)
     */
    private final long sequenceMask = ~(-1L << sequenceBits);

    /**
     * 工作机器ID(0~63)
     */
    private final long workerId;

    /**
     * 数据中心ID(0~15)
     */
    private final long dataCenterId;

    /**
     * 毫秒内序列(0~4095)
     */
    private long sequence = 0L;

    /**
     * 上次生成ID的时间截
     */
    private long lastTimeStamp = -1L;

    public SnowFlakeManager() {
        long workerId = 0L;
        long dataCenterId = 0L;
        try {
            workerId = getWorkId();
            dataCenterId = getDataCenterId();
        }catch (RuntimeException e){
            throw new RuntimeException("workerId or dataCenterId create failed");
        }

        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("workerId can't be greater than %d or less than 0", maxWorkerId));
        }
        if (dataCenterId > maxDataCenterId || dataCenterId < 0) {
            throw new IllegalArgumentException(String.format("dataCenterId can't be greater than %d or less than 0", maxDataCenterId));
        }
        this.workerId = workerId;
        this.dataCenterId = dataCenterId;
    }

    /**
     * 获得下一个ID (该方法是线程安全的)
     *
     * @return SnowflakeId
     */
    public  synchronized long nextId() {
        long timeStamp = timeGen();

        //如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
        if (timeStamp < lastTimeStamp) {
            throw new RuntimeException(
                    String.format("Clock moved backwards. Refusing to generate id for %d milliseconds", lastTimeStamp - timeStamp));
        }

        //如果是同一时间生成的，则进行毫秒内序列
        if (lastTimeStamp == timeStamp) {
            sequence = (sequence + 1) & sequenceMask;
            //毫秒内序列溢出
            if (sequence == 0) {
                //阻塞到下一个毫秒,获得新的时间戳
                timeStamp = tilNextMillis(lastTimeStamp);
            }
        }
        //时间戳改变，毫秒内序列重置
        else {
            sequence = 0L;
        }

        //上次生成ID的时间截
        lastTimeStamp = timeStamp;

        //移位并通过或运算拼到一起组成64位的ID
        return ((timeStamp - timeStart) << timestampLeftShift)
                | (dataCenterId << dataCenterIdShift)
                | (workerId << workerIdShift)
                | sequence;
    }

    /**
     * 阻塞到下一个毫秒，直到获得新的时间戳
     *
     * @param lastTimeStamp 上次生成ID的时间截
     * @return 当前时间戳
     */
    protected long tilNextMillis(long lastTimeStamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimeStamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    /**
     * 返回以毫秒为单位的当前时间
     *
     * @return 当前时间(毫秒)
     */
    protected long timeGen() {
        return System.currentTimeMillis();
    }

    private Long getWorkId() {
        try {
            String hostAddress = Inet4Address.getLocalHost().getHostAddress();
            int[] ints = StringUtils.toCodePoints(hostAddress);
            int sums = 0;
            for (int b : ints) {
                sums += b;
            }
            return (long) (sums % 64);
        } catch (UnknownHostException e) {
            // 如果获取失败，则使用随机数备用

            return RandomUtils.nextLong(0, 64);
        }
    }

    private static Long getDataCenterId() {
        int[] ints = new int[0];
        try {
            ints = StringUtils.toCodePoints(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        int sums = 0;
        for (int i : ints) {
            sums += i;
        }
        return (long) (sums % 16);
    }
}
