package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import java.time.*;
import java.util.Date;

@Utils("datetime")
public final class DateTimeUtils {

    public static boolean isToday(Date date, ZoneId zone) {
        return DateTime.isToday(date, zone);
    }

    public static boolean isToday(Date date) {
        return DateTime.isToday(date);
    }

    public static boolean isPastDay(Date date, ZoneId zone) {
        return DateTime.isPastDay(date, zone);
    }

    public static boolean isPastDay(Date date) {
        return DateTime.isPastDay(date);
    }

    public static boolean isFutureDay(Date date, ZoneId zone) {
        return DateTime.isFutureDay(date, zone);
    }

    public static boolean isFutureDay(Date date) {
        return DateTime.isFutureDay(date);
    }

    public static Date todayStart(ZoneId zone) {
        return DateTime.todayStart(zone);
    }

    public static Date todayStart() {
        return DateTime.todayStart();
    }

    public static Date todayEnd(ZoneId zone) {
        return DateTime.todayEnd(zone);
    }

    public static Date todayEnd() {
        return DateTime.todayEnd();
    }

    public static long nowMillis() {
        return Instant.now().toEpochMilli();
    }

    public static Date now() {
        return new Date();
    }

    public static Date lastDayOfWeek(ZoneId zone) {
        return DateTime.lastDayOfWeek(zone);
    }

    public static Date lastDayOfWeek() {
        return DateTime.lastDayOfWeek();
    }

    public static LocalDate nextDayOfWeek(DayOfWeek dayOfWeek, ZoneId zone) {
        return DateTime.nextDayOfWeek(dayOfWeek, zone);
    }

    public static LocalDate nextDayOfWeek(DayOfWeek dayOfWeek) {
        return DateTime.nextDayOfWeek(dayOfWeek);
    }

    public static Date dayStart(Date date, ZoneId zone) {
        return DateTime.dayStart(date, zone);
    }

    public static Date dayStart(Date date) {
        return DateTime.dayStart(date);
    }

    public static Date dayEnd(Date date, ZoneId zone) {
        return DateTime.dayEnd(date, zone);
    }

    public static Date dayEnd(Date date) {
        return DateTime.dayEnd(date);
    }

    public static Date date(LocalTime localTime, LocalDate localDate, ZoneId zone) {
        return DateTime.date(localTime, localDate, zone);
    }

    public static Date date(LocalTime localTime, LocalDate localDate) {
        return DateTime.date(localTime, localDate);
    }

    public static Date date(LocalDate localDate, ZoneId zone) {
        return DateTime.date(localDate, zone);
    }

    public static Date date(LocalDate localDate) {
        return DateTime.date(localDate);
    }

    public static Date date(LocalDateTime localDateTime, ZoneId zone) {
        return DateTime.date(localDateTime, zone);
    }

    public static Date date(LocalDateTime localDateTime) {
        return DateTime.date(localDateTime);
    }

    public static LocalTime localTime(long millis, ZoneId zone) {
        return DateTime.localTime(millis, zone);
    }

    public static LocalTime localTime(long millis) {
        return DateTime.localTime(millis);
    }

    public static LocalDate localDate(long millis, ZoneId zone) {
        return DateTime.localDate(millis, zone);
    }

    public static LocalDate localDate(long millis) {
        return DateTime.localDate(millis);
    }

    public static LocalTime localTime(Date date, ZoneId zone) {
        return DateTime.localTime(date, zone);
    }

    public static LocalTime localTime(Date date) {
        return DateTime.localTime(date);
    }

    public static LocalDate localDate(Date date, ZoneId zone) {
        return DateTime.localDate(date, zone);
    }

    public static LocalDate localDate(Date date) {
        return DateTime.localDate(date);
    }

    public static String format(Date date, String pattern) {
        return DateTime.format(date, pattern);
    }

    public static String formatDate(Date date) {
        return DateTime.format(date, "yyyy-MM-dd");
    }

    public static String formatDateTime(Date date) {
        return DateTime.format(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date parseDate(String dateString, String pattern, ZoneId zone) {
        return DateTime.parseDate(dateString, pattern, zone);
    }

    public static Date parseDate(String dateString, String pattern) {
        return DateTime.parseDate(dateString, pattern);
    }

    public static LocalDate parseLocalDate(String dateString, String pattern) {
        return DateTime.parseLocalDate(dateString, pattern);
    }

    public static LocalTime parseLocalTime(String dateString, String pattern) {
        return DateTime.parseLocalTime(dateString, pattern);
    }

    public static LocalDateTime parseDateTime(String dateString, String pattern) {
        return DateTime.parseDateTime(dateString, pattern);
    }

    public static LocalDateTime localDateTime(Date date, ZoneId zone) {
        return DateTime.localDateTime(date, zone);
    }

    public static LocalDateTime localDateTime(Date date) {
        return DateTime.localDateTime(date);
    }

    public static LocalTime localTime(Instant instant, ZoneId zone) {
        return DateTime.localTime(instant, zone);
    }

    public static LocalTime localTime(Instant instant) {
        return DateTime.localTime(instant);
    }

    public static LocalDate localDate(Instant instant, ZoneId zone) {
        return DateTime.localDate(instant, zone);
    }

    public static LocalDate localDate(Instant instant) {
        return DateTime.localDate(instant);
    }

    public static LocalDateTime localDateTime(Instant instant, ZoneId zone) {
        return DateTime.localDateTime(instant, zone);
    }

    public static LocalDateTime localDateTime(Instant instant) {
        return DateTime.localDateTime(instant);
    }

    public static LocalDateCollection localDateItr(LocalDate begin, LocalDate end, Period step) {
        return DateTime.localDateItr(begin, end, step);
    }

    public static int currentYear() {
        return DateTime.currentYear();
    }

    public static Date getLastDateOfMonth(Date date) {
        return DateTime.getLastDateOfMonth(date);
    }
}
