package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.api.utils.Tools;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author knightplat
 *
 * edited by Jon | 2019/8/30
 * edited  | 2020/12/30
 **/
@Utils("template")
public final class RenderTemplateUtils {

    /**
     * 模板渲染
     *
     * @param templateContent 模板内容 示例：您的“${CarBrand}”(车牌${CarPlateNum})已经完成租用(订单号：${orderNo})。
     * @param contentParamMap 模板Map数据  示例：put("CarBrand", "丰田")
     * @return 模版渲染结果
     */
    public static String renderTemplate(String templateContent, Map<String, Object> contentParamMap) {
        if (contentParamMap == null || contentParamMap.isEmpty()) {
            return templateContent;
        }
        return Tools.patternReplace(templateContent, Pattern.compile("\\$\\{([^$^{}]*?)}"), contentParamMap::get);
    }
}
