/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils.helper;

import com.google.common.collect.Lists;
import com.knight.plat.api.utils.HolidayManager;
import java.util.Calendar;
import java.util.List;


public class SimpleHolidayManager implements HolidayManager {

    private final List<Calendar> list = Lists.newArrayList();

    private final List<Calendar> exceptList = Lists.newArrayList();

    @Override
    public void addHoliday(Calendar holiday) {
        list.add(holiday);
    }

    @Override
    public void addWorkday(Calendar workday) {
        exceptList.add(workday);
    }

    @Override
    public void initHolidayList() {
        // TODO: 2016/10/19 初始化假期列表
    }

    @Override
    public boolean isHoliday(Calendar date) {
        int weekday = date.get(Calendar.DAY_OF_WEEK);
        return (!exceptList.contains(date)) &&
                (weekday == Calendar.SATURDAY
                        || weekday == Calendar.SUNDAY
                        || list.contains(date));
    }
}