package io.vulcan.worker.impl;

import com.knight.plat.api.commons.Callback;
import javax.annotation.Nonnull;

public class RunnableWrapper implements Runnable {

    @Nonnull
    private final Callback<Void, Throwable> callback;

    @Nonnull
    private final com.knight.plat.api.commons.Runnable task;

    RunnableWrapper(@Nonnull com.knight.plat.api.commons.Runnable task, @Nonnull Callback<Void, Throwable> callback) {
        this.callback = callback;
        this.task = task;
    }

    @Nonnull
    public com.knight.plat.api.commons.Runnable getTask() {
        return task;
    }

    @Override
    public void run() {
        try {
            task.run();
            callback.onSuccess(null);
        } catch (Throwable t) {
            callback.onException(t);
        }
    }
}
