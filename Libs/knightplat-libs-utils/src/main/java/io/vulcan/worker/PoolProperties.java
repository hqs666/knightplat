package io.vulcan.worker;

import java.time.Duration;

public class PoolProperties {
    private int queueCapacity = 500;

    /**
     * Core number of threads.
     */
    private int coreSize = 8;

    /**
     * Maximum allowed number of threads. If tasks are filling up the queue, the pool
     * can expand up to that size to accommodate the load. Ignored if the queue is
     * unbounded.
     */
    private int maxSize = 50;

    /**
     * Time limit for which threads may remain idle before being terminated.
     */
    private Duration keepAlive = Duration.ofSeconds(60);

    public int getQueueCapacity() {
        return queueCapacity;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    public int getCoreSize() {
        return coreSize;
    }

    public void setCoreSize(int coreSize) {
        this.coreSize = coreSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public Duration getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(Duration keepAlive) {
        this.keepAlive = keepAlive;
    }
}
