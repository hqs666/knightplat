package io.vulcan.worker.impl;

import com.knight.plat.api.commons.Callback;
import javax.annotation.Nonnull;

public class CallableWrapper<R> implements Runnable {

    @Nonnull
    private final Callback<R, Throwable> callback;

    @Nonnull
    private final com.knight.plat.api.commons.Callable<R> task;

    CallableWrapper(@Nonnull com.knight.plat.api.commons.Callable<R> task, @Nonnull Callback<R, Throwable> callback) {
        this.callback = callback;
        this.task = task;
    }

    @Nonnull
    public com.knight.plat.api.commons.Callable<R> getTask() {
        return task;
    }

    @Override
    public void run() {
        try {
            R result = task.call();
            callback.onSuccess(result);
        } catch (Throwable t) {
            callback.onException(t);
        }
    }
}
