package io.vulcan.worker;

import com.knight.plat.api.commons.Callable;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.commons.Runnable;
import com.knight.plat.api.executor.RunnableFilter;
import io.vulcan.worker.impl.WorkerPoolImpl;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public interface WorkerPool extends AutoCloseable {

    static WorkerPool create(String name) {
        return new WorkerPoolImpl(name);
    }

    static WorkerPool create(String name, int coreSize, int maxSize, long keepAlive, TimeUnit unit, int queueSize) {
        return new WorkerPoolImpl(name, coreSize, maxSize, keepAlive, unit, queueSize);
    }

    static WorkerPool create(String name, RunnableHandler runnableHandler) {
        final WorkerPoolImpl pool = new WorkerPoolImpl(name);
        pool.onRejected(runnableHandler);
        return pool;
    }

    static WorkerPool create(String name, CallableHandler callableHandler) {
        final WorkerPoolImpl pool = new WorkerPoolImpl(name);
        pool.onRejected(callableHandler);
        return pool;
    }

    static WorkerPool create(String name, RunnableHandler runnableHandler, CallableHandler callableHandler) {
        final WorkerPoolImpl pool = new WorkerPoolImpl(name);
        pool.onRejected(runnableHandler);
        pool.onRejected(callableHandler);
        return pool;
    }

    Executor executor();

    void setFilters(List<RunnableFilter> filters);

    int coreSize();

    int maxSize();

    int queueSize();

    void execute(Runnable runnable, Callback<Void, Throwable> callback);

    <R> void execute(Callable<R> callable, Callback<R, Throwable> callback);

    default CompletableFuture<Void> execute(Runnable runnable) {
        final CompletableFuture<Void> future = new CompletableFuture<>();
        execute(runnable, Callback.make(future));
        return future;
    }

    default <R> CompletableFuture<R> execute(Callable<R> callable) {
        final CompletableFuture<R> future = new CompletableFuture<>();
        execute(callable, Callback.make(future));
        return future;
    }

    interface RunnableHandler extends Consumer<com.knight.plat.api.commons.Runnable> {}

    interface CallableHandler extends Consumer<com.knight.plat.api.commons.Callable<?>> {}
}
