/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

/**
 * Created on 2016/10/13. handle with ThreadLocal object.
 *
 * @since 0.5.0.RELEASE
 */
@file:JvmName("ThreadLocal")

package com.knight.plat.utils

import org.slf4j.LoggerFactory

private val threadBucket = InheritableThreadLocalMap<Any, Any>()
private val log = LoggerFactory.getLogger("knightplat - ThreadLocalUtils")

internal fun <T> getValue(threadBucket: InheritableThreadLocalMap<Any, Any>, key: Any, clazz: Class<T>): T? {
    val bucket = threadBucket.get()
    if (null == bucket) {
        if (log.isWarnEnabled) {
            log.warn("ThreadLocal bucket: $key not exists.")
        }
        return null
    }
    val valueSrc = bucket[key]
    if (clazz.isInstance(valueSrc)) {
        return clazz.cast(valueSrc)
    } else {
        if (log.isWarnEnabled) {
            if (null == valueSrc) {
                log.warn(
                    "Cannot find object from ThreadLocal bucket: " + key
                            + "; no object found"
                )
            } else {
                log.warn(
                    "Cannot find object from ThreadLocal bucket: " + key
                            + "; except type is " + clazz.simpleName
                            + " but got " + valueSrc.javaClass.simpleName
                )
            }
        }
        return null
    }
}

internal fun <T> getValue(key: Any, clazz: Class<T>) = getValue(threadBucket, key, clazz)

internal fun setValue(threadBucket: InheritableThreadLocalMap<Any, Any>, key: Any, value: Any) {
    var bucket: MutableMap<Any, Any>? = threadBucket.get()
    if (null == bucket) {
        val newBucket = HashMap<Any, Any>()
        threadBucket.set(newBucket)
        bucket = newBucket
    }
    bucket[key] = value
}

internal fun setValue(key: Any, value: Any) = setValue(threadBucket, key, value)

internal fun clear(threadBucket: InheritableThreadLocalMap<Any, Any>, key: Any) {
    val bucket = threadBucket.get() ?: return
    bucket.remove(key)
}

internal fun clear(key: Any) = clear(threadBucket, key)

internal class InheritableThreadLocalMap<K, V> : InheritableThreadLocal<MutableMap<K, V>>() {

    override fun childValue(parentValue: MutableMap<K, V>?): MutableMap<K, V>? {
        return if (parentValue != null) {
            HashMap(parentValue)
        } else {
            null
        }
    }
}
