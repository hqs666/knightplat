/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

/**
 * Created on 16/8/19.
 */
@file:JvmName("KeyGen")

package com.knight.plat.utils

import com.google.common.base.Strings
import com.knight.plat.utils.helper.SnowFlakeManager
import java.security.SecureRandom
import java.util.*

@JvmOverloads
internal fun newUuid(separator: String = "") = UUID.randomUUID().toString().replace("-", separator)

internal fun random(from: Long, to: Long) = (from..to).random()

internal fun randomString(from: Long, to: Long, length: Int): String {
    val numValue = random(from, to)
    val str = numValue.toString()
    return Strings.padStart(str, length, '0')
}

private const val SYMBOLS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
private val secureRandomProvider = SecureRandom()

internal fun secureRandom(bound: Int) = secureRandomProvider.nextInt(bound)

internal fun nonce(len: Int): String {
    val nonceChars = CharArray(len)
    val symbolLength = SYMBOLS.length
    for (i in 0 until len) {
        nonceChars[i] = SYMBOLS[secureRandom(symbolLength)]
    }
    return String(nonceChars)
}

private val snowFlakeManager = SnowFlakeManager()

internal fun snowFlake() = snowFlakeManager.nextId()