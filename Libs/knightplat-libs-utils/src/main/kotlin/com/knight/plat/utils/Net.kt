/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

/**
 * Created on 2016/10/29. 网络相关工具
 */
@file:JvmName("Net")

package com.knight.plat.utils

import com.google.common.base.Strings
import javax.servlet.http.HttpServletRequest

/**
 * 获取客户端IP地址

 * @param request 客户端请求对象
 * *
 * @return 客户端IP地址
 */
internal fun getRemoteIp(request: HttpServletRequest?): String {

    if (null == request) {
        return ""
    }

    // 有CDN源地址则取CDN地址
    var ip = request.getHeader("Cdn-Src-Ip")
    if (Strings.isNullOrEmpty(ip) || "unknown".equals(ip, ignoreCase = true)) {
        // x-forwarded-for存在值则说明有经过反向代理
        ip = request.getHeader("x-forwarded-for")
    }
    if (Strings.isNullOrEmpty(ip) || "unknown".equals(ip, ignoreCase = true)) {
        // 如果反向代理关闭了x-forwarded-for取值选项
        ip = request.getHeader("Proxy-Client-IP")
    }
    if (Strings.isNullOrEmpty(ip) || "unknown".equals(ip, ignoreCase = true)) {
        // 同上
        ip = request.getHeader("WL-Proxy-Client-IP")
    }
    if (Strings.isNullOrEmpty(ip) || "unknown".equals(ip, ignoreCase = true)) {
        // 没有经过反向代理则可以直接取出请求来源IP
        ip = request.remoteAddr
    }
    return ip
}

/**
 * 获取user-agent值

 * @param request 请求对象
 * *
 * @return user-agent值
 */
internal fun getUserAgent(request: HttpServletRequest?): String {

    if (null == request) {
        return ""
    }

    val userAgent = request.getHeader("user-agent")
    return if (Strings.isNullOrEmpty(userAgent)) "" else userAgent
}

/**
 * 获取平台信息

 * @param request 请求对象
 * *
 * @return 平台代码
 */
internal fun getPlatform(request: HttpServletRequest?): String {

    if (null == request) {
        return ""
    }

    val platform = request.getHeader("platform")
    return if (Strings.isNullOrEmpty(platform)) "web" else platform
}
