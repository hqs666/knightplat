/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

/**
 * QR utils
 *
 */
@file:JvmName("QRCode")
package com.knight.plat.utils

import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import java.awt.image.BufferedImage

private val writer = MultiFormatWriter()

internal fun renderQRImage(content: String, width: Int, height: Int): BufferedImage? {
    val hints: Map<EncodeHintType, Any> = mapOf(
        EncodeHintType.CHARACTER_SET to "UTF-8",
        EncodeHintType.ERROR_CORRECTION to ErrorCorrectionLevel.H,
        EncodeHintType.MARGIN to 1
    )

    val bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height, hints)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}