/*
 * Copyright (c) 2016. Knight Co., Ltd. All rights reserved.
 */

/**
 * QR utils
 *
 */
@file:JvmName("BarCode")
package com.knight.plat.utils

import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.client.j2se.MatrixToImageWriter
import java.awt.image.BufferedImage

private val writer = MultiFormatWriter()

internal fun renderEAN8(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.EAN_8, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderEAN13(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.EAN_13, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderCODABAR(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.CODABAR, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderCODE39(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.CODE_39, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderCODE93(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.CODE_93, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderCODE128(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.CODE_128, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderITF(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.ITF, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderUPCA(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.UPC_A, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}

internal fun renderUPCE(content: String, width: Int, height: Int): BufferedImage? {
    val bitMatrix = writer.encode(content, BarcodeFormat.UPC_E, width, height, null)
    return MatrixToImageWriter.toBufferedImage(bitMatrix)
}