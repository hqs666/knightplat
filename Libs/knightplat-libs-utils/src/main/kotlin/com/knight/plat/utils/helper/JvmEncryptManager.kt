/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

/**
 * Created on 16/8/4. 编码工具类
 */

package com.knight.plat.utils.helper

import com.knight.plat.utils.*
import org.bouncycastle.crypto.InvalidCipherTextException
import org.bouncycastle.crypto.params.ParametersWithRandom
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil
import org.slf4j.LoggerFactory
import java.security.*
import java.security.spec.InvalidKeySpecException
import javax.crypto.Cipher
import javax.crypto.Mac
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.DESKeySpec
import javax.crypto.spec.SecretKeySpec
import kotlin.math.min

internal object JvmEncryptManager {

    private val log = LoggerFactory.getLogger("knightplat - EncryptUtils")

    /**
     * md5编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    internal fun md5Encrypt(source: ByteArray): ByteArray = md5(source)

    /**
     * sha1编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    internal fun sha1Encrypt(source: ByteArray): ByteArray = sha1(source)

    /**
     * sha256编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    internal fun sha256Encrypt(source: ByteArray): ByteArray = sha256(source)

    /**
     * sha512编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    internal fun sha512Encrypt(source: ByteArray): ByteArray = sha512(source)


    /**
     * sm3编码
     *
     * @param source 原始数据
     * @return 编码后数据
     */
    internal fun sm3Encrypt(source: ByteArray): ByteArray = sm3(source)

    internal fun hmacEncrypt(algorithm: String, source: ByteArray, secret: ByteArray): ByteArray {
        val keySpec = SecretKeySpec(secret, algorithm)
        try {
            val mac = Mac.getInstance(algorithm)
            mac.init(keySpec)
            return mac.doFinal(source)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("$algorithm encoder is not supported.", e)
        } catch (e: InvalidKeyException) {
            throw RuntimeException("$algorithm key is not supported.", e)
        }
    }

    /**
     * base64解码
     *
     * @param origin 原始数据
     * @return 解码后数据字节数组
     */
    internal fun base64DecodeBytes(origin: String): ByteArray {
        return base64Engine(true).decode(origin)
    }


    /**
     * SM4加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    @Throws(InvalidKeySpecException::class)
    internal fun sm4Encrypt(source: ByteArray, keyStr: String): ByteArray {
        if (source.isEmpty()) {
            return ByteArray(0)
        }
        return try {
            val cipher = makeSm4Cipher(keyStr, Cipher.ENCRYPT_MODE)
            cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "SM4 encode error"
            log.error(warn, e)
            ByteArray(0)
        }
    }

    @Throws(InvalidKeySpecException::class)
    internal fun sm4Decrypt(source: ByteArray, keyStr: String): ByteArray {
        if (source.isEmpty()) {
            return ByteArray(0)
        }
        sm4Lock.lock()
        return try {
            val cipher = makeSm4Cipher(keyStr, Cipher.DECRYPT_MODE)
            cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "SM4 encode error"
            log.error(warn, e)
            ByteArray(0)
        } finally {
            sm4Lock.unlock()
        }
    }

    /**
     * DES加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    @Throws(InvalidKeySpecException::class)
    internal fun desEncrypt(source: ByteArray, keyStr: String): ByteArray {
        if (keyStr.length < 8) {
            throw InvalidKeySpecException("key material is shorter than 8 bytes")
        }
        if (source.isEmpty()) {
            return ByteArray(0)
        }
        desLock.lock()
        return try {
            val cipher = makeDesCipher(keyStr, Cipher.ENCRYPT_MODE)
            cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "DES encode error"
            log.error(warn, e)
            ByteArray(0)
        } finally {
            desLock.unlock()
        }
    }

    @Throws(InvalidKeySpecException::class)
    internal fun desDecrypt(source: ByteArray, keyStr: String): ByteArray {
        if (keyStr.length < 8) {
            throw InvalidKeySpecException("key material is shorter than 8 bytes")
        }
        if (source.isEmpty()) {
            return ByteArray(0)
        }
        desLock.lock()
        return try {
            val cipher = makeDesCipher(keyStr, Cipher.DECRYPT_MODE)
            cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "DES encode error"
            log.error(warn, e)
            ByteArray(0)
        } finally {
            desLock.unlock()
        }
    }

    /**
     * AES加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param keyStr 秘钥
     * @return 加密结果
     */
    @Throws(InvalidKeySpecException::class)
    internal fun aesEncrypt(source: ByteArray, keyStr: String): ByteArray {
        if (keyStr.length < 8) {
            throw InvalidKeySpecException("key material is shorter than 8 bytes")
        }
        if (source.isEmpty()) {
            return ByteArray(0)
        }
        aesLock.lock()
        return try {
            val cipher = makeAesCipher(keyStr, Cipher.ENCRYPT_MODE)
            cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "AES encode error"
            log.error(warn, e)
            ByteArray(0)
        } finally {
            aesLock.unlock()
        }
    }

    @Throws(InvalidKeySpecException::class)
    internal fun aesDecrypt(source: ByteArray, keyStr: String): ByteArray {
        if (keyStr.length < 8) {
            throw InvalidKeySpecException("key material is shorter than 8 bytes")
        }
        if (source.isEmpty()) {
            return ByteArray(0)
        }
        aesLock.lock()
        return try {
            val cipher = makeAesCipher(keyStr, Cipher.DECRYPT_MODE)
            cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "AES encode error"
            log.error(warn, e)
            ByteArray(0)
        } finally {
            aesLock.unlock()
        }
    }

    // RSA加解密byte数组部分

    /**
     * RSA公钥加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    internal fun rsaEncrypt(source: ByteArray, publicKey: PublicKey): ByteArray? {
        rsaLock.lock()
        try {
            val cipher = Cipher.getInstance("RSA")
            cipher.init(Cipher.ENCRYPT_MODE, publicKey)
            if (source.size > 117) {
                var dataReturn = ByteArray(0)
                var i = 0
                val size = source.size
                while (i < size) {
                    val doFinal = cipher.doFinal(source.copyOfRange(i, min(size, i + 100)))
                    dataReturn += doFinal
                    i += 100
                }
                return dataReturn
            }
            return cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "RSA encode error"
            log.error(warn, e)
            return ByteArray(0)
        } finally {
            rsaLock.unlock()
        }

    }

    /**
     * RSA私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    internal fun rsaDecrypt(source: ByteArray, privateKey: PrivateKey): ByteArray? {
        rsaLock.lock()
        try {
            val cipher = Cipher.getInstance("RSA")
            cipher.init(Cipher.DECRYPT_MODE, privateKey)
            if (source.size > 128) {
                var dataReturn = ByteArray(0)
                var i = 0
                val size = source.size
                while (i < size) {
                    val doFinal = cipher.doFinal(source.copyOfRange(i, min(size, i + 128)))
                    dataReturn += doFinal
                    i += 128
                }
                return dataReturn
            }
            return cipher.doFinal(source)
        } catch (e: GeneralSecurityException) {
            val warn = "RSA decode error"
            log.error(warn, e)
            return ByteArray(0)
        } finally {
            rsaLock.unlock()
        }
    }

    // SM2加解密byte数组部分

    /**
     * SM2公钥加密 - 字节版 加密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param publicKey 秘钥对象
     * @return 加密结果
     */
    internal fun sm2Encrypt(source: ByteArray, publicKey: PublicKey): ByteArray? {
        val engine = getSM2Engine()
        sm2Lock.lock()
        return try {
            val keyParams = ParametersWithRandom(ECUtil.generatePublicKeyParameter(publicKey))
            engine.init(true, keyParams)
            engine.processBlock(source, 0, source.size)
        } catch (e: InvalidCipherTextException) {
            val warn = "SM2 encode error"
            log.error(warn, e)
            return ByteArray(0)
        } finally {
            sm2Lock.unlock()
        }
    }

    /**
     * SM2私钥解密 - 字节版 解密的最终处理方法
     *
     * @param source 原始数据字节数组
     * @param privateKey 私钥对象
     * @return 解密结果
     */
    internal fun sm2Decrypt(source: ByteArray, privateKey: PrivateKey): ByteArray? {
        val engine = getSM2Engine()
        sm2Lock.lock()
        return try {
            engine.init(false, ECUtil.generatePrivateKeyParameter(privateKey))
            engine.processBlock(source, 0, source.size)
        } catch (e: InvalidCipherTextException) {
            val warn = "SM2 decode error"
            log.error(warn, e)
            return ByteArray(0)
        } finally {
            sm2Lock.unlock()
        }
    }

    @Throws(InvalidKeySpecException::class)
    private fun generateDesKey(keyStr: String): Key {
        val keyFactory: SecretKeyFactory
        val dks: DESKeySpec
        try {
            dks = DESKeySpec(keyStr.toByteArray(Charsets.UTF_8))
            keyFactory = SecretKeyFactory.getInstance("DES")
            return keyFactory.generateSecret(dks)
        } catch (e: InvalidKeyException) {
            val warn = "Key: $keyStr is not valid."
            throw RuntimeException(warn, e)
        } catch (e: NoSuchAlgorithmException) {
            val warn = "DES encoder is not supported."
            throw RuntimeException(warn, e)
        } catch (e: InvalidKeySpecException) {
            val warn = "KeySpec is not supported."
            log.error(warn)
            throw InvalidKeySpecException(warn, e)
        }
    }

    @Throws(InvalidKeySpecException::class)
    private fun generateSm4Key(keyStr: String): Key {
        try {
            val keyBytes = if (keyStr.length <= 16) {
                keyStr.padEnd(16, '0').toByteArray(Charsets.UTF_8)
            } else {
                keyStr.slice(0..15).toByteArray(Charsets.UTF_8)
            }
            return SecretKeySpec(keyBytes, "SM4")
        } catch (e: InvalidKeyException) {
            val warn = "Key: $keyStr is not valid."
            throw RuntimeException(warn, e)
        } catch (e: NoSuchAlgorithmException) {
            val warn = "DES encoder is not supported."
            throw RuntimeException(warn, e)
        } catch (e: InvalidKeySpecException) {
            val warn = "KeySpec is not supported."
            log.error(warn)
            throw InvalidKeySpecException(warn, e)
        }
    }

    @Throws(InvalidKeySpecException::class)
    private fun generateAesKey(keyStr: String): Key {
        try {
            val keyBytes = if (keyStr.length <= 16) {
                keyStr.padEnd(16, '0').toByteArray(Charsets.UTF_8)
            } else if (keyStr.length <= 24) {
                keyStr.padEnd(24, '0').toByteArray(Charsets.UTF_8)
            } else if (keyStr.length <= 32) {
                keyStr.padEnd(32, '0').toByteArray(Charsets.UTF_8)
            } else {
                keyStr.slice(0..31).toByteArray(Charsets.UTF_8)
            }
            return SecretKeySpec(keyBytes, "AES")
        } catch (e: InvalidKeyException) {
            val warn = "Key: $keyStr is not valid."
            throw RuntimeException(warn, e)
        } catch (e: NoSuchAlgorithmException) {
            val warn = "AES encoder is not supported."
            throw RuntimeException(warn, e)
        } catch (e: InvalidKeySpecException) {
            val warn = "KeySpec is not supported."
            log.error(warn)
            throw InvalidKeySpecException(warn, e)
        }
    }

    @Throws(InvalidKeySpecException::class)
    private fun makeDesCipher(keyStr: String, mode: Int): Cipher {
        val key = generateDesKey(keyStr)
        val cipher = Cipher.getInstance("DES/ECB/PKCS5Padding", getProvider())
        cipher.init(mode, key)
        //    val cipher = Cipher.getInstance("DES/CBC/PKCS5Padding")
        //    val iv = IvParameterSpec(keyStr.substring(0, 8).toByteArray(Charsets.UTF_8))
        //    cipher.init(mode, key, iv)
        return cipher
    }

    @Throws(InvalidKeySpecException::class)
    private fun makeAesCipher(keyStr: String, mode: Int): Cipher {
        val key = generateAesKey(keyStr)
        val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", getProvider())
        cipher.init(mode, key)
        return cipher
    }

    @Throws(InvalidKeySpecException::class)
    private fun makeSm4Cipher(keyStr: String, mode: Int): Cipher {
        val key = generateSm4Key(keyStr)
        val cipher = Cipher.getInstance("SM4/ECB/PKCS5Padding", getProvider())
        cipher.init(mode, key)
        return cipher
    }
}
