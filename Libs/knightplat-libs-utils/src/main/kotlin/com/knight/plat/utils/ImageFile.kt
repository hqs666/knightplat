/*
 * Copyright (c) 2017. knight Co., Ltd. All rights reserved.
 */

/**
 * Created on 2017/4/5. 图片相关处理工具类
 */
@file:JvmName("ImageFile")

package com.knight.plat.utils

import com.knight.plat.api.exceptions.BusinessException
import com.knight.plat.utils.helper.JvmEncryptManager
import java.io.*
import java.nio.file.Files
import java.nio.file.Path

/**
 * 获取图片的格式类型
 *
 * @param data 图片二进制数据
 *
 * @return 格式
 */
internal fun getImageType(data: ByteArray): ImageType = when {

    (data.size < 12) ->
        ImageType.NONE
    // Png test:
    (data[1].toInt().toChar() == 'P' && data[2].toInt().toChar() == 'N' && data[3].toInt().toChar() == 'G') ->
        ImageType.PNG

    // Gif test:
    (data[0].toInt().toChar() == 'G' && data[1].toInt().toChar() == 'I' && data[2].toInt().toChar() == 'F') ->
        ImageType.GIF

    // JPG test:
    (data[0] == 0xFF.toByte() && data[1] == 0xD8.toByte() && data[2] == 0xFF.toByte()) ->
        ImageType.JPEG

    // BMP test:
    (data[0].toInt().toChar() == 'B' && data[1].toInt().toChar() == 'M') ->
        ImageType.BMP

    // WebP test:
    (data[8].toInt().toChar() == 'W' && data[9].toInt().toChar() == 'E' && data[10].toInt().toChar() == 'B' && data[11].toInt()
        .toChar() == 'P') ->
        ImageType.WEBP

    else -> ImageType.NONE
}

/**
 * 获取图片的格式类型
 *
 * @param inputStream 图片二进制流
 *
 * @return 格式
 */
internal fun getImageType(inputStream: InputStream): ImageType {
    val data = ByteArray(20)
    try {
        val read = inputStream.read(data)
        if (read < 12) {
            return ImageType.NONE
        }
    } catch (e: IOException) {
        throw BusinessException("Image data inputStream read error.", e)
    } finally {
        if (inputStream.markSupported()) {
            inputStream.reset()
        }
    }

    return getImageType(data)
}

/**
 * 获取图片的格式类型
 *
 * @param file 图片文件
 *
 * @return 格式
 */
internal fun getImageType(file: File): ImageType {
    FileInputStream(file).use {
        try {
            return getImageType(it)
        } catch (e: IOException) {
            throw BusinessException("Image file read error.", e)
        }
    }
}

/**
 * 获取图片的格式类型
 *
 * @param filePath 图片文件路径
 *
 * @return 格式
 */
internal fun getImageType(filePath: Path): ImageType {
    Files.newInputStream(filePath).use {
        try {
            return getImageType(it)
        } catch (e: IOException) {
            throw BusinessException("Image file read error.", e)
        }
    }
}

/**
 * 获取base64编码后的图片的格式类型
 *
 * @param base64 base64编码后的图片数据
 *
 * @return 格式
 */
internal fun getImageTypeFromBase64(base64: String): ImageType {
    val data = JvmEncryptManager.base64DecodeBytes(base64)
    return getImageType(data)
}

