package com.knight.plat.utils

import org.apache.commons.codec.binary.Base64
import org.bouncycastle.crypto.digests.SM3Digest
import org.bouncycastle.crypto.engines.SM2Engine
import org.bouncycastle.crypto.signers.SM2Signer
import org.bouncycastle.crypto.signers.StandardDSAEncoding
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.slf4j.LoggerFactory
import java.security.*
import java.security.spec.*
import java.util.concurrent.locks.ReentrantLock

private val log = LoggerFactory.getLogger("knightplat - EncryptUtils")

internal val rsaLock = ReentrantLock()
internal val rsaSignLock = ReentrantLock()
internal val sm2Lock = ReentrantLock()
internal val sm2SignLock = ReentrantLock()
internal val aesLock = ReentrantLock()
internal val desLock = ReentrantLock()
internal val sm4Lock = ReentrantLock()

private val base64Url = Base64(0, null, true)
private val base64 = Base64(0, null, false)
internal fun base64Engine(isUrl: Boolean = false) = if (isUrl) base64Url else base64

private var provider: Provider? = null
internal fun getProvider() : Provider {
    if (provider == null) {
        provider = BouncyCastleProvider()
    }
    return provider as Provider
}

private val sm3Digest = SM3Digest()
private var sm2Engine: SM2Engine? = null
private var sm2Signer: SM2Signer? = null
internal fun getSM2Engine(): SM2Engine {
    if (sm2Engine == null) {
        sm2Engine = SM2Engine(sm3Digest, SM2Engine.Mode.C1C3C2)
    }
    sm3Digest.reset()
    return sm2Engine as SM2Engine
}
internal fun getSM2Signer(): SM2Signer {
    if (sm2Signer == null) {
        sm2Signer = SM2Signer(StandardDSAEncoding.INSTANCE, sm3Digest)
    }
    sm3Digest.reset()
    return sm2Signer as SM2Signer
}

/**
 * 生成RSA加密公钥/私钥对
 *
 * @return 公钥/私钥对
 */
internal fun generateRSAKeyPair(): KeyResolver {
    return SimpleRSAKeyResolver(generateKeyPair("RSA", null, 1024))
}

/**
 * 生成RSA加密公钥/私钥对 - 2048位
 *
 * @return 公钥/私钥对
 */
internal fun generateStrongRSAKeyPair(): KeyResolver {
    return SimpleRSAKeyResolver(generateKeyPair("RSA", null, 2048))
}

/**
 * 生成RSA加密公钥/私钥对 - 4096位
 *
 * @return 公钥/私钥对
 */
internal fun generateBestRSAKeyPair(): KeyResolver {
    return SimpleRSAKeyResolver(generateKeyPair("RSA", null, 4096))
}

/**
 * 生成RSA加密公钥/私钥对
 *
 * @return 公钥/私钥对
 */
internal fun generateSM2KeyPair(): KeyResolver {
    return SimpleSM2KeyResolver(generateKeyPair("EC", ECGenParameterSpec("sm2p256v1"), 256))
}

// 非对称加密算法公钥/私钥对生成
internal fun generateKeyPair(algorithm: String, spec: AlgorithmParameterSpec?, keySize: Int): KeyPair {
    val generator: KeyPairGenerator
    try {
        generator = KeyPairGenerator.getInstance(algorithm, getProvider())
    } catch (e: NoSuchAlgorithmException) {
        val warn = "$algorithm encoder is not supported."
        throw RuntimeException(warn, e)
    }

    val random = SecureRandom(System.currentTimeMillis().toString().toByteArray())
    if (spec != null) {
        generator.initialize(spec, random)
    } else {
        generator.initialize(keySize, random)
    }
    return generator.generateKeyPair()
}

// 从字符串中生成RSA公钥对象
@Throws(InvalidKeySpecException::class)
internal fun readRSAPublicKey(publicKey: String): PublicKey {
    val publicKeyBytes = base64Engine().decode(publicKey)
    return readRSAPublicKey(publicKeyBytes)
}

@Throws(InvalidKeySpecException::class)
internal fun readRSAPublicKey(publicKeyBytes: ByteArray): PublicKey {
    val specPublic = X509EncodedKeySpec(publicKeyBytes)
    val kf: KeyFactory
    try {
        kf = KeyFactory.getInstance("RSA", getProvider())
        return kf.generatePublic(specPublic)
    } catch (e: NoSuchAlgorithmException) {
        val warn = "RSA encoder is not supported."
        throw RuntimeException(warn, e)
    } catch (e: InvalidKeySpecException) {
        val warn = "KeySpec is not supported."
        log.error(warn)
        throw InvalidKeySpecException(warn, e)
    }
}

// 从字符串中生成RSA私钥对象
@Throws(InvalidKeySpecException::class)
internal fun readRSAPrivateKey(privateKey: String): PrivateKey {
    val privateKeyBytes = base64Engine().decode(privateKey)
    return readRSAPrivateKey(privateKeyBytes)
}

@Throws(InvalidKeySpecException::class)
internal fun readRSAPrivateKey(privateKeyBytes: ByteArray): PrivateKey {
    val specPrivate = PKCS8EncodedKeySpec(privateKeyBytes)
    val kf: KeyFactory
    try {
        kf = KeyFactory.getInstance("RSA", getProvider())
        return kf.generatePrivate(specPrivate)
    } catch (e: NoSuchAlgorithmException) {
        val warn = "RSA encoder is not supported."
        log.error(warn, e)
        throw Error(warn, e)
    } catch (e: InvalidKeySpecException) {
        val warn = "KeySpec is not supported."
        log.error(warn, e)
        throw InvalidKeySpecException(e)
    }
}

// 从字符串中生成RSA公钥对象
@Throws(InvalidKeySpecException::class)
internal fun readSM2PublicKey(publicKey: String): PublicKey {
    val publicKeyBytes = base64Engine().decode(publicKey)
    return readSM2PublicKey(publicKeyBytes)
}

@Throws(InvalidKeySpecException::class)
internal fun readSM2PublicKey(publicKeyBytes: ByteArray): PublicKey {
    val specPublic = X509EncodedKeySpec(publicKeyBytes)
    val kf: KeyFactory
    try {
        kf = KeyFactory.getInstance("EC", getProvider())
        return kf.generatePublic(specPublic)
    } catch (e: NoSuchAlgorithmException) {
        val warn = "SM2 encoder is not supported."
        throw RuntimeException(warn, e)
    } catch (e: InvalidKeySpecException) {
        val warn = "KeySpec is not supported."
        log.error(warn)
        throw InvalidKeySpecException(warn, e)
    }
}

// 从字符串中生成RSA私钥对象
@Throws(InvalidKeySpecException::class)
internal fun readSM2PrivateKey(privateKey: String): PrivateKey {
    val privateKeyBytes = base64Engine().decode(privateKey)
    return readSM2PrivateKey(privateKeyBytes)
}

@Throws(InvalidKeySpecException::class)
internal fun readSM2PrivateKey(privateKeyBytes: ByteArray): PrivateKey {
    val specPrivate = PKCS8EncodedKeySpec(privateKeyBytes)
    val kf: KeyFactory
    try {
        kf = KeyFactory.getInstance("EC", getProvider())
        return kf.generatePrivate(specPrivate)
    } catch (e: NoSuchAlgorithmException) {
        val warn = "SM2 encoder is not supported."
        log.error(warn, e)
        throw Error(warn, e)
    } catch (e: InvalidKeySpecException) {
        val warn = "KeySpec is not supported."
        log.error(warn, e)
        throw InvalidKeySpecException(e)
    }
}

/**
 * 密钥处理器接口
 */
interface KeyResolver {

    val publicKey: String

    val privateKey: String

    val keyPair: KeyPair?

    @Throws(InvalidKeySpecException::class)
    fun readKeys(publicKey: String, privateKey: String)
}

// RSA秘钥处理
private class SimpleRSAKeyResolver(keyPair: KeyPair) : KeyResolver {

    override var keyPair: KeyPair? = null
        private set

    init {
        this.keyPair = keyPair
    }

    override val publicKey: String
        get() {
            val encoded = keyPair?.public?.encoded ?: ByteArray(0)
            return base64Engine().encodeToString(encoded)
        }

    override val privateKey: String
        get() {
            val encoded = keyPair?.private?.encoded ?: ByteArray(0)
            return base64Engine().encodeToString(encoded)
        }

    @Throws(InvalidKeySpecException::class)
    override fun readKeys(publicKey: String, privateKey: String) {
        this.keyPair = KeyPair(readRSAPublicKey(publicKey), readRSAPrivateKey(privateKey))
    }
}

// SM2秘钥处理
private class SimpleSM2KeyResolver(keyPair: KeyPair) : KeyResolver {

    override var keyPair: KeyPair? = null
        private set

    init {
        this.keyPair = keyPair
    }

    override val publicKey: String
        get() {
            val encoded = keyPair?.public?.encoded ?: ByteArray(0)
            return base64Engine().encodeToString(encoded)
        }

    override val privateKey: String
        get() {
            val encoded = keyPair?.private?.encoded ?: ByteArray(0)
            return base64Engine().encodeToString(encoded)
        }

    @Throws(InvalidKeySpecException::class)
    override fun readKeys(publicKey: String, privateKey: String) {
        this.keyPair = KeyPair(readSM2PublicKey(publicKey), readSM2PrivateKey(privateKey))
    }
}