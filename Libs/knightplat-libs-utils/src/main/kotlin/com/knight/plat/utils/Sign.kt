@file:JvmName("Sign")

package com.knight.plat.utils

import com.alibaba.fastjson2.JSON
import com.alibaba.fastjson2.JSONWriter
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil
import org.slf4j.LoggerFactory
import java.security.GeneralSecurityException
import java.security.Signature

private val log = LoggerFactory.getLogger("knightplat - SignUtils")

private fun objectMapToStringMap(origin: Map<String, Any?>): Map<String, String> {
    return origin.mapValues {
        when(it.value) {
            null -> "null"
            is String -> it.value as String
            is Boolean -> (it.value as Boolean).toString()
            is Byte -> (it.value as Byte).toString()
            is Char -> (it.value as Char).toString()
            is Double -> (it.value as Double).toString()
            is Float -> (it.value as Float).toString()
            is Int -> (it.value as Int).toString()
            is Long -> (it.value as Long).toString()
            is Short -> (it.value as Short).toString()
            else -> null
        }
    }.filterValues { it != null }.mapValues { it.value as String }
}

private fun createLinkString(params: Map<String, String>): String {
    if (params.isEmpty()) {
        return ""
    }

    val sorted = params.toSortedMap()
    val sb = StringBuilder()
    sorted.forEach { (key, value) ->
        sb.append(key).append('=').append(value).append('&')
    }
    sb.setLength(sb.length - 1)
    return sb.toString()
}

// RSA签名部分

internal fun sign(content: String, privateKey: String): String {
    rsaSignLock.lock()
    return try {
        val priKey = readRSAPrivateKey(privateKey)
        val signature = Signature.getInstance("SHA1WithRSA", getProvider())
        signature.initSign(priKey)
        signature.update(content.toByteArray(Charsets.UTF_8))
        val signed = signature.sign()
        base64Engine().encodeToString(signed)
    } catch (e: GeneralSecurityException) {
        log.error("签名异常", e)
        ""
    } finally {
        rsaSignLock.unlock()
    }
}

internal fun signStringMap(map: Map<String, String>, privateKey: String) = sign(createLinkString(map), privateKey)

internal fun signObjectMap(map: Map<String, Any?>, privateKey: String) = signStringMap(objectMapToStringMap(map), privateKey)

internal fun signList(timestamp: Long, list: List<Any?>, privateKey: String): String {
    val signMap = mapOf("timestamp" to timestamp, "list_data" to JSON.toJSONString(list, JSONWriter.Feature.MapSortField))
    return signObjectMap(signMap, privateKey)
}

internal fun verify(content: String, sign: String, publicKey: String): Boolean {
    rsaSignLock.lock()
    return try {
        val finalSign = sign.replace("_zq".toRegex(), "+")
        val pubKey = readRSAPublicKey(publicKey)
        val signature = Signature.getInstance("SHA1WithRSA", getProvider())
        signature.initVerify(pubKey)
        signature.update(content.toByteArray(Charsets.UTF_8))
        signature.verify(base64Engine().decode(finalSign))
    } catch (e: GeneralSecurityException) {
        log.error("验签异常", e)
        false
    } finally {
        rsaSignLock.unlock()
    }
}

internal fun verifyStringMap(map: Map<String, String>, sign: String, publicKey: String) = verify(createLinkString(map), sign, publicKey)

internal fun verifyStringMap(map: MutableMap<String, String>, publicKey: String): Boolean {
    val sign = map["sign_val"] ?: throw RuntimeException("签名值为空('sign_val'字段)")
    map.remove("ws_sign_val")
    return verifyStringMap(map, sign, publicKey)
}

internal fun verifyObjectMap(map: MutableMap<String, Any?>, publicKey: String) = verifyStringMap(objectMapToStringMap(map).toMutableMap(), publicKey)

internal fun verifyObjectMap(map: MutableMap<String, Any?>, sign: String, publicKey: String) = verifyStringMap(
    objectMapToStringMap(map), sign, publicKey)

internal fun verifyList(timestamp: Long, list: List<Any?>, sign: String, publicKey: String): Boolean {
    val signMap: MutableMap<String, Any?> = mutableMapOf("timestamp" to timestamp, "list_data" to JSON.toJSONString(list, JSONWriter.Feature.MapSortField))
    return verifyObjectMap(signMap, sign, publicKey)
}

// SM2签名部分

internal fun sm2Sign(content: String, privateKey: String): String {
    sm2SignLock.lock()
    val signer = getSM2Signer()
    return try {
        val priKey = readSM2PrivateKey(privateKey)
        val keyParams = ECUtil.generatePrivateKeyParameter(priKey)
        signer.init(true, keyParams)
        val bytes = content.toByteArray(Charsets.UTF_8)
        signer.update(bytes, 0, bytes.size)
        val signed = signer.generateSignature()
        base64Engine().encodeToString(signed)
    } catch (e: GeneralSecurityException) {
        log.error("签名异常", e)
        ""
    } finally {
        sm2SignLock.unlock()
    }
}

internal fun sm2SignStringMap(map: Map<String, String>, privateKey: String) = sm2Sign(createLinkString(map), privateKey)

internal fun sm2SignObjectMap(map: Map<String, Any?>, privateKey: String) = sm2SignStringMap(objectMapToStringMap(map), privateKey)

internal fun sm2SignList(timestamp: Long, list: List<Any?>, privateKey: String): String {
    val signMap = mapOf("timestamp" to timestamp, "list_data" to JSON.toJSONString(list, JSONWriter.Feature.MapSortField))
    return sm2SignObjectMap(signMap, privateKey)
}

internal fun sm2Verify(content: String, sign: String, publicKey: String): Boolean {
    sm2SignLock.lock()
    val signer = getSM2Signer()
    return try {
        val finalSign = sign.replace("_zq".toRegex(), "+")
        val pubKey = readSM2PublicKey(publicKey)
        val keyParams = ECUtil.generatePublicKeyParameter(pubKey)
        signer.init(false, keyParams)
        val bytes = content.toByteArray(Charsets.UTF_8)
        signer.update(bytes, 0, bytes.size)
        signer.verifySignature(base64Engine().decode(finalSign))
    } catch (e: GeneralSecurityException) {
        log.error("验签异常", e)
        false
    } finally {
        sm2SignLock.unlock()
    }
}

internal fun sm2VerifyStringMap(map: Map<String, String>, sign: String, publicKey: String) = sm2Verify(createLinkString(map), sign, publicKey)

internal fun sm2VerifyStringMap(map: MutableMap<String, String>, publicKey: String): Boolean {
    val sign = map["sign_val"] ?: throw RuntimeException("签名值为空('sign_val'字段)")
    map.remove("ws_sign_val")
    return sm2VerifyStringMap(map, sign, publicKey)
}

internal fun sm2VerifyObjectMap(map: MutableMap<String, Any?>, publicKey: String) = sm2VerifyStringMap(
    objectMapToStringMap(map).toMutableMap(), publicKey)

internal fun sm2VerifyObjectMap(map: MutableMap<String, Any?>, sign: String, publicKey: String) = sm2VerifyStringMap(
    objectMapToStringMap(map), sign, publicKey)

internal fun sm2VerifyList(timestamp: Long, list: List<Any?>, sign: String, publicKey: String): Boolean {
    val signMap: MutableMap<String, Any?> = mutableMapOf("timestamp" to timestamp, "list_data" to JSON.toJSONString(list, JSONWriter.Feature.MapSortField))
    return sm2VerifyObjectMap(signMap, sign, publicKey)
}