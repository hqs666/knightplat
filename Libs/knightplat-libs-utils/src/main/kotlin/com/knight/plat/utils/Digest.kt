/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */
@file:JvmName("Digest")
package com.knight.plat.utils

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

internal fun md5(source: ByteArray) = encrypt(source, "MD5")

internal fun sha1(source: ByteArray) = encrypt(source, "SHA-1")

internal fun sha256(source: ByteArray) = encrypt(source, "SHA-256")

internal fun sha512(source: ByteArray) = encrypt(source, "SHA-512")

internal fun sm3(source: ByteArray) = encrypt(source, "SM3")

// 散列算法加密方法
private fun encrypt(source: ByteArray, algorithm: String): ByteArray {
    val digest: MessageDigest
    try {
        digest = MessageDigest.getInstance(algorithm, getProvider())
    } catch (e: NoSuchAlgorithmException) {
        val warn = "$algorithm encoder is not supported."
        throw RuntimeException(warn, e)
    }

    //加密后的字符
    return digest.digest(source)
}
