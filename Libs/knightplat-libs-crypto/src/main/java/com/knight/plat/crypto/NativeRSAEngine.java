package com.knight.plat.crypto;

import com.knight.plat.api.crypto.RSAEngine;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.spec.InvalidKeySpecException;

public class NativeRSAEngine implements RSAEngine {

    private final CryptoNativeManager manager = CryptoNativeManager.getInstance();

    @Override
    public ByteBuffer rsaEncode(byte[] data, byte[] publicKey, boolean isBase64) throws InvalidKeySpecException {
        try {
            return manager.rsaEncode(manager.getDirectBuffer(data), manager.getDirectBuffer(publicKey), isBase64);
        } catch (GeneralSecurityException e) {
            rethrow(e);
        }
        return null;
    }

    @Override
    public ByteBuffer rsaDecode(byte[] data, byte[] privateKey, boolean isBase64) throws InvalidKeySpecException {
        try {
            return manager.rsaDecode(manager.getDirectBuffer(data), manager.getDirectBuffer(privateKey), isBase64);
        } catch (GeneralSecurityException e) {
            rethrow(e);
        }
        return null;
    }

    private void rethrow(GeneralSecurityException e) throws InvalidKeySpecException {
        if (e instanceof InvalidKeySpecException) {
            throw (InvalidKeySpecException) e;
        }
        throw new InvalidKeySpecException(e);
    }

}
