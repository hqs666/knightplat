package com.knight.plat.crypto;

import fr.stardustenterprises.yanl.NativeLoader;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;

public class CryptoNativeManager {

    private static final CryptoNativeManager instance = new CryptoNativeManager();

    private final ThreadLocal<ByteBuffer> flagBuffer = ThreadLocal.withInitial(() -> ByteBuffer.allocateDirect(1));

    private CryptoNativeManager() {
        new NativeLoader.Builder().build().loadLibrary("knight_plat_crypto", false);
    }

    public static CryptoNativeManager getInstance() {
        return instance;
    }

    native ByteBuffer nativeTest(ByteBuffer data);

    public ByteBuffer getDirectBuffer(byte[] data) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(data.length);
        buffer.put(data);
        return buffer;
    }

    private native ByteBuffer rsaEncode(ByteBuffer data, ByteBuffer publicKey, ByteBuffer flag, boolean base64);

    public ByteBuffer rsaEncode(ByteBuffer data, ByteBuffer publicKey, boolean base64) throws GeneralSecurityException {
        ByteBuffer flagBuffer = getFlag();
        ByteBuffer resultBuffer = rsaEncode(data, publicKey, flagBuffer, base64);
        checkAndThrow(flagBuffer, resultBuffer);
        return resultBuffer;
    }

    private native ByteBuffer rsaDecode(ByteBuffer data, ByteBuffer privateKey, ByteBuffer flag, boolean base64);

    public ByteBuffer rsaDecode(ByteBuffer data, ByteBuffer privateKey, boolean base64)
            throws GeneralSecurityException {
        ByteBuffer flagBuffer = getFlag();
        ByteBuffer resultBuffer = rsaDecode(data, privateKey, flagBuffer, base64);
        checkAndThrow(flagBuffer, resultBuffer);
        return resultBuffer;
    }

    private ByteBuffer getFlag() {
        return flagBuffer.get();
    }

    private void initFlag() {
        ByteBuffer flagBuffer = getFlag();
        flagBuffer.clear();
        flagBuffer.put(0, (byte) -1);
    }

    private void checkAndThrow(ByteBuffer flagBuffer, ByteBuffer resultBuffer) throws GeneralSecurityException {
        byte flag = flagBuffer.get(0);
        if (flag == -1) {
            throw new GeneralSecurityException("Unknown error");
        }
        if (flag != 0) {
            byte[] decodeBytes = new byte[resultBuffer.limit()];
            resultBuffer.get(decodeBytes);
            String msg = new String(decodeBytes, StandardCharsets.UTF_8);
            if (flag == 2) {
                throw new InvalidKeyException(msg);
            }
            throw new GeneralSecurityException(msg);
        }
        initFlag();
    }
}
