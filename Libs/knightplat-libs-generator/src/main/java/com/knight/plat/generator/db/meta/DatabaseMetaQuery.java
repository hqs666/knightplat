package com.knight.plat.generator.db.meta;

import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.knight.plat.generator.config.GeneratorProperties;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

public class DatabaseMetaQuery {

    private final GeneratorProperties properties;

    public DatabaseMetaQuery(GeneratorProperties properties) {
        this.properties = properties;
    }

    public Multimap<String, TableMeta> getTableMetas()
            throws InstantiationException, IllegalAccessException, SQLException, NoSuchMethodException, InvocationTargetException {
        System.out.println("begin connect database....");
        com.mysql.cj.jdbc.Driver.class.getDeclaredConstructor().newInstance();

        Properties dbProperties = new Properties();
        dbProperties.put("user", properties.datasource.username);
        dbProperties.put("password", properties.datasource.password);
        dbProperties.put("remarksReporting", "true");
        dbProperties.put("useSSL", "false");
        dbProperties.put("nullCatalogMeansCurrent", "true");
        Connection conn = DriverManager.getConnection(properties.datasource.url, dbProperties);

        DatabaseMetaData dm = conn.getMetaData();

        String[] types = new String[]{"TABLE"};

        Multimap<String, TableMeta> tables = HashMultimap.create();
        System.out.println("开始处理数据库表信息....");
        for (GeneratorProperties.TableModule tableModule : properties.datasource.tables) {
            ResultSet rs = dm.getTables(null, "", tableModule.pattern, types);
            while (rs.next()) {

                String tbName = rs.getString("TABLE_NAME").toUpperCase();
                String tbDesc = rs.getString("REMARKS");
                if (tableModule.isLower) {
                    tbName = tbName.toLowerCase(Locale.ROOT);
                }
                ResultSet rsCol = dm.getColumns(null, null, tbName, null);

                Set<String> pkSet = Sets.newHashSet();
                boolean usePk = tableModule.usePk();
                if (usePk) {
                    ResultSet pkRs = dm.getPrimaryKeys(null, null, tbName);
                    while (pkRs.next()) {
                        String colName = pkRs.getString("COLUMN_NAME");
                        pkSet.add(colName);
                    }
                } else {
                    ResultSet pkRs = dm.getIndexInfo(null, null, tbName, true, true);
                    while (pkRs.next()) {
                        String colName = pkRs.getString("COLUMN_NAME");
                        String indexName = pkRs.getString("INDEX_NAME");
                        if (!"pk_id".equals(colName.toLowerCase(Locale.getDefault())) && indexName != null && indexName.contains("INDEX_PK")) {
                            pkSet.add(colName);
                        }
                    }
                }
                String packageName;
                if (Strings.isNullOrEmpty(properties.generator.projectName)) {
                    packageName = properties.generator.basePackage;
                } else {
                    packageName = properties.generator.basePackage + "." + properties.generator.projectName;
                }
                TableMeta tableMeta = new TableMeta(tbName, tbDesc, tableModule.module, packageName,
                        new ColumnMetaSet(rsCol), pkSet, tableModule.prefix, tableModule.usePk(), tableModule.usePlural());

                tables.put(tableModule.module, tableMeta);
                System.out.println("数据表：" + tbName + " 处理完毕, 主键：" + pkSet);
            }
        }
        conn.close();
        System.out.println("数据库处理完毕....");
        return tables;
    }

}
