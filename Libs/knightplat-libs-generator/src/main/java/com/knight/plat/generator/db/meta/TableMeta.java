package com.knight.plat.generator.db.meta;

import com.google.common.base.CaseFormat;
import com.google.common.collect.Lists;
import net.oschina.util.Inflector;

import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TableMeta {

    private final NameHolder nameHolder;
    private final PackageHolder packageHolder;

    private final String tableName;
    private final String remarks;

    private final List<ColumnMetaSet.ColumnMeta> columnSet;

    private final Set<String> pkNameSet;

    private final String prefix;

    private final boolean usePlural;

    private final Inflector inflector = Inflector.getInstance();

    private boolean hasDate = false;
    private boolean hasJson = false;
    private boolean hasOffsetTime = false;
    private boolean hasOffsetDateTime = false;
    private boolean hasBigDecimal = false;

    public TableMeta(String tableName, String remarks, String module, String basePackage,
            ColumnMetaSet columnSet, Set<String> pkSet ,String prefix, boolean usePk, boolean usePlural) {
        this.tableName = tableName;
        this.nameHolder = new NameHolder(tableName);
        this.remarks = remarks;
        Stream<ColumnMetaSet.ColumnMeta> stream = usePk ? Lists.newArrayList(columnSet)
                .stream() : Lists.newArrayList(columnSet).stream().filter(c -> !"pk_id".equals(c.getColumnName().toLowerCase(Locale.getDefault())));
        this.columnSet = stream
                .peek(c -> hasDate = hasDate || c.isDate())
                .peek(c -> hasJson = hasJson || c.isJson())
                .peek(c -> hasOffsetTime = hasOffsetTime || c.isOffsetTime())
                .peek(c -> hasOffsetDateTime = hasOffsetDateTime || c.isOffsetDateTime())
                .peek(c -> hasBigDecimal = hasBigDecimal || c.isBigDecimal())
                .collect(Collectors.toList());
        this.packageHolder = new PackageHolder(module, basePackage);
        this.pkNameSet = pkSet;
        this.prefix = prefix;
        this.usePlural = usePlural;
    }

    public boolean isPk(String columnName) {
        return pkNameSet.contains(columnName);
    }

    public boolean isSinglePk() {
        return pkNameSet != null && pkNameSet.size() == 1;
    }

    public boolean hasDate() {
        return hasDate;
    }

    public boolean hasJson() {
        return hasJson;
    }

    public boolean hasOffsetTime() {
        return hasOffsetTime;
    }

    public boolean hasOffsetDateTime() {
        return hasOffsetDateTime;
    }

    public boolean hasBigDecimal() {
        return hasBigDecimal;
    }

    public String getSinglePk() {
        return pkNameSet.iterator().next();
    }

    public List<ColumnMetaSet.ColumnMeta> getColumnSet() {
        return columnSet;
    }

    public String getRemarks() {
        if (remarks == null || remarks.isEmpty()) {
            return getEntityName();
        }
        return remarks;
    }

    public String getTableName() {
        return tableName;
    }

    public String getEntityName() {
        return nameHolder.getEntityName(prefix);
    }

    public String getPluralEntityName() {
        if (!usePlural) {
            return getEntityName();
        }
        return inflector.pluralize(getEntityName());
    }

    public String getPropertyName() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, getEntityName());
    }

    public String getPluralPropertyName() {
        if (!usePlural) {
            return getPropertyName();
        }
        return inflector.pluralize(getPropertyName());
    }

    public String getEntityPackage() {
        return packageHolder.getEntityPackage();
    }

    public String getDefaultMapperName() {
        return nameHolder.getMapperName(true, prefix);
    }

    public String getDefaultMapperPropertyName() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, getDefaultMapperName());
    }

    public String getDefaultMapperPackage() {
        return packageHolder.getMapperPackage(true);
    }

    public String getMapperName() {
        return nameHolder.getMapperName(false, prefix);
    }

    public String getMapperPropertyName() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, getMapperName());
    }

    public String getMapperPackage() {
        return packageHolder.getMapperPackage(false);
    }

    public String getDefaultServiceInterfaceName() {
        return nameHolder.getServiceInterfaceName(true, prefix);
    }

    public String getDefaultServiceInterfacePackage() {
        return packageHolder.getServiceInterfacePackage(true);
    }

    public String getServiceInterfaceName() {
        return nameHolder.getServiceInterfaceName(false, prefix);
    }

    public String getServiceInterfacePackage() {
        return packageHolder.getServiceInterfacePackage(false);
    }

    public String getDefaultServiceName() {
        return nameHolder.getServiceImplName(true, prefix);
    }

    public String getDefaultServicePackage() {
        return packageHolder.getServiceImplPackage(true);
    }

    public String getServiceName() {
        return nameHolder.getServiceImplName(false, prefix);
    }

    public String getServicePackage() {
        return packageHolder.getServiceImplPackage(false);
    }

    public String getDtoPackage() {
        return packageHolder.getDtoPackage();
    }

    public String getDtoName() {
        return nameHolder.getDtoName(prefix);
    }
}
