package com.knight.plat.generator;

import com.google.common.base.Charsets;
import com.knight.plat.generator.struct.NodeType;
import com.knight.plat.generator.struct.StructNode;
import com.knight.plat.generator.struct.StructTree;
import com.knight.plat.generator.config.GeneratorProperties;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.knight.plat.generator.base.Utils.createParentDirs;
import static com.knight.plat.generator.base.Utils.touch;


public abstract class ProjectBuilder {

    private final StructTree tree;

    protected boolean generateCustoms = false;

    private final VelocityEngine ve = new VelocityEngine();
    protected final GeneratorProperties properties;

    public ProjectBuilder(GeneratorProperties properties) {
        this.properties = properties;
        StructNode root = new StructNode();
        root.setName(properties.generator.projectName);
        root.setNodeType(NodeType.DIR);
        tree = StructTree.newTree(root);

        ve.setProperty(RuntimeConstants.RESOURCE_LOADERS, "file,classpath");
        ve.setProperty("resource.loader.classpath.class", ClasspathResourceLoader.class.getName());
        ve.init();
    }

    protected StructNode createDir(String name) {
        StructNode dir = new StructNode();
        dir.setNodeType(NodeType.DIR);
        dir.setName(name);
        dir.setParent(0);
        dir.setTree(tree);
        dir.setOverwrite(true);
        tree.add(dir, null);
        return dir;
    }

    protected StructNode createFile(String name, String template, VelocityContext context) {
        StructNode file = new StructNode();
        file.setNodeType(NodeType.FILE);
        file.setName(name);
        file.setTemplate(template);
        file.setParent(0);
        file.setTree(tree);
        file.setContext(context);
        file.setOverwrite(true);
        tree.add(file, null);
        return file;
    }

    protected StructNode createFile(String name, String template, VelocityContext context, boolean overwrite) {
        StructNode file = createFile(name, template, context);
        file.setOverwrite(overwrite);
        return file;
    }

    /**
     * This method saves the files in the root path
     * according to the StructNodes in the tree.
     * If the overwrite flag is set, existing files will be overwritten,
     * otherwise, they will be ignored.
     * After saving a file, the canonical path will be printed on the console.
     */
    public void saveFiles() {
        for (StructNode node: tree.getNodes()) {
            NodeType type = node.getNodeType();
            boolean overwrite = node.isOverwrite();
            File file = new File( properties.generator.rootPath + File.separator + node.getPath());
            String flag = " ";
            try {
                switch (type) {
                    case DIR:
                        flag = "D";
                        boolean ignored = file.mkdirs();
                        break;
                    case FILE:
                        flag = "F";
                        if (!overwrite && file.exists()) {
                            break;
                        }
                        createParentDirs(file);
                        touch(file);
                        Template template = ve.getTemplate(
                                this.getTemplatePath() + "/" + node.getTemplate(), "UTF-8");
                        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                                Files.newOutputStream(file.toPath()), Charsets.UTF_8))) {
                            template.merge(node.getContext(), writer);
                            writer.flush();
                        }
                        break;
                    default:
                }
                System.out.println("+ " + flag + " " + file.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    protected VelocityContext makeContext() {

        VelocityContext ctx = new VelocityContext();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        ctx.put("basePackage", properties.generator.basePackage);
        ctx.put("knightplatVersion", properties.generator.platVersion);
        ctx.put("projectName", properties.generator.projectName);
        ctx.put("supporters", properties.generator.supporters);
        ctx.put("date", sdf.format(new Date()));
        ctx.put("jdbcUrl", properties.datasource.url);
        ctx.put("jdbcUser", properties.datasource.username);
        ctx.put("jdbcPassword", properties.datasource.password);

        return ctx;
    }

    protected abstract String getTemplatePath();

    protected abstract void make();
}
