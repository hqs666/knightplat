package com.knight.plat.generator.base;

import com.google.common.base.Strings;

import java.io.File;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

public class Utils {
    public static void touch(File file) throws IOException {
        checkNotNull(file);
        if (!file.createNewFile() && !file.setLastModified(System.currentTimeMillis())) {
            throw new IOException("Unable to update modification time of " + file);
        }
    }

    public static void createParentDirs(File file) throws IOException {
        checkNotNull(file);
        File parent = file.getCanonicalFile().getParentFile();
        if (parent == null) {
            /*
             * The given directory is a filesystem root. All zero of its ancestors exist. This doesn't
             * mean that the root itself exists -- consider x:\ on a Windows machine without such a drive
             * -- or even that the caller can create it, but this method makes no such guarantees even for
             * non-root files.
             */
            return;
        }
        boolean ignored = parent.mkdirs();
        if (!parent.isDirectory()) {
            throw new IOException("Unable to create parent directories of " + file);
        }
    }

    /**
     * This method escapes the remarks present in the given string. This method replaces "\n" with a space
     * and also replaces double quote - " with the string \".
     *
     * @param origin string which contains potentially unescaped remarks
     * @return the String with escaped remarks
     */
    public static String escapeRemarks(String origin) {
        if (Strings.isNullOrEmpty(origin)) {
            return "";
        }
        String result = origin.replace("\n", " ");
        if (Strings.isNullOrEmpty(result)) {
            return "";
        }
        return result.replace("\"", "\\\"");
    }
}
