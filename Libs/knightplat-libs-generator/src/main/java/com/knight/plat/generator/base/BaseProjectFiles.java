package com.knight.plat.generator.base;

import com.google.common.base.Joiner;
import com.knight.plat.generator.ProjectBuilder;
import com.knight.plat.generator.config.GeneratorProperties;
import com.knight.plat.generator.struct.StructNode;
import org.apache.velocity.VelocityContext;

import java.io.File;

public class BaseProjectFiles extends ProjectBuilder {

    public BaseProjectFiles(GeneratorProperties properties) {
        super(properties);
    }

    @Override
    public void make() {
        VelocityContext ctx = makeContext();

        boolean isMultiDB = false, hasRedis = false;
        for (String s : properties.generator.supporters) {
            if (s.equals("multidatasource")) {
                isMultiDB = true;
                continue;
            }
            if (s.equals("redis")) {
                hasRedis = true;
            }
        }
        ctx.put("isMultiDB", isMultiDB);
        ctx.put("hasRedis", hasRedis);
        ctx.put("tables", properties.datasource.tables);

        // Start building project directories and files
        String classBasePath = Joiner.on(File.separator).join(
                properties.generator.basePackage.replace(".", File.separator), properties.generator.projectName);
        StructNode classParent = createDir(classBasePath);
        StructNode resourceBase = createDir("resources");
        createDir("src").addChildren( // maven src directory
                createDir("main").addChildren(
                        createDir("java").addChild(classParent)
                        , resourceBase.addChildren(
                                createFile("application.yml", "application.yml.vm", ctx)
                                , createFile("application-dev.yml", "application-dev.yml.vm", ctx)
                                , createDir("static")
                        )
                )
                , createDir("test").addChildren(
                        createDir("java")
                        , createDir("resources")
                )
        );
        createFile("pom.xml", "pom.xml.vm", ctx);
        createFile(".gitignore", "gitignore.vm", ctx);
        classParent.addChildren(
                createDir("utils")
                , createDir("commons")
                        .addChildren(
                                createDir("controller") // Spring MVC controllers
                                        .addChild(createFile("CommonController.java", "CommonController.java.vm", ctx))
                        )
                , createDir("export")
                , createDir("rpc")
                , createFile("App.java", "App.java.vm", ctx)
                , createFile("UrlMapping.java", "UrlMapping.java.vm", ctx)
        );
    }

    @Override
    public String getTemplatePath() {
        return "templates";
    }
}
