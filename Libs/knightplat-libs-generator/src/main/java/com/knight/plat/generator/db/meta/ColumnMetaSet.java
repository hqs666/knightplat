package com.knight.plat.generator.db.meta;

import com.google.common.base.CaseFormat;
import com.knight.plat.generator.base.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

public class ColumnMetaSet implements Iterable<ColumnMetaSet.ColumnMeta> {

    private final ColumnMetaIterator iterator;

    ColumnMetaSet(ResultSet columns) {
        this.iterator = new ColumnMetaIterator(columns);
    }

    public static class ColumnMeta {

        private final String colName;
        private final String colType;
        private final String digits;
        private final int length;
        private final String remarks;

        ColumnMeta(ResultSet columns) throws SQLException {
            colName = columns.getString("COLUMN_NAME");
            colType = columns.getString("TYPE_NAME");
            digits = columns.getString("DECIMAL_DIGITS");
            length = Integer.parseInt(columns.getString("COLUMN_SIZE"));
            remarks = Utils.escapeRemarks(columns.getString("REMARKS"));
        }

        public String getColumnName() {
            return colName;
        }

        public String getFieldName() {
            String original = colName.toLowerCase();
            return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, original);
        }

        public String getGetterName() {
            String original = colName.toLowerCase();
            return "get" + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, original);
        }

        public String getSetterName() {
            String original = colName.toLowerCase();
            return "set" + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, original);
        }

        public String getRemarks() {
            return remarks;
        }

        public String getTypeName() {
            return DBTypeConvertor.getJavaType(colType, digits, length);
        }

        public boolean isNumber() {
            return "NUMBER".equals(colType) || "INT".equals(colType) || "DECIMAL".equals(colType) || "DOUBLE"
                    .equals(colType)
                    || "BIGINT".equals(colType) || "INTEGER".equals(colType) || "SMALLINT".equals(colType);
        }

        public boolean isJson() {
            return "JsonRoot".equals(getTypeName());
        }

        public boolean isDate() {
            return "Date".equals(getTypeName());
        }

        public boolean isOffsetTime() {
            return "OffsetTime".equals(getTypeName());
        }

        public boolean isOffsetDateTime() {
            return "OffsetDateTime".equals(getTypeName());
        }

        public boolean isBigDecimal() {
            return "BigDecimal".equals(getTypeName());
        }

        public boolean isString() {
            return "String".equals(getTypeName());
        }
    }

    static class ColumnMetaIterator implements Iterator<ColumnMeta> {

        private final ResultSet set;
        private boolean hasNext;
        private ColumnMeta current;

        ColumnMetaIterator(ResultSet set) {
            this.set = set;
            try {
                hasNext = set.first();
                if (hasNext) {
                    current = new ColumnMeta(set);
                }
            } catch (SQLException e) {
                hasNext = false;
                e.printStackTrace();
            }
        }

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public ColumnMeta next() {
            ColumnMeta last = current;
            try {
                hasNext = set.next();
                if (hasNext) {
                    current = new ColumnMeta(set);
                }
            } catch (SQLException e) {
                hasNext = false;
                e.printStackTrace();
            }
            return last;
        }
    }

    @Override
    public Iterator<ColumnMeta> iterator() {
        return iterator;
    }
}
