package com.knight.plat.generator.db.meta;

import com.google.common.base.CaseFormat;
import org.apache.commons.lang3.StringUtils;

public class NameHolder {

    private final String originalTableName;
    private final CaseFormat tableFormat = CaseFormat.LOWER_UNDERSCORE;

    public NameHolder(String originalTableName) {
        this.originalTableName = originalTableName.toLowerCase();
    }

    public String getEntityName(String prefix) {
        String entityName = "";
        if (!StringUtils.isEmpty(prefix)) {
            entityName = prefix;
        }
        String[] split = originalTableName.split("_", 3);
        if (split.length < 2) {
            return tableFormat.to(CaseFormat.UPPER_CAMEL, entityName) + tableFormat.to(CaseFormat.UPPER_CAMEL, originalTableName);
        } else if (split.length == 2) {
            return tableFormat.to(CaseFormat.UPPER_CAMEL, entityName) + tableFormat.to(CaseFormat.UPPER_CAMEL, split[1]);
        }
        return tableFormat.to(CaseFormat.UPPER_CAMEL, entityName) + tableFormat.to(CaseFormat.UPPER_CAMEL, split[2]);
    }

    public String getMapperName(boolean isDefault, String prefix) {
        return getEntityName(prefix) + (isDefault ? "Default" : "") + "Mapper";
    }

    public String getServiceInterfaceName(boolean isDefault, String prefix) {
        return getEntityName(prefix) + (isDefault ? "Default" : "") + "Service";
    }

    public String getServiceImplName(boolean isDefault, String prefix) {
        return getEntityName(prefix) + (isDefault ? "Default" : "") + "ServiceImpl";
    }

    public String getDtoName(String prefix) {
        return getEntityName(prefix) + "Dto";
    }
}
