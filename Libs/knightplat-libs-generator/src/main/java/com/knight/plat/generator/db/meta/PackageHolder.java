package com.knight.plat.generator.db.meta;

public class PackageHolder {

    private final String module;
    private final String basePackage;

    public PackageHolder(String module, String basePackage) {
        this.module = module;
        this.basePackage = basePackage;
    }

    public String getEntityPackage() {
        return basePackage + "." + module + ".persistence.entities";
    }

    public String getMapperPackage(boolean isDefault) {
        return basePackage + (isDefault ? ".commons.defaults." : "." + module + ".persistence.") + "mappers";
    }

    public String getServiceInterfacePackage(boolean isDefault) {
        return basePackage + (isDefault ? ".commons.defaults." : "." + module + ".") + "services";
    }

    public String getServiceImplPackage(boolean isDefault) {
        return getServiceInterfacePackage(isDefault) + ".impl";
    }

    public String getDtoPackage() {
        return basePackage + "." + module + ".dto";
    }

}
