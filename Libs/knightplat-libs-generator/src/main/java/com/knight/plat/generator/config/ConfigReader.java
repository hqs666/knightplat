package com.knight.plat.generator.config;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

public class ConfigReader {

    private static final GeneratorProperties properties;

    static {
        InputStream is = ConfigReader.class.getResourceAsStream("/application.yml");
        properties = new Yaml().loadAs(is, GeneratorProperties.class);
    }

    public static GeneratorProperties get() {
        return properties;
    }
}
