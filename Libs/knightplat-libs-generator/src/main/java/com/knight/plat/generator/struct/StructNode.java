package com.knight.plat.generator.struct;

import org.apache.velocity.VelocityContext;

import java.io.File;


public class StructNode {

    private StructTree tree;

    private NodeType nodeType;

    private String name;

    private String template = "";

    private int parent;

    private boolean overwrite;

    private VelocityContext ctx;

    public StructTree getTree() {
        return tree;
    }

    public void setTree(StructTree tree) {
        this.tree = tree;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getPath() {
        StringBuilder path = new StringBuilder();
        for (StructNode cur = this; cur.getParent() >= -1; cur = tree.getNode(cur.getParent())) {
            String thisPath = cur.getNodeType() == NodeType.DIR
                    ? cur.getName() + File.separator
                    : cur.getName();
            path.insert(0, thisPath);
            if (cur.getParent() == -1) {
                break;
            }
        }
        return path.toString();
    }

    public StructNode addChild(StructNode node) {
        node.setParent(getTree().position(this));
        return this;
    }

    public StructNode addChildren(StructNode... nodes) {
        for (StructNode node : nodes) {
            addChild(node);
        }
        return this;
    }

    public VelocityContext getContext() {
        return ctx;
    }

    public void setContext(VelocityContext ctx) {
        this.ctx = ctx;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }
}
