package com.knight.plat.generator.db;

import com.google.common.base.Joiner;
import com.google.common.collect.Multimap;
import com.knight.plat.generator.db.meta.DatabaseMetaQuery;
import com.knight.plat.generator.db.meta.TableMeta;
import com.knight.plat.generator.struct.StructNode;
import com.knight.plat.generator.ProjectBuilder;
import com.knight.plat.generator.config.GeneratorProperties;
import org.apache.velocity.VelocityContext;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Collection;

public class DbProjectFiles extends ProjectBuilder {

    private final DatabaseMetaQuery databaseMeta;

    public DbProjectFiles(GeneratorProperties properties) {
        super(properties);
        this.databaseMeta = new DatabaseMetaQuery(properties);
    }

    @Override
    public String getTemplatePath() {
        return "db_templates";
    }

    @Override
    public void make() {
        String classBasePath = Joiner.on(File.separator).join(
                "src", "main", "java"
        );
        try {
            Multimap<String, TableMeta> tableMetas = databaseMeta.getTableMetas();
            Collection<TableMeta> metas = tableMetas.values();
            StructNode classParent = createDir(classBasePath);
            System.out.println(classParent.getPath());
            for (TableMeta meta : metas) {
                
                VelocityContext ctx = makeContext();
                ctx.put("meta", meta);
                classParent.addChildren(
                        createDir(meta.getEntityPackage().replace(".", File.separator))
                                .addChild(createFile(meta.getEntityName() + ".java", "domain.java.vm", ctx)),

                        createDir(meta.getDtoPackage().replace(".", File.separator))
                                .addChild(createFile(meta.getDtoName() + ".java", "domainDto.java.vm", ctx)),

                        createDir(meta.getDefaultMapperPackage().replace(".", File.separator))
                                .addChildren(
                                        createFile(meta.getDefaultMapperName() + ".java",
                                                "domainDefaultMapper.java.vm", ctx),
                                        createFile(meta.getDefaultMapperName() + ".xml", "domainDefaultMapper.xml.vm",
                                                ctx)
                                ),
                        createDir(meta.getDefaultServiceInterfacePackage().replace(".", File.separator))
                                .addChild(createFile(meta.getDefaultServiceInterfaceName() + ".java",
                                        "domainDefaultService.java.vm", ctx)),
                        createDir(meta.getDefaultServicePackage().replace(".", File.separator))
                                .addChild(createFile(meta.getDefaultServiceName() + ".java",
                                        "domainDefaultServiceImpl.java.vm", ctx))

                );
                classParent.addChildren(
                        createDir(meta.getMapperPackage().replace(".", File.separator))
                                .addChildren(
                                        createFile(meta.getMapperName() + ".java", "domainMapper.java.vm", ctx,
                                                generateCustoms),
                                        createFile(meta.getMapperName() + ".xml", "domainMapper.xml.vm", ctx,
                                                generateCustoms)
                                ),

                        createDir(meta.getServiceInterfacePackage().replace(".", File.separator))
                                .addChildren(
                                        createFile(meta.getServiceInterfaceName() + ".java",
                                                "domainService.java.vm", ctx, generateCustoms)
                                ),
                        createDir(meta.getServicePackage().replace(".", File.separator))
                                .addChild(createFile(meta.getServiceName() + ".java", "domainServiceImpl.java.vm",
                                        ctx, generateCustoms))
                );
            }

        } catch (InstantiationException | IllegalAccessException | SQLException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public void setGenerateCustoms(boolean generateCustoms) {
        this.generateCustoms = generateCustoms;
    }
}
