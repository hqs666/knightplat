package com.knight.plat.generator.config;

import com.google.common.base.Strings;


public class GeneratorProperties {

    GeneratorProperties() {}

    public GeneratorProperties(DatasourceConfig datasource, GeneratorConfig generator) {
        this.datasource = datasource;
        this.generator = generator;
    }

    public DatasourceConfig datasource;
    public GeneratorConfig generator;

    public static class DatasourceConfig {
        public String name;
        public String url;
        public String username;
        public String password;
        public TableModule[] tables;
    }

    public static class TableModule {
        public String module;
        public String pattern;
        public String prefix;
        public String usePk;
        public String usePlural;

        public boolean isLower = true;

        public String getModule() {
            return module;
        }

        public String getPattern() {
            return pattern;
        }

        public String getPrefix(){
            return prefix;
        }

        public boolean hasPrefix() {
            return !Strings.isNullOrEmpty(prefix);
        }

        public boolean usePk() {
            return "true".equalsIgnoreCase(usePk);
        }

        public boolean usePlural() {
            return !"false".equalsIgnoreCase(usePlural);
        }
    }

    public static class GeneratorConfig {
        public String rootPath = "./tmp";
        public String basePackage = "com.knight.article";
        public String projectName = "article";
        public String platVersion = "1.0.19.78";
        public String[] supporters = new String[0];
        public String type = "web"; // web or service
    }
}
