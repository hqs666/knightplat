package com.knight.plat.generator.db.meta;

public class DBTypeConvertor {

    private static String convertNumber(String digits, int length) {
        if ("0".equals(digits)) {
            if (length > 10) {
                return  "Long";
            } else {
                return "Integer";
            }
        } else {
            return "BigDecimal";
        }
    }

    public static String getJavaType(String dbType, String digits, int length) {
        switch (dbType) {
            case "BIT":
            case "BOOLEAN": return "Boolean";
            case "TINYINT UNSIGNED":
            case "TINYINT": return "Byte";
            case "SMALLINT UNSIGNED":
            case "SMALLINT": return "Short";
            case "INT":
            case "INT UNSIGNED":
            case "INTEGER UNSIGNED":
            case "INTEGER": return "Integer";
            case "BIGINT":
            case "BIGINT UNSIGNED": return "Long";
            case "REAL": return "Float";
            case "NUMERIC":
            case "NUMBER":
            case "FLOAT":
            case "DOUBLE":
            case "DECIMAL": return convertNumber(digits, length);
            case "CHAR":
            case "CLOB":
            case "LONGNVARCHAR":
            case "LONGVARCHAR":
            case "NCHAR":
            case "NCLOB":
            case "NVARCHAR":
            case "LONGTEXT":
            case "MEDIUMTEXT":
            case "TEXT":
            case "VARCHAR": return "String";
            case "DATE":
            case "TIME":
            case "DATETIME":
            case "TIMESTAMP": return "Date";
            case "BINARY":
            case "BLOB":
            case "LONGVARBINARY":
            case "VARBINARY": return "byte[]";
            case "ARRAY":
            case "DATALINK":
            case "DISTINCT":
            case "JAVA_OBJECT":
            case "NULL":
            case "OTHER":
            case "REF":
            case "STRUCT":
            case "ROWID":
            case "SQLXML":
            case "REF_CURSOR": return "Object";
            case "TIME_WITH_TIMEZONE": return "OffsetTime";
            case "TIMESTAMP_WITH_TIMEZONE": return "OffsetDateTime";
            case "JSON": return "JsonRoot";
        }
        System.out.println(dbType);
        return "Object";
    }
}
