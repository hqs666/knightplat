package com.knight.plat.generator.struct;

import com.google.common.collect.Lists;

import java.util.List;


public class StructTree {

    private final List<StructNode> nodes = Lists.newArrayList();
    private StructNode root = null;

    private StructTree() {
    }

    public static StructTree newTree(StructNode root) {
        StructTree tree = new StructTree();
        tree.addRoot(root);
        return tree;
    }

    private void addRoot(StructNode root) {
        root.setParent(-1);
        if (null != this.root) {
            nodes.add(position(this.root), root);
        } else {
            nodes.add(root);
        }
        this.root = root;
        root.setTree(this);
    }

    public int position(StructNode node) {
        if (this != node.getTree()) {
            return -1;
        }
        return nodes.indexOf(node);
    }

    public void add(StructNode node, StructNode parent) {
        node.setTree(this);
        if (null != parent) {
            node.setParent(position(parent));
        } else {
            node.setParent(0);
        }
        nodes.add(node);
    }

    public StructNode getRoot() {
        return root;
    }

    public StructNode getNode(int index) {
        if (index == -1) {
            return null;
        }
        return nodes.get(index);
    }

    public List<StructNode> getNodes() {
        return nodes;
    }
}
