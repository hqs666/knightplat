package com.knight.plat.plugins.generator;

import com.google.common.base.Strings;
import com.knight.plat.generator.config.GeneratorProperties;
import com.knight.plat.generator.config.GeneratorProperties.DatasourceConfig;
import com.knight.plat.generator.config.GeneratorProperties.GeneratorConfig;
import com.knight.plat.generator.config.GeneratorProperties.TableModule;
import com.knight.plat.generator.db.DbProjectFiles;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.Arrays;

@Mojo(name = "gen", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class MojoMain extends AbstractMojo {

    @Parameter(name = "basePackage")
    private String basePackage;

    @Parameter(name = "url")
    private String url;

    @Parameter(name = "userName")
    private String userName;

    @Parameter(name = "password")
    private String password;

    @Parameter(name = "tables")
    private TableModule[] tables;

    @Parameter(name = "basePath")
    private String basePath;

    @Override
    public void execute() throws MojoFailureException {
        if (Strings.isNullOrEmpty(basePath)) {
            basePath = ".";
        }
        Log log = getLog();
        log.info("basePackege: " + basePackage);
        log.info("basePath: " + basePath);
        log.info("url: " + url);
        log.info("userName: " + userName);
        log.info("password: " + password);
        log.info("tables: " + Arrays.toString(tables));

        DatasourceConfig datasource = new DatasourceConfig();
        datasource.tables = tables;
        datasource.url = url;
        datasource.username = userName;
        datasource.password = password;

        GeneratorConfig generatorConfig = new GeneratorConfig();
        generatorConfig.basePackage = basePackage;
        generatorConfig.projectName = "";
        generatorConfig.rootPath = basePath;
        GeneratorProperties properties = new GeneratorProperties(datasource, generatorConfig);

        try {
            DbProjectFiles handler = new DbProjectFiles(properties);
            handler.setGenerateCustoms(false);
            handler.make();
            handler.saveFiles();
        } catch (Exception e) {
            throw new MojoFailureException("Project files generating fail.", e);
        }
    }
}
