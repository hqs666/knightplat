package com.knight.plat.easyexcel.core;

/**
 * 项目名: obei-file-operation
 * 文件名: ExcelDynamicSelect
 * 模块说明:
 * 修改历史:
 * 2022/9/8 - Knight - 创建
 */
public interface ExcelDynamicSelect {
    /**
     * 获取动态生成的下拉框可选数据
     * @return 动态生成的下拉框可选数据
     */
    String[] getSource();


}
