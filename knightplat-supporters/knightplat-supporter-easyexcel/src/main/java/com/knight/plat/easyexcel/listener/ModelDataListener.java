package com.knight.plat.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ModelDataListener<T> extends AnalysisEventListener<T> {

    private int headSize;     //头部大小，一般是指对象的总属性

    private List<T> datas = new ArrayList<>();   // excel文档的内容存储

    /**
     * 逐行解析
     * object : 当前行的数据
     */
    @Override
    public void invoke(T object, AnalysisContext context) {
        if (object != null) {
            datas.add(object);
        }
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        super.invokeHeadMap(headMap, context);
        headSize = headMap.size();
    }

    public int getHeadSize() {
        return headSize;
    }

    public void setHeadSize(int headSize) {
        this.headSize = headSize;
    }

    /**
     * 解析完所有数据后会调用该方法
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        //销毁不用的资源
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }
}