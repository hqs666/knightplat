package com.knight.plat.easyexcel.core;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.knight.plat.easyexcel.annotation.ExcelSelected;
import com.knight.plat.easyexcel.exception.ExcelException;
import com.knight.plat.easyexcel.listener.ModelDataListener;
import com.knight.plat.easyexcel.listener.ValidatorDataListener;
import com.knight.plat.easyexcel.listener.NoModelDataListener;
import com.knight.plat.utils.KeyGenUtils;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

public class ExcelManager {


    /**
     * 简单Excel对象创建填充，仅支持单sheet填充，并指定写文件位置和名称
     *
     * @param filePath  文件下载位置
     * @param fileName  文件名
     * @param clazz     datalist对应的行对象
     * @param sheetName sheet名称
     * @param dataList  行对象集合
     */
    @Deprecated
    public <R, D> void writeWithModel(String filePath, String fileName, Class<R> clazz, String sheetName,
                                      List<D> dataList) {
        String absoluteName = filePath + File.separator + fileName;
        try {
            EasyExcel.write(absoluteName, clazz).sheet(sheetName).doWrite(dataList);
        } catch (ExcelException e) {
            e.getStackTrace();
        }
    }


    /**
     * Excel对象创建填充，仅支持单sheet填充，指定写文件位置和名称并且将含有数据的文件转换成流返回
     * 实现逻辑如下：
     * 1.生成并且写入信息，存入指定位置
     * 2.根据文件绝对路径获取 inputStream,并且删除系统所生成文件
     * 3.将根绝绝对路径获取的输入流返回
     *
     * @param fileName 文件名
     * @param clazz    datalist对应的行对象
     * @param dataList 行对象集合
     * @return 含有数据的 Excel 文件流
     */
    public <R, D> InputStream writeWithModelAlsoReturnStream(String fileName, Class<R> clazz, List<D> dataList) {
        InputStream input = null;
        try {
            File tempFile = File.createTempFile("tempExcel", ".xlsx");
            EasyExcel.write(tempFile, clazz).sheet("sheet1").doWrite(dataList);
            input = getInputStreamByAbsoluteName(tempFile.getAbsolutePath());
        } catch (ExcelException e) {
            e.getStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    /**
     * Excel对象创建填充，多sheet填充
     * 实现逻辑如下：
     * 1.生成并且写入信息，存入指定位置
     * 2.根据文件绝对路径获取 inputStream,并且删除系统所生成文件
     * 3.将根绝绝对路径获取的输入流返回
     *
     * @param sheetNameList sheet名称列表
     * @param clazzList     当前sheet对应存储对象
     * @param dataList      sheet行对象集合
     * @param needHeadList  当前sheet是否需要表头
     * @return 含有数据的 Excel 文件流
     */
    public <R, D> InputStream writeWithModelMultipleSheets(List<String> sheetNameList, List<Class<R>> clazzList, List<List<D>> dataList, List<Boolean> needHeadList) {
        InputStream input = null;
        try {
            File tempFile = File.createTempFile("tempExcel", ".xlsx");
            WriteSheet sheet = null;
            ExcelWriter excelWriter = EasyExcel.write(tempFile).build();
            for (int i = 0; i < clazzList.size(); i++) {
                sheet = EasyExcel.writerSheet(i, sheetNameList.get(i)).needHead(needHeadList.get(i)).head(clazzList.get(i)).build();
                excelWriter.write(dataList.get(i), sheet);
            }
            excelWriter.finish();
            input = getInputStreamByAbsoluteName(tempFile.getAbsolutePath());
        } catch (ExcelException e) {
            e.getStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    /**
     * 方法功能：根据文件名称获取 inputStream,并且删除系统所生成文件
     * 实现逻辑如下：
     * 1.根据绝对名称，将信息取出
     * 2.取出含有信息的excel，将其转换为inputStream
     * 3.删除系统所生成文件
     * 4.返回输入流
     *
     * @param absoluteName 绝对名称
     * @return 根据绝对路径获取文件所转输入流
     */
    private InputStream getInputStreamByAbsoluteName(String absoluteName) {
        InputStream input = null;
        File file = null;
        try {
            file = new File(absoluteName);
            input = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            file.delete();
        }
        return input;
    }

    /**
     * 简单Excel集合创建填充，仅支持单sheet填充，并指定下载位置和名称
     *
     * @param filePath  文件读取位置
     * @param fileName  文件名
     * @param sheetName sheet名称
     * @param headList  行头集合
     * @param dataList  行集合
     */
    @Deprecated
    public void writeWithoutModel(String filePath, String fileName, String sheetName,
                                  List<List<String>> headList, List<List<Object>> dataList) {
        String absoluteName = filePath + File.separator + fileName;
        try {
            EasyExcel.write(absoluteName).head(headList).sheet(sheetName).doWrite(dataList);
        } catch (ExcelException e) {
            e.getStackTrace();
        }
    }


    /**
     * Excel对象创建填充，仅支持单sheet填充，指定写文件位置和名称并且将含有数据的文件转换成流返回
     * 实现逻辑如下：
     * 1.生成并且写入信息，存入指定位置
     * 2.根据文件绝对路径获取 inputStream,并且删除系统所生成文件
     * 3.将根绝绝对路径获取的输入流返回
     *
     * @param fileName 文件名
     * @param headList 行头集合
     * @param dataList 行集合
     * @return 含有数据的 Excel 文件流
     */
    public InputStream writeWithoutModelAlsoReturnStream(String fileName, List<List<String>> headList, List<List<Object>> dataList) {
        InputStream input = null;
        try {
            File tempFile = File.createTempFile("tempExcel", ".xlsx");
            EasyExcel.write(tempFile).head(headList).sheet("sheet1").doWrite(dataList);
            input = getInputStreamByAbsoluteName(tempFile.getAbsolutePath());
        } catch (ExcelException | IOException e) {
            e.getStackTrace();
        }
        return input;
    }

    /**
     * Excel对象创建填充，多sheet填充
     * 实现逻辑如下：
     * 1.生成并且写入信息，存入指定位置
     * 2.根据文件绝对路径获取 inputStream,并且删除系统所生成文件
     * 3.将根绝绝对路径获取的输入流返回
     *
     * @param sheetNameList sheetName列表
     * @param headList      不同sheet行头集合
     * @param dataList      不同sheet行集合
     * @param needHeadList  是否需要表头
     * @return 含有数据的 Excel 文件流
     */
    public <T> InputStream writeWithoutModelMultipleSheets(List<String> sheetNameList, List<List<List<String>>> headList, List<List<List<T>>> dataList, List<Boolean> needHeadList) {
        InputStream input = null;
        try {
            File tempFile = File.createTempFile("tempExcel", ".xlsx");
            ExcelWriter excelWriter = EasyExcel.write(tempFile).build();
            WriteSheet sheet1 = null;
            for (int i = 0; i < sheetNameList.size(); i++) {
                sheet1 = EasyExcel.writerSheet(i, sheetNameList.get(i)).head(headList.get(i)).needHead(needHeadList.get(i)).build();
                excelWriter.write(dataList.get(i), sheet1);
            }
            excelWriter.finish();
            input = getInputStreamByAbsoluteName(tempFile.getAbsolutePath());
        } catch (ExcelException | IOException e) {
            e.getStackTrace();
        }
        return input;
    }

    /**
     * Excel无对象结构读取，转换成二维集合
     *
     * @param filePath 文件读取位置
     * @param fileName 文件名
     */
    public List<Map<String, Object>> readResultMap(String filePath, String fileName) {
        String absoluteName = filePath + File.separator + fileName;
        NoModelDataListener noModelDataListener = new NoModelDataListener();
        try {
            EasyExcel.read(absoluteName, noModelDataListener).doReadAll();
        } catch (ExcelException e) {
            e.getStackTrace();
        }
        return noModelDataListener.getList();
    }

    /**
     * Excel无对象结构读取，转换成二维集合
     *
     * @param fileName 文件名
     */
    public List<Map<String, Object>> readResultMapByInputStream(InputStream inputStream, String fileName) {
        NoModelDataListener noModelDataListener = new NoModelDataListener();
        try {
            EasyExcel.read(inputStream, noModelDataListener).doReadAll();
        } catch (ExcelException e) {
            e.getStackTrace();
        }
        return noModelDataListener.getList();
    }

    /**
     * Excel对象结构读取，转换成对象集合
     *
     * @param filePath 文件读取位置
     * @param fileName 文件名
     */
    public <R> List<Object> readResultModel(String filePath, String fileName, Class<R> clazz) {
        String absoluteName = filePath + File.separator + fileName;
        ModelDataListener modelDataListener = new ModelDataListener();
        try {
            EasyExcel.read(absoluteName, clazz, modelDataListener).doReadAll();
        } catch (ExcelException e) {
            e.getStackTrace();
        }
        return modelDataListener.getDatas();
    }

    /**
     * Excel对象结构读取，转换成对象集合
     *
     * @param filePath 文件读取位置
     * @param fileName 文件名
     * @return
     */
    public <R> Map<String, List<Object>> readResultModelValidator(String filePath, String fileName, Class<R> clazz, Integer sheetNo) {
        String absoluteName = filePath + File.separator + fileName;
        ExcelReader excelReader = null;
        ValidatorDataListener validatorDataListener = new ValidatorDataListener();
        try {
            InputStream inputStream = new BufferedInputStream(new FileInputStream(absoluteName));
            excelReader = EasyExcel.read(inputStream, clazz, validatorDataListener).build();
            ReadSheet readSheet = EasyExcel.readSheet(sheetNo).build();
            excelReader.read(readSheet);
        } catch (ExcelException e) {
            e.getStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            excelReader.finish();
        }
        return validatorDataListener.getResultMap();
    }

    /**
     * 对象格式自定义监听器读取excel
     * @param inputStream excel文件流
     * @param clazz excel字段对象类型
     * @param sheetNo 读取页脚号
     * @param listener 自定义监听器，需要实现BaseDataListener
     * @param <R> excel字段对象类型
     * @return
     */
    public <R> List<R> readResultModelSelfDefinedListener(InputStream inputStream, Class<R> clazz, Integer sheetNo, ModelDataListener listener) {
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(inputStream, clazz, listener).build();
            ReadSheet readSheet = EasyExcel.readSheet(sheetNo).build();
            excelReader.read(readSheet);
        } catch (ExcelException e) {
            e.getStackTrace();
        } finally {
            excelReader.finish();
        }
        return listener.getDatas();
    }

    /**
     * 对象格式自定义监听器读取excel
     * @param <R> excel字段对象类型
     * @param inputStream excel文件流
     * @param sheetNo 读取页脚号
     * @param listener 自定义监听器，需要实现BaseDataListener
     * @return
     */
    public <R> List<Map<String, Object>> readResultNoModelSelfDefinedListener(InputStream inputStream, Integer sheetNo, NoModelDataListener listener) {
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(inputStream,listener).build();
            ReadSheet readSheet = EasyExcel.readSheet(sheetNo).build();
            excelReader.read(readSheet);
        } catch (ExcelException e) {
            e.getStackTrace();
        } finally {
            excelReader.finish();
        }
        return listener.getList();
    }

    /**
     * Excel对象结构读取，转换成对象集合
     *
     * @param fileName 文件名
     */
    public <R> List<Object> readResultModelByInputStream(InputStream inputStream, String fileName, Class<R> clazz) {
        ModelDataListener modelDataListener = new ModelDataListener();
        try {
            EasyExcel.read(inputStream, clazz, modelDataListener).doReadAll();
        } catch (ExcelException e) {
            e.getStackTrace();
        }
        return modelDataListener.getDatas();
    }

    /**
     * 创建一个空Excel文件
     *
     * @param filePath 创建路径
     * @param fileName 创建名称
     */
    public void createNullExcel(String filePath, String fileName) {
        //文件输出位置
        String absoluteName = filePath + File.separator + fileName;
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        EasyExcel.write(absoluteName).build().finish();
    }

    /**
     * 根据参数只导出指定列的数据，对应到对象中的字段
     *
     * @param fileName       文件名
     * @param filePath       文件路径
     * @param excludeColumns 不包含列
     * @param objectClass    对象类
     * @param sheetName      sheet名称
     * @param data           填充数据
     */
    public <R, D> void excludeWrite(String filePath, String fileName, List<String> excludeColumns,
                                    Class<R> objectClass, String sheetName, List<D> data) {
        String file = filePath + File.separator + fileName;

        // 根据用户传入字段 假设我们要忽略 date
        Set<String> excludeColumnFiledNames = new HashSet<>(excludeColumns);
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        EasyExcel.write(file, objectClass).excludeColumnFiledNames(excludeColumnFiledNames).sheet(sheetName)
                .doWrite(data);
    }

    /**
     * 根据参数只导出指定列的数据，对应到对象中的字段
     *
     * @param fileName       文件名
     * @param filePath       文件路径
     * @param includeColumns 不包含列
     * @param objectClass    对象类
     * @param sheetName      sheet名称
     * @param data           填充数据
     */
    public <R, D> void includeWrite(String filePath, String fileName, List<String> includeColumns,
                                    Class<R> objectClass, String sheetName, List<D> data) {
        String file = filePath + File.separator + fileName;
        Set<String> includeColumnFiledNames = new HashSet<>(includeColumns);
        EasyExcel.write(file, objectClass).includeColumnFiledNames(includeColumnFiledNames).sheet(sheetName)
                .doWrite(data);
    }

    /**
     * 朝模板excel中填充数据
     *
     * @param filePath     目标文件路径
     * @param fileName     目标文件名称
     * @param data         对象数据
     * @param map          Map数据
     * @param tempFilePath 模板文件路径
     * @param tempFileName 模板文件名称
     */
    public void fillExcel(String filePath, String fileName, List<Object> data, Map<String, Object> map,
                          String tempFilePath, String tempFileName) {
        ExcelWriter excelWriter = EasyExcel.write(filePath + File.separator + fileName)
                .withTemplate(tempFilePath + tempFileName).build();
        WriteSheet writeSheet = EasyExcel.writerSheet().build();
        FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
        excelWriter.fill(data, fillConfig, writeSheet);
        excelWriter.fill(map, writeSheet);
        excelWriter.finish();
    }

    /**
     * 创建即将导出的sheet页（sheet页中含有带下拉框的列）
     * @param head 导出的表头信息和配置
     * @param sheetNo sheet索引
     * @param sheetName sheet名称
     * @param <T> 泛型
     * @param data 行数据
     * @return sheet页
     */
    public  <T> InputStream writeSelectedSheet(Class<T> head, Integer sheetNo, String sheetName,List<T> data) {

        InputStream input = null;
        File tempFile = null;
        try {
            tempFile = File.createTempFile(KeyGenUtils.newUuid(), ".xlsx");
            EasyExcel.write(tempFile)
                    .build()
                    .write(data,ExcelManager.writeSelectedSheet(head, sheetNo, sheetName))
                    .finish();
            input = getInputStreamByAbsoluteName(tempFile.getAbsolutePath());
        }catch (IOException e){
            e.getStackTrace();
        }finally {
            if (tempFile != null && tempFile.exists()){
                tempFile.delete();
            }
        }
        return input;


    }

    /**
     * 创建即将导出的sheet页（sheet页中含有带下拉框的列）
     * @param head 导出的表头信息和配置
     * @param sheetNo sheet索引
     * @param sheetName sheet名称
     * @param <T> 泛型
     * @param data 行数据
     * @return file 临时文件使用完成后注意删除
     */
    public  <T> File writeSelectedSheetFile(Class<T> head, Integer sheetNo, String sheetName,List<T> data,String absolutePath) {


        File targetFile = null;
        try {
            if (absolutePath != null && new File(absolutePath).exists()){
                File originFile = new File(absolutePath);
                targetFile = File.createTempFile(KeyGenUtils.newUuid(), ".xlsx");
                ExcelWriter excelWriter = EasyExcel.write(targetFile, head).needHead(true).withTemplate(originFile).file(targetFile).build();
                WriteSheet writeSheet = ExcelManager.writeSelectedSheet(head, sheetNo, sheetName);
                excelWriter.write(data,writeSheet).finish();
            }else {
                targetFile = File.createTempFile(KeyGenUtils.newUuid(), ".xlsx");
                EasyExcel.write(targetFile, head)
                        .build()
                        .write(data,ExcelManager.writeSelectedSheet(head, sheetNo, sheetName))
                        .finish();
            }
        }catch (IOException e){
            e.getStackTrace();
        }
        return targetFile;
    }

    /**
     * 创建即将导出的sheet页（sheet页中含有带下拉框的列）
     * @param head 导出的表头信息和配置
     * @param sheetNo sheet索引
     * @param sheetName sheet名称
     * @param <T> 泛型
     * @return sheet页
     */
    public static <T> WriteSheet writeSelectedSheet(Class<T> head, Integer sheetNo, String sheetName) {
        Map<Integer, ExcelSelectedResolve> selectedMap = resolveSelectedAnnotation(head);

        return EasyExcel.writerSheet(sheetNo, sheetName)
                .head(head)
                .registerWriteHandler(new SelectedSheetWriteHandler(selectedMap))
                .build();
    }

    /**
     * 解析表头类中的下拉注解
     * @param head 表头类
     * @param <T> 泛型
     * @return Map<下拉框列索引, 下拉框内容> map
     */
    private static <T> Map<Integer, ExcelSelectedResolve> resolveSelectedAnnotation(Class<T> head) {
        Map<Integer, ExcelSelectedResolve> selectedMap = new HashMap<>();

        // getDeclaredFields(): 返回全部声明的属性；getFields(): 返回public类型的属性
        Field[] fields = head.getDeclaredFields();
        for (int i = 0; i < fields.length; i++){
            Field field = fields[i];
            // 解析注解信息
            ExcelSelected selected = field.getAnnotation(ExcelSelected.class);
            ExcelProperty property = field.getAnnotation(ExcelProperty.class);
            if (selected != null) {
                ExcelSelectedResolve excelSelectedResolve = new ExcelSelectedResolve();
                String[] source = excelSelectedResolve.resolveSelectedSource(selected);
                if (source != null && source.length > 0){
                    excelSelectedResolve.setSource(source);
                    excelSelectedResolve.setFirstRow(selected.firstRow());
                    excelSelectedResolve.setLastRow(selected.lastRow());
                    if (property != null && property.index() >= 0){
                        selectedMap.put(property.index(), excelSelectedResolve);
                    } else {
                        selectedMap.put(i, excelSelectedResolve);
                    }
                }
            }
        }

        return selectedMap;
    }

}
