package com.knight.plat.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.knight.plat.easyexcel.core.ExcelValidator;
import com.knight.plat.utils.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidatorDataListener extends AnalysisEventListener<Object> {

    Logger logger = LoggerFactory.getLogger("ValidatorDataListener.class");
    private int headSize;     //头部大小，一般是指对象的总属性

    private List<Object> dataList = new ArrayList<>();   // excel文档的内容存储
    private List<Object> errorDataList = new ArrayList<>();   // excel文档的内容存储


    private Map<String, List<Object>> resultMap = new HashMap<>();   // excel文档的内容存储

    /**
     * 逐行解析
     * object : 当前行的数据
     */
    @Override
    public void invoke(Object object, AnalysisContext context) {
        String message = null;
        try {
            message = ExcelValidator.validateEntity(object);
        } catch (NoSuchFieldException e) {
            logger.error("无Field错误:{}" + message);
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            logger.error("非法数据类型错误:{}" + message);
            e.printStackTrace();
        }
        if (null != message && !message.isEmpty()) {
            logger.error("数据校验错误:" + message);
            errorDataList.add(SerializationUtils.jsonSerialize(object) + message);
        } else if (object != null) {
            dataList.add(object);
        }
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        super.invokeHeadMap(headMap, context);
        headSize = headMap.size();
    }

    public int getHeadSize() {
        return headSize;
    }

    public void setHeadSize(int headSize) {
        this.headSize = headSize;
    }

    /**
     * 解析完所有数据后会调用该方法
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        resultMap.put("data", dataList);
        resultMap.put("error", errorDataList);

        //销毁不用的资源
    }

    public List<Object> getDataList() {
        return dataList;
    }

    public void setDataList(List<Object> dataList) {
        this.dataList = dataList;
    }

    public List<Object> getErrorDataList() {
        return errorDataList;
    }

    public void setErrorDataList(List<Object> errorDataList) {
        this.errorDataList = errorDataList;
    }

    public Map<String, List<Object>> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, List<Object>> resultMap) {
        this.resultMap = resultMap;
    }
}