package com.knight.plat.easyexcel;

import com.knight.plat.easyexcel.core.ExcelManager;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

/**
 * Created by LeLe on 2023/12/26.
 */
@AutoConfiguration
public class ExcelAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public ExcelManager excelManager() {
        return new ExcelManager();
    }
}
