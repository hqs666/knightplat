package com.knight.plat.ftp;

import com.knight.plat.api.filesystem.FileChannel;

public class FtpFile implements FileChannel {
    @Override
    public String getName() {
        return "ftp";
    }

    private FtpFile() {
    }

    private static final FtpFile ftpFile = new FtpFile();

    public static FtpFile channel() {
        return ftpFile;
    }

}
