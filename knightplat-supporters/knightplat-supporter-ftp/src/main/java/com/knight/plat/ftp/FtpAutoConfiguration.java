package com.knight.plat.ftp;

import com.knight.plat.api.filesystem.FileChannelPair;
import io.vertx.core.Vertx;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
@EnableConfigurationProperties(FtpProperties.class)
public class FtpAutoConfiguration {

    private final FtpProperties ftpProperties;
    private final Vertx vertx;

    public FtpAutoConfiguration(FtpProperties ftpProperties, Vertx vertx) {
        this.ftpProperties = ftpProperties;
        this.vertx = vertx;
    }


    @Bean
    public FileChannelPair FtpChannel() {
        return new FileChannelPair(new FtpFileManger(ftpProperties, vertx), FtpFile.channel().getName());
    }


//    @Bean(destroyMethod = "close")
//    public FtpFileManger ftpFileManger() {
//        return new FtpFileManger(ftpProperties);
//    }
}
