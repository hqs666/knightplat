package com.knight.plat.ftp;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("knightplat.ftp")
public class FtpProperties {

    private String encoding = System.getProperty("file.encoding");
    private String ip = "10.60.218.141";
    private int port = 21;
    private String user = "ftpuser1";
    private String password = "666666";


    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
