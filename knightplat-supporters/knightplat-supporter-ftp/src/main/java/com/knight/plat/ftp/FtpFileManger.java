package com.knight.plat.ftp;

import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.filesystem.FileManager;
import com.knight.plat.api.filesystem.Part;
import io.vertx.core.Vertx;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class FtpFileManger implements FileManager {

    private static final Logger logger = LoggerFactory.getLogger(FtpFileManger.class);

    private static final String encoding = System.getProperty("file.encoding");


    FTPClient ftpClient = new FTPClient();
    private final Vertx vertx;

    public FtpFileManger(FtpProperties ftpProperties, Vertx vertx) {
        this.vertx = vertx;
        initClient(ftpProperties);
    }

    public FTPClient initClient(FtpProperties properties) {
        int reply;
        try {
            ftpClient.connect(properties.getIp(), properties.getPort());
            ftpClient.login(properties.getUser(), properties.getPassword());
            ftpClient.setControlEncoding(encoding);
            ftpClient.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // 检验是否连接成功
            reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                logger.info("连接失败");
                ftpClient.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("==========FTP client 连接成功==========");
        return ftpClient;
    }


    @Override
    public String save(InputStream data, String bucket, String path) throws IOException {
        boolean b = ftpClient.storeFile(buildPath(bucket, path), data);

        logger.info("ftp保存结果：{}, ftp 应答码：{}", b, ftpClient.getReplyCode());
        data.close();
        return bucket + "/" + path;
    }

    @Override
    public String save(InputStream data, long contentLength, String bucket, String path) throws IOException {
        // TODO:长度处理
        return save(data, bucket, path);
    }

    @Override
    public void saveAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        vertx.<String>executeBlocking(promise-> CompletableFuture.supplyAsync(()-> {
            String res = null;
            try {
                res = save(data, bucket, path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;
        }).whenComplete((r, err)-> {
            if (r != null) {
                promise.complete(r);
            } else {
                promise.fail(err);
            }
        }), result-> {
            if (result.succeeded()) {
                callback.onSuccess(result.result());
            } else {
                throw new RuntimeException("ftp 异步文件上传异常", result.cause());
            }
        });
    }

    @Override
    public void saveAsync(InputStream data, long contentLength, String bucket, String path,
            Callback<String, IOException> callback) {
        // TODO:长度处理
        saveAsync(data, bucket, path, callback);
    }

    @Override
    public String append(InputStream data, String bucket, String path) throws IOException {
        String size = ftpClient.getSize(buildPath(bucket, path));

        if (size == null) {
            logger.info("文件不存在， 执行上传....");
            return save(data, bucket, path);
        }

        //续传
        int serverAvailable = Integer.parseInt(size);
        int localAvailable = data.available();
        if (serverAvailable < localAvailable) {
            double l = (double) serverAvailable / (double) localAvailable * 100.0;
            logger.info("文件大小：{} ， 已上传文件大小：{}, 上传进度{}%", localAvailable, serverAvailable, (float) l);

            data.skip(serverAvailable);
            boolean b = ftpClient.appendFile(buildPath(bucket, path), data);
            boolean b1 = ftpClient.completePendingCommand();

            logger.info("文件续传结束 结果：{}", b&b1);

            return bucket + "/" + path;
        }

        logger.warn("文件已存在");
        return bucket + "/" + path;
    }

    @Override
    public void appendAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        vertx.<String>executeBlocking(promise-> CompletableFuture.supplyAsync(()-> {
            String finalPath = null;
            try {
                finalPath = append(data, bucket, path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return finalPath;
        }).whenComplete((r, err)-> {
            if (r != null) {
                promise.complete(r);
            } else {
                promise.fail(err);
            }
        }), result-> {
            if (result.succeeded()) {
                callback.onSuccess(result.result());
            } else {
                throw new RuntimeException("ftp 异步文件续传异常", result.cause());
            }
        });

    }


    @Override
    public byte[] read(String bucket, String path) throws IOException {
        InputStream inputStream = readAsInputStream(bucket, path);

        if (inputStream == null) {
            return new byte[0];
        }

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] bytes;
        bytes = new byte[4096];
        int len;
        while ((len = inputStream.read(bytes)) != -1) {
            bao.write(bytes,0, len);
        }

        inputStream.close();

        return bao.toByteArray();
    }

    @Override
    public long size(String bucket, String path) throws IOException {
        return ftpClient.size(buildPath(bucket, path));
    }

    @Override
    public InputStream readAsInputStream(String bucket, String path) throws IOException {

        return ftpClient
                .retrieveFileStream(buildPath(bucket, path));
    }

    @Override
    public void readAsync(String bucket, String path, Callback<byte[], IOException> callback) {
        vertx.<byte[]>executeBlocking(promise-> CompletableFuture.supplyAsync(()-> {
            byte[] read = new byte[0];
            try {
                read = read(bucket, path);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return read;
        }).whenComplete((r, err)-> {
            if (r != null) {
                promise.complete(r);
            } else {
                promise.fail(err);
            }
        }), result-> {
            if (result.succeeded()) {
                callback.onSuccess(result.result());
            } else {
                throw new RuntimeException("ftp 异步读取文件流异常", result.cause());
            }
        });
    }

    @Override
    public void readAsInputStreamAsync(String bucket, String path, Callback<InputStream, IOException> callback) {
        vertx.<InputStream>executeBlocking(promise-> CompletableFuture.supplyAsync(()-> {
            InputStream inputStream = null;
            try {
                inputStream = readAsInputStream(bucket, path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return inputStream;

        }).whenComplete((r, err)-> {
           if (r != null) {
               promise.complete(r);
           } else {
               promise.fail(err);
           }
        }), result-> {
            if (result.succeeded()) {
                callback.onSuccess(result.result());
            } else {
                throw new RuntimeException("ftp 异步读取文件流异常", result.cause());
            }
        });
    }

    @Override
    public byte[] read(String bucket, String path, String params) throws IOException {
        return read(bucket, path);
    }

    @Override
    public void readAsync(String bucket, String path, String params, Callback<byte[], IOException> callback) {
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public String createMultipartUpload(String bucket, String path) {
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public Part multipartUpload(String bucket, String path, Integer chunk, InputStream data, String uploadId) throws IOException {
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public String completeMultipartUpload(String bucket, String path, List<Part> parts, String uploadId) throws IOException {
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public String getExpireUrl(String bucketName, String key, Long expireMillisecond) {
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public boolean dropFile(String bucket, String path) throws IOException {
        return ftpClient.deleteFile(buildPath(bucket, path));
    }

    @Override
    public void dropFileAsync(String bucket, String path , Callback<Boolean, IOException> callback){
        vertx.<Boolean>executeBlocking(promise-> CompletableFuture.supplyAsync(()-> {
            boolean b = false;
            try {
                b = dropFile(bucket, path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return b;
        }).whenComplete((r, err)-> {
            if (r != null) {
                promise.complete(r);
            } else {
                promise.fail(err);
            }
        }), res-> {
           if (res.succeeded()) {
                callback.onSuccess(res.result());
           } else {
               throw new RuntimeException("异步删除文件异常", res.cause());
           }
        });
    }

    public void close() {
        try {
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String buildPath(String bucket, String path) {
        return new String((bucket + "/" + path).getBytes(), StandardCharsets.ISO_8859_1);
    }

}
