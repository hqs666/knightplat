package com.knight.plat.job;

import com.google.common.base.Strings;
import com.knight.plat.api.job.JobLogger;
import com.knight.plat.api.job.JobStrategyPair;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.executor.XxlJobExecutor;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import com.xxl.job.core.handler.IJobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
@EnableConfigurationProperties({XxlJobProperties.class, OldXxlJobProperties.class})
public class XxlJobAutoConfiguration {

    private final Logger logger = LoggerFactory.getLogger(XxlJobAutoConfiguration.class);

    private final XxlJobProperties jobProperties;

    // 兼容老的配置格式
    private final OldXxlJobProperties oldJobProperties;

    public XxlJobAutoConfiguration(XxlJobProperties jobProperties,
            OldXxlJobProperties oldJobProperties) {
        this.jobProperties = jobProperties;
        this.oldJobProperties = oldJobProperties;
    }

    @Bean
    public JobStrategyPair schemaStrategyPair() {
        return new JobStrategyPair("xxl",
                ((name, jobHandler, logger) -> XxlJobExecutor.registJobHandler(name, new IJobHandler() {
            @Override
            public void execute() {
                final String params = XxlJobHelper.getJobParam();
                try {
                    jobHandler.loggerContainer.set(logger);
                    jobHandler.execute(params);
                    XxlJobHelper.handleSuccess();
                } catch (Exception e) {
                    XxlJobHelper.handleFail(e.getMessage());
                } finally {
                    jobHandler.loggerContainer.remove();
                }
            }
        })), new JobLogger() {
            @Override
            public void log(String pattern, Object... args) {
                XxlJobHelper.log(pattern, args);
            }

            @Override
            public void log(Throwable throwable) {
                XxlJobHelper.log(throwable);
            }
        });
    }

    @Bean(destroyMethod = "destroy")
    public XxlJobSpringExecutor xxlJobExecutor() {
        logger.info(">>>>>>>>>>> xxl-job config init.");
        final XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(getAdminAddress());
        xxlJobSpringExecutor.setAppname(getAppName());
        xxlJobSpringExecutor.setIp(getIp());
        xxlJobSpringExecutor.setPort(getPort());
        xxlJobSpringExecutor.setAccessToken(getAccessToken());
        xxlJobSpringExecutor.setLogPath(getLogPath());
        xxlJobSpringExecutor.setLogRetentionDays(getLogRetentionDays());

        return xxlJobSpringExecutor;
    }

    private String getAdminAddress() {
        final String adminAddress = oldJobProperties.getAdmin().getAddress();
        return Strings.isNullOrEmpty(adminAddress) ? jobProperties.getAdminAddress() : adminAddress;
    }

    private String getAppName() {
        final String appName = oldJobProperties.getAppName();
        return Strings.isNullOrEmpty(appName) ? jobProperties.getAppName() : appName;
    }

    private String getIp() {
        final String ip = oldJobProperties.getIp();
        return Strings.isNullOrEmpty(ip) ? jobProperties.getIp() : ip;
    }

    private int getPort() {
        final int port = oldJobProperties.getPort();
        return port < 0 ? jobProperties.getPort() : port;
    }

    private String getAccessToken() {
        final String accessToken = oldJobProperties.getAccessToken();
        return Strings.isNullOrEmpty(accessToken) ? jobProperties.getAccessToken() : accessToken;
    }

    private String getLogPath() {
        final String logPath = oldJobProperties.getLogPath();
        return Strings.isNullOrEmpty(logPath) ? jobProperties.getLogPath() : logPath;
    }

    private int getLogRetentionDays() {
        final int logRetentionDays = oldJobProperties.getLogRetentionDays();
        return logRetentionDays < 0 ? jobProperties.getLogRetentionDays() : logRetentionDays;
    }
}
