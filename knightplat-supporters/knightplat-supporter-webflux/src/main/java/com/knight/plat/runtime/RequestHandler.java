package com.knight.plat.runtime;

import com.knight.plat.api.commons.Cookie;
import com.knight.plat.api.runtime.Context;
import com.knight.plat.api.runtime.Deployment;
import com.knight.plat.async.AsyncManager;
import org.springframework.http.HttpCookie;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.time.format.DateTimeFormatter.RFC_1123_DATE_TIME;

public class RequestHandler {

    private final Deployment deployment;
    private final AsyncManager asyncManager;

    RequestHandler(Deployment deployment, AsyncManager asyncManager) {
        this.deployment = deployment;
        this.asyncManager = asyncManager;
    }

    Mono<ResponseEntity<?>> handle(ServerWebExchange exchange, @RequestBody(required = false) String body) {
        final ServerHttpRequest request = exchange.getRequest();
        // 获取请求cookies
        final List<Cookie> cookies = new ArrayList<>();
        for (List<HttpCookie> rawList : request.getCookies().values()) {
            for (HttpCookie raw : rawList) {
                cookies.add(convertCookie(raw));
            }
        }
        // 构建上下文对象
        final Context context = Context.builder()
                .body(body)
                .headers(request.getHeaders())
                .cookies(cookies)
                .uri(request.getURI())
                .build();
        // 线程池中异步执行function逻辑
        final Mono<Context> bodyMono = asyncManager.run(() -> deployment.call(context));
        // 处理后的Mono映射到返回体对象
        return bodyMono.map(handledContext -> {
            final Map<String, List<String>> setHeaders = deployment.fetchSetHeaders(handledContext);
            final List<Cookie> setCookies = deployment.fetchSetCookies(handledContext);
            final Object resBody = deployment.fetchResponseBody(handledContext);
            final BodyBuilder resBuilder = ResponseEntity.ok();
            // 设置返回头
            boolean hasDateHeader = false;
            boolean hasConnection = false;
            if (!setHeaders.isEmpty()) {
                for (Entry<String, List<String>> entry : setHeaders.entrySet()) {
                    String name = entry.getKey();
                    List<String> values = entry.getValue();
                    if ("Date".equalsIgnoreCase(name)) {
                        hasDateHeader = true;
                    }
                    if ("Connection".equalsIgnoreCase(name)) {
                        hasConnection = true;
                    }
                    resBuilder.header(name, values.toArray(new String[0]));
                }
            }
            if (!hasDateHeader) {
                resBuilder.header("Date", ZonedDateTime.now().format(RFC_1123_DATE_TIME));
            }
            if (!hasConnection) {
                resBuilder.header("Connection", "keep-alive");
            }
            // 设置返回cookies
            if (!setCookies.isEmpty()) {
                setCookies.forEach(cookie -> {
                    final String cookieHeader = ResponseCookie.from(cookie.getName(), cookie.getValue())
                            .domain(cookie.getDomain())
                            .httpOnly(cookie.isHttpOnly())
                            .maxAge(cookie.getMaxAge())
                            .path(cookie.getPath())
                            .secure(cookie.isSecure())
                            .sameSite(cookie.getSameSite())
                            .build().toString();
                    resBuilder.header("Set-Cookie", cookieHeader);
                });
            }
            return resBuilder.body(resBody);
        });
    }

    private Cookie convertCookie(HttpCookie rawCookie) {
        if (rawCookie == null) {
            return null;
        }

        final Cookie cookie = new Cookie();
        cookie.setName(rawCookie.getName());
        cookie.setValue(rawCookie.getValue());
        return cookie;
    }
}
