package com.knight.plat.commons;

import com.knight.plat.commons.filters.XssFilter;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.web.server.WebFilter;

@AutoConfiguration
public class CommonAutoConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "knightplat.filter.xss", name = "enabled", havingValue = "true", matchIfMissing = true)
    public WebFilter xssFilter() {
        return new XssFilter();
    }
}
