package com.knight.plat.runtime;

import com.knight.plat.api.commons.HttpMethod;
import com.knight.plat.api.runtime.Deployment;
import com.knight.plat.api.runtime.ServiceRegister;
import com.knight.plat.async.AsyncManager;
import com.knight.plat.utils.IoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.result.method.RequestMappingInfo;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.server.ServerWebExchange;

import java.util.HashMap;
import java.util.Map;

public class ServiceRegisterImpl implements ServiceRegister {

    private static final Logger log = LoggerFactory.getLogger(ServiceRegisterImpl.class);

    private final Map<RequestMappingInfo, Deployment> deploymentMap = new HashMap<>();

    private final RequestMappingHandlerMapping handlerMapping;
    private final AsyncManager asyncManager;

    public ServiceRegisterImpl(
            RequestMappingHandlerMapping handlerMapping, AsyncManager asyncManager) {
        this.handlerMapping = handlerMapping;
        this.asyncManager = asyncManager;
    }

    @Override
    public void addService(String path, HttpMethod method, Deployment deployment, Map<String, Object> globalMap) {
        final RequestMappingInfo mappingInfo = makeMappingInfo(path, method);
        try(Deployment existsDep = deploymentMap.remove(mappingInfo)) {
            if (existsDep != null) {
                log.warn("Duplicated path: {} replacing", path);
                handlerMapping.unregisterMapping(mappingInfo);
            }
        } catch (Exception e) {
            log.warn("Function Runtime Deployment closing failed when replacing function, path: " + path, e);
        }
        deploymentMap.put(mappingInfo, deployment.start(globalMap));
        try {
            handlerMapping.registerMapping(
                    mappingInfo,
                    new RequestHandler(deployment, asyncManager),
                    RequestHandler.class.getDeclaredMethod("handle", ServerWebExchange.class, String.class));
        } catch (NoSuchMethodException e) {
            throw new Error("Cannot find method 'handle' on com.knight.plat.runtime.ServiceRegisterImpl.RequestHandler, please check your lib version.");
        }
    }

    @Override
    public void removeService(String path, HttpMethod method) {
        final RequestMappingInfo mappingInfo = makeMappingInfo(path, method);
        try(Deployment ignored = deploymentMap.remove(mappingInfo)) {
            handlerMapping.unregisterMapping(mappingInfo);
        } catch (Exception e) {
            log.warn("Function Runtime Deployment closing failed when replacing function, path: " + path, e);
        }
    }

    @Override
    public void close() {
        if (!deploymentMap.isEmpty()) {
            deploymentMap.values().forEach(IoUtils::closeQuietly);
        }
    }

    private RequestMappingInfo makeMappingInfo(String path, HttpMethod method) {
        return RequestMappingInfo
                .paths(path)
                .methods(convertMethod(method))
                .build();
    }
}
