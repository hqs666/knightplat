/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.annotation;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.reactive.result.condition.PatternsRequestCondition;
import org.springframework.web.reactive.result.method.RequestMappingInfo;
import org.springframework.web.server.ServerWebExchange;

import java.util.Map;
import java.util.Set;

/**
 * Created by LeLe on 2023/12/3.
 */
public class SkipXssGetter {

    private final Map<RequestMappingInfo, HandlerMethod> ignoreAuthList;

    private final Set<PatternsRequestCondition> ignoreAuthUrls;

    public SkipXssGetter(Map<RequestMappingInfo, HandlerMethod> ignoreAuthList,
            Set<PatternsRequestCondition> ignoreAuthUrls) {
        this.ignoreAuthUrls = ignoreAuthUrls;
        this.ignoreAuthList = ignoreAuthList;
    }

    public Map<RequestMappingInfo, HandlerMethod> get() {
        return ignoreAuthList;
    }

    public boolean isSkip(ServerWebExchange exchange) {
        for (PatternsRequestCondition condition : ignoreAuthUrls) {
            PatternsRequestCondition matchingCondition = condition.getMatchingCondition(exchange);
            if (matchingCondition != null && !matchingCondition.isEmpty()) {
                return true;
            }
        }
        return false;
    }

}
