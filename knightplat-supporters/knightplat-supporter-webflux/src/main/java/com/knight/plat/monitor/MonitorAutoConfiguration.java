package com.knight.plat.monitor;

import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.WebFilter;

import java.util.concurrent.atomic.LongAdder;

@AutoConfiguration
@EnableConfigurationProperties(MonitorProperties.class)
public class MonitorAutoConfiguration {

    private static final String CHANNEL_ADDRESS = "_knightplat_monitor_vertx_channel";
    private static final long INC_SIG = 0L;
    private static final long DEC_SIG = 1L;

    private final static Logger log = LoggerFactory.getLogger(MonitorAutoConfiguration.class);

    private final Vertx vertx;
    private final MonitorProperties properties;
    private final LongAdder counter = new LongAdder();

    public MonitorAutoConfiguration(Vertx vertx, MonitorProperties properties) {
        this.properties = properties;
        vertx.eventBus().<Long>consumer(CHANNEL_ADDRESS, msg -> {
            if (msg.body() == INC_SIG) {
                counter.increment();
            } else if (msg.body() == DEC_SIG) {
                counter.decrement();
            }
        });
        this.vertx = vertx;
    }

    private void doFinal(final String uri) {
        if (properties.checkIsMoniterUrl(uri)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("end request: " + uri);
        }
        vertx.eventBus().send(CHANNEL_ADDRESS, DEC_SIG);
    }

    @Bean
    public WebFilter monitorFilter() {
        return ((exchange, chain) -> {
            final ServerHttpRequest request = exchange.getRequest();
            final String uri = request.getURI().getPath();
            if (properties.checkIsMoniterUrl(uri)) {
                return chain.filter(exchange);
            }
            if (log.isDebugEnabled()) {
                log.debug("start request: " + uri);
            }
            vertx.eventBus().send(CHANNEL_ADDRESS, INC_SIG);
            return chain.filter(exchange).doFinally(s -> doFinal(uri));
        });
    }

    @Bean
    public RouterFunction<ServerResponse> monitorRoute() {
        return RouterFunctions.route()
                .GET(properties.getRequesting(), req -> ServerResponse.ok().bodyValue(counter.longValue()))
                .GET(properties.getPing(), req -> ServerResponse.ok().bodyValue("OK"))
                .build();
    }
}
