/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.annotation;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.reactive.result.method.RequestMappingInfo;

import java.util.Map;

/**
 * Created by LeLe on 2023/12/3.
 */
public class RequireUserGetter {

    private final Map<RequestMappingInfo, HandlerMethod> requireUserList;

    public RequireUserGetter(Map<RequestMappingInfo, HandlerMethod> requireUserList) {
        this.requireUserList = requireUserList;
    }

    public Map<RequestMappingInfo, HandlerMethod> get() {
        return requireUserList;
    }

}
