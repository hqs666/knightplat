package com.knight.plat.runtime;

import com.knight.plat.api.runtime.ServiceRegister;
import com.knight.plat.async.AsyncManager;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;

import java.util.List;

@AutoConfiguration
public class WebfluxRuntimeAutoConfiguration {

    @Bean(destroyMethod = "close")
    public ServiceRegister serviceRegister(List<RequestMappingHandlerMapping> handlerMapping, AsyncManager asyncManager) {
        return new ServiceRegisterImpl(handlerMapping.get(0), asyncManager);
    }
}
