package com.knight.plat.demo.utils;

import com.knight.plat.demo.config.WordProperties;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class WordUtils {

    public static void replaceInPara(XWPFDocument doc, Map<String, Object> params, WordProperties wordProperties) {
        Iterator<XWPFParagraph> iterator = doc.getParagraphsIterator();
        XWPFParagraph paragraph;
        while (iterator.hasNext()) {
            paragraph = iterator.next();
            if (!StringUtils.isEmpty(paragraph.getParagraphText())) {
                replaceInPara(paragraph, params, wordProperties);
            }
        }
    }

    public static void replaceInPara(XWPFParagraph paragraph, Map<String, Object> params, WordProperties wordProperties) {
        String sourceText = paragraph.getParagraphText();
        boolean replaceFlag = false;
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            String key = entry.getKey();
            if (sourceText.contains(key)) {
                Object value = entry.getValue();
                if (value instanceof String) {
                    sourceText = sourceText.replace(wordProperties.getPrePlaceholder() + key + wordProperties.getSubPlaceholder(), value.toString());
                    replaceFlag = true;
                }
            }
        }

        if (replaceFlag) {
            List<XWPFRun> runList = paragraph.getRuns();
            for (int i = runList.size(); i >= 0; i--) {
                paragraph.removeRun(i);
            }
            XWPFRun run = paragraph.createRun();
            if (sourceText.matches("^*[0-9]{1,}.*||.*[a-z]{1,}.*$")) {
                run.setFontFamily(wordProperties.getParagraphRunNumFontFamily());
                run.setFontSize(wordProperties.getParagraphRunNumFontSize());
            } else {
                run.setFontFamily(wordProperties.getParagraphRunStringFontFamily());
                run.setFontSize(wordProperties.getParagraphRunStringFontSize());
            }
            run.setText(sourceText);
        }
    }

    public static void replaceInTable(XWPFDocument doc, Map<String, Object> params, WordProperties wordProperties) {
        Iterator<XWPFTable> iterator = doc.getTablesIterator();
        XWPFTable table;
        List<XWPFTableRow> rows;
        List<XWPFTableCell> cells;
        List<XWPFParagraph> paragraphs;
        while (iterator.hasNext()) {
            table = iterator.next();
            if (table.getRows().size() > 1) {
                if (matcherPlaceholder(table.getText(), wordProperties.getPrePlaceholder(), wordProperties.getSubPlaceholder()).find()) {
                    rows = table.getRows();
                    for (XWPFTableRow row : rows) {
                        cells = row.getTableCells();
                        for (XWPFTableCell cell : cells) {
                            paragraphs = cell.getParagraphs();
                            for (XWPFParagraph paragraph : paragraphs) {
                                replaceInPara(paragraph, params, wordProperties);
                            }
                        }
                    }
                }
            }
        }

    }

    private static Matcher matcherPlaceholder(String str, String prePlaceholder, String subPlaceholder) {
        Pattern pattern = Pattern.compile(prePlaceholder + "(.+?)" + subPlaceholder, Pattern.CASE_INSENSITIVE);
        return pattern.matcher(str);
    }

    /**
     * 生成动态表格
     *
     */
    public static void dynamicCreateTableRows(XWPFDocument doc, List<List<String>> tableList, List<Integer> tablePlace, WordProperties wordProperties) {
        int replaceTableIndex = 0;
        int tableIndex = 0;
        int tableListIndex = 0;
        List<XWPFTable> tables = doc.getTables();
        for (XWPFTable table : tables) {
            if (tableIndex == (tablePlace.get(replaceTableIndex))) {
                tableListIndex++;
                for (List<String> list : tableList) {
                    XWPFTableRow row = table.createRow();
                    for (int j = 0; j < tableList.get(0).size(); j++) {
                        XWPFTableCell cell = row.getCell(j);
                        cell.setVerticalAlignment(wordProperties.getCellVerticalAlignment());
                        XWPFParagraph paragraph = cell.addParagraph();
                        paragraph.setAlignment(wordProperties.getParagraphAlignment());
                        XWPFRun run = paragraph.createRun();
                        if (list.get(j) != null && tableList.get(tableListIndex).get(j).matches("^*[0-9]{1,}.*|.*[a-z]{1,}.*$")) {
                            run.setFontFamily(wordProperties.getTableRunNumFontFamily());
                            run.setFontSize(wordProperties.getTableRunNumFontSize());
                            run.setText(list.get(j));
                        } else if (list.get(j) != null) {
                            run.setFontFamily(wordProperties.getTableRunStringFontFamily());
                            run.setFontSize(wordProperties.getTableRunStringFontSize());
                            run.setText(list.get(j));
                        }
                    }
                }
            }
            tableIndex++;
        }

    }

}
