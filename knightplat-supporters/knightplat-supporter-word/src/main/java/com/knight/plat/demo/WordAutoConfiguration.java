/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.demo;

import com.knight.plat.demo.core.WordManager;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

/**
 * Created by LeLe on 2023/12/26.
 */
@AutoConfiguration
public class WordAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public WordManager wordManager() {
        return new WordManager();
    }
}
