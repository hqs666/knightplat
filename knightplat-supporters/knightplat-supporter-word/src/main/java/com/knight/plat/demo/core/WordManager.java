package com.knight.plat.demo.core;

import com.knight.plat.demo.config.WordProperties;
import com.knight.plat.demo.utils.WordUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;


public class WordManager {
    public byte[] replaceWordText(byte[] bytes, Map<String, Object> replaceMap, List<List<String>> tableList, List<Integer> tablePlace, WordProperties wordProperties) {
        byte[] resultBytes = null;
        try (InputStream inputStream = new ByteArrayInputStream(bytes);) {
            //字符替换
            OPCPackage srcPackage = OPCPackage.open(inputStream);
            XWPFDocument doc = new XWPFDocument(srcPackage);
            WordUtils.replaceInPara(doc, replaceMap, wordProperties);
            WordUtils.replaceInTable(doc, replaceMap, wordProperties);
            WordUtils.dynamicCreateTableRows(doc, tableList, tablePlace, wordProperties);
            //将替换后的doc转成byte[]返回
            ByteArrayOutputStream bytesResult = new ByteArrayOutputStream();//二进制OutputStream
            doc.write(bytesResult);//文档写入流
            resultBytes = bytesResult.toByteArray();
        } catch (InvalidFormatException | IOException e) {
            e.printStackTrace();
        }
        return resultBytes;
    }

}
