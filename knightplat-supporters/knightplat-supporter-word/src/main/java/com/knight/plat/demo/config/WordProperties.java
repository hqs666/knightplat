package com.knight.plat.demo.config;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;

public class WordProperties {

    /**
     * Replace Placeholder
     */
    String prePlaceholder = "${";
    String subPlaceholder = "}";

    /**
     * RowProperties
     */
    Integer rowHeight = 4;
    /**
     * CellProperties
     */
    XWPFTableCell.XWPFVertAlign cellVerticalAlignment = XWPFTableCell.XWPFVertAlign.CENTER;

    /**
     * ParagraphProperties
     */
    ParagraphAlignment paragraphAlignment = ParagraphAlignment.CENTER;
    /**
     * RunProperties
     */
    Integer tableRunNumFontSize = 9;
    String tableRunNumFontFamily = "Calibri";

    Integer tableRunStringFontSize = 9;
    String tableRunStringFontFamily = "宋体";
    Integer paragraphRunNumFontSize = 10;
    String paragraphRunNumFontFamily = "Calibri";

    Integer paragraphRunStringFontSize = 10;
    String paragraphRunStringFontFamily = "宋体";

    public String getPrePlaceholder() {
        return prePlaceholder;
    }

    public void setPrePlaceholder(String prePlaceholder) {
        this.prePlaceholder = prePlaceholder;
    }

    public String getSubPlaceholder() {
        return subPlaceholder;
    }

    public void setSubPlaceholder(String subPlaceholder) {
        this.subPlaceholder = subPlaceholder;
    }

    public Integer getRowHeight() {
        return rowHeight;
    }

    public void setRowHeight(Integer rowHeight) {
        this.rowHeight = rowHeight;
    }

    public XWPFTableCell.XWPFVertAlign getCellVerticalAlignment() {
        return cellVerticalAlignment;
    }

    public void setCellVerticalAlignment(XWPFTableCell.XWPFVertAlign cellVerticalAlignment) {
        this.cellVerticalAlignment = cellVerticalAlignment;
    }

    public ParagraphAlignment getParagraphAlignment() {
        return paragraphAlignment;
    }

    public void setParagraphAlignment(ParagraphAlignment paragraphAlignment) {
        this.paragraphAlignment = paragraphAlignment;
    }

    public Integer getTableRunNumFontSize() {
        return tableRunNumFontSize;
    }

    public void setTableRunNumFontSize(Integer tableRunNumFontSize) {
        this.tableRunNumFontSize = tableRunNumFontSize;
    }

    public String getTableRunNumFontFamily() {
        return tableRunNumFontFamily;
    }

    public void setTableRunNumFontFamily(String tableRunNumFontFamily) {
        this.tableRunNumFontFamily = tableRunNumFontFamily;
    }

    public Integer getTableRunStringFontSize() {
        return tableRunStringFontSize;
    }

    public void setTableRunStringFontSize(Integer tableRunStringFontSize) {
        this.tableRunStringFontSize = tableRunStringFontSize;
    }

    public String getTableRunStringFontFamily() {
        return tableRunStringFontFamily;
    }

    public void setTableRunStringFontFamily(String tableRunStringFontFamily) {
        this.tableRunStringFontFamily = tableRunStringFontFamily;
    }

    public Integer getParagraphRunNumFontSize() {
        return paragraphRunNumFontSize;
    }

    public void setParagraphRunNumFontSize(Integer paragraphRunNumFontSize) {
        this.paragraphRunNumFontSize = paragraphRunNumFontSize;
    }

    public String getParagraphRunNumFontFamily() {
        return paragraphRunNumFontFamily;
    }

    public void setParagraphRunNumFontFamily(String paragraphRunNumFontFamily) {
        this.paragraphRunNumFontFamily = paragraphRunNumFontFamily;
    }

    public Integer getParagraphRunStringFontSize() {
        return paragraphRunStringFontSize;
    }

    public void setParagraphRunStringFontSize(Integer paragraphRunStringFontSize) {
        this.paragraphRunStringFontSize = paragraphRunStringFontSize;
    }

    public String getParagraphRunStringFontFamily() {
        return paragraphRunStringFontFamily;
    }

    public void setParagraphRunStringFontFamily(String paragraphRunStringFontFamily) {
        this.paragraphRunStringFontFamily = paragraphRunStringFontFamily;
    }
}
