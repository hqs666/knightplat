package com.knight.plat.demo.exception;

import com.knight.plat.api.exceptions.BusinessException;

/**
 * Created with IntelliJ IDEA
 *
 * @Author yuanhaoyue swithaoy@gmail.com
 * @Description Excel 解析 Exception
 * @Date 2018-06-06
 * @Time 15:56
 */
public class PoiException extends BusinessException {
    public PoiException(String message) {
        super(message);
    }
}
