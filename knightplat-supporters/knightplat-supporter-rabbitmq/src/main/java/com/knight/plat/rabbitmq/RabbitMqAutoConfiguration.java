package com.knight.plat.rabbitmq;


import com.knight.plat.api.eventbus.SchemaStrategyPair;
import com.knight.plat.api.executor.RunnableFilter;
import io.vertx.core.Vertx;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vulcan.worker.PoolProperties;
import io.vulcan.worker.WorkerPool;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.concurrent.TimeUnit;

@AutoConfiguration
@EnableConfigurationProperties(RabbitMqProperties.class)
public class RabbitMqAutoConfiguration {
    private final Log log = LogFactory.getLog("knightplat-rabbitmq");

    private final RabbitMqProperties properties;
    private final Vertx vertx;
    private final WorkerPool workerPool;

    private static final Object clientLock = new Object();
    private static boolean finished = false;

    @Autowired
    public RabbitMqAutoConfiguration(RabbitMqProperties properties, Vertx vertx, List<RunnableFilter> filters) {
        final PoolProperties poolProperties = properties.getRabbitmq().getPool();
        this.properties = properties;
        this.vertx = vertx;
        this.workerPool = WorkerPool.create("knight-rabbitmq", poolProperties.getCoreSize(), poolProperties.getMaxSize(),
                poolProperties.getKeepAlive().toMillis(), TimeUnit.MILLISECONDS, poolProperties.getQueueCapacity());
        this.workerPool.setFilters(filters);
    }

    @Bean(destroyMethod = "stop")
    public RabbitMQClient rabbitMQClient() throws InterruptedException {
        RabbitMQClient client = RabbitMQClient.create(vertx, properties.getRabbitmq());
        client.start(asyncResult -> {
            if (asyncResult.succeeded()) {
                log.info("RabbitMQ successfully connected");
            } else {
                log.error("Fail to connect to RabbitMQ", asyncResult.cause());
            }
            synchronized (clientLock) {
                finished = true;
                clientLock.notifyAll();
            }
        });
        // TODO: 死锁检查
        synchronized (clientLock) {
            while (!finished) {
                clientLock.wait(10000);
            }
        }
        return client;
    }

    @Bean
    @SuppressWarnings("unused")
    public SchemaStrategyPair rabbitMqEventStrategyInfo(RabbitMQClient rabbitMQClient) {
        CustomMQOptions options = properties.getRabbitmq();
        RabbitMqEventStrategy rabbitMqEventStrategy = new RabbitMqEventStrategy(rabbitMQClient, vertx, workerPool,
                options.getDurable(),
                options.getExclusive(), options.getAutoDelete(), options.getCanPublish());
        return new SchemaStrategyPair("rabbit", rabbitMqEventStrategy);
    }
}
