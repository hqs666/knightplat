package com.knight.plat.rabbitmq;

import io.vertx.rabbitmq.RabbitMQOptions;
import io.vulcan.worker.PoolProperties;

import java.util.Map;

public class CustomMQOptions extends RabbitMQOptions {
    private PoolProperties pool = new PoolProperties();
    private Map<String, Boolean> durable;
    private Map<String, Boolean> exclusive;
    private Map<String, Boolean> autoDelete;
    private Map<String, Boolean> canPublish;

    public PoolProperties getPool() {
        return pool;
    }

    public void setPool(PoolProperties pool) {
        this.pool = pool;
    }

    public Map<String, Boolean> getDurable() {
        return durable;
    }

    public void setDurable(Map<String, Boolean> durableMap) {
        this.durable = durableMap;
    }

    public Map<String, Boolean> getExclusive() {
        return exclusive;
    }

    public void setExclusive(Map<String, Boolean> exclusive) {
        this.exclusive = exclusive;
    }

    public Map<String, Boolean> getAutoDelete() {
        return autoDelete;
    }

    public void setAutoDelete(Map<String, Boolean> autoDelete) {
        this.autoDelete = autoDelete;
    }

    public Map<String, Boolean> getCanPublish() {
        return canPublish;
    }

    public void setCanPublish(Map<String, Boolean> canPublish) {
        this.canPublish = canPublish;
    }
}
