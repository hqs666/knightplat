package com.knight.plat.rabbitmq;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "knightplat")
public class RabbitMqProperties {
    private CustomMQOptions rabbitmq = new CustomMQOptions();

    public CustomMQOptions getRabbitmq() {
        return rabbitmq;
    }

    public void setRabbitmq(CustomMQOptions rabbitmq) {
        this.rabbitmq = rabbitmq;
    }
}
