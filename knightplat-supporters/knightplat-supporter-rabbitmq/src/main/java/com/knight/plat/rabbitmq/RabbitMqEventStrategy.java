package com.knight.plat.rabbitmq;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.eventbus.Actions;
import com.knight.plat.api.eventbus.ConsumerHandler;
import com.knight.plat.api.eventbus.ErrorHandler;
import com.knight.plat.api.eventbus.EventStrategy;
import com.knight.plat.utils.JsonUtils;
import com.rabbitmq.client.AMQP;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.RabbitMQConsumer;
import io.vertx.rabbitmq.RabbitMQPublisher;
import io.vertx.rabbitmq.RabbitMQPublisherOptions;
import io.vulcan.worker.WorkerPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class RabbitMqEventStrategy implements EventStrategy {

    private final Set<Actions> actions = Sets.newEnumSet(Lists.newArrayList(Actions.SEND, Actions.CONSUME, Actions.PUBLISH), Actions.class);

    private static final Logger log = LoggerFactory.getLogger("knightplat-eventbus-rabbitmq");

    private final RabbitMQClient rabbitMQClient;
    private final RabbitMQPublisher publisher;
    private final WorkerPool workerPool;

    private final Map<String, Boolean> durableMap;
    private final Map<String, Boolean> exclusiveMap;
    private final Map<String, Boolean> autoDeleteMap;
    private final Map<String, Boolean> canPublishMap;

    private final Map<String, Set<ConsumerMeta<?>>> handlerMap = Maps.newHashMap();
    private final ReentrantReadWriteLock handlerLock = new ReentrantReadWriteLock();

    public RabbitMqEventStrategy(RabbitMQClient rabbitMQClient, Vertx vertx, WorkerPool workerPool,
                                 Map<String, Boolean> durableMap, Map<String, Boolean> exclusiveMap, Map<String, Boolean> autoDeleteMap,
                                 Map<String, Boolean> canPublishMap) {
        this.rabbitMQClient = rabbitMQClient;
        this.workerPool = workerPool;
        this.durableMap = durableMap != null ? durableMap : Collections.emptyMap();
        this.exclusiveMap = exclusiveMap != null ? exclusiveMap : Collections.emptyMap();
        this.autoDeleteMap = autoDeleteMap != null ? autoDeleteMap : Collections.emptyMap();
        this.canPublishMap = canPublishMap != null ? canPublishMap : Collections.emptyMap();

        publisher = RabbitMQPublisher.create(vertx, rabbitMQClient, new RabbitMQPublisherOptions());
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumer(router, clazz, handler, e -> log.error("Got message from RabbitMQ queue: " + router + ", handling exception with error.", e));
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumerSync(router, clazz, handler, e -> log.error("Got message from RabbitMQ queue: " + router + ", handling exception with error.", e));
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        consumer0(router, clazz, handler, errorHandler, true);
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        consumer0(router, clazz, handler, errorHandler, false);
    }

    @Override
    public <T> void send(String router, Class<T> clazz, T payload, Callback<T, Throwable> r) {
        final boolean durable = durableMap.getOrDefault(router, false);
        final boolean exclusive = exclusiveMap.getOrDefault(router, false);
        final boolean autoDelete = autoDeleteMap.getOrDefault(router, true);

        final byte[] payloadBytes;
        try {
            payloadBytes = encodeValue(payload, clazz, JsonUtils::encodeToBytes);
        } catch (Throwable e) {
            r.onException(e);
            return;
        }
        rabbitMQClient.queueDeclare(router, durable, exclusive, autoDelete, result -> {
            if (!result.succeeded()) {
                r.onException(result.cause());
            } else {
                final String queue = result.result().getQueue();
                com.rabbitmq.client.BasicProperties properties = new AMQP.BasicProperties.Builder()
                        .build();
                try {
                    publisher.publish("", queue, properties, Buffer.buffer(payloadBytes), pResult -> {
                        if (pResult.succeeded()) {
                            r.onSuccess(payload);
                        } else {
                            r.onException(pResult.cause());
                        }
                    });
                } catch (Throwable e) {
                    r.onException(e);
                }
            }
        });
    }

    @Override
    public <T> void publish(String publishRouter, Class<T> clazz, T payload, Callback<T, Throwable> r) {
        final boolean canPublish = canPublishMap.getOrDefault(publishRouter, false);
        if (!canPublish) {
            log.error("This router: [" + publishRouter + ", RabbitMq] not support publish action.");
        }

        String router = publishRouter + "-PUBLISH";
        final boolean durable = durableMap.getOrDefault(router, true);
        final boolean autoDelete = autoDeleteMap.getOrDefault(router, false);

        final byte[] payloadBytes;
        try {
            payloadBytes = encodeValue(payload, clazz, JsonUtils::encodeToBytes);
        } catch (Throwable e) {
            r.onException(e);
            return;
        }

        rabbitMQClient.exchangeDeclare(router, "fanout", durable, autoDelete, result -> {
            if (!result.succeeded()) {
                log.error("RabbitMQ exchange: " + router + " cannot be declared(when sending)", result.cause());
            } else {
                if (!result.succeeded()) {
                    r.onException(result.cause());
                } else {
                    com.rabbitmq.client.BasicProperties properties = new AMQP.BasicProperties.Builder()
                            .build();
                    try {
                        rabbitMQClient.basicPublish(router, "", properties, Buffer.buffer(payloadBytes), pResult -> {
                            if (pResult.succeeded()) {
                                r.onSuccess(payload);
                            } else {
                                r.onException(pResult.cause());
                            }
                        });
                    } catch (Throwable e) {
                        r.onException(e);
                    }

                }
            }
        });
    }

    @Override
    public Set<Actions> supportedActions() {
        return actions;
    }

    @Override
    public void close() {
        // do nothing
    }

    private <T> void consumer0(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler, boolean async) {
        handlerLock.writeLock().lock();
        try {
            final boolean canPublish = canPublishMap.getOrDefault(router, false);
            if (canPublish) { // 匹配则监听广播 exchange信息 队列信息
                final boolean durable = durableMap.getOrDefault(router, true);
                final boolean autoDelete = autoDeleteMap.getOrDefault(router, false);
                String exchangeName = router + "-PUBLISH";

                rabbitMQClient.exchangeDeclare(exchangeName, "fanout", durable, autoDelete, result -> {
                    if (!result.succeeded()) {
                        throw new RuntimeException("RabbitMQ exchange: " + exchangeName + " cannot be declared(when sending)", result.cause());
                    } else {
                        rabbitMQClient.queueBind(router, exchangeName, "");
                    }
                });
            }

            final Set<ConsumerMeta<?>> metaSet = handlerMap.get(router);
            final ConsumerMeta<T> meta = ConsumerMeta.of(clazz, handler, errorHandler);
            if (metaSet != null) { // 已存在队列处理器，将新的处理器加入处理列表
                metaSet.add(meta);
            } else { // 未存在队列处理器，创建队列订阅
                handlerMap.put(router, Sets.newHashSet(meta));
                final boolean durable = durableMap.getOrDefault(router, false);
                final boolean exclusive = exclusiveMap.getOrDefault(router, false);
                final boolean autoDelete = autoDeleteMap.getOrDefault(router, true);
                // 声明队列
                rabbitMQClient.queueDeclare(router, durable, exclusive, autoDelete, result -> {
                    if (!result.succeeded()) {
                        throw new RuntimeException("RabbitMQ queue: " + router + " cannot be declared(when consuming)", result.cause());
                    }
                    final String queue = result.result().getQueue();

                    // 创建消费者对象
                    rabbitMQClient.basicConsumer(queue, rabbitMQConsumerAsyncResult -> {
                        if (!rabbitMQConsumerAsyncResult.succeeded()) {
                            throw new RuntimeException("RabbitMQ consumer went wrong", rabbitMQConsumerAsyncResult.cause());
                        }
                        log.info("RabbitMQ consumer for queue: {} created", router);
                        final RabbitMQConsumer mqConsumer = rabbitMQConsumerAsyncResult.result();
                        consumerPipe(router, mqConsumer, async);
                    });
                });
            }
        } finally {
            handlerLock.writeLock().unlock();
        }
    }

    private void consumerPipe(String router, RabbitMQConsumer mqConsumer, boolean async) {
        // 处理消费动作
        mqConsumer.handler(message -> {
            final Buffer buffer = message.body();
            handlerLock.readLock().lock();
            final Set<ConsumerMeta<?>> currentSet;
            try {
                currentSet = Collections.unmodifiableSet(new HashSet<>(handlerMap.get(router)));
            } finally {
                handlerLock.readLock().unlock();
            }
            if (async) {
                asyncHandle(buffer, currentSet);
            } else {
                syncHandle(buffer, currentSet);
            }
        });

        mqConsumer.exceptionHandler(cause -> {
            final Set<ConsumerMeta<?>> currentSet;
            try {
                currentSet = Collections.unmodifiableSet(new HashSet<>(handlerMap.get(router)));
            } finally {
                handlerLock.readLock().unlock();
            }
            for (@SuppressWarnings("rawtypes") ConsumerMeta m : currentSet) {
                m.fail(cause);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void asyncHandle(Buffer buffer, Set<ConsumerMeta<?>> currentSet) {
        final CompletionStage<Void> stage = workerPool.execute(() -> {
            // 数据类型安全由ConsumerMeta的范型来保证
            for (@SuppressWarnings("rawtypes") ConsumerMeta m : currentSet) {
                Object value = decodeValue(buffer.getBytes(), m.valueType(), JsonUtils::decode);
                m.handle(value);
            }
        });
        stage.whenComplete((r, e) -> {
            if (e != null) {
                for (@SuppressWarnings("rawtypes") ConsumerMeta m : currentSet) {
                    m.fail(e);
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void syncHandle(Buffer buffer, Set<ConsumerMeta<?>> currentSet) {
        // 数据类型安全由ConsumerMeta的范型来保证
        for (@SuppressWarnings("rawtypes") ConsumerMeta m : currentSet) {
            try {
                Object value = decodeValue(buffer.getBytes(), m.valueType(), JsonUtils::decode);
                m.handle(value);
            } catch (Throwable e) {
                m.fail(e);
                break;
            }
        }
    }
}
