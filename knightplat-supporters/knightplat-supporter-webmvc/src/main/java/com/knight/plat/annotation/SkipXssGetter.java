/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.annotation;

import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.RequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

/**
 * Created on 2022/6/14.
 */
public class SkipXssGetter {

    private final Map<RequestMappingInfo, HandlerMethod> ignoreAuthList;

    private final Set<RequestCondition<?>> ignoreAuthUrls;

    public SkipXssGetter(Map<RequestMappingInfo, HandlerMethod> ignoreAuthList,
            Set<RequestCondition<?>> ignoreAuthUrls) {
        this.ignoreAuthUrls = ignoreAuthUrls;
        this.ignoreAuthList = ignoreAuthList;
    }

    public Map<RequestMappingInfo, HandlerMethod> get() {
        return ignoreAuthList;
    }

    public boolean isSkip(HttpServletRequest request) {
        for (RequestCondition<?> condition : ignoreAuthUrls) {
            if (null != condition.getMatchingCondition(request)) {
                return true;
            }
        }
        return false;
    }

}
