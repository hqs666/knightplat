package com.knight.plat.commons.filters;

import com.knight.plat.annotation.SkipXssGetter;
import com.knight.plat.api.exceptions.XssRiskException;
import com.knight.plat.utils.IoUtils;
import com.knight.plat.utils.XssUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.springframework.http.MediaType;
import org.springframework.web.filter.GenericFilterBean;

public class XssFilter extends GenericFilterBean {

    private final SkipXssGetter skipXssGetter;

    public XssFilter(SkipXssGetter skipXssGetter) {
        this.skipXssGetter = skipXssGetter;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            final String contentType = request.getContentType();
            if (!skipXssGetter.isSkip((HttpServletRequest) request) && contentType != null &&
                    (contentType.toLowerCase(Locale.ROOT).contains(MediaType.APPLICATION_JSON_VALUE) || contentType.toLowerCase(Locale.ROOT).contains("text/"))) {
                chain.doFilter(new XssHttpRequestWrapper((HttpServletRequest)request), response);
            } else {
                chain.doFilter(request, response);
            }
        } else {
            throw new XssRiskException("仅支持http/https请求");
        }
//        chain.doFilter(request, response);
    }

    static class XssHttpRequestWrapper extends HttpServletRequestWrapper {

        private Charset charSet;
        private final String bodyString;

        public XssHttpRequestWrapper(HttpServletRequest request) {
            super(request);
            String localString;
            try {
                localString = getBodyString();
            } catch (IOException e) {
                localString = "";
            }
            bodyString = localString;
        }

        @Override
        public String getParameter(String name) {
            final String param = super.getParameter(name);
            if (XssUtils.hasXssRisk(param)) {
                throw new XssRiskException();
            }
            return XssUtils.stripTags(param);
        }

        @Override
        public String[] getParameterValues(String name) {
            final String[] params = super.getParameterValues(name);
            if (params == null || params.length <= 0) {
                return params;
            }

            final String[] handled = new String[params.length];

            for (int i = 0; i < params.length; i++) {
                final String param = params[i];
                if (XssUtils.hasXssRisk(param)) {
                    throw new XssRiskException();
                }
                handled[i] = XssUtils.stripTags(param);
            }

            return handled;
        }

        @Override
        public ServletInputStream getInputStream() throws IOException {
            final byte[] body;
            if (bodyString == null || bodyString.isEmpty()) {
                body = new byte[0];
            } else {
                if (XssUtils.hasXssRisk(bodyString)) {
                    throw new XssRiskException();
                }
                body = XssUtils.stripTags(bodyString).getBytes(charSet);
            }

            final ByteArrayInputStream bodyIs = new ByteArrayInputStream(body);

            return new ServletInputStream() {

                @Override
                public int read() throws IOException {
                    return bodyIs.read();
                }

                @Override
                public boolean isFinished() {
                    return false;
                }

                @Override
                public boolean isReady() {
                    return false;
                }

                @Override
                public void setReadListener(ReadListener readListener) {

                }
            };
        }

        private String getBodyString() throws IOException {
            final String charSetStr = super.getCharacterEncoding();
            if (charSetStr == null) {
                charSet = StandardCharsets.UTF_8;
            } else {
                charSet = Charset.forName(charSetStr);
            }

            try (ServletInputStream is = super.getInputStream()) {
                return IoUtils.toString(is, charSet);
            }
        }
    }

}
