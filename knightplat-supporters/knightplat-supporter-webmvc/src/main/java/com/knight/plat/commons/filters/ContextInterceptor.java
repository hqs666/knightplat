package com.knight.plat.commons.filters;

import com.knight.plat.api.commons.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.AsyncHandlerInterceptor;

public class ContextInterceptor implements AsyncHandlerInterceptor {

    public static final String RY_REQUEST_KEY = Constants.RY_REQUEST_KEY;

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) {
        RequestContextHolder.currentRequestAttributes()
                .setAttribute(RY_REQUEST_KEY, request, RequestAttributes.SCOPE_REQUEST);
        return true;
    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler,
            Exception ex) {
        RequestContextHolder.resetRequestAttributes();
    }
}
