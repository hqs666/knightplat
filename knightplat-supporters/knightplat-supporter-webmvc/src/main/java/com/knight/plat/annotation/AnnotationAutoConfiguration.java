package com.knight.plat.annotation;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.api.security.annotations.IgnoreAuth;
import com.knight.plat.api.security.annotations.RequireUser;
import com.knight.plat.api.security.annotations.SkipXssFilter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.RequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@AutoConfigImportFilter
@AutoConfiguration(after = {WebMvcAutoConfiguration.class, WebMvcAutoConfiguration.EnableWebMvcConfiguration.class})
@ConditionalOnBean(RequestMappingHandlerMapping.class)
public class AnnotationAutoConfiguration {

    private static final Log log = LogFactory.getLog(AnnotationAutoConfiguration.class);

    private final Map<RequestMappingInfo, HandlerMethod> ignoreAuthList;

    private final Map<RequestMappingInfo, HandlerMethod> requireUserList;

    private final Map<RequestMappingInfo, HandlerMethod> skipXssList;

    @Autowired
    public AnnotationAutoConfiguration(List<RequestMappingHandlerMapping> handlerMappingList) {
        final Map<RequestMappingInfo, HandlerMethod> handlerMethods = Maps.newHashMap();
        handlerMappingList.forEach(handlerMapping -> handlerMethods.putAll(handlerMapping.getHandlerMethods()));
        ignoreAuthList = Maps.filterValues(handlerMethods,
                handlerMethod -> handlerMethod != null && handlerMethod.hasMethodAnnotation(IgnoreAuth.class));
        log.info("Registered IgnoreAuth List: " + ignoreAuthList.size());
        requireUserList = Maps.filterValues(handlerMethods,
                handlerMethod -> handlerMethod != null && handlerMethod.hasMethodAnnotation(RequireUser.class));
        log.info("Registered RequireUser List: " + requireUserList.size());
        skipXssList = Maps.filterValues(handlerMethods,
                handlerMethod -> handlerMethod != null && handlerMethod.hasMethodAnnotation(SkipXssFilter.class));
        log.info("Registered SkipXssFilter List: " + skipXssList.size());
    }

    @Bean
    public IgnoreAuthGetter ignoreAuthGetter() {
        final Set<RequestMappingInfo> infos = ignoreAuthList.keySet();
        final Set<RequestCondition<?>> urlConditions = Sets.newHashSet();
        for (RequestMappingInfo info : infos) {
            urlConditions.add(info.getActivePatternsCondition());
        }
        return new IgnoreAuthGetter(ignoreAuthList, urlConditions);
    }

    @Bean
    public RequireUserGetter requireUserGetter() {
        return new RequireUserGetter(requireUserList);
    }

    @Bean
    public SkipXssGetter skipXssGetter() {
        final Set<RequestMappingInfo> infos = skipXssList.keySet();
        final Set<RequestCondition<?>> urlConditions = Sets.newHashSet();
        for (RequestMappingInfo info : infos) {
            urlConditions.add(info.getActivePatternsCondition());
        }
        return new SkipXssGetter(skipXssList, urlConditions);
    }

    // TODO: 编写注解的拦截效果，现在只有信息收集，没有处理逻辑
}
