package com.knight.plat.monitor;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "knightplat.monitor")
public class MonitorProperties {
    private String requesting = "_knightplat/requesting";
    private String ping = "_knightplat/ping";

    public String getRequesting() {
        return requesting;
    }

    public void setRequesting(String requesting) {
        this.requesting = requesting;
    }

    public String getPing() {
        return ping;
    }

    public void setPing(String ping) {
        this.ping = ping;
    }

    public boolean checkIsMoniterUrl(String url) {
        return url.contains(requesting) || url.contains(ping);
    }
}
