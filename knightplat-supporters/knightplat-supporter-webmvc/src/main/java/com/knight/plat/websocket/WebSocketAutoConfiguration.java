package com.knight.plat.websocket;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.api.eventbus.SchemaStrategyPair;
import io.vertx.core.Vertx;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.SockJsServiceRegistration;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistration;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@AutoConfigImportFilter
@AutoConfiguration
@EnableWebSocket
@EnableConfigurationProperties(WebSocketProperties.class)
@ConditionalOnProperty(prefix = "knightplat.websocket", name = "enabled", havingValue = "ture", matchIfMissing = true)
public class WebSocketAutoConfiguration implements WebSocketConfigurer {

    private final WebSocketHandler handler;
    private final WebSocketProperties properties;

    @Autowired
    public WebSocketAutoConfiguration(Vertx vertx, WebSocketProperties properties) {
        handler = new WebSocketHandler(vertx);
        this.properties = properties;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        WebSocketHandlerRegistration registration = registry.addHandler(handler, "/eventbus", "/eventbus/");
        List<String> allowedOrigins = properties.getAllowedOrigins();
        final boolean hasOrigins = allowedOrigins != null && !allowedOrigins.isEmpty();
        if (hasOrigins) {
            registration.setAllowedOrigins(allowedOrigins.toArray(new String[0]));
        }
        if (properties.getSockjs().isEnabled()) {
            SockJsServiceRegistration sockJsServiceRegistration = registration.withSockJS()
                    .setClientLibraryUrl(properties.getSockjs().getLibraryUrl());
            if (hasOrigins) {
                sockJsServiceRegistration.setSupressCors(true);
            }
        }
    }

    @Bean
    @SuppressWarnings("unused")
    public SchemaStrategyPair webSocketEventStrategyInfo() {
        return new SchemaStrategyPair("ws", new WebSocketEventStrategy(handler));
    }
}
