/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.annotation;

import java.util.Map;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

/**
 * Created on 2016/12/3.
 */
public class RequireUserGetter {

    private final Map<RequestMappingInfo, HandlerMethod> requireUserList;

    public RequireUserGetter(Map<RequestMappingInfo, HandlerMethod> requireUserList) {
        this.requireUserList = requireUserList;
    }

    public Map<RequestMappingInfo, HandlerMethod> get() {
        return requireUserList;
    }

}
