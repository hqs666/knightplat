package com.knight.plat.runtime;

import com.knight.plat.api.commons.Cookie;
import com.knight.plat.api.runtime.Context;
import com.knight.plat.api.runtime.Deployment;
import com.knight.plat.api.runtime.FunctionException;
import com.knight.plat.async.AsyncManager;
import java.net.URI;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;

public class RequestHandler {

    private final Deployment deployment;
    private final AsyncManager asyncManager;

    RequestHandler(Deployment deployment, AsyncManager asyncManager) {
        this.deployment = deployment;
        this.asyncManager = asyncManager;
    }

    Mono<ResponseEntity<?>> handle(HttpServletRequest request, @RequestBody(required = false) String body) throws FunctionException {
        // 获取请求头，准备填充到上下文对象中
        final Map<String, List<String>> headers = new HashMap<>();
        final Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            final String name = headerNames.nextElement();
            final List<String> values = headers.computeIfAbsent(name, k -> new ArrayList<>());
            final Enumeration<String> rawValues = request.getHeaders(name);
            while (rawValues.hasMoreElements()) {
                values.add(rawValues.nextElement());
            }
        }
        // 获取请求cookie，准备填充到上下文中
        final List<Cookie> cookies = new ArrayList<>();
        javax.servlet.http.Cookie[] originCookies = request.getCookies();
        if (originCookies != null) {
            for (javax.servlet.http.Cookie raw : originCookies) {
                cookies.add(convertCookie(raw));
            }
        }
        // 构建上下文对象
        final Context context = Context.builder()
                .body(body)
                .headers(headers)
                .cookies(cookies)
                .uri(URI.create(request.getRequestURI()))
                .build();
        // 线程池中异步执行function逻辑
        final Mono<Context> bodyMono = asyncManager.run(() -> deployment.call(context));
        // 处理后的Mono映射到返回体对象
        return bodyMono.map(handledContext -> {
            final Map<String, List<String>> setHeaders = deployment.fetchSetHeaders(handledContext);
            final List<Cookie> setCookies = deployment.fetchSetCookies(handledContext);
            final Object resBody = deployment.fetchResponseBody(handledContext);
            final BodyBuilder resBuilder = ResponseEntity.ok();
            // 设置返回头
            if (!setHeaders.isEmpty()) {
                setHeaders.forEach((name, values) -> resBuilder.header(name, values.toArray(new String[0])));
            }
            // 设置返回cookies
            if (!setCookies.isEmpty()) {
                setCookies.forEach(cookie -> {
                    final String cookieHeader = ResponseCookie.from(cookie.getName(), cookie.getValue())
                            .domain(cookie.getDomain())
                            .httpOnly(cookie.isHttpOnly())
                            .maxAge(cookie.getMaxAge())
                            .path(cookie.getPath())
                            .secure(cookie.isSecure())
                            .sameSite(cookie.getSameSite())
                            .build().toString();
                    resBuilder.header("Set-Cookie", cookieHeader);
                });
            }
            return resBuilder.body(resBody);
        });
    }

    // servlet标准cookie对象转为框架使用的内部对象
    private Cookie convertCookie(javax.servlet.http.Cookie rawCookie) {
        if (rawCookie == null) {
            return null;
        }

        final Cookie cookie = new Cookie();
        cookie.setName(rawCookie.getName());
        cookie.setValue(rawCookie.getValue());
        return cookie;
    }
}
