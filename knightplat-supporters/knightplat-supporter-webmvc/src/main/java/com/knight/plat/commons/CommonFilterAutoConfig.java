package com.knight.plat.commons;

import com.knight.plat.annotation.SkipXssGetter;
import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.commons.filters.ContextInterceptor;
import com.knight.plat.commons.filters.XssFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.ServletRequestPathFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@AutoConfigImportFilter
@Configuration
public class CommonFilterAutoConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ContextInterceptor()).addPathPatterns("/**");
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public ServletRequestPathFilter servletRequestPathFilter() {
        return new ServletRequestPathFilter();
    }

    @Bean
    @Order
    @ConditionalOnProperty(prefix = "knightplat.filter.xss", name = "enabled", havingValue = "true", matchIfMissing = true)
    public XssFilter xssFilter(SkipXssGetter skipXssGetter) {
        return new XssFilter(skipXssGetter);
    }
}
