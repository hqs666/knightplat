package com.knight.plat.websocket;

import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("knightplat.websocket")
public class WebSocketProperties {
    private List<String> allowedOrigins;
    private SockProperties sockjs = new SockProperties();

    public List<String> getAllowedOrigins() {
        return allowedOrigins;
    }

    public void setAllowedOrigins(List<String> allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }

    public SockProperties getSockjs() {
        return sockjs;
    }

    public void setSockjs(SockProperties sockjs) {
        this.sockjs = sockjs;
    }

    public static class SockProperties {
        private boolean enabled = true;
        private String libraryUrl = "https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js";

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getLibraryUrl() {
            return libraryUrl;
        }

        public void setLibraryUrl(String libraryUrl) {
            this.libraryUrl = libraryUrl;
        }
    }
}
