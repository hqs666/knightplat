package com.knight.plat.websocket;

import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.eventbus.Actions;
import com.knight.plat.api.eventbus.ConsumerHandler;
import com.knight.plat.api.eventbus.ErrorHandler;
import com.knight.plat.api.eventbus.EventStrategy;
import com.knight.plat.utils.SerializationUtils;
import java.util.EnumSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebSocketEventStrategy implements EventStrategy {

    private final Logger log = LoggerFactory.getLogger("knightplat-eventbus-websocket");

    private final WebSocketHandler webSocketHandler;
    private final Set<Actions> actions = EnumSet.of(Actions.SEND, Actions.PUBLISH, Actions.CONSUME);

    public WebSocketEventStrategy(WebSocketHandler webSocketHandler) {
        this.webSocketHandler = webSocketHandler;
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumer(router, clazz, handler, e -> log.error("Received message from websocket topic: " + router + ", but fail on handling", e));
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumerSync(router, clazz, handler, e -> log.error("Received message from websocket topic: " + router + ", but fail on handling", e));
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        webSocketHandler.registerRunner(router, msg -> {
            try {
                handler.handle(SerializationUtils.jsonDeserialize(msg, clazz));
            } catch (Throwable e) {
                errorHandler.handle(e);
            }
        });
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        // TODO:区分同步异步逻辑
        consumer(router, clazz, handler, errorHandler);
    }

    @Override
    public <T> void send(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        webSocketHandler.send(router, SerializationUtils.jsonSerialize(payload));
        // TODO: check
        result.onSuccess(payload);
    }

    @Override
    public <T> void publish(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        webSocketHandler.publish(router, SerializationUtils.jsonSerialize(payload));
        // TODO: check
        result.onSuccess(payload);
    }

    @Override
    public Set<Actions> supportedActions() {
        return actions;
    }

    @Override
    public void close() {
        webSocketHandler.clear();
    }
}
