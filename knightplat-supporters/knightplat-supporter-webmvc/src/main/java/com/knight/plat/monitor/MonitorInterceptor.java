package com.knight.plat.monitor;

import io.vertx.core.Vertx;
import java.util.concurrent.atomic.LongAdder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.AsyncHandlerInterceptor;

public final class MonitorInterceptor implements AsyncHandlerInterceptor {

    private static final String CHANNEL_ADDRESS = "_knightplat_monitor_vertx_channel";
    private static final long INC_SIG = 0L;
    private static final long DEC_SIG = 1L;

    private final static Logger log = LoggerFactory.getLogger(MonitorInterceptor.class);

    private final Vertx vertx;
    private final MonitorProperties properties;

    public MonitorInterceptor(Vertx vertx, LongAdder counter, MonitorProperties properties) {
        this.properties = properties;
        vertx.eventBus().<Long>consumer(CHANNEL_ADDRESS, msg -> {
            if (msg.body() == INC_SIG) {
                counter.increment();
            } else if (msg.body() == DEC_SIG) {
                counter.decrement();
            }
        });
        this.vertx = vertx;
    }

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) {
        final String uri = request.getRequestURI();
        if (properties.checkIsMoniterUrl(uri)) {
            return true;
        }
        if (log.isDebugEnabled()) {
            log.debug("start request: " + uri);
        }
        vertx.eventBus().send(CHANNEL_ADDRESS, INC_SIG);
        return true;
    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) {
        final String uri = request.getRequestURI();
        if (properties.checkIsMoniterUrl(uri)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("end request: " + uri);
        }
        vertx.eventBus().send(CHANNEL_ADDRESS, DEC_SIG);
    }

    @Override
    public void afterConcurrentHandlingStarted(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) {
        final String uri = request.getRequestURI();
        if (properties.checkIsMoniterUrl(uri)) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("async request: " + uri);
        }
        vertx.eventBus().send(CHANNEL_ADDRESS, DEC_SIG);
    }
}
