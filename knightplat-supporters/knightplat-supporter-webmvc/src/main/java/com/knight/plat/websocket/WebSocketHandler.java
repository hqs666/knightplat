package com.knight.plat.websocket;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.knight.plat.api.commons.Callback;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class WebSocketHandler extends TextWebSocketHandler {

    private static final String VERTX_CHANNEL = "###knightplat-websocket-channel###";
    private static final Logger log = LoggerFactory.getLogger(WebSocketHandler.class);

    private final Vertx vertx;
    private final InheritableThreadLocalMap<String, Set<Callback<String, Exception>>> runnerContainer = new InheritableThreadLocalMap<>();
    private final InheritableThreadLocalMap<String, WebSocketSession> sessionContainer = new InheritableThreadLocalMap<>();
    private final ConcurrentMap<String, MessageConsumer<String>> consumerMap = Maps.newConcurrentMap();

    public WebSocketHandler(Vertx vertx) {
        this.vertx = vertx;
        final Map<String, WebSocketSession> sessionMap = Maps.newHashMap();
        sessionContainer.set(sessionMap);

        final Map<String, Set<Callback<String, Exception>>> runnerMap = Maps.newHashMap();
        runnerContainer.set(runnerMap);
    }

    private void addSession(WebSocketSession session) {
        final Map<String, WebSocketSession> sessionMap = sessionContainer.get();
        sessionMap.put(session.getId(), session);
    }

    private void removeSession(String sessionId) {
        final Map<String, WebSocketSession> sessionMap = sessionContainer.get();
        if (sessionMap != null) {
            sessionMap.remove(sessionId);
        }
    }

    public void registerRunner(String topic, Callback<String, Exception> runner) {
        final Map<String, Set<Callback<String, Exception>>> runnerMap = runnerContainer.get();
        final Set<Callback<String, Exception>> runnerList = runnerMap.computeIfAbsent(topic, k -> Sets.newLinkedHashSet());
        runnerList.add(runner);
    }

    private Set<Callback<String, Exception>> getRunner(String router) {
        final Map<String, Set<Callback<String, Exception>>> runnerMap = runnerContainer.get();
        return runnerMap.get(router);
    }

    public void clear() {
        runnerContainer.remove();
        final Map<String, WebSocketSession> sessionMap = sessionContainer.get();
        if (sessionMap != null) {
            sessionMap.forEach((k, v) -> {
                try {
                    if (v != null) v.close();
                } catch (IOException e) {
                    log.error("Session: " + v.getId() + " cannot close", e);
                }
            });
        }
        sessionContainer.remove();
        log.info("Clear websocket register info");
    }

    public void publish(String topic, String payload) {
        vertx.eventBus().publish(VERTX_CHANNEL + topic, payload);
    }

    public void send(String topic, String payload) {
        vertx.eventBus().send(VERTX_CHANNEL + topic, payload);
    }

    @Override
    public void afterConnectionEstablished(@NotNull WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        addSession(session);
        log.info("WebSocket connected, session-id: {}, from: {}", session.getId(), session.getRemoteAddress());
    }

    @Override
    public void afterConnectionClosed(@NotNull WebSocketSession session, @NotNull CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        removeSession(session.getId());
        log.info("WebSocket closed, session-id: {}, cause: {}", session.getId(), status);
    }

    @Override
    protected void handleTextMessage(@NotNull WebSocketSession session, @NotNull TextMessage message) throws Exception {
        super.handleTextMessage(session, message);
        final String payload = message.getPayload();
        final String subscribePrefix = "#subscribe#";
        if (payload.startsWith(subscribePrefix)) {
            final String topic = payload.substring(subscribePrefix.length());
            final MessageConsumer<String> consumer = vertx.eventBus().consumer(VERTX_CHANNEL + topic, msg -> {
                String consumerKey = session.getId() + msg.address();
                if (!session.isOpen()) {
                    MessageConsumer<String> currentConsumer = consumerMap.remove(consumerKey);
                    if (currentConsumer != null) {
                        currentConsumer.unregister();
                    }
                    log.warn("Send message to closed websocket session: <{}>, unregistering this session", session.getId());
                    return;
                }
                try {
                    session.sendMessage(new TextMessage(msg.body()));
                } catch (IOException e) {
                    log.error("Send message to websocket session: <" + session.getId() + "> fail", e);
                }
            });
            consumerMap.put(session.getId() + VERTX_CHANNEL + topic, consumer);
            log.info("Register websocket session: <{}> to topic: <{}>", session.getId(), topic);
            return;
        }

        final Pattern pattern = Pattern.compile("^#to#(.+)#(.+)$");
        final Matcher matcher = pattern.matcher(payload);
        if (matcher.find()) {
            final String topic = matcher.group(1);
            final String msg = matcher.group(2);
            final Set<Callback<String, Exception>> runners = getRunner(topic);
            if (runners != null) {
                runners.stream()
                        .filter(Objects::nonNull)
                        .forEach(action -> {
                            try {
                                action.onSuccess(msg);
                            } catch (Exception e) {
                                action.onException(e);
                            }
                        });
            }
        }
    }

    static class InheritableThreadLocalMap<K, V> extends InheritableThreadLocal<Map<K, V>> {

        @Override
        public Map<K, V> childValue(Map<K, V> parentValue) {
            if (parentValue != null) {
                return new HashMap<>(parentValue);
            } else {
                return null;
            }
        }
    }
}
