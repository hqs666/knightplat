package com.knight.plat.commons;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.api.executor.RunnableFilter;
import io.undertow.server.DefaultByteBufferPool;
import io.undertow.websockets.jsr.WebSocketDeploymentInfo;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@AutoConfigImportFilter
@AutoConfiguration
@ConditionalOnClass(UndertowServletWebServerFactory.class)
public class CommonAutoConfiguration {

    @Component
    @SuppressWarnings("unused")
    public static class CustomizationBean implements WebServerFactoryCustomizer<UndertowServletWebServerFactory> {

        @Override
        public void customize(UndertowServletWebServerFactory factory) {
            factory.addDeploymentInfoCustomizers(deploymentInfo -> {
                WebSocketDeploymentInfo webSocketDeploymentInfo = new WebSocketDeploymentInfo();
                webSocketDeploymentInfo.setBuffers(new DefaultByteBufferPool(false, 1024));
                deploymentInfo.addServletContextAttribute("io.undertow.websockets.jsr.WebSocketDeploymentInfo", webSocketDeploymentInfo);
            });
        }
    }

    @Bean
    public RunnableFilter asyncTraceRunnableFilter() {
        return origin -> {
            final RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
            return () -> {
                if (attributes != null) {
                    RequestContextHolder.setRequestAttributes(attributes);
                    origin.run();
                    RequestContextHolder.resetRequestAttributes();
                } else {
                    origin.run();
                }
            };
        };
    }
}
