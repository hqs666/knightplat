package com.knight.plat.monitor;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import io.vertx.core.Vertx;
import java.util.concurrent.atomic.LongAdder;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.RouterFunctions;
import org.springframework.web.servlet.function.ServerResponse;
@AutoConfigImportFilter
@AutoConfiguration
@EnableConfigurationProperties(MonitorProperties.class)
public class MonitorAutoConfiguration implements WebMvcConfigurer {

    private final Vertx vertx;
    private final MonitorProperties properties;

    private final LongAdder counter = new LongAdder();

    public MonitorAutoConfiguration(Vertx vertx, MonitorProperties properties) {
        this.vertx = vertx;
        this.properties = properties;
    }

    @Override
    public void addInterceptors(@NotNull final InterceptorRegistry registry) {
        registry.addInterceptor(new MonitorInterceptor(vertx, counter, properties)).addPathPatterns("/**");
    }

    @Bean
    public RouterFunction<ServerResponse> monitorRoute() {
        return RouterFunctions.route()
                .GET(properties.getRequesting(), req -> ServerResponse.ok().body(counter.longValue()))
                .GET(properties.getPing(), req -> ServerResponse.ok().body("OK"))
                .build();
    }
}
