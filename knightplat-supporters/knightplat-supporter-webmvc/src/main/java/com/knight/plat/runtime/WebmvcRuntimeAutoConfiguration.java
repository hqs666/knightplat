package com.knight.plat.runtime;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.api.runtime.ServiceRegister;
import com.knight.plat.async.AsyncManager;
import java.util.List;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
@AutoConfigImportFilter
@AutoConfiguration
public class WebmvcRuntimeAutoConfiguration {

    @Bean(destroyMethod = "close")
    public ServiceRegister serviceRegister(List<RequestMappingHandlerMapping> handlerMapping, AsyncManager asyncManager) {
        return new ServiceRegisterImpl(handlerMapping.get(0), asyncManager);
    }
}
