/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.auth;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created on 2016/12/26.
 */
@AutoConfigImportFilter
@AutoConfiguration
@EnableFeignClients
public class CloudAutoConfiguration {

}
