package com.knight.plat.pdf.core;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.rendering.RenderDestination;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PdfManager {
    public static final int PDFBOX = 0;
    public static final int ICEPDF = 1;

    private static final float PFD_DEFAULT_SCALE = 1.25f;

    /**
     * 将PDF按页数 每页分别转换成图片,比例因子 1.25
     * @param pdfFilePath pdf文件路径
     * @param imageDirectory 存储图片的路径
     * @return 图片文件路径list
     */
    public List<String> pdfToMultiImagesFileByPage(String pdfFilePath, String imageDirectory) {
        return pdfToMultiImagesFileByPage(pdfFilePath, imageDirectory, PFD_DEFAULT_SCALE);
    }

    /**
     * 将PDF转换成一张图片,比例因子 1.25
     * @param pdfFilePath pdf文件路径
     * @param imageDirectory 存储图片的路径
     * @return 图片文件路径
     */
    public String pdfToOneImageFile(String pdfFilePath, String imageDirectory) {
        return pdfToOneImageFile(pdfFilePath, imageDirectory, PFD_DEFAULT_SCALE);
    }

    /**
     * 将PDF按页数 每页分别转换成图片
     * @param pdfFilePath pdf文件路径
     * @param imageDirectory 存储图片的路径
     * @param scale 缩放比--比例因子
     * @return 图片文件路径list
     */
    public List<String> pdfToMultiImagesFileByPage(String pdfFilePath, String imageDirectory, float scale) {

        List<String> filePathList = new ArrayList<>();

        File f = new File(imageDirectory);
        if (!f.exists()) {
            f.mkdir();
        }

        File file = new File(pdfFilePath);

        String fileName = file.getName();
        String fn = fileName.substring(0, fileName.lastIndexOf("."));//获取去除后缀的文件名

        List<BufferedImage> list = pdfToMultiImagesByPage(file, scale);

        for (int i = 0; i < list.size(); i++) {
            String imagePath = imageDirectory + File.separator + fn + "_" + (i + 1) + ".png";
            try {
//                ImageIOUtil.writeImage(combined, filePath + ".png", 96);
                ImageIO.write(list.get(i), "PNG", new File(imagePath));
                filePathList.add(imagePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return filePathList;
    }

    /**
     * 将PDF转换成一张图片
     * @param pdfFilePath pdf文件路径
     * @param imageDirectory 存储图片的路径
     * @param scale 缩放比--比例因子
     * @return 图片文件路径
     */
    public String pdfToOneImageFile(String pdfFilePath, String imageDirectory, float scale) {
        File f = new File(imageDirectory);
        if (!f.exists()) {
            f.mkdir();
        }

        File file = new File(pdfFilePath);

        String fileName = file.getName();
        String fn = fileName.substring(0, fileName.lastIndexOf("."));//获取去除后缀的文件名

        BufferedImage image = pdfToOneImage(file, scale);
        String imagePath = imageDirectory + File.separator + fn + ".png";

        try {
            ImageIO.write(image, "PNG", new File(imagePath));
            return imagePath;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将PDF按页数 每页分别转换成图片
     * @param pdfFilePath pdf文件路径
     * @param scale 缩放比--比例因子
     * @return 图片文件list
     */
    public List<BufferedImage> pdfToMultiImagesByPage(String pdfFilePath, float scale) {
        File file = new File(pdfFilePath);
        return pdfToMultiImagesByPage(file, scale);
    }

    /**
     * 将PDF转换成一张图片
     * @param pdfFilePath pdf文件路径
     * @param scale 缩放比--比例因子
     * @return 图片文件
     */
    public BufferedImage pdfToOneImage(String pdfFilePath, float scale) {
        File file = new File(pdfFilePath);
        return pdfToOneImage(file, scale);
    }

    /**
     * 将PDF按页数 每页分别转换成图片
     * @param pdfFile pdf文件
     * @return 图片文件list
     */
    public List<BufferedImage> pdfToMultiImagesByPage(File pdfFile) {
        return pdfToMultiImagesByPage(pdfFile, PFD_DEFAULT_SCALE);
    }

    /**
     * 将PDF转换成一张图片
     * @param pdfFile pdf文件
     * @return 图片文件
     */
    public BufferedImage pdfToOneImage(File pdfFile) {
        return pdfToOneImage(pdfFile, PFD_DEFAULT_SCALE);
    }

    /**
     * 将PDF按页数 每页分别转换成图片，如果失败，会使用ice进行重试
     * @param pdfFile pdf文件
     * @param scale 缩放比--比例因子
     * @return 图片文件list
     */
    public List<BufferedImage> pdfToMultiImagesByPage(File pdfFile, float scale) {
        InputStream in = null;

        try {
            in = new FileInputStream(pdfFile);
            return pdfToMultiImagesByPage(in, scale);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
            //尝试 ice
            return pdfToMultiImagesByPageWithIce(pdfFile.getAbsolutePath(), scale);
        }finally {
            if(null != in){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return new ArrayList<>();
    }

    /**
     * 将PDF转换成一张图片，暂时不支持ice
     * @param pdfFile pdf文件
     * @param scale 缩放比--比例因子
     * @return 图片文件
     */
    public BufferedImage pdfToOneImage(File pdfFile, float scale) {

        InputStream in = null;

        try {
            in = new FileInputStream(pdfFile);
            return pdfToOneImage(in, scale);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            if(null != in){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 将PDF按页数 每页分别转换成图片,本方法暂时不支持ice
     * @param pdfIn pdf文件输入流
     * @param scale 缩放比--比例因子
     * @return 图片文件list
     */
    public List<BufferedImage> pdfToMultiImagesByPage(InputStream pdfIn, float scale) {
        List<BufferedImage> list = new ArrayList<>();
        PDDocument doc = null;

        try {
            doc = PDDocument.load(pdfIn);
            PDFRenderer renderer = new PDFRenderer(doc);
            for (int page = 0; page < doc.getNumberOfPages(); ++page) {
                //方式1，第二个参数是设置缩放比--像素 the DPI (dots per inch) to render at
//                 BufferedImage image = renderer.renderImageWithDPI(i, 296);

                //方式2，第二个参数是设置缩放比--比例因子 the scaling factor, where 1 = 72 DPI
                // 第二个参数越大生成图片分辨率越高，转换时间也就越长
                BufferedImage image = renderer.renderImage(page, scale, ImageType.RGB, RenderDestination.EXPORT); //1.25f

                list.add(image);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (doc != null) {
                try {
                    //关闭文件,不然该pdf文件会一直被占用
                    doc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return list;
    }

    /**
     * 将PDF转换成一张图片
     * @param pdfIn pdf文件输入流
     * @param scale 缩放比--比例因子
     * @return 图片文件
     */
    public BufferedImage pdfToOneImage(InputStream pdfIn, float scale) {

        PDDocument doc = null;
        try {
            BufferedImage combined = null;

            doc = PDDocument.load(pdfIn);
            PDFRenderer renderer = new PDFRenderer(doc);
            for (int page = 0; page < doc.getNumberOfPages(); ++page) {
//                BufferedImage image = renderer.renderImageWithDPI(page, 96, ImageType.RGB);
                BufferedImage image = renderer.renderImage(page, scale, ImageType.RGB); //1.25f
                if (page == 0) {
                    combined = image;
                } else {
                    combined = merge(combined, image); //合并
                }
            }

            return combined; //返回完整的图片，出错了，不完整就返回null

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (doc != null) {
                try {
                    //关闭文件,不然该pdf文件会一直被占用
                    doc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 合并图片
     * 将宽度相同的图片，竖向追加在一起
     * 注意：宽度必须相同
     * @param image1  上方的图片
     * @param image2  追加在下方的图片
     * @return
     */
    private BufferedImage merge(BufferedImage image1, BufferedImage image2) {
        int maxWidth = image1.getWidth() >= image2.getWidth() ? image1.getWidth() : image2.getWidth();

        BufferedImage combined = new BufferedImage(
                maxWidth,
                image1.getHeight() + image2.getHeight(),
                BufferedImage.TYPE_INT_RGB);

        Graphics g = combined.getGraphics();

//        g.setColor(Color.white); //默认是黑底，这样可以画成自己定义的底部颜色
//        g.fillRect(0, 0, maxWidth, image1.getHeight() + image2.getHeight());

        //(maxWidth - image1.getWidth())/2 ---居中
        g.drawImage(image1, (maxWidth - image1.getWidth())/2, 0, null);
        g.drawImage(image2, (maxWidth - image2.getWidth())/2, image1.getHeight(), null);

        //Disposes of this graphics context once it is no longer referenced.
        g.dispose();

        return combined;
    }

    /**
     * ice工具，将PDF按页数 每页分别转换成图片
     * @param pdfPath pdf文件输入流
     * @param scale 缩放比--比例因子
     * @return 图片文件list
     */
    public List<BufferedImage> pdfToMultiImagesByPageWithIce(String pdfPath, float scale) {
        List<BufferedImage> list = new ArrayList<>();
        org.icepdf.core.pobjects.Document document = new org.icepdf.core.pobjects.Document();

        try {
            document.setFile(pdfPath);
            //旋转角度
            float rotation = 0f;
            
            for (int page = 0; page < document.getNumberOfPages(); page++) {
                BufferedImage image = (BufferedImage) document.getPageImage(page, GraphicsRenderingHints.SCREEN, Page.BOUNDARY_CROPBOX, rotation, scale);
                list.add(image);
            }
        } catch (PDFException e) {
            e.printStackTrace();
        } catch (PDFSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return list;
    }

//    public static void main(String[] args) throws IOException {
//        File file = new File("/Users/JiaLe/Downloads/证书设计(10).pdf");
//        InputStream in = new FileInputStream(file);
//        PDDocument doc = PDDocument.load(in);
//
//        PDPage pdPage = doc.getPage(0);
//
//        PDResources resources = pdPage.getResources();
//        for (COSName cname : resources.getXObjectNames()) {
//            if (resources.isImageXObject(cname)) {
//                System.out.println(cname);
//                pdPage.getCOSObject().removeItem(cname);
//            }
//        }
//        for (COSName name :  pdPage.getResources().getPatternNames()) {
//            PDAbstractPattern pattern = pdPage.getResources().getPattern(name);
//            System.out.println("have pattern");
//        }
//
//        PDFStreamParser parser = new PDFStreamParser(pdPage);
//        parser.parse();
//        List<Object> tokens = parser.getTokens();
//        System.out.println("original tokens size" + tokens.size());
//        List<Object> newTokens = new ArrayList<>();
//
//        for (Object token : tokens) {
//            if (token instanceof Operator) {
//                Operator op = (Operator) token;
//
//                System.out.println("operation" + op.getName());
//                //find image - remove it
//                if (op.getName().equals("Do")) {
//                    System.out.println("op equals Do");
//                    newTokens.remove(newTokens.size() - 1);
//                    continue;
//                } else if ("BI".equals(op.getName())) {
//                    System.out.println("inline -- op equals BI");
//                } else {
//                    System.out.println("op not quals Do");
//                }
//            }
//            newTokens.add(token);
//        }
//
//        PDDocument newDoc = new PDDocument();
//        PDPage newPage = newDoc.importPage(pdPage);
//        newPage.setResources(pdPage.getResources());
//
//        InputStream oldContentStream = newPage.getContents();
//        byte[] ba = IOUtils.toByteArray(oldContentStream);
//        System.out.println(ba.length);
//        oldContentStream.close();
//
//        File file2 = new File("/Users/JiaLe/Downloads/test.pdf");
//        PDDocument doc2 = new PDDocument();
//        doc2.addPage(newPage);
//
//        PDPageContentStream newContentStream = new PDPageContentStream(doc2, newPage, AppendMode.OVERWRITE, true);
//
//        PDImageXObject pdImage = PDImageXObject.createFromFile("/Users/JiaLe/Downloads/WechatIMG8617.jpeg", doc2);
//        newContentStream.saveGraphicsState();
//        newContentStream.drawImage(pdImage, 0, 0, newPage.getTrimBox().getWidth(), newPage.getTrimBox().getHeight());
//        newContentStream.restoreGraphicsState();
//        newContentStream.close();
//
//        PDPageContentStream newContentStream2 = new PDPageContentStream(doc2, newPage, AppendMode.APPEND, true);
//        newContentStream2.appendRawCommands(ba);
//        newContentStream2.close();
//
//        doc2.save(file2);
//    }
}