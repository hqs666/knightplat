/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */
package com.knight.plat.pdf;


import com.knight.plat.pdf.core.PdfManager;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;


@AutoConfiguration
public class PdfAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public PdfManager pdfManager() {
        return new PdfManager();
    }

}


