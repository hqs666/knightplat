package com.knight.plat.kafka;

import com.google.common.collect.Maps;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.eventbus.Actions;
import com.knight.plat.api.eventbus.ConsumerHandler;
import com.knight.plat.api.eventbus.ErrorHandler;
import com.knight.plat.api.eventbus.EventStrategy;
import com.knight.plat.utils.JsonUtils;
import io.vertx.core.Future;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import io.vertx.kafka.client.consumer.KafkaConsumerRecord;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import io.vertx.kafka.client.producer.RecordMetadata;
import io.vulcan.worker.WorkerPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class KafkaEventStrategy implements EventStrategy {

    private static final Logger log = LoggerFactory.getLogger("knightplat-eventbus-kafka");

    private final Set<Actions> actions = EnumSet.of(Actions.SEND, Actions.CONSUME, Actions.PUBLISH);
    private final KafkaConsumerFactory kafkaConsumerFactory;
    private final KafkaProducer<String, byte[]> kafkaProducer;
    private final Map<String, Boolean> canPublishMap;
    private final WorkerPool workerPool;

    private final Map<String, KafkaConsumer<String, byte[]>> handlerMap = Maps.newHashMap();

    public KafkaEventStrategy(KafkaConsumerFactory kafkaConsumerFactory,
            KafkaProducer<String, byte[]> kafkaProducer, Map<String, Boolean> canPublishMap,
            WorkerPool workerPool) {
        this.kafkaConsumerFactory = kafkaConsumerFactory;
        this.kafkaProducer = kafkaProducer;
        this.canPublishMap = canPublishMap != null ? canPublishMap : new HashMap<>();
        this.workerPool = workerPool;
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumer(router, clazz, handler, e -> log.error("Got message from kafka topic: " + router + ", handling exception with error.", e));
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumerSync(router, clazz, handler, e -> log.error("Got message from kafka topic: " + router + ", handling exception with error.", e));
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        consumer0(router, clazz, handler, errorHandler, true);
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        consumer0(router, clazz, handler, errorHandler, false);
    }

    @Override
    public <T> void send(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        byte[] sendValue;
        try {
            sendValue = encodeValue(payload, clazz, JsonUtils::encodeToBytes);
        } catch (Throwable e) {
            result.onException(e);
            return;
        }
        Future<RecordMetadata> sendFuture = kafkaProducer
                .send(KafkaProducerRecord.create(router, sendValue));
        sendFuture.onFailure(result::onException);
        sendFuture.onSuccess(r -> result.onSuccess(payload));
    }

    @Override
    public <T> void publish(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        router += "-PUBLISH";
        byte[] sendValue;
        try {
            sendValue = encodeValue(payload, clazz, JsonUtils::encodeToBytes);
        } catch (Throwable e) {
            result.onException(e);
            return;
        }
        Future<RecordMetadata> sendFuture = kafkaProducer
                .send(KafkaProducerRecord.create(router, sendValue));
        sendFuture.onFailure(result::onException);
        sendFuture.onSuccess(r -> result.onSuccess(payload));
    }

    @Override
    public Set<Actions> supportedActions() {
        return actions;
    }

    @Override
    public void close() {
        kafkaProducer.close();
        synchronized (handlerMap) {
            handlerMap.values().forEach(KafkaConsumer::close);
            handlerMap.clear();
        }
    }

    private <T> void consumer0(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler, boolean async) {
        synchronized (handlerMap) {
            KafkaConsumer<String, byte[]> kafkaConsumer = handlerMap.get(router);
            if (kafkaConsumer == null) {
                kafkaConsumer = kafkaConsumerFactory.build(router);
                handlerMap.put(router, kafkaConsumer);
            }
            kafkaConsumer.handler(record -> consumerPipe(record, clazz, handler, errorHandler, async));
            kafkaConsumer.subscribe(router).onFailure(e -> {
                throw new RuntimeException("Kafka consumer register fail", e);
            });

            //订阅广播,生成广播topic
            final boolean canPublish = canPublishMap.getOrDefault(router, false);
            if (canPublish) {
                router += "-PUBLISH";
                kafkaConsumer = handlerMap.get(router);

                if (kafkaConsumer == null) {
                    kafkaConsumer = kafkaConsumerFactory.buildPublish(router);
                    handlerMap.put(router, kafkaConsumer);
                    //kafkaConsumerFactory.
                }
                kafkaConsumer.handler(record -> consumerPipe(record, clazz, handler, errorHandler, async));
                kafkaConsumer.subscribe(router).onFailure(e -> {
                    throw new RuntimeException("Kafka consumer register fail", e);
                });
            }
        }
    }

    private <T> void consumerPipe(KafkaConsumerRecord<String, byte[]> record, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler, boolean async) {
        if (async) {
            workerPool.execute(() -> syncHandle(record, clazz, handler, errorHandler));
        } else {
            syncHandle(record, clazz, handler, errorHandler);
        }
    }

    private <T> void syncHandle(KafkaConsumerRecord<String, byte[]> record, Class<T> clazz, ConsumerHandler<T> handler,
            ErrorHandler errorHandler) {
        try {
            T value = decodeValue(record.value(), clazz, JsonUtils::decode);
            handler.handle(value);
        } catch (Throwable e) {
            errorHandler.handle(e);
        }
    }

}
