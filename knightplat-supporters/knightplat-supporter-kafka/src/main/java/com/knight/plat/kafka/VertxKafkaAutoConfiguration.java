package com.knight.plat.kafka;

import com.knight.plat.api.eventbus.SchemaStrategyPair;
import com.knight.plat.api.executor.RunnableFilter;
import com.knight.plat.api.history.HistoryRecorder;
import com.knight.plat.history.HistoryAutoConfiguration;
import io.vertx.core.Vertx;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import io.vulcan.worker.PoolProperties;
import io.vulcan.worker.WorkerPool;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Lazy
@AutoConfiguration(before = HistoryAutoConfiguration.class)
@EnableConfigurationProperties(VertxKafkaProperties.class)
public class VertxKafkaAutoConfiguration {

    private final VertxKafkaProperties properties;
    private final Vertx vertx;
    private final WorkerPool workerPool;

    @Autowired
    public VertxKafkaAutoConfiguration(VertxKafkaProperties properties, Vertx vertx, List<RunnableFilter> filters) {
        final PoolProperties poolProperties = properties.getPool();
        this.vertx = vertx;
        properties.getProducer().setKeySerializer(StringSerializer.class);
        properties.getProducer().setValueSerializer(ByteArraySerializer.class);
        this.properties = properties;
        this.workerPool = WorkerPool.create("knight-kafka", poolProperties.getCoreSize(), poolProperties.getMaxSize(),
                poolProperties.getKeepAlive().toMillis(), TimeUnit.MILLISECONDS, poolProperties.getQueueCapacity());
        this.workerPool.setFilters(filters);
    }

    @Bean
    @SuppressWarnings("unused")
    public KafkaProducer<String, byte[]> vertxKafkaProducer() {
        return KafkaProducer.create(
                vertx,
                new org.apache.kafka.clients.producer.KafkaProducer<>(properties.buildProducerProperties())
        );
    }

    @Bean
    @SuppressWarnings("unused")
    public KafkaConsumerFactory vertxKafkaConsumerFactory() {
        return new KafkaConsumerFactory(properties, vertx);
    }

    @Bean
    @SuppressWarnings("unused")
    public SchemaStrategyPair kafkaEventStrategyInfo(KafkaProducer<String, byte[]> vertxKafkaProducer, KafkaConsumerFactory consumerFactory) {
        KafkaEventStrategy kafkaEventStrategy = new KafkaEventStrategy(consumerFactory, vertxKafkaProducer, properties.getCanPublish(), workerPool);
        return new SchemaStrategyPair("kafka", kafkaEventStrategy);
    }

    @Bean
    @SuppressWarnings("unused")
    @ConditionalOnProperty(prefix = "knightplat.kafka.history", name = "enabled", havingValue = "true")
    public HistoryRecorder kafkaHistoryRecorder(KafkaProducer<String, byte[]> vertxKafkaProducer) {
        String historyTopic = properties.getHistoryTopic();
        return logContent -> vertxKafkaProducer.send(KafkaProducerRecord.create(
                historyTopic, logContent.serialize().getBytes(
                StandardCharsets.UTF_8)));
    }

    @Bean
    @SuppressWarnings("unused")
    @ConditionalOnProperty(prefix = "knightplat.kafka.history", name = "enabled", havingValue = "true")
    public KafkaHistoryHandler kafkaHistoryHandler() {
        return new KafkaHistoryHandler(properties, vertx);
    }
}
