package com.knight.plat.kafka;

import com.knight.plat.kafka.VertxKafkaProperties.Consumer;
import com.knight.plat.utils.KeyGenUtils;
import io.vertx.core.Vertx;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class KafkaConsumerFactory {
    private final VertxKafkaProperties properties;
    private final Vertx vertx;

    private static final Logger log = LoggerFactory.getLogger("knightplat-eventbus-kafka");

    public KafkaConsumerFactory(VertxKafkaProperties properties, Vertx vertx) {
        this.properties = properties;
        this.vertx = vertx;
    }

    public KafkaConsumer<String, byte[]> build(String topic) {
        Consumer consumerProperties = properties.getConsumer();
        consumerProperties.setKeyDeserializer(StringDeserializer.class);
        consumerProperties.setValueDeserializer(ByteArrayDeserializer.class);
        String groupId = consumerProperties.getGroupId();

        if (groupId == null) {
            consumerProperties.setGroupId("knightplat-group-" + topic);
        }
        log.info("Register kafka topic {}, group {}", topic, consumerProperties.getGroupId());
        Map<String, Object> properties = this.properties.buildConsumerProperties();
        consumerProperties.setGroupId(groupId);
        return KafkaConsumer.create(
                vertx,
                new org.apache.kafka.clients.consumer.KafkaConsumer<>(properties)
        );
    }

    /**
     * kafka广播 给每一个consumer绑定一个groupID
     * @param topic 订阅主题
     * @return 消费者对象
     */
    public KafkaConsumer<String, byte[]> buildPublish(String topic) {
        Consumer consumerProperties = properties.getConsumer();

        consumerProperties.setKeyDeserializer(StringDeserializer.class);
        consumerProperties.setValueDeserializer(ByteArrayDeserializer.class);
        String uid = String.valueOf(KeyGenUtils.snowFlake());
        String groupId = consumerProperties.getGroupId();

        if (groupId == null) {
            consumerProperties.setGroupId("knightplat-group-" + topic + "-" + uid);
        } else {
            groupId += "-" + topic + "-" + uid;
        }

        log.info("Register kafka publish topic {}, group {}", topic, consumerProperties.getGroupId());
        Map<String, Object> properties = this.properties.buildConsumerProperties();
        consumerProperties.setGroupId(groupId);

        return KafkaConsumer.create(
                vertx,
                new org.apache.kafka.clients.consumer.KafkaConsumer<>(properties)
        );
    }
}
