package com.knight.plat.kafka;

import com.knight.plat.api.commons.Callback;
import com.knight.plat.history.HistoryLog;
import com.knight.plat.kafka.VertxKafkaProperties.Consumer;
import com.knight.plat.utils.SerializationUtils;
import io.vertx.core.Vertx;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

public class KafkaHistoryHandler {
    private final Log log = LogFactory.getLog(KafkaHistoryHandler.class);

    private final VertxKafkaProperties properties;
    private final Vertx vertx;

    public KafkaHistoryHandler(VertxKafkaProperties properties, Vertx vertx) {
        this.properties = properties;
        this.vertx = vertx;
    }

    public void register(Callback<HistoryLog, Throwable> action) {
        Consumer consumerProperties = properties.getConsumer();
        consumerProperties.setKeyDeserializer(StringDeserializer.class);
        consumerProperties.setValueDeserializer(ByteArrayDeserializer.class);
        String groupId = consumerProperties.getGroupId();
        String historyTopic = properties.getHistoryTopic();
        if (groupId == null) {
            consumerProperties.setGroupId(historyTopic);
        }
        KafkaConsumer<String, byte[]> kafkaConsumer = KafkaConsumer.create(
                vertx,
                new org.apache.kafka.clients.consumer.KafkaConsumer<>(properties.buildConsumerProperties())
        );

        kafkaConsumer.handler(record -> {
            try {
                HistoryLog value = SerializationUtils.jsonDeserializeFromBytes(record.value(), HistoryLog.class);
                action.onSuccess(value);
            } catch (Exception e) {
                log.error("Got message from kafka topic: " + record.topic() + ", handling exception with error.", e);
                action.onException(e);
            }
        });
        kafkaConsumer.subscribe(historyTopic).onFailure(action::onException);
    }
}
