/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.aliyun;

import com.knight.plat.aliyun.oss.OssFactory;
import com.knight.plat.aliyun.oss.OssFile;
import com.knight.plat.aliyun.oss.OssFileManager;
import com.knight.plat.aliyun.oss.OssProperties;
import com.knight.plat.api.filesystem.FileChannelPair;
import com.knight.plat.api.filesystem.FileManager;
import io.vertx.core.Vertx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * Created by LeLe on 2023/10/14. all things about Aliyun
 */
@AutoConfiguration
@EnableConfigurationProperties({AliyunProperties.class})
public class AliyunOssAutoConfiguration {

    private final AliyunProperties properties;

    @Autowired
    public AliyunOssAutoConfiguration(AliyunProperties properties) {
        this.properties = properties;
    }

    /**
     *  OSS configuration
     */
    @Configuration(proxyBeanMethods = false)
    @EnableConfigurationProperties(OssProperties.class)
    @ConditionalOnClass(FileManager.class)
    @ConditionalOnProperty(name = "knightplat.aliyun.oss.enabled", havingValue = "true", matchIfMissing = true)
    class OssConfig {

        private final OssProperties ossProperties;
        private final Vertx vertx;

        @Autowired
        public OssConfig(OssProperties ossProperties, Vertx vertx) {
            this.ossProperties = ossProperties;
            this.vertx = vertx;
        }

        @Bean
        @ConditionalOnMissingBean
        public OssFactory ossFactory() {
            return new OssFactory(properties, ossProperties);
        }

        @Bean
        public FileChannelPair ossChannalPair(OssFactory ossFactory) {
            return new FileChannelPair(new OssFileManager(ossFactory, vertx), OssFile.channel().getName());
        }

    }


}
