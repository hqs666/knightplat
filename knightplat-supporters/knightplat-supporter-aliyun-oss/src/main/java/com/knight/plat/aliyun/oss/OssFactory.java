/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.aliyun.oss;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.google.common.base.Strings;
import com.knight.plat.aliyun.AliyunProperties;

/**
 * Created by LeLe on 2023/11/3.
 */
public class OssFactory {

    private final AliyunProperties properties;

    private final OssProperties ossProperties;

    private final ClientConfiguration configuration;

    public OssFactory(AliyunProperties properties, OssProperties ossProperties) {
        this.properties = properties;
        this.ossProperties = ossProperties;

        configuration = new ClientConfiguration();
        configuration.setMaxConnections(ossProperties.getMaxConnections());
        configuration.setSocketTimeout(ossProperties.getSocketTimeout());
        configuration.setConnectionTimeout(ossProperties.getConnectionTimeout());
        configuration.setIdleConnectionTime(ossProperties.getIdleConnectionTime());
        configuration.setMaxErrorRetry(ossProperties.getMaxErrorRetry());
        configuration.setProtocol(ossProperties.getProtocol());
        configuration.setUserAgent(ossProperties.getUserAgent());
    }

    public OSSClient getClient() {

        // 如果oss独立命名空间中没有配置则使用aliyun的主配置
        String accessKey = Strings.isNullOrEmpty(ossProperties.getAccessKey()) ? properties.getAccessKey()
                : ossProperties.getAccessKey();
        // 同上
        String secretKey = Strings.isNullOrEmpty(ossProperties.getSecretKey()) ? properties.getSecretKey()
                : ossProperties.getSecretKey();

        return new OSSClient(ossProperties.getEndpoint(), new DefaultCredentialProvider(accessKey, secretKey),
                configuration);
    }

}
