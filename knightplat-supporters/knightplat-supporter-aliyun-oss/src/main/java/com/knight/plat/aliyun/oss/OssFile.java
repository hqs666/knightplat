package com.knight.plat.aliyun.oss;

import com.knight.plat.api.filesystem.FileChannel;

public final class OssFile implements FileChannel {

    private OssFile() {}

    private static final OssFile INSTANCE = new OssFile();

    public static OssFile channel() {
        return INSTANCE;
    }

    @Override
    public String getName() {
        return "oss";
    }
}
