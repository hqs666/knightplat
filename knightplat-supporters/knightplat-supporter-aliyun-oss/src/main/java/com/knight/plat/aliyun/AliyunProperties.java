/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.aliyun;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by LeLe on 2023/10/14.
 */
@ConfigurationProperties("knightplat.aliyun")
public class AliyunProperties {

    private String accessKey = "";

    private String secretKey = "";

    private String appName = "";

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

}
