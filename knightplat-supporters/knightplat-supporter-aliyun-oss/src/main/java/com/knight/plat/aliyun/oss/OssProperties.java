/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.aliyun.oss;

import com.aliyun.oss.common.comm.Protocol;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by LeLe on 2023/11/3.
 */
@ConfigurationProperties("knightplat.aliyun.oss")
public class OssProperties {

    private int maxConnections = 1024; // 允许打开的最大HTTP连接数。默认为1024

    private int socketTimeout = 50000; // Socket层传输数据的超时时间（单位：毫秒）。默认为50000毫秒

    private int connectionTimeout = 50000; // 建立连接的超时时间（单位：毫秒）。默认为50000毫秒

    private int idleConnectionTime = 60000; // 如果空闲时间超过此参数的设定值，则关闭连接（单位：毫秒）。默认为60000毫秒

    private int maxErrorRetry = 3; // 请求失败后最大的重试次数。默认3次

    private Protocol protocol = Protocol.HTTP; // 连接OSS所采用的协议（HTTP/HTTPS），默认为HTTP

    private String userAgent = "aliyun-sdk-java"; // 用户代理，指HTTP的User-Agent头。默认为”aliyun-sdk-java”

    private String endpoint = "http://oss-cn-shanghai.aliyuncs.com";

    private String accessKey = null;

    private String secretKey = null;

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getIdleConnectionTime() {
        return idleConnectionTime;
    }

    public void setIdleConnectionTime(int idleConnectionTime) {
        this.idleConnectionTime = idleConnectionTime;
    }

    public int getMaxErrorRetry() {
        return maxErrorRetry;
    }

    public void setMaxErrorRetry(int maxErrorRetry) {
        this.maxErrorRetry = maxErrorRetry;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

}
