/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.aliyun.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.ObjectMetadata;
import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.filesystem.FileManager;
import com.knight.plat.api.filesystem.Part;
import io.vertx.core.Vertx;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created by LeLe on 2023/11/2. FileManager implementation for aliyun oss
 */
public class OssFileManager implements FileManager {

    private final OssFactory clientFactory;
    private final Vertx vertx;

    public OssFileManager(OssFactory clientFactory, Vertx vertx) {
        this.clientFactory = clientFactory;
        this.vertx = vertx;
    }

    private String getBasePath(String endpoint, String bucket) {
        if (endpoint.startsWith("http://")) {
            StringBuilder endpointBuilder = new StringBuilder(endpoint);
            return endpointBuilder.insert(7, bucket + ".").toString();
        } else if (endpoint.startsWith("https://")) {
            StringBuilder endpointBuilder = new StringBuilder(endpoint);
            return endpointBuilder.insert(8, bucket + ".").toString();
        }
        return bucket + "." + endpoint;
    }

    @Override
    public String save(InputStream data, String bucket, String path) throws IOException {
        return save(data, -1, bucket, path);
    }

    @Override
    public String save(InputStream data, long contentLength, String bucket, String path) throws IOException {
        OSSClient client = clientFactory.getClient();
        try {
            if (!client.doesBucketExist(bucket)) {
                client.createBucket(bucket);
                client.setBucketAcl(bucket, CannedAccessControlList.PublicRead);
            }
            if (contentLength == -1) {
                client.putObject(bucket, path, data);
            } else {
                final ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(contentLength);
                client.putObject(bucket, path, data, metadata);
            }
        } finally {
            if (null != client) {
                client.shutdown();
            }
        }

        return "/" + path;
    }

    @Override
    public void saveAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        saveAsync(data, -1, bucket, path, callback);
    }

    @Override
    public void saveAsync(InputStream data, long contentLength, String bucket, String path,
            Callback<String, IOException> callback) {
        vertx.<String>executeBlocking(promise -> {
            try {
                save(data, contentLength, bucket, path);
                promise.complete();
            } catch (IOException e) {
                promise.fail(e);
            }
        }, res -> {
            if (res.succeeded()) {
                callback.onSuccess(res.result());
            } else {
                Throwable cause = res.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    throw new RuntimeException("Unknown exception when saving file.", cause);
                }
            }
        });
    }

    @Override
    public String append(InputStream data, String bucket, String path) throws IOException {
        // TODO
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public void appendAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        // TODO
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public byte[] read(String bucket, String path) throws IOException {
        return read(bucket, path, null);
    }

    @Override
    public long size(String bucket, String path) throws IOException {
        OSSClient client = clientFactory.getClient();
        try {
            return client.getObjectMetadata(bucket, path).getContentLength();
        } finally {
            client.shutdown();
        }
    }

    @Override
    public InputStream readAsInputStream(String bucket, String path) throws IOException {
        return readAsInputStream(bucket, path, null);
    }

    @Override
    public void readAsync(String bucket, String path, Callback<byte[], IOException> callback) {
        vertx.<byte[]>executeBlocking(promise -> {
            try {
                promise.complete(read(bucket, path));
            } catch (IOException e) {
                promise.fail(e);
            }
        }, res -> {
            if (res.succeeded()) {
                callback.onSuccess(res.result());
            } else {
                Throwable cause = res.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    @Override
    public void readAsInputStreamAsync(String bucket, String path, Callback<InputStream, IOException> callback) {
        vertx.<InputStream>executeBlocking(promise -> {
            try {
                promise.complete(readAsInputStream(bucket, path));
            } catch (IOException e) {
                promise.fail(e);
            }
        }, res -> {
            if (res.succeeded()) {
                callback.onSuccess(res.result());
            } else {
                Throwable cause = res.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    @Override
    public byte[] read(String bucket, String path, String params) throws IOException {
        OSSClient client = clientFactory.getClient();
        GetObjectRequest request = new GetObjectRequest(bucket, path);
        if (!Strings.isNullOrEmpty(params)) {
            request.setProcess(params);
        }
        byte[] bytes;
        OSSObject object = client.getObject(request);
        try(InputStream inputStream = object.getObjectContent();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            ByteStreams.copy(inputStream, outputStream);
            bytes = outputStream.toByteArray();
        } finally {
            client.shutdown();
        }
        return bytes;
    }

    private InputStream readAsInputStream(String bucket, String path, String params) throws IOException {
        OSSClient client = clientFactory.getClient();
        GetObjectRequest request = new GetObjectRequest(bucket, path);
        if (!Strings.isNullOrEmpty(params)) {
            request.setProcess(params);
        }
        OSSObject object = client.getObject(request);
        return object.getObjectContent();
    }

    @Override
    public void readAsync(String bucket, String path, String params, Callback<byte[], IOException> callback) {
        vertx.<byte[]>executeBlocking(promise -> {
            try {
                promise.complete(read(bucket, path, params));
            } catch (IOException e) {
                promise.fail(e);
            }
        }, res -> {
            if (res.succeeded()) {
                callback.onSuccess(res.result());
            } else {
                Throwable cause = res.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    private void readAsInputStreamAsync(String bucket, String path, String params, Callback<InputStream, IOException> callback) {
        vertx.<InputStream>executeBlocking(promise -> {
            try {
                promise.complete(readAsInputStream(bucket, path, params));
            } catch (IOException e) {
                promise.fail(e);
            }
        }, res -> {
            if (res.succeeded()) {
                callback.onSuccess(res.result());
            } else {
                Throwable cause = res.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    @Override
    public String createMultipartUpload(String bucket, String path) {
        return null;
    }

    @Override
    public Part multipartUpload(String bucket, String path, Integer chunk, InputStream data, String uploadId)
            throws IOException {
        return null;
    }

    @Override
    public String completeMultipartUpload(String bucket, String path, List<Part> parts, String uploadId) {
        return null;
    }


    @Override
    public String getExpireUrl(String bucketName, String key, Long expireMillisecond) {
        OSSClient client = clientFactory.getClient();

        GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(bucketName,key);
        urlRequest.setExpiration(new Date(System.currentTimeMillis() + expireMillisecond));

        return client.generatePresignedUrl(urlRequest).toString();
    }

    @Override
    public boolean dropFile(String bucket, String path) throws IOException {
        OSSClient client = clientFactory.getClient();
        client.deleteObject(bucket, path);
        return true;
    }

    @Override
    public void dropFileAsync(String bucket, String path, Callback<Boolean, IOException> callback) {
            vertx.<Boolean>executeBlocking(promise-> CompletableFuture.supplyAsync(()-> {
                OSSClient client = clientFactory.getClient();
                client.deleteObject(bucket, path);
                return true;
            }).whenComplete((r, err)-> {
              if (r != null) {
                  promise.complete(r);
              } else {
                  promise.fail(err);
              }

            }), res-> {
                if (res.succeeded()) {
                    callback.onSuccess(res.result());
                } else {
                    Throwable cause = res.cause();
                    if (cause instanceof IOException) {
                        callback.onException((IOException) cause);
                    } else {
                        throw new RuntimeException("oss 文件删除异常", cause);
                    }
                }
            });
    }
}
