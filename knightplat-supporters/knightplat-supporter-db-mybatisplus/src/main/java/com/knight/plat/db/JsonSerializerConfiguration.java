package com.knight.plat.db;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.commons.JsonRoot;
import com.knight.plat.commons.JsonTypeHandler;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfigImportFilter
@AutoConfiguration(before = MybatisPlusAutoConfiguration.class)
public class JsonSerializerConfiguration {

    private static final Logger log = LoggerFactory.getLogger(JsonSerializerConfiguration.class);

    @Bean
    public TypeHandler<JsonRoot> jsonObjectTypeHandler() {
        log.info("[Mybatis typeHandler]: Mybatis JsonTypeHandler Loaded.");
        return new JsonTypeHandler();
    }
}
