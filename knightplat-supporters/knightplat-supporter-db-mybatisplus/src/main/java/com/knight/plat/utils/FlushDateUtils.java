package com.knight.plat.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Date;

/**
 * Created on 2016/12/27. Utils to handle flush date.
 */
public class FlushDateUtils {

    private static final Logger log = LoggerFactory.getLogger(FlushDateUtils.class);

    public static long determineLastDate(JdbcTemplate jdbcTemplate, String createDateSql, String updateDateSql) {
        Date maxCreateDate;
        Date maxUpdateDate;
        try {
            maxCreateDate = jdbcTemplate.queryForObject(createDateSql, Date.class);
            maxUpdateDate = jdbcTemplate.queryForObject(updateDateSql, Date.class);
        } catch (DataAccessException e) {
            if (log.isErrorEnabled()) {
                log.error("Load last date error, returned 0.");
            }
            return 0;
        }
        if (null != maxCreateDate && null != maxUpdateDate) {
            long maxCreateDateLong = maxCreateDate.getTime();
            long maxUpdateDateLong = maxUpdateDate.getTime();
            return Math.max(maxCreateDateLong, maxUpdateDateLong);
        } else if (null != maxCreateDate) {
            return maxCreateDate.getTime();
        }
        return 0;
    }
}
