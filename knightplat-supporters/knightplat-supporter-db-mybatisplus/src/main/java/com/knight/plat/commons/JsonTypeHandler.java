package com.knight.plat.commons;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JsonTypeHandler extends BaseTypeHandler<JsonRoot> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, JsonRoot parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.encode());
    }

    @Override
    public JsonRoot getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String valueStr = rs.getString(columnName);
        if (valueStr == null) {
            return null;
        }
        return new JsonRoot(valueStr);
    }

    @Override
    public JsonRoot getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String valueStr = rs.getString(columnIndex);
        if (valueStr == null) {
            return null;
        }
        return new JsonRoot(valueStr);
    }

    @Override
    public JsonRoot getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String valueStr = cs.getString(columnIndex);
        if (valueStr == null) {
            return null;
        }
        return new JsonRoot(valueStr);
    }
}
