/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.db;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.github.pagehelper.PageInterceptor;
import com.knight.plat.api.annotations.AutoConfigImportFilter;
import org.apache.commons.beanutils.BeanMap;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * PageHelper plugin config.
 * Created  at 2016/8/1.
 * Edited  at 2020/1/5.
 *
 * @author knightplat
 */
@AutoConfigImportFilter
@AutoConfiguration(after = MybatisPlusAutoConfiguration.class)
@ConditionalOnBean(SqlSessionFactory.class)
public class MybatisPluginAutoConfiguration {

    private static final Logger log = LoggerFactory.getLogger(MybatisPluginAutoConfiguration.class);

    @Autowired
    public MybatisPluginAutoConfiguration(
            @SuppressWarnings("SpringJavaAutowiringInspection") SqlSessionFactory sqlSessionFactory) {

        List<Interceptor> interceptors = sqlSessionFactory.getConfiguration().getInterceptors();
        for (Interceptor interceptor : interceptors) {
            if (interceptor instanceof PageInterceptor) {
                log.info("[Mybatis plugin]: PageHelper existed.");
                return;
            }
        }

        PageInterceptor pageHelper = new PageInterceptor();
        Properties pageProperties = new Properties();
        pageProperties.setProperty("pageSizeZero", "true");
        pageHelper.setProperties(pageProperties);

        sqlSessionFactory.getConfiguration().addInterceptor(pageHelper);
        log.info("[Mybatis plugin]: Mybatis PageHelper Loaded.");

        QueryCollector queryCollector = new QueryCollector();
        UpdateCollector updateCollector = new UpdateCollector();
        sqlSessionFactory.getConfiguration().addInterceptor(queryCollector);
        sqlSessionFactory.getConfiguration().addInterceptor(updateCollector);
    }

    @Intercepts({@Signature(
            type = Executor.class,
            method = "query",
            args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
    private static class QueryCollector implements Interceptor {

        @Override
        @SuppressWarnings({"unchecked", "rawtypes"})
        public Object intercept(Invocation invocation) throws Throwable {
            final Object[] args = invocation.getArgs();
            MappedStatement statement = (MappedStatement) args[0];
            BoundSql boundSql = statement.getBoundSql(args[1]);
            if (log.isTraceEnabled()) {
                log.trace("[QUERY]: " + boundSql.getSql());
                Object parameterObject = boundSql.getParameterObject();
                Set<Map.Entry<Object, Object>> entries;
                if (parameterObject instanceof Map) {
                    entries = ((Map) parameterObject).keySet();
                } else {
                    entries = new BeanMap(parameterObject).entrySet();
                }
                log.trace("[QUERY]: " + entries);
            }
            return invocation.proceed();
        }

        @Override
        public Object plugin(Object target) {
            if (target instanceof Executor) {
                return Plugin.wrap(target, this);
            }
            return target;
        }

        @Override
        public void setProperties(Properties properties) {
            // nothing to be set up.
        }
    }

    @Intercepts({@Signature(
            type = Executor.class,
            method = "update",
            args = {MappedStatement.class, Object.class})})
    private static class UpdateCollector implements Interceptor {

        @Override
        @SuppressWarnings({"unchecked", "rawtypes"})
        public Object intercept(Invocation invocation) throws Throwable {
            final Object[] args = invocation.getArgs();
            MappedStatement statement = (MappedStatement) args[0];
            BoundSql boundSql = statement.getBoundSql(args[1]);
            if (log.isTraceEnabled()) {
                log.trace("[UPDATE]: " + boundSql.getSql());
                Object parameterObject = boundSql.getParameterObject();
                Set<Map.Entry<Object, Object>> entries;
                if (parameterObject instanceof Map) {
                    entries = ((Map) parameterObject).keySet();
                } else {
                    entries = new BeanMap(parameterObject).entrySet();
                }
                log.trace("[UPDATE]: " + entries);
            }
            return invocation.proceed();
        }

        @Override
        public Object plugin(Object target) {
            if (target instanceof Executor) {
                return Plugin.wrap(target, this);
            }
            return target;
        }

        @Override
        public void setProperties(Properties properties) {
            // nothing to be set up.
        }
    }

}
