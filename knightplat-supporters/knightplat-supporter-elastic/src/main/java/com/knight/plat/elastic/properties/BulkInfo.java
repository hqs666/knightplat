package com.knight.plat.elastic.properties;

import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;

/**
 * @author Yongqi Ren
 * @version 1.0
 * @description:
 * @date 2021/5/21 16:39
 */
public class BulkInfo {

    private int bulkActions = 1000;

    private int bulkSize = 5;

    private int flushInterval = 10;

    private int maxNumberOfRetries = 3;

    private int delay = 10;

    private int concurrentRequests = 1;

    public int getBulkActions() {
        return bulkActions;
    }

    public void setBulkActions(int bulkActions) {
        this.bulkActions = bulkActions;
    }

    public ByteSizeValue getBulkSize() {
        return new ByteSizeValue(this.bulkSize, ByteSizeUnit.MB);
    }

    public void setBulkSize(int bulkSize) {
        this.bulkSize = bulkSize;
    }

    public TimeValue getFlushInterval() {
        return TimeValue.timeValueSeconds(this.flushInterval);
    }

    public void setFlushInterval(int flushInterval) {
        this.flushInterval = flushInterval;
    }

    public int getMaxNumberOfRetries() {
        return maxNumberOfRetries;
    }

    public void setMaxNumberOfRetries(int maxNumberOfRetries) {
        this.maxNumberOfRetries = maxNumberOfRetries;
    }

    public TimeValue getDelay() {
        return TimeValue.timeValueMillis(this.delay);
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getConcurrentRequests() {
        return concurrentRequests;
    }

    public void setConcurrentRequests(int concurrentRequests) {
        this.concurrentRequests = concurrentRequests;
    }
}
