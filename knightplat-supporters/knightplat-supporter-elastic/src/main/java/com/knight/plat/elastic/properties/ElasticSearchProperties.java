package com.knight.plat.elastic.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
@ConfigurationProperties("knightplat.elasticsearch")
public class ElasticSearchProperties {

    private long keepAlive = -1;

    private BulkInfo bulk = new BulkInfo();

    private UserInfo user = new UserInfo();

    private List<HostInfo> host = new ArrayList<>(Collections.singletonList(new HostInfo()));

    private RequestConfigInfo requestConfig;

    private ConnectionConfigInfo connectionConfig;

    public List<HostInfo> getHost() {
        return host;
    }

    public void setHost(List<HostInfo> host) {
        this.host = host;
    }

    public BulkInfo getBulk() {
        return bulk;
    }

    public void setBulk(BulkInfo bulk) {
        this.bulk = bulk;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public long getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(long keepAlive) {
        this.keepAlive = keepAlive;
    }

    public RequestConfigInfo getRequestConfig() {
        return requestConfig;
    }

    public void setRequestConfig(RequestConfigInfo requestConfig) {
        this.requestConfig = requestConfig;
    }

    public ConnectionConfigInfo getConnectionConfig() {
        return connectionConfig;
    }

    public void setConnectionConfig(ConnectionConfigInfo connectionConfig) {
        this.connectionConfig = connectionConfig;
    }
}