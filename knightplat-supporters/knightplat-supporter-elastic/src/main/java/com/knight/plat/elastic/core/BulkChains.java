package com.knight.plat.elastic.core;

import com.beust.jcommander.internal.Lists;
import com.knight.plat.commons.JsonObject;
import com.knight.plat.utils.KeyGenUtils;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;

import java.util.List;


public class BulkChains {

    private final List<DocWriteRequest<?>> requests = Lists.newArrayList();

    private String index;

    private BulkChains() {
    }

    private BulkChains(String index) {
        this.index = index;
    }

    public static BulkChains newBulkChains(String index) {
        return new BulkChains(index);
    }

    public BulkChains addIndexRequest(String id, JsonObject jsonObject) {
        requests.add(new IndexRequest(index).opType(DocWriteRequest.OpType.INDEX).id(id).source(jsonObject.getMap()));
        return this;
    }

    public <T> BulkChains addIndexRequest(String id, T obj) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.INDEX)
                .id(id)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public <T> BulkChains addIndexRequestRouting(String id, T obj,String routing) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.INDEX)
                .id(id)
                .routing(routing)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }


    public BulkChains addCreateRequest(String id, JsonObject jsonObject) {
        requests.add(new IndexRequest(index).opType(DocWriteRequest.OpType.CREATE).id(id).source(jsonObject.getMap()));
        return this;
    }

    public <T> BulkChains addCreateRequest(String id, T obj) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.CREATE)
                .id(id)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }


    public <T> BulkChains addCreateRequestRouting(String id, T obj,String routing) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.CREATE)
                .routing(routing)
                .id(id)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public BulkChains addUpdateRequest(String id, JsonObject jsonObject) {
        requests.add(new UpdateRequest(index, id).doc(jsonObject.getMap()));
        return this;
    }


    public <T> BulkChains addUpdateRequest(String id, T obj) {
        requests.add(new UpdateRequest(index, id)
                .doc(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public <T> BulkChains addUpdateRequestRouting(String id, T obj,String routing) {
        requests.add(new UpdateRequest(index, id)
                .routing(routing)
                .doc(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public BulkChains addDeleteRequest(String id) {
        requests.add(new DeleteRequest(index, id));
        return this;
    }
    public BulkChains addDeleteRequestRouting(String id,String routing) {
        requests.add(new DeleteRequest(index, id).routing(routing));
        return this;
    }

    public BulkChains addIndexRequest(JsonObject jsonObject) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.INDEX)
                .id(String.valueOf(KeyGenUtils.snowFlake()))
                .source(jsonObject.getMap()));
        return this;
    }



    public <T> BulkChains addIndexRequest(T obj) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.INDEX)
                .id(String.valueOf(KeyGenUtils.snowFlake()))
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public <T> BulkChains addIndexRequestRouting(T obj,String routing) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.INDEX)
                .id(String.valueOf(KeyGenUtils.snowFlake()))
                .routing(routing)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public BulkChains addCreateRequest(JsonObject jsonObject) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.CREATE)
                .id(String.valueOf(KeyGenUtils.snowFlake()))
                .source(jsonObject.getMap()));
        return this;
    }


    public <T> BulkChains addCreateRequest(T obj) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.CREATE)
                .id(String.valueOf(KeyGenUtils.snowFlake()))
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public <T> BulkChains addCreateRequestRouting(T obj,String routing) {
        requests.add(new IndexRequest(index)
                .opType(DocWriteRequest.OpType.CREATE)
                .id(String.valueOf(KeyGenUtils.snowFlake()))
                .routing(routing)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public List<DocWriteRequest<?>> valRequest() {
        return requests;
    }
}
