package com.knight.plat.elastic.core;

import com.beust.jcommander.internal.Lists;
import com.knight.plat.elastic.entity.SubAggregationLocal;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;


public class AggregationLocal implements Serializable {

    private static final long serialVersionUID = 7582600758138841L;

    private List<SubAggregationLocal> subAggregationLocal = Lists.newArrayList();

    private String name;

    private String field;

    public AggregationLocal(String name, String field) {
        this.name = name;
        this.field = field;
    }

    private AggregationLocal() {
    }

    public static AggregationLocal builderAggregationLocal(String name, String field) {
        return new AggregationLocal(name, field);
    }

    public String getName() {
        return name;
    }

    public String getField() {
        return field;
    }

    public List<SubAggregationLocal> getSubAggregationLocal() {
        return subAggregationLocal;
    }

    private void addSubAggregationLocal(List<SubAggregationLocal> subAggregationLocals) {
        this.subAggregationLocal.addAll(subAggregationLocals);
    }

    private void addSubAggregationLocal(SubAggregationLocal... subAggregationLocals) {
        this.subAggregationLocal.addAll(Arrays.asList(subAggregationLocals));
    }

    public static final class Builder {
        private final List<SubAggregationLocal> subAggregationLocal = Lists.newArrayList();
        private String name;
        private String field;

        private Builder() {
        }

        public Builder subAggregationLocal(SubAggregationLocal... subAggregations) {
            this.subAggregationLocal.addAll(Arrays.asList(subAggregations));
            return this;
        }

        public Builder subAggregationLocal(String subName, String subField) {
            this.subAggregationLocal.add(new SubAggregationLocal(subName, subField));
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder field(String field) {
            this.field = field;
            return this;
        }

        public AggregationLocal build() {
            AggregationLocal aggregationLocalChains = new AggregationLocal(name, field);
            aggregationLocalChains.addSubAggregationLocal(subAggregationLocal);
            return aggregationLocalChains;
        }
    }

    public static SubAggregationLocal builderSubAggregation(String subName, String subField) {
        return new SubAggregationLocal(subName, subField);
    }

    public static Builder builder() {
        return new Builder();
    }
}
