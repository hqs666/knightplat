package com.knight.plat.elastic.entity;

public class ResultEntity {
    public static final Integer SUCCESS = 1;
    public static final Integer FAIL = 0;

    Integer resultType;
    Object resultData;
    Object falseMessage;

    public ResultEntity(Integer resultType, Object resultData) {
        this.resultType = resultType;
        this.resultData = resultData;
    }

    public Integer getResultType() {
        return resultType;
    }

    public void setResultType(Integer resultType) {
        this.resultType = resultType;
    }

    public Object getResultData() {
        return resultData;
    }

    public void setResultData(Object resultData) {
        this.resultData = resultData;
    }

    public Object getFalseMessage() {
        return falseMessage;
    }

    public void setFalseMessage(Object falseMessage) {
        this.falseMessage = falseMessage;
    }
}
