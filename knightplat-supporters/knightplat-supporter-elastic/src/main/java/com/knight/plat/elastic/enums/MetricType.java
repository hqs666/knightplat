package com.knight.plat.elastic.enums;


public enum MetricType {
    /**
     * 平均值
     */
    AVG(1),
    /**
     * 最大值
     */
    MAX(2),
    /**
     * 最小值
     */
    MIN(3),
    /**
     * 求和
     */
    SUM(4),
    /**
     *
     */
    STATS(5),
    /**
     * 总数
     */
    COUNT(6),

    DISTINCT(7);

    MetricType(){

    }

    MetricType(Integer code){
        this.code = code;
    }
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
