package com.knight.plat.elastic.core;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.commons.JsonArray;
import com.knight.plat.commons.JsonObject;
import com.knight.plat.commons.JsonRoot;
import com.knight.plat.elastic.core.base.AggregationsChainBase;
import com.knight.plat.elastic.core.base.BulkChainBase;
import com.knight.plat.elastic.core.base.QueryChainBase;
import com.knight.plat.elastic.core.base.SuggestChainBase;
import com.knight.plat.elastic.entity.DataEntity;
import com.knight.plat.elastic.entity.QueryEntity;
import com.knight.plat.elastic.entity.ResultEntity;
import com.knight.plat.elastic.enums.AnalyzerTypeEnums;
import com.knight.plat.elastic.enums.MetricType;
import com.knight.plat.elastic.exception.ElasticSearchException;
import com.knight.plat.utils.SerializationUtils;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.PrefixQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.bucket.range.RangeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.CardinalityAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.MaxAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.MinAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.StatsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.SumAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.ValueCountAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ElasticSearchManager {

    private final static Logger log = LoggerFactory.getLogger(ElasticSearchManager.class);

    public RestHighLevelClient getClient() {
        return client;
    }

    public BulkProcessor getBulkProcessor() {
        return bulkProcessor;
    }

    private final RestHighLevelClient client;

    private BulkProcessor bulkProcessor;

    public ElasticSearchManager(RestHighLevelClient client) {
        this.client = client;
    }

    public ElasticSearchManager(RestHighLevelClient client, BulkProcessor bulkProcessor) {
        this.client = client;
        this.bulkProcessor = bulkProcessor;
    }

    public QueryChain query() {
        return new QueryChain(client);
    }

    public AggregationsChain aggregation(QueryChain queryChain) {
        return new AggregationsChain(queryChain);
    }

    public AggregationsChain aggregation() {
        return new AggregationsChain(client);
    }

    public BulkChain bulk() {
        return new BulkChain(client);
    }

    public SuggestChain suggest() {
        return new SuggestChain(client);
    }

    public static class QueryChain extends QueryChainBase {
        public QueryChain(RestHighLevelClient client) {
            super(client);
        }
    }

    public static class SuggestChain extends SuggestChainBase {
        public SuggestChain(RestHighLevelClient client) {
            super(client);
        }
    }

    public static class BulkChain extends BulkChainBase {


        public BulkChain(RestHighLevelClient client) {
            super(client);
        }

        public BulkChain(RestHighLevelClient client, BulkProcessor bulkProcessor) {
            super(client, bulkProcessor);
        }
    }

    public static class AggregationsChain extends AggregationsChainBase {
        public AggregationsChain(RestHighLevelClient client) {
            super(client);
        }

        public AggregationsChain(QueryChain queryChain) {
            super(queryChain.client);
            super.searchSourceBuilder.query(queryChain.builder);
            for (FieldSortBuilder fieldSortBuilder : queryChain.sortBuilder) {
                searchSourceBuilder.sort(fieldSortBuilder);
            }
        }
    }

    /**
     * 添加索引
     *
     * @param index 索引字段值
     * @return ResultEntity resultData字段返回插入结果（true/false）
     */
    public ResultEntity indexAdd(String index) throws IOException {
        CreateIndexRequest indexRequest;
        if (index.isEmpty()) {
            return new ResultEntity(ResultEntity.FAIL, "索引为空");
        }
        if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
            return new ResultEntity(ResultEntity.FAIL, "索引已经存在");
        }
        indexRequest = new CreateIndexRequest(index);
        CreateIndexResponse createIndexResponse = client.indices().create(indexRequest, RequestOptions.DEFAULT);
        if (createIndexResponse.isAcknowledged()) {
            return new ResultEntity(ResultEntity.SUCCESS, "索引添加成功");
        } else {
            return new ResultEntity(ResultEntity.FAIL, "索引添加失败");
        }
    }

    /**
     * 查看所有索引，可以使用printHits获取Hits数据
     *
     * @param index 索引值
     * @return ResultEntity resultData字段返回查询结果SearchResponse
     */
    public ResultEntity indexQuery(String... index) throws IOException {
        if (ResultEntity.FAIL.equals(indexExist(index).getResultType())) {
            return new ResultEntity(ResultEntity.FAIL, "索引不存在");
        }
        SearchRequest searchRequest;
        SearchResponse response;
        searchRequest = (index == null ? new SearchRequest() : new SearchRequest(index));
        response = client.search(searchRequest, RequestOptions.DEFAULT);
        return new ResultEntity(ResultEntity.SUCCESS, new JsonRoot(JsonObject.mapFrom(response)));
    }

    /**
     * 删除多条索引
     *
     * @param index 索引值字段
     * @return ResultEntity 返回操作结果resultType=1，resultData=true为删除成功
     */
    public ResultEntity indexDelete(String... index) throws IOException {
        if (ResultEntity.FAIL.equals(indexExist(index).getResultType())) {
            return new ResultEntity(ResultEntity.FAIL, "索引不存在");
        }
        DeleteIndexRequest request = new DeleteIndexRequest(index);
        boolean acknowledged = client.indices()
                .delete(request, RequestOptions.DEFAULT).isAcknowledged();
        if (acknowledged) {
            return new ResultEntity(ResultEntity.SUCCESS, "删除成功");
        } else {
            return new ResultEntity(ResultEntity.FAIL, "删除失败");
        }

    }

    /**
     * 索引是否存在
     *
     * @param index 需要判断是否存在的多条索引值
     * @return ResultEntity 查询结果
     */
    public ResultEntity indexExist(String... index) throws IOException {
        if (index == null || index.length == 0) {
            return new ResultEntity(ResultEntity.FAIL, "索引输入为空");
        } else {
            for (String i : index) {
                if (i == null || "".equals(i)) {
                    return new ResultEntity(ResultEntity.FAIL, "索引输入为空");
                }
            }
        }
        GetIndexRequest request = new GetIndexRequest(index);
        if (client.indices().exists(request, RequestOptions.DEFAULT)) {
            return new ResultEntity(ResultEntity.SUCCESS, "索引存在");
        } else {
            return new ResultEntity(ResultEntity.FAIL, "索引不存在");
        }
    }

    /**
     * 添加document
     *
     * @param dataEntity 要添加的document实体，包含index，documentId，objectData
     * @return 返回添加结果 resultType为1且resultData结果为CREATE为添加成功
     */
    public ResultDTO<String> documentAdd(DataEntity dataEntity) throws IOException {
        if (ResultEntity.FAIL.equals(indexExist(dataEntity.getIndex()).getResultType())) {
            new ResultEntity(ResultEntity.FAIL, "索引不存在");
        }
        if (documentExists(dataEntity.getIndex(), dataEntity.getDocumentId())) {
            throw new ElasticSearchException("Document已经存在");
        }
        IndexRequest indexRequest = new IndexRequest(dataEntity.getIndex());
        indexRequest.id(dataEntity.getDocumentId());
        if (dataEntity.getJsonObject() != null) {
            indexRequest.source(dataEntity.getJsonObject().getMap());
        } else {
            throw new ElasticSearchException("Document数据为空");
        }
        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
        return ResultDTO.ok(indexResponse.status().toString());
    }

    /**
     * 检查文档是否存在
     *
     * @param index      文档索引
     * @param documentId 文档id
     * @return boolean
     */
    public boolean documentExists(@NotNull String index, @NotNull String documentId) throws IOException {
        GetRequest getRequest = new GetRequest(index, documentId);
        return client.exists(getRequest, RequestOptions.DEFAULT);
    }

    /**
     * 获得文档信息
     *
     * @param index      索引
     * @param documentId documentId
     * @return ResultDTO resultData返回document的数据信息
     */
    public ResultDTO<JsonRoot> documentInfo(String index, String documentId) throws IOException {
        if (index == null || documentId == null) {
            throw new ElasticSearchException("index或documentId为空");
        }
        if (!documentExists(index, documentId)) {
            throw new ElasticSearchException("文档不存在");
        }
        GetRequest getRequest = new GetRequest(index, documentId);
        GetResponse documentFields = client.get(getRequest, RequestOptions.DEFAULT);
        ResultDTO<JsonRoot> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(1L);
        resultDTO.setData(new JsonRoot(documentFields.getSourceAsMap()));
        return resultDTO;
    }


    /**
     * 更新文档的信息
     *
     * @param dataEntity 数据实体
     * @return ResultDTO 返回结果成功resultData为OK
     */
    public ResultDTO<String> documentUpdate(DataEntity dataEntity) throws IOException {
        if (dataEntity.getIndex() == null || dataEntity.getDocumentId() == null) {
            throw new ElasticSearchException("index或documentId为空");
        }
        if (!documentExists(dataEntity.getIndex(), dataEntity.getDocumentId())) {
            throw new ElasticSearchException("文档不存在");
        }
        UpdateRequest updateRequest = new UpdateRequest(dataEntity.getIndex(), dataEntity.getDocumentId());
        updateRequest.doc(dataEntity.getJsonObject().getMap());
        return ResultDTO.ok(client.update(updateRequest, RequestOptions.DEFAULT).status().toString());
    }

    /**
     * 删除文档记录
     *
     * @param index,documentId 需要传入index和documentId
     * @return ResultDTO 返回结果成功resultData为OK
     */
    public ResultDTO<String> documentDelete(String index, String documentId) throws IOException {
        if (index == null || documentId == null) {
            throw new ElasticSearchException("index或documentId为空");
        }
        if (!documentExists(index, documentId)) {
            throw new ElasticSearchException("文档不存在");
        }
        DeleteRequest request = new DeleteRequest(index, documentId);
        return ResultDTO.ok(client.delete(request, RequestOptions.DEFAULT).status().toString());
    }

    /**
     * 查询并删除
     *
     * @param queryBuilder 查询条件
     * @param batchSize    一次删除数量
     * @param index        索引
     * @return ResultDTO<BulkByScrollResponse>
     * @throws IOException 执行deleteByQuery异常
     */
    public ResultDTO<BulkByScrollResponse> deleteQuery(QueryBuilder queryBuilder, Integer batchSize, String index) throws IOException {
        DeleteByQueryRequest deleteByQueryRequest = new DeleteByQueryRequest();
        deleteByQueryRequest.setQuery(queryBuilder);
        deleteByQueryRequest.setBatchSize(batchSize);
        deleteByQueryRequest.indices(index);
        return ResultDTO.ok(client.deleteByQuery(deleteByQueryRequest, RequestOptions.DEFAULT));
    }

    /**
     * 使用ElasticSearch的查询参数
     *
     * @param queryBuilder ES查询QueryBuilder类
     * @return ResultDTO resultData中为SearchResponse对象
     */
    public ResultDTO<SearchResponse> documentSearchByQueryBuilder(QueryBuilder queryBuilder, @NotNull Integer from,
                                                                  @NotNull Integer size, String... index) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引不存在");
            }
        }
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.from(from);
        sourceBuilder.size(size);
        sourceBuilder.query(queryBuilder);
        searchRequest.source(sourceBuilder);
        return ResultDTO.ok(client.search(searchRequest, RequestOptions.DEFAULT));
    }

    /**
     * 单条件查询数据，根据queryType来判断查询方式
     *
     * @param field     字段名
     * @param queryType 查询类型
     * @param from      查询起始位置
     * @param size      查询数量
     * @param index     索引值
     * @param value     字段值
     * @return 查询结果
     * @throws IOException 查询异常
     */
    public ResultDTO<List<JsonObject>> searchDocumentByField(
            String field, Integer queryType, @NotNull Integer from, @NotNull Integer size, String index, String value)
            throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        QueryBuilder queryBuilder;
        switch (queryType) {
            case QueryEntity.MATCH_QUERY:
                queryBuilder = new MatchQueryBuilder(field, value);
                break;
            case QueryEntity.TERM_QUERY:
                queryBuilder = new TermQueryBuilder(field, value);
                break;
            case QueryEntity.WILDCARD_QUERY:
                queryBuilder = new WildcardQueryBuilder(field, value);
                break;
            case QueryEntity.PREFIX_QUERY:
                queryBuilder = new PrefixQueryBuilder(field, value);
                break;
            default:
                throw new ElasticSearchException("目前只支持MATCH_QUERY/TERM_QUERY/WILDCARD_QUERY/PREFIX_QUERY,其他查询方式请选择其他查询方法");
        }
        searchSourceBuilder.query(queryBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search));
        return resultDTO;
    }


    /**
     * 单条件查询数据，根据queryType来判断查询方式
     *
     * @param field     字段名
     * @param queryType 查询类型
     * @param from      查询起始位置
     * @param size      查询数量
     * @param index     索引值
     * @param value     字段值
     * @param clazz     指定泛型
     * @return 查询结果
     * @throws IOException 查询异常
     */
    public <T> ResultDTO<List<T>> searchDocumentByField(
            String field, Integer queryType, @NotNull Integer from, @NotNull Integer size, String index, String value, Class<T> clazz)
            throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        QueryBuilder queryBuilder;
        switch (queryType) {
            case QueryEntity.MATCH_QUERY:
                queryBuilder = new MatchQueryBuilder(field, value);
                break;
            case QueryEntity.TERM_QUERY:
                queryBuilder = new TermQueryBuilder(field, value);
                break;
            case QueryEntity.WILDCARD_QUERY:
                queryBuilder = new WildcardQueryBuilder(field, value);
                break;
            case QueryEntity.PREFIX_QUERY:
                queryBuilder = new PrefixQueryBuilder(field, value);
                break;
            default:
                throw new ElasticSearchException("目前只支持MATCH_QUERY/TERM_QUERY/WILDCARD_QUERY/PREFIX_QUERY,其他查询方式请选择其他查询方法");
        }
        searchSourceBuilder.query(queryBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search, clazz));
        return resultDTO;
    }


    /**
     * 模糊查询
     *
     * @param index         索引值
     * @param field         字段名
     * @param value         字段值
     * @param maxExpansions 最大匹配项数
     * @param from          查询起始位置
     * @param size          查询数量
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public ResultDTO<List<JsonObject>> searchDocumentByFuzzy(String index, String field, String value,
                                                             Integer maxExpansions, @NotNull Integer from, @NotNull Integer size) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.fuzzyQuery(field, value).maxExpansions(maxExpansions));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search));
        return resultDTO;
    }


    /**
     * 模糊查询
     *
     * @param index         索引值
     * @param field         字段名
     * @param value         字段值
     * @param maxExpansions 最大匹配项数
     * @param from          查询起始位置
     * @param size          查询数量
     * @param clazz         指定泛型
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public <T>ResultDTO<List<T>> searchDocumentByFuzzy(String index, String field, String value,
                                                             Integer maxExpansions, @NotNull Integer from,
                                                             @NotNull Integer size, Class<T> clazz) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.fuzzyQuery(field, value).maxExpansions(maxExpansions));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search,clazz));
        return resultDTO;
    }


    /**
     * 范围查询
     *
     * @param index        索引值
     * @param field        字段值
     * @param gt           大于条件
     * @param lt           小于条件
     * @param from         查询起始位置
     * @param size         查询数量
     * @param includeLower 是否包含下界
     * @param includeUpper 是否包含上界
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public ResultDTO<List<JsonObject>> searchDocumentByRange(String index, String field, String gt, String lt,
                                                             @NotNull Integer from, @NotNull Integer size,
                                                             boolean includeLower, boolean includeUpper) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.rangeQuery(field)
                .from(gt)
                .to(lt)
                .includeLower(includeLower)
                .includeUpper(includeUpper));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search));
        return resultDTO;
    }


    /**
     * 范围查询
     *
     * @param index        索引值
     * @param field        字段值
     * @param gt           大于条件
     * @param lt           小于条件
     * @param from         查询起始位置
     * @param size         查询数量
     * @param includeLower 是否包含下界
     * @param includeUpper 是否包含上界
     * @param clazz        指定泛型
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public <T> ResultDTO<List<T>> searchDocumentByRange(String index, String field, String gt, String lt,
                                                        @NotNull Integer from, @NotNull Integer size,
                                                        boolean includeLower, boolean includeUpper,
                                                        Class<T> clazz) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.rangeQuery(field)
                .from(gt)
                .to(lt)
                .includeLower(includeLower)
                .includeUpper(includeUpper));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search, clazz));
        return resultDTO;
    }

    /**
     * 多字段查询
     *
     * @param index 索引
     * @param value 字段值
     * @param from  查询起始位置
     * @param size  查询数量
     * @param field 字段值
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public ResultDTO<List<JsonObject>> searchDocumentByMultiMatch(String index, String value, @NotNull Integer from,
                                                                  @NotNull Integer size, String... field) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.multiMatchQuery(value, field));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search));
        return resultDTO;
    }

    /**
     * 多字段查询
     *
     * @param index 索引
     * @param value 字段值
     * @param from  查询起始位置
     * @param size  查询数量
     * @param clazz 指定泛型
     * @param field 字段值
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public <T> ResultDTO<List<T>> searchDocumentByMultiMatch(String index, String value, @NotNull Integer from,
                                                             @NotNull Integer size, Class<T> clazz,
                                                             String... field) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.multiMatchQuery(value, field));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search, clazz));
        return resultDTO;
    }

    /**
     * 通过stringQuery查询
     *
     * @param field 查询字段
     * @param value 查询值
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public ResultDTO<List<JsonObject>> searchDocumentByQueryString(String index, String field, String value,
                                                                   @NotNull Integer from, @NotNull Integer size) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.queryStringQuery(value).field(field));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search));
        return resultDTO;
    }

    /**
     * 通过stringQuery查询
     *
     * @param field 查询字段
     * @param value 查询值
     * @param clazz 指定泛型
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public <T> ResultDTO<List<T>> searchDocumentByQueryString(String index, String field, String value,
                                                              @NotNull Integer from, @NotNull Integer size,
                                                              Class<T> clazz) throws IOException {
        SearchRequest searchRequest;
        if (index == null) {
            searchRequest = new SearchRequest();
        } else {
            if (ResultEntity.SUCCESS.equals(indexExist(index).getResultType())) {
                searchRequest = new SearchRequest(index);
            } else {
                throw new ElasticSearchException("索引可能不存在");
            }
        }
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(QueryBuilders.queryStringQuery(value).field(field));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search, clazz));
        return resultDTO;
    }


    /**
     * 查询es中所有数据 数量大时使用 一次查询数据量最大不超过10000
     *
     * @param from  起始位置
     * @param size  数据量
     * @param index 索引值
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public ResultDTO<List<JsonObject>> searchAllDocument(@NotNull Integer from, @NotNull Integer size, String... index)
            throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(size);
        searchSourceBuilder.from(from);
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search));
        return resultDTO;
    }

    /**
     * 查询es中所有数据 数量大时使用 一次查询数据量最大不超过10000
     *
     * @param from  起始位置
     * @param size  数据量
     * @param clazz 指定泛型
     * @param index 索引值
     * @return ResultDTO中resultData为List<JsonRoot>
     */
    public <T> ResultDTO<List<T>> searchAllDocument(@NotNull Integer from, @NotNull Integer size,
                                                    Class<T> clazz, String... index)
            throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(size);
        searchSourceBuilder.from(from);
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(search.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(search, clazz));
        return resultDTO;
    }

    /**
     * 将SearchResponse对象中的Source值转换成Map格式，key为documentId，value为document的source
     *
     * @param searchResponse SearchResponse对象
     * @return Map格式的数据<documentId, < String, Object>>
     */
    public Map<String, Map<String, Object>> printHitsInMap(SearchResponse searchResponse) {
        Map<String, Map<String, Object>> map = new HashMap<>();
        Map<String, Object> mapHit = new HashMap<>();
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            mapHit.putAll(hit.getSourceAsMap());
            map.put(hit.getId(), mapHit);
        }
        return map;
    }

    /**
     * 方法已废弃，最新方法地址：QueryChains#printHitsInJsonRootList
     * <p>
     * 将SearchResponse对象中的Source值转换成List格式
     *
     * @param searchResponse SearchResponse对象
     * @return List格式的数据<String, Object>
     */
    @Deprecated
    public static List<JsonObject> printHitsInJsonRootList(SearchResponse searchResponse) {
        List<JsonObject> list = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            list.add(new JsonObject(hit.getSourceAsMap()));
        }
        return list;
    }

    /**
     * 方法已废弃，最新方法地址：QueryChains#printInnerHitsInJsonRootList
     *
     * 将SearchResponse innerHits对象中的Source值转换成List格式
     *
     * @param searchResponse SearchResponse对象
     * @return List格式的数据<String, Object>
     */
    @Deprecated
    public static List<JsonObject> printInnerHitsInJsonRootList(SearchResponse searchResponse) {
        List<JsonObject> list = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            JsonObject jsonObject = new JsonObject(hit.getSourceAsMap());

            Map<String, SearchHits> map = hit.getInnerHits();
            Collection<SearchHits> values = map.values();

            for (SearchHits object:values) {
                SearchHit[] results = object.getHits();
                for (SearchHit innerHit : results) {
                    jsonObject.put("inner_hit", innerHit.getSourceAsMap());
                }
            }

            list.add(jsonObject);
        }
        return list;
    }

    /**
     * 方法已废弃，最新方法地址：QueryChains#printHitsInJsonRootList
     *
     * 将SearchResponse对象中的Source值转换成List格式
     *
     * @param searchResponse SearchResponse对象
     * @return List格式的数据<String, Object>
     */
    @Deprecated
    public static <T> List<T> printHitsInJsonRootList(SearchResponse searchResponse, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            list.add(SerializationUtils.jsonDeserialize(SerializationUtils.jsonSerialize(hit.getSourceAsMap()), clazz));
        }
        return list;
    }


    /**
     * 方法已废弃，最新方法地址：AggregationChains#printAggregationInJsonArray
     *
     * 将SearchResponse对象中的Aggregation值转换成JsonObject格式
     *
     * @param searchResponse SearchResponse对象
     * @return JsonObject
     */
    @Deprecated
    public static JsonArray printAggregationInJsonArray(final SearchResponse searchResponse, String name) {
        Aggregation agg = searchResponse.getAggregations().get(name);
        JsonObject result = new JsonObject(Strings.toString(agg));
        return result.getJsonObject("sterms#" + name + "").getJsonArray("buckets");
    }


    /**
     * 获取SearchResponse对象中的Took
     *
     * @param searchResponse SearchResponse对象
     * @return TimeValue 耗时
     */
    public TimeValue getTook(SearchResponse searchResponse) {
        return searchResponse.getTook();
    }

    /**
     * 获取SearchResponse对象中的Hits总条数
     *
     * @param searchResponse SearchResponse对象
     * @return Hits总条数
     */
    public long getHitsTotalCount(SearchResponse searchResponse) {
        return searchResponse.getHits().getTotalHits().value;
    }

    /**
     * 平均值聚合查询: AggregationBuilders.avg(name).field(field)
     *
     * @param index 索引
     * @param name  聚合名称
     * @param field 聚合字段
     * @param type  聚合类型
     * @return 查询结果JsonObject
     * @throws IOException
     */
    public JsonObject metricAggregation(String index, String name, String field, MetricType type) throws IOException {
        //创建一个查询请求，并指定索引名称
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        switch (type) {
            case AVG:
                AvgAggregationBuilder avgAggregationBuilder = AggregationBuilders.avg(name).field(field);
                searchSourceBuilder.aggregation(avgAggregationBuilder);
                break;
            case MAX:
                MaxAggregationBuilder maxAggregationBuilder = AggregationBuilders.max(name).field(field);
                searchSourceBuilder.aggregation(maxAggregationBuilder);
                break;
            case MIN:
                MinAggregationBuilder minAggregationBuilder = AggregationBuilders.min(name).field(field);
                searchSourceBuilder.aggregation(minAggregationBuilder);
                break;
            case SUM:
                SumAggregationBuilder sumAggregationBuilder = AggregationBuilders.sum(name).field(field);
                searchSourceBuilder.aggregation(sumAggregationBuilder);
                break;
            case STATS:
                StatsAggregationBuilder statsAggregationBuilder = AggregationBuilders.stats(name).field(field);
                searchSourceBuilder.aggregation(statsAggregationBuilder);
                break;
            case COUNT:
                ValueCountAggregationBuilder valueCountAggregationBuilder = AggregationBuilders.count(name)
                        .field(field);
                searchSourceBuilder.aggregation(valueCountAggregationBuilder);
                break;
            case DISTINCT:
                CardinalityAggregationBuilder cardinalityAggregationBuilder = AggregationBuilders.cardinality(name)
                        .field(field);
                searchSourceBuilder.aggregation(cardinalityAggregationBuilder);
                break;
        }
        //聚合查询
        searchRequest.source(searchSourceBuilder);
        SearchResponse response;
        Aggregations aggregations;
        //发起请求，获取响应结果
        response = client.search(searchRequest, RequestOptions.DEFAULT);
        //获取聚合的结果
        aggregations = response.getAggregations();
        if (aggregations == null) {
            throw new ElasticSearchException("Got a null aggregations from search response");
        }
        return JsonObject.mapFrom(aggregations.getAsMap().get(name));
    }

    /**
     * 平均值聚合查询: AggregationBuilders.avg(name).field(field)
     *
     * @param index 索引
     * @param name  聚合名称
     * @param field 聚合字段
     * @param type  聚合类型
     * @param clazz 指定泛型
     * @return 查询结果JsonObject
     * @throws IOException
     */
    public <T> T metricAggregation(String index, String name, String field, MetricType type,Class<T> clazz) throws IOException {
        //创建一个查询请求，并指定索引名称
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        switch (type) {
            case AVG:
                AvgAggregationBuilder avgAggregationBuilder = AggregationBuilders.avg(name).field(field);
                searchSourceBuilder.aggregation(avgAggregationBuilder);
                break;
            case MAX:
                MaxAggregationBuilder maxAggregationBuilder = AggregationBuilders.max(name).field(field);
                searchSourceBuilder.aggregation(maxAggregationBuilder);
                break;
            case MIN:
                MinAggregationBuilder minAggregationBuilder = AggregationBuilders.min(name).field(field);
                searchSourceBuilder.aggregation(minAggregationBuilder);
                break;
            case SUM:
                SumAggregationBuilder sumAggregationBuilder = AggregationBuilders.sum(name).field(field);
                searchSourceBuilder.aggregation(sumAggregationBuilder);
                break;
            case STATS:
                StatsAggregationBuilder statsAggregationBuilder = AggregationBuilders.stats(name).field(field);
                searchSourceBuilder.aggregation(statsAggregationBuilder);
                break;
            case COUNT:
                ValueCountAggregationBuilder valueCountAggregationBuilder = AggregationBuilders.count(name)
                        .field(field);
                searchSourceBuilder.aggregation(valueCountAggregationBuilder);
                break;
            case DISTINCT:
                CardinalityAggregationBuilder cardinalityAggregationBuilder = AggregationBuilders.cardinality(name)
                        .field(field);
                searchSourceBuilder.aggregation(cardinalityAggregationBuilder);
                break;
        }
        //聚合查询
        searchRequest.source(searchSourceBuilder);
        SearchResponse response;
        Aggregations aggregations;
        //发起请求，获取响应结果
        response = client.search(searchRequest, RequestOptions.DEFAULT);
        //获取聚合的结果
        aggregations = response.getAggregations();
        if (aggregations == null) {
            throw new ElasticSearchException("Got a null aggregations from search response");
        }
        return JsonObject.mapFrom(aggregations.getAsMap().get(name)).mapTo(clazz);
    }


    /**
     * term自定义分组桶聚合查询:
     *
     * @param index 索引名称
     * @param name  聚合名称
     * @param field 字段名称
     * @param size  指示应该返回多少个分组，默认10个
     * @return JsonObject
     * @throws IOException
     */
    public JsonObject termsAggregation(String index, String name, String field, Integer size) throws IOException {
        //创建一个查询请求，并指定索引名称
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders.terms(name).field(field).size(size);
        searchSourceBuilder.aggregation(termsAggregationBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response;
        Map<String, Long> termsMap = new HashMap<>();
        //发起请求，获取响应结果
        response = client.search(searchRequest, RequestOptions.DEFAULT);
        //获取聚合的结果
        Aggregations aggregations = response.getAggregations();
        Aggregation aggregation = aggregations.get(name);
        //获取桶聚合结果
        List<? extends Terms.Bucket> buckets = ((Terms) aggregation).getBuckets();
        //循环遍历各个桶结果
        for (Terms.Bucket bucket : buckets) {
            //分组的key
            String key = bucket.getKeyAsString();
            //分组的值
            long docCount = bucket.getDocCount();
            termsMap.put(key, docCount);
        }
        return JsonObject.mapFrom(termsMap);
    }

    /**
     * term自定义分组桶聚合查询:
     *
     * @param index 索引名称
     * @param name  聚合名称
     * @param field 字段名称
     * @param size  指示应该返回多少个分组，默认10个
     * @param clazz 指示泛型
     * @return JsonObject
     * @throws IOException
     */
    public <T> T termsAggregation(String index, String name, String field, Integer size,Class<T> clazz) throws IOException {
        //创建一个查询请求，并指定索引名称
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders.terms(name).field(field).size(size);
        searchSourceBuilder.aggregation(termsAggregationBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response;
        Map<String, Long> termsMap = new HashMap<>();
        //发起请求，获取响应结果
        response = client.search(searchRequest, RequestOptions.DEFAULT);
        //获取聚合的结果
        Aggregations aggregations = response.getAggregations();
        Aggregation aggregation = aggregations.get(name);
        //获取桶聚合结果
        List<? extends Terms.Bucket> buckets = ((Terms) aggregation).getBuckets();
        //循环遍历各个桶结果
        for (Terms.Bucket bucket : buckets) {
            //分组的key
            String key = bucket.getKeyAsString();
            //分组的值
            long docCount = bucket.getDocCount();
            termsMap.put(key, docCount);
        }
        return JsonObject.mapFrom(termsMap).mapTo(clazz);
    }

    /**
     * range范围桶聚合查询:
     *
     * @param index    索引名称
     * @param name     聚合名称
     * @param field    字段名称
     * @param rangeMap 范围Map
     * @return 查询结果 JsonObject
     * @throws IOException
     */
    public JsonObject rangeAggregation(String index, String name, String field, Map<Double, Double> rangeMap)
            throws IOException {
        //创建一个查询请求，并指定索引名称
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        RangeAggregationBuilder rangeAggregationBuilder = AggregationBuilders.range(name + ".keyword")
                .field(field);
        rangeMap.forEach(rangeAggregationBuilder::addRange);
        searchSourceBuilder.aggregation(rangeAggregationBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response;
        Map<String, Long> rangeCountMap = new HashMap<>();
        //发起请求，获取响应结果
        response = client.search(searchRequest, RequestOptions.DEFAULT);
        //获取聚合的结果
        Aggregations aggregations = response.getAggregations();
        Aggregation aggregation = aggregations.get(name);
        //获取桶聚合结果
        List<? extends Range.Bucket> buckets = ((Range) aggregation).getBuckets();
        //循环遍历各个桶结果
        for (Range.Bucket bucket : buckets) {
            //分组的key
            String key = bucket.getKeyAsString();
            //分组的值
            long docCount = bucket.getDocCount();
            rangeCountMap.put(key, docCount);
        }
        return JsonObject.mapFrom(rangeCountMap);
    }

    /**
     * range范围桶聚合查询:
     *
     * @param index    索引名称
     * @param name     聚合名称
     * @param field    字段名称
     * @param rangeMap 范围Map
     * @param clazz    指定泛型
     * @return 查询结果 JsonObject
     * @throws IOException
     */
    public <T> T rangeAggregation(String index, String name, String field, Map<Double, Double> rangeMap, Class<T> clazz)
            throws IOException {
        //创建一个查询请求，并指定索引名称
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        RangeAggregationBuilder rangeAggregationBuilder = AggregationBuilders.range(name + ".keyword")
                .field(field);
        rangeMap.forEach(rangeAggregationBuilder::addRange);
        searchSourceBuilder.aggregation(rangeAggregationBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response;
        Map<String, Long> rangeCountMap = new HashMap<>();
        //发起请求，获取响应结果
        response = client.search(searchRequest, RequestOptions.DEFAULT);
        //获取聚合的结果
        Aggregations aggregations = response.getAggregations();
        Aggregation aggregation = aggregations.get(name);
        //获取桶聚合结果
        List<? extends Range.Bucket> buckets = ((Range) aggregation).getBuckets();
        //循环遍历各个桶结果
        for (Range.Bucket bucket : buckets) {
            //分组的key
            String key = bucket.getKeyAsString();
            //分组的值
            long docCount = bucket.getDocCount();
            rangeCountMap.put(key, docCount);
        }
        return JsonObject.mapFrom(rangeCountMap).mapTo(clazz);
    }

    /**
     * 获取分词结果
     *
     * @param text              分词内容
     * @param analyzerTypeEnums 分词方式
     * @return List<String>
     * @throws IOException
     */
    public List<String> getAnalyze(String text, AnalyzerTypeEnums analyzerTypeEnums) throws IOException {
        JsonObject entity = new JsonObject();
        Request request = new Request("GET", "_analyze");
        entity.put("analyzer", analyzerTypeEnums.getType());
        entity.put("text", text);
        request.setJsonEntity(entity.toString());
        Response response = client.getLowLevelClient().performRequest(request);
        JsonObject tokens = new JsonObject(EntityUtils.toString(response.getEntity()));
        JsonArray arrays = tokens.getJsonArray("tokens");
        List<String> analyzeResult = new ArrayList<>();
        for (int i = 0; i < arrays.size(); i++) {
            analyzeResult.add(arrays.getJsonObject(i).getString("token"));
        }
        return analyzeResult;
    }
}

