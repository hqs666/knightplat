package com.knight.plat.elastic.enums;


public enum AnalyzerTypeEnums {
    /**
     * IK分词器，会做最粗粒度的拆分，已被分出的词语将不会再次被其它词语占有
     */
    IK_SMART("ik_smart"),
    /**
     * IK分词器，做最细粒度的拆分，尽可能多的拆分出词语
     */
    IK_MAX_WORD("ik_max_word"),
    /**
     * 标准分词器
     */
    STANDARD("standard"),
    /**
     * keyword 分词器，不分词，整个输入作为一个单独词汇单元，方便特殊类型的文本进行索引和检索。针对邮政编码，地址等文本信息使用关键词分词器进行索引项建立非常方便。
     */
    KEYWORD("keyword"),
    /**
     * pattern 分词器，正则表达式在匹配分词器
     */
    PATTERN("pattern"),
    /**
     * whitespace 分词器，空格为分隔符
     */
    WHITESPACE("whitespace");

    AnalyzerTypeEnums(){

    }

    AnalyzerTypeEnums(String type){
        this.type = type;
    }
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
