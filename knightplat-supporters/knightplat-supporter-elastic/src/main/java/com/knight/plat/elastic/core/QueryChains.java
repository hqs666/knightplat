package com.knight.plat.elastic.core;

import com.google.common.collect.Maps;
import com.knight.plat.commons.JsonObject;
import com.knight.plat.utils.SerializationUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public final class QueryChains {

    private final static HashMap<String, Object> queryMusts = Maps.newHashMap();

    public static QueryChains newQueryChains() {
        return new QueryChains();
    }

    /**
     * 将SearchResponse innerHits对象中的Source值转换成List格式
     *
     * @param searchResponse SearchResponse对象
     * @param clazz          指定泛型
     * @return List格式的数据<String, Object>
     */
    public static <T> List<T> printInnerHitsInJsonRootList(SearchResponse searchResponse, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            JsonObject jsonObject = new JsonObject(hit.getSourceAsMap());

            Map<String, SearchHits> map = hit.getInnerHits();
            Collection<SearchHits> values = map.values();

            for (SearchHits object : values) {
                SearchHit[] results = object.getHits();
                for (SearchHit innerHit : results) {
                    jsonObject.put("inner_hit", innerHit.getSourceAsMap());
                }
            }

            list.add(jsonObject.mapTo(clazz));
        }
        return list;
    }

    public <V> QueryChains must(String k, V value) {
        queryMusts.put(k, value);
        return this;
    }

    /**
     * Bool嵌套查询 must 带权重
     *
     * @param boost 权重值
     * @return BoolQueryBuilder
     */
    public BoolQueryBuilder builderMusts(float boost) {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        queryMusts.forEach(((k, v) -> {
            if (v instanceof List)
                ((List<?>) v).forEach(obj -> boolQueryBuilder.should(QueryBuilders.matchQuery(k, obj)));
            else boolQueryBuilder.must(QueryBuilders.matchQuery(k, v));
        }));
        return boolQueryBuilder.boost(boost);
    }

    /**
     * Bool嵌套查询 must 不带权重
     *
     * @return BoolQueryBuilder
     */
    public BoolQueryBuilder builderMusts() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        queryMusts.forEach(((k, v) -> {
            if (v instanceof List)
                ((List<?>) v).forEach(obj -> boolQueryBuilder.should(QueryBuilders.matchQuery(k, obj)));
            else boolQueryBuilder.must(QueryBuilders.matchQuery(k, v));
        }));
        return boolQueryBuilder;
    }


    /**
     * 将SearchResponse对象中的Source值转换成List格式
     *
     * @param searchResponse SearchResponse对象
     * @return List格式的数据<String, Object>
     */
    public static List<JsonObject> printHitsInJsonRootList(SearchResponse searchResponse) {
        List<JsonObject> list = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            list.add(new JsonObject(hit.getSourceAsMap()));
        }
        return list;
    }

    /**
     * 将SearchResponse对象中的Source值转换成List格式
     *
     * @param searchResponse SearchResponse对象
     * @return List格式的数据<String, Object>
     */
    public static <T> List<T> printHitsInJsonRootList(SearchResponse searchResponse, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            list.add(SerializationUtils.jsonDeserialize(SerializationUtils.jsonSerialize(hit.getSourceAsMap()), clazz));
        }
        return list;
    }

    /**
     * true 只读
     * false 非只读
     *
     * @param settings 配置列表
     * @return
     */
    public boolean indexBlocksReadOnly(Settings settings) {
        return settings.getAsBoolean("index.blocks.read_only", false);
    }
}
