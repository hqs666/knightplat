package com.knight.plat.elastic.entity;

import java.util.Map;

public class QueryEntity {
    public static final int MATCH_QUERY = 0;
    public static final int TERM_QUERY = 1;
    public static final int WILDCARD_QUERY = 2;
    public static final int PREFIX_QUERY = 3;
//    public static final int FUZZY_QUERY = 4;
//    public static final int RANGE_QUERY = 5;
//    public static final int QUERY_STRING = 6;
//    public static final int TEXT_QUERY = 7;
//    public static final int MISSING_QUERY = 8;

    Integer size;
    Integer from;
    String index;
//    String type;
    String field;
    String documentId;

    Map<String,Object> andMap;
    Map<String,Object> orMap;
    Map<String,Object> timeBetweenMap;
    Map<String,Object> numberBetweenMap;
    Map<String,Object> inMap;
    Map<String,Object> notInMap;
    Map<String,Object> likeMap;


    //查询类型
    Integer queryType;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }


    public Integer getQueryType() {
        return queryType;
    }

    public void setQueryType(Integer queryType) {
        this.queryType = queryType;
    }

//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }

    public Map<String, Object> getAndMap() {
        return andMap;
    }

    public void setAndMap(Map<String, Object> andMap) {
        this.andMap = andMap;
    }

    public Map<String, Object> getOrMap() {
        return orMap;
    }

    public void setOrMap(Map<String, Object> orMap) {
        this.orMap = orMap;
    }

    public Map<String, Object> getTimeBetweenMap() {
        return timeBetweenMap;
    }

    public void setTimeBetweenMap(Map<String, Object> timeBetweenMap) {
        this.timeBetweenMap = timeBetweenMap;
    }

    public Map<String, Object> getNumberBetweenMap() {
        return numberBetweenMap;
    }

    public void setNumberBetweenMap(Map<String, Object> numberBetweenMap) {
        this.numberBetweenMap = numberBetweenMap;
    }

    public Map<String, Object> getInMap() {
        return inMap;
    }

    public void setInMap(Map<String, Object> inMap) {
        this.inMap = inMap;
    }

    public Map<String, Object> getNotInMap() {
        return notInMap;
    }

    public void setNotInMap(Map<String, Object> notInMap) {
        this.notInMap = notInMap;
    }

    public Map<String, Object> getLikeMap() {
        return likeMap;
    }

    public void setLikeMap(Map<String, Object> likeMap) {
        this.likeMap = likeMap;
    }

}
