package com.knight.plat.elastic.properties;

import io.netty.util.internal.StringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

/**
 * Create by neo | 2021/8/6 10:39
 */
public class RequestConfigInfo {

    private boolean expectContinueEnabled;
    private boolean staleConnectionCheckEnabled;
    private String cookieSpec;
    private boolean redirectsEnabled;
    private boolean relativeRedirectsAllowed;
    private boolean circularRedirectsAllowed;
    private int maxRedirects;
    private boolean authenticationEnabled;
    private Collection<String> targetPreferredAuthSchemes;
    private Collection<String> proxyPreferredAuthSchemes;
    private int connectionRequestTimeout;
    private int connectTimeout;
    private int socketTimeout;
    private boolean contentCompressionEnabled;
    private boolean normalizeUri;

    private final static Log logger = LogFactory.getLog(RequestConfigInfo.class);

    /**
     * example http://172.0.0.1:8080
     */
    private String httpHostStr;

    /**
     * ipv4 host or ipv6 host
     */
    private String inetAddressStr;

    public boolean isExpectContinueEnabled() {
        return expectContinueEnabled;
    }

    public void setExpectContinueEnabled(boolean expectContinueEnabled) {
        this.expectContinueEnabled = expectContinueEnabled;
    }

    public boolean isStaleConnectionCheckEnabled() {
        return staleConnectionCheckEnabled;
    }

    public void setStaleConnectionCheckEnabled(boolean staleConnectionCheckEnabled) {
        this.staleConnectionCheckEnabled = staleConnectionCheckEnabled;
    }

    public String getCookieSpec() {
        return cookieSpec;
    }

    public void setCookieSpec(String cookieSpec) {
        this.cookieSpec = cookieSpec;
    }

    public boolean isRedirectsEnabled() {
        return redirectsEnabled;
    }

    public void setRedirectsEnabled(boolean redirectsEnabled) {
        this.redirectsEnabled = redirectsEnabled;
    }

    public boolean isRelativeRedirectsAllowed() {
        return relativeRedirectsAllowed;
    }

    public void setRelativeRedirectsAllowed(boolean relativeRedirectsAllowed) {
        this.relativeRedirectsAllowed = relativeRedirectsAllowed;
    }

    public boolean isCircularRedirectsAllowed() {
        return circularRedirectsAllowed;
    }

    public void setCircularRedirectsAllowed(boolean circularRedirectsAllowed) {
        this.circularRedirectsAllowed = circularRedirectsAllowed;
    }

    public int getMaxRedirects() {
        return maxRedirects;
    }

    public void setMaxRedirects(int maxRedirects) {
        this.maxRedirects = maxRedirects;
    }

    public boolean isAuthenticationEnabled() {
        return authenticationEnabled;
    }

    public void setAuthenticationEnabled(boolean authenticationEnabled) {
        this.authenticationEnabled = authenticationEnabled;
    }

    public Collection<String> getTargetPreferredAuthSchemes() {
        return targetPreferredAuthSchemes;
    }

    public void setTargetPreferredAuthSchemes(Collection<String> targetPreferredAuthSchemes) {
        this.targetPreferredAuthSchemes = targetPreferredAuthSchemes;
    }

    public Collection<String> getProxyPreferredAuthSchemes() {
        return proxyPreferredAuthSchemes;
    }

    public void setProxyPreferredAuthSchemes(Collection<String> proxyPreferredAuthSchemes) {
        this.proxyPreferredAuthSchemes = proxyPreferredAuthSchemes;
    }

    public int getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(int connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getSocketTimeout() {
        return socketTimeout;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public boolean isContentCompressionEnabled() {
        return contentCompressionEnabled;
    }

    public void setContentCompressionEnabled(boolean contentCompressionEnabled) {
        this.contentCompressionEnabled = contentCompressionEnabled;
    }

    public boolean isNormalizeUri() {
        return normalizeUri;
    }

    public void setNormalizeUri(boolean normalizeUri) {
        this.normalizeUri = normalizeUri;
    }

    public String getHttpHostStr() {
        return httpHostStr;
    }

    public void setHttpHostStr(String httpHostStr) {
        this.httpHostStr = httpHostStr;
    }

    public String getInetAddressStr() {
        return inetAddressStr;
    }

    public void setInetAddressStr(String inetAddressStr) {
        this.inetAddressStr = inetAddressStr;
    }


    public RequestConfig getConfig() {
        HttpHost httpHost = null;
        final String httpHostStr = this.getHttpHostStr();
        if (!StringUtil.isNullOrEmpty(httpHostStr)) {
            httpHost = HttpHost.create(httpHostStr);
        }

        InetAddress inetAddress = null;
        try {
            if (!StringUtil.isNullOrEmpty(this.inetAddressStr)) {
                inetAddress = InetAddress.getByName(this.inetAddressStr);
            }
        } catch (UnknownHostException e) {
            logger.error("check your 'inetAddressStr' value :", e);
        }

        return RequestConfig.custom()
                .setAuthenticationEnabled(this.isAuthenticationEnabled())
                .setCircularRedirectsAllowed(this.circularRedirectsAllowed)
                .setConnectionRequestTimeout(this.connectionRequestTimeout)
                .setConnectTimeout(this.connectTimeout)
                .setContentCompressionEnabled(this.contentCompressionEnabled)
                .setCookieSpec(this.cookieSpec)
                .setExpectContinueEnabled(this.expectContinueEnabled)
                .setLocalAddress(inetAddress)
                .setMaxRedirects(this.maxRedirects)
                .setNormalizeUri(this.normalizeUri)
                .setProxy(httpHost)
                .setProxyPreferredAuthSchemes(this.proxyPreferredAuthSchemes)
                .setRedirectsEnabled(this.redirectsEnabled)
                .setRelativeRedirectsAllowed(this.relativeRedirectsAllowed)
                .setSocketTimeout(this.socketTimeout)
                .setTargetPreferredAuthSchemes(this.targetPreferredAuthSchemes)
                .setStaleConnectionCheckEnabled(this.staleConnectionCheckEnabled)
                .build();

    }
}
