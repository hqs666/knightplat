package com.knight.plat.elastic.exception;

import com.knight.plat.api.exceptions.BusinessException;

public class ElasticSearchException extends BusinessException {

    private static final String code = "RYES01";

    public ElasticSearchException() {
        setCode(code);
    }

    public ElasticSearchException(String message) {
        super(message);
        setCode(code);
    }

    public ElasticSearchException(String message, Throwable cause) {
        super(message, cause);
        setCode(code);
    }

    public ElasticSearchException(Throwable cause) {
        super(cause);
        setCode(code);
    }
}
