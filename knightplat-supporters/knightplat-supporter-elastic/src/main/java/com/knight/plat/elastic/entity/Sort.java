package com.knight.plat.elastic.entity;

public enum Sort {
    DESC("-"),
    ASC("+");

    String sortChar;

    private Sort(String sortChar) {
        this.sortChar = sortChar;
    }

    public String getChar() {
        return this.sortChar;
    }
}
