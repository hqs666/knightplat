package com.knight.plat.elastic.entity;

import com.beust.jcommander.internal.Lists;

import java.util.Arrays;
import java.util.List;


public class SubAggregationLocal {
    private final String subName;

    private final String subField;

    private final List<SubAggregationLocal> subAggregationLocal = Lists.newArrayList();

    public SubAggregationLocal(String subName, String subField) {
        this.subName = subName;
        this.subField = subField;
    }

    public static SubAggregationLocal builderSubAggregationLocal(String subName, String subField) {
        return new SubAggregationLocal(subName, subField);
    }

    public void addSubAggregationLocal(List<SubAggregationLocal> subAggregations) {
        this.subAggregationLocal.addAll(subAggregations);
    }

    public void addSubAggregationLocal(SubAggregationLocal... subAggregations) {
        this.subAggregationLocal.addAll(Arrays.asList(subAggregations));
    }

    public String getSubName() {
        return subName;
    }

    public String getSubField() {
        return subField;
    }

    public List<SubAggregationLocal> getSubAggregationLocal() {
        return subAggregationLocal;
    }

    public static final class Builder {
        private String subName;
        private String subField;
        private final List<SubAggregationLocal> subAggregationLocals = Lists.newArrayList();

        private Builder() {
        }

        public Builder subName(String subName) {
            this.subName = subName;
            return this;
        }

        public Builder subField(String subField) {
            this.subField = subField;
            return this;
        }

        public Builder subAggregationLocal(SubAggregationLocal... subAggregations) {
            this.subAggregationLocals.addAll(Arrays.asList(subAggregations));
            return this;
        }

        public Builder subAggregationLocal(String subName, String subField) {
            this.subAggregationLocals.add(SubAggregationLocal.builderSubAggregationLocal(subName, subField));
            return this;
        }

        public SubAggregationLocal build() {
            SubAggregationLocal subAggregation = new SubAggregationLocal(subName, subField);
            subAggregation.addSubAggregationLocal(subAggregationLocals);
            return subAggregation;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

}
