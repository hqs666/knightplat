package com.knight.plat.elastic.core;

import com.beust.jcommander.internal.Lists;
import com.knight.plat.commons.JsonArray;
import com.knight.plat.commons.JsonObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.Strings;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.ParsedAggregation;

import java.util.List;


public class AggregationChains {


    private AggregationChains() {

    }

    public static AggregationChains newAggregationChains() {
        return new AggregationChains();
    }

    /**
     * 获取聚合内容
     *
     * @param response 查询结果
     * @param name     聚合条件数组
     * @return 查询结果
     */
    public <T> T aggregationRead(SearchResponse response, String name, Class<T> clazz) {
        ParsedAggregation aggregation = response.getAggregations().get(name);
        return JsonObject.mapFrom(aggregation.getMetaData()).mapTo(clazz);
    }

    /**
     * 将SearchResponse对象中的Aggregation值转换成JsonObject格式
     *
     * @param searchResponse SearchResponse对象
     * @return JsonObject
     */
    public static <T> List<T> printAggregationInJsonArray(final SearchResponse searchResponse, String name, Class<T> clazz) {
        Aggregation agg = searchResponse.getAggregations().get(name);
        JsonObject result = new JsonObject(Strings.toString(agg));
        JsonArray buckets = result.getJsonObject("sterms#" + name + "").getJsonArray("buckets");

        List<T> list = Lists.newArrayList();
        for (int i = 0; i < buckets.size(); i++) {
            list.add(buckets.getJsonObject(i).mapTo(clazz));
        }
        return list;
    }

}
