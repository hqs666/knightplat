/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.elastic;

import com.google.common.base.Strings;
import com.knight.plat.elastic.core.ElasticSearchManager;
import com.knight.plat.elastic.properties.BulkInfo;
import com.knight.plat.elastic.properties.ConnectionConfigInfo;
import com.knight.plat.elastic.properties.ElasticSearchProperties;
import com.knight.plat.elastic.properties.HostInfo;
import com.knight.plat.elastic.properties.RequestConfigInfo;
import com.knight.plat.elastic.properties.UserInfo;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.action.bulk.BackoffPolicy;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.util.List;

/**
 * ElasticSearch configuration class.
 * Configuration for ElasticSearch annotation-driven support.
 */
@AutoConfiguration
@ConditionalOnClass({Client.class, ElasticSearchProperties.class})
@ConditionalOnProperty(prefix = "knightplat.elasticsearch", name = "enabled", havingValue = "true", matchIfMissing = true)
@EnableConfigurationProperties(ElasticSearchProperties.class)
public class ElasticSearchAutoConfiguration {

    private final ElasticSearchProperties elasticSearchProperties;

    @Autowired
    public ElasticSearchAutoConfiguration(ElasticSearchProperties elasticSearchProperties) {
        this.elasticSearchProperties = elasticSearchProperties;
    }

    @Bean
    @Lazy
    @ConditionalOnMissingBean
    public ElasticSearchManager elasticsearchManager(RestHighLevelClient client, BulkProcessor bulkProcessor) {
        return new ElasticSearchManager(client, bulkProcessor);
    }


    @Bean
    @Lazy
    @ConditionalOnMissingBean
    public RestHighLevelClient restHighLevelClient() {
        List<HostInfo> hostInfos = elasticSearchProperties.getHost();
        HttpHost[] httpHosts = new HttpHost[hostInfos.size()];
        for (int i = 0; i < hostInfos.size(); i++) {
            HostInfo hostInfo = hostInfos.get(i);
            httpHosts[i] = new HttpHost(hostInfo.getHostname(), hostInfo.getPort(), hostInfo.getSchemeName());
        }

        RestClientBuilder builder = RestClient
                .builder(httpHosts)
                .setHttpClientConfigCallback(this::configCallback);

        return new RestHighLevelClient(builder);
    }

    private HttpAsyncClientBuilder configCallback(HttpAsyncClientBuilder builder) {
        long keepAlive = elasticSearchProperties.getKeepAlive();
        UserInfo user = elasticSearchProperties.getUser();
        String username = user.getUsername();
        String password = user.getPassword();

        if (keepAlive > -1) {
            builder.setKeepAliveStrategy((r, c) -> keepAlive);
        }

        if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
            builder.setDefaultCredentialsProvider(credentialsProvider(username, password));
        }

        RequestConfigInfo requestConfig = elasticSearchProperties.getRequestConfig();
        ConnectionConfigInfo connectionConfig = elasticSearchProperties.getConnectionConfig();
        if (requestConfig != null) {
            builder.setDefaultRequestConfig(requestConfig.getConfig());
        }

        if (connectionConfig != null) {
            builder.setDefaultConnectionConfig(connectionConfig.getConfig());
        }

        return builder;
    }

    private CredentialsProvider credentialsProvider(String username,String password) {

        final CredentialsProvider credentialsProvider =
                new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));

        return credentialsProvider;
    }

    @Bean
    @Lazy
    @ConditionalOnMissingBean
    public BulkProcessor bulkProcessor(RestHighLevelClient client) {
        BulkInfo bulk = elasticSearchProperties.getBulk();
        return BulkProcessor.builder(
                (request, bulkListener) -> client.bulkAsync(request, RequestOptions.DEFAULT, bulkListener),
                new BulkProcessor.Listener() {

                    @Override
                    public void beforeBulk(long executionId, BulkRequest request) {
                    }

                    @Override
                    public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
                    }

                    @Override
                    public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
                        System.out.println("写入ES 重新消费");
                    }
                }).setBulkActions(bulk.getBulkActions()) //达到刷新的条数
                .setBulkSize(bulk.getBulkSize()) // 达到 刷新的大小
                .setFlushInterval(bulk.getFlushInterval()) // 固定刷新的时间频率
                .setConcurrentRequests(bulk.getConcurrentRequests()) //并发线程数
                .setBackoffPolicy(BackoffPolicy.exponentialBackoff(bulk.getDelay(), bulk.getMaxNumberOfRetries())) // 重试补偿策略
                .build();
    }
}
