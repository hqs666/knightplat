package com.knight.plat.elastic.properties;

/**
 *
 */
public class HostInfo {

    private String hostname = "localhost";

    private int port = 9200;

    private String schemeName = "http";

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

}
