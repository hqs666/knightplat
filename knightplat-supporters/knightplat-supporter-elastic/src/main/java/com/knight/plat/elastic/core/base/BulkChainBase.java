package com.knight.plat.elastic.core.base;

import com.beust.jcommander.internal.Lists;
import com.knight.plat.commons.JsonObject;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;


public class BulkChainBase {

    private final static Logger log = LoggerFactory.getLogger(BulkChainBase.class);

    RestHighLevelClient client;

    BulkProcessor bulkProcessor;

    public BulkChainBase(RestHighLevelClient client) {
        this.client = client;
    }

    public BulkChainBase(RestHighLevelClient client, BulkProcessor bulkProcessor) {
        this.client = client;
        this.bulkProcessor = bulkProcessor;
    }

    private final BulkRequest bulkRequest = new BulkRequest();


    /**
     * Index the source. If there an existing document with the id, it will be replaced.
     */
    public BulkChainBase index(String index, String id, JsonObject jsonObject) {
        bulkRequest.add(new IndexRequest(index).opType(DocWriteRequest.OpType.INDEX).id(id)
                .source(jsonObject.getMap()));
        return this;
    }

    /**
     * Index the source. If there an existing document with the id, it will be replaced.
     */
    public <T> BulkChainBase index(String index, String id, T obj) {
        bulkRequest.add(new IndexRequest(index).opType(DocWriteRequest.OpType.INDEX).id(id)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    public <T> BulkChainBase indexRouting(String index, String id, T obj,String routing) {
        bulkRequest.add(new IndexRequest(index).opType(DocWriteRequest.OpType.INDEX).id(id).routing(routing)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    /**
     * Creates the resource. Simply adds it to the index, if there is an existing document with the id, then it
     * won't be removed.
     */
    public BulkChainBase create(String index, String id, JsonObject jsonObject) {
        bulkRequest.add(new IndexRequest(index).opType(DocWriteRequest.OpType.CREATE).id(id)
                .source(jsonObject.getMap()));
        return this;
    }

    /**
     * Creates the resource. Simply adds it to the index, if there is an existing document with the id, then it
     * won't be removed.
     */
    public <T> BulkChainBase create(String index, String id, T obj) {
        bulkRequest.add(new IndexRequest(index).opType(DocWriteRequest.OpType.CREATE).id(id)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    /**
     * Creates the resource. Simply adds it to the index, if there is an existing document with the id, then it
     * won't be removed.
     */
    public <T> BulkChainBase createRouting(String index, String id, T obj,String routing) {
        bulkRequest.add(new IndexRequest(index).opType(DocWriteRequest.OpType.CREATE).id(id).routing(routing)
                .source(JsonObject.mapFrom(obj).getMap()));
        return this;
    }


    /**
     * Updates a document
     */
    public BulkChainBase update(String index, String id, JsonObject jsonObject) {
        bulkRequest.add(new UpdateRequest(index, id).doc(jsonObject.getMap()));
        return this;
    }

    /**
     * Updates a document
     */
    public <T> BulkChainBase update(String index, String id, T obj) {
        bulkRequest.add(new UpdateRequest(index, id).doc(JsonObject.mapFrom(obj).getMap()));
        return this;
    }


    /**
     * Updates a document
     */
    public <T> BulkChainBase updateRouting(String index, String id, T obj,String routing) {
        bulkRequest.add(new UpdateRequest(index, id).routing(routing).doc(JsonObject.mapFrom(obj).getMap()));
        return this;
    }

    /**
     * Deletes a document
     */
    public BulkChainBase delete(String index, String id) {
        bulkRequest.add(new DeleteRequest(index, id));
        return this;
    }

    public BulkResponse execute() {
        BulkResponse bulkResponse = null;
        try {
            bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("Bulk search error", e);
        }
        return bulkResponse;

    }


    public BulkResponse execute(Boolean refresh) {
        BulkResponse bulkResponse = null;
        try {
            bulkResponse = client.bulk(bulkRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL), RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("Bulk search error", e);
        }
        return bulkResponse;

    }

    /**
     * Bulk requests are sent in bulk and rerequested according to the specified configuration if an error occurs.
     */
    public BulkChainBase retry(DocWriteRequest<?>... requests) {

        Lists.newArrayList(requests).forEach(req -> bulkProcessor.add(req));
        bulkProcessor.close();
        return this;
    }

    /**
     * Bulk requests are sent in bulk and rerequested according to the specified configuration if an error occurs.
     */
    public BulkChainBase retry(List<DocWriteRequest<?>> requests) {
        requests.forEach(req -> bulkProcessor.add(req));
        return this;
    }


    /**
     * 检查bulk是否有错误
     *
     * @param bulkResponse execute执行结果BulkResponse
     * @return boolean
     */
    public boolean checkBulkResponse(BulkResponse bulkResponse) {
        return bulkResponse.hasFailures();
    }

    /**
     * 获取错误的信息
     *
     * @param bulkResponse execute执行结果BulkResponse
     * @return BulkItemResponse.Failure
     */
    public BulkItemResponse.Failure getFailureBulkResponse(BulkResponse bulkResponse) {
        BulkItemResponse.Failure failure = null;
        for (BulkItemResponse bulkItemResponse : bulkResponse) {
            if (bulkItemResponse.isFailed()) {
                failure =
                        bulkItemResponse.getFailure();
            }
        }
        return failure;
    }
}
