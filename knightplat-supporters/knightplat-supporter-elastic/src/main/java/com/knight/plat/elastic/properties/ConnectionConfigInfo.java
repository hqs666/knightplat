package com.knight.plat.elastic.properties;

import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.MessageConstraints;

import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;

/**
 * Create by neo | 2021/8/6 10:48
 */
public class ConnectionConfigInfo {

    private int bufferSize;
    private int fragmentSizeHint;
    private Charset charset;
    private CodingErrorAction malformedInputAction;
    private CodingErrorAction unmappableInputAction;

    //inner field for MessageConstraints.class

    private int maxLineLength;
    private int maxHeaderCount;


    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public int getFragmentSizeHint() {
        return fragmentSizeHint;
    }

    public void setFragmentSizeHint(int fragmentSizeHint) {
        this.fragmentSizeHint = fragmentSizeHint;
    }

    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public CodingErrorAction getMalformedInputAction() {
        return malformedInputAction;
    }

    public void setMalformedInputAction(CodingErrorAction malformedInputAction) {
        this.malformedInputAction = malformedInputAction;
    }

    public CodingErrorAction getUnmappableInputAction() {
        return unmappableInputAction;
    }

    public void setUnmappableInputAction(CodingErrorAction unmappableInputAction) {
        this.unmappableInputAction = unmappableInputAction;
    }

    public int getMaxLineLength() {
        return maxLineLength;
    }

    public void setMaxLineLength(int maxLineLength) {
        this.maxLineLength = maxLineLength;
    }

    public int getMaxHeaderCount() {
        return maxHeaderCount;
    }

    public void setMaxHeaderCount(int maxHeaderCount) {
        this.maxHeaderCount = maxHeaderCount;
    }

    public ConnectionConfig getConfig() {
        MessageConstraints constraints = MessageConstraints.custom()
                .setMaxHeaderCount(this.maxHeaderCount)
                .setMaxLineLength(this.maxLineLength)
                .build();

        return ConnectionConfig.custom()
                .setBufferSize(this.getBufferSize())
                .setCharset(this.getCharset())
                .setFragmentSizeHint(this.getFragmentSizeHint())
                .setMalformedInputAction(this.getMalformedInputAction())
                .setMessageConstraints(constraints)
                .setUnmappableInputAction(this.getUnmappableInputAction())
                .build();
    }
}
