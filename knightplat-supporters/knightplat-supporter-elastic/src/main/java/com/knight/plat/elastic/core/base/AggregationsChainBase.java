package com.knight.plat.elastic.core.base;

import com.knight.plat.commons.JsonObject;
import com.knight.plat.elastic.core.AggregationLocal;
import com.knight.plat.elastic.entity.SubAggregationLocal;
import org.apache.commons.collections.CollectionUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.ParsedAggregation;
import org.elasticsearch.search.aggregations.bucket.filter.FiltersAggregator;
import org.elasticsearch.search.aggregations.bucket.global.Global;
import org.elasticsearch.search.aggregations.bucket.global.GlobalAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.SumAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class AggregationsChainBase {


    private final RestHighLevelClient client;

    public final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    public AggregationsChainBase(RestHighLevelClient client) {
        this.client = client;
    }

    /**
     * 分组
     *
     * @param name  name值自定义
     * @param field 为需要分组的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationGroupBy(String name, String field) {
        searchSourceBuilder.aggregation(AggregationBuilders.terms(name).field(field));
        return this;
    }

    /**
     * 分组
     *
     * @param name  name值自定义
     * @param field 为需要分组的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationNested(String name, String field,String subName,String subField) {
        searchSourceBuilder.aggregation(AggregationBuilders.nested(name, field)
                .subAggregation(AggregationBuilders.terms(subName).field(subField)));
        return this;
    }

    /**
     * 分组
     *
     * @param name  name值自定义
     * @param field 为需要分组的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationNested(String name, String field, String subName, Script script) {
        searchSourceBuilder.aggregation(AggregationBuilders.nested(name, field)
                .subAggregation(AggregationBuilders.terms(subName).script(script)));
        return this;
    }

    /**
     * 分组
     *
     * @param name  name值自定义
     * @param field 为需要分组的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationNested(String name, String field, String subName, Script script, int size) {
        searchSourceBuilder.aggregation(AggregationBuilders.nested(name, field)
                .subAggregation(AggregationBuilders.terms(subName).script(script).size(size)));
        return this;
    }

    /**
     * 分组
     *
     * @param aggregationLocal
     *
     * @return 查询链
     */
    public AggregationsChainBase aggregationNested(AggregationLocal aggregationLocal) {
        NestedAggregationBuilder builder = AggregationBuilders.nested(aggregationLocal.getName(), aggregationLocal.getField());
        aggregationLocal.getSubAggregationLocal().forEach(e -> builder.subAggregation(builderAggregationBuilder(e)));
        searchSourceBuilder.aggregation(builder);
        return this;
    }

    private AggregationBuilder builderAggregationBuilder(SubAggregationLocal aggr) {
        AggregationBuilder builder = AggregationBuilders.terms(aggr.getSubName()).field(aggr.getSubField());
        if (CollectionUtils.isNotEmpty(aggr.getSubAggregationLocal())) {
            aggr.getSubAggregationLocal().forEach(e -> builder.subAggregation(builderAggregationBuilder(e)));
        }
        return builder;
    }


    /**
     * 统计 根据字段进行统计总数
     *
     * @param name  name值自定义
     * @param field 需要统计的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationCount(String name, String field) {
        searchSourceBuilder.aggregation(AggregationBuilders.count(name).field(field));
        return this;
    }

    /**
     * 统计 根据字段进行最大值统计
     *
     * @param name  name值自定义
     * @param field 需要统计的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationMax(String name, String field) {
        searchSourceBuilder.aggregation(AggregationBuilders.max(name).field(field));
        return this;
    }

    /**
     * 统计 聚合排序
     *
     * @param name  name值自定义
     * @param field 需要统计的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationSort(String name, String field, Integer size) {
        searchSourceBuilder.aggregation(AggregationBuilders.topHits(field).sort(name).size(size));
        return this;
    }

    /**
     * 统计 根据字段进行平均值统计
     *
     * @param name  name值自定义
     * @param field 需要统计的key
     * @return 查询链
     */
    public AggregationsChainBase aggregationAvg(String name, String field) {
        searchSourceBuilder.aggregation(AggregationBuilders.avg(name).field(field));
        return this;
    }

    /**
     * 统计 根据字段最小值进行统计
     *
     * @return 查询结果
     */
    public AggregationBuilder buildSubAggregation(AggregationBuilder parentAgg, AggregationBuilder childAgg) {
        return parentAgg.subAggregation(childAgg);
    }

    /**
     * 统计 根据字段最小值进行统计
     *
     * @return 查询结果
     */
    public AggregationsChainBase addSubAggregation(AggregationBuilder aggregationBuilder) {
        searchSourceBuilder.aggregation(aggregationBuilder);
        return this;
    }

    /**
     * 统计 根据字段求和
     *
     * @param name  name值自定义
     * @param field 需要统计的key
     * @return 查询结果
     */
    public AggregationsChainBase aggregationSum(String name, String field) {
        searchSourceBuilder.aggregation(AggregationBuilders.sum(name).field(field));
        return this;
    }

    /**
     * 统计 查看字段最大值/最小值/平均值/求和信息
     *
     * @param name  name值自定义
     * @param field 需要统计的key
     * @return 查询结果
     */
    public AggregationsChainBase aggregationStats(String name, String field) {
        searchSourceBuilder.aggregation(AggregationBuilders.stats(name).field(field));
        return this;
    }

    /**
     * 统计 查看字段去重总数统计
     *
     * @param name  name值自定义
     * @param field 需要统计的key
     * @return 查询结果
     */
    public AggregationsChainBase aggregationDistinct(String name, String field) {
        searchSourceBuilder.aggregation(AggregationBuilders.cardinality(name).field(field));
        return this;
    }


    /**
     * 统计 根据字段进行统计
     *
     * @param name 根据name全局进行聚合
     * @return 查询结果
     */
    public GlobalAggregationBuilder aggregationGlobal(String name) {
        return AggregationBuilders.global(name);
    }


    /**
     * 获取聚合内容
     *
     * @param response 查询结果
     * @param name     聚合条件
     * @return 查询结果
     */
    public Global aggregationGlobalRead(SearchResponse response, String name) {
        return response.getAggregations().get(name);
    }

    /**
     * 获取聚合内容
     *
     * @param response 查询结果
     * @param name     聚合条件
     * @return 查询结果
     */
    public Global aggregationGlobalRea(SearchResponse response, String name) {
        SumAggregationBuilder aggregation = AggregationBuilders.sum("agg").field("height");
        return response.getAggregations().get(name);
    }

    /**
     * 已废弃，最新方法地址:AggregationChains#aggregationRead
     * <p>
     * 获取聚合内容
     *
     * @param response 查询结果
     * @param name     聚合条件数组
     * @return 查询结果
     */
    @Deprecated
    public JsonObject aggregationRead(SearchResponse response, String name) {
        ParsedAggregation aggregation = response.getAggregations().get(name);
        return JsonObject.mapFrom(aggregation);
    }


    /**
     * 过滤查询条件，过滤出满足查询条件的文档
     *
     * @param name        聚合名称
     * @param aggregators 查询条件数组
     * @return 查询结果
     */
    public AggregationsChainBase aggregationCount(String name, FiltersAggregator.KeyedFilter... aggregators) {
        searchSourceBuilder.aggregation(AggregationBuilders.filters(name, aggregators));
        return this;
    }


    /**
     * 聚合查询执行
     *
     * @param indices 索引
     * @return 聚合对象返回JSON
     * @throws IOException 查询通信错误
     */
    public SearchResponse execute(String... indices) throws IOException {
        SearchRequest searchRequest = new SearchRequest(indices);
        searchRequest.source(searchSourceBuilder);
        return client.search(searchRequest, RequestOptions.DEFAULT);
    }

    /**
     * 聚合查询执行设置分页
     *
     * @param indices 索引
     * @return 聚合对象返回JSON
     * @throws IOException 查询通信错误
     */
    public SearchResponse execute(Integer from, Integer size, String... indices) throws IOException {
        SearchRequest searchRequest = new SearchRequest(indices);
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchRequest.source(searchSourceBuilder);
        return client.search(searchRequest, RequestOptions.DEFAULT);
    }


    /**
     * 聚合查询执行设置分页
     *
     * @param from     开始位置
     * @param size     查询条数
     * @param includes
     * @param excludes
     * @param indices  索引
     * @return 聚合对象返回JSON
     * @throws IOException 查询通信错误
     */
    public SearchResponse execute(Integer from, Integer size, String[] includes, String[] excludes, String... indices) throws IOException {
        SearchRequest searchRequest = new SearchRequest(indices);
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        if (includes != null || excludes != null) {
            searchSourceBuilder.fetchSource(includes, excludes);
        }
        searchSourceBuilder.trackTotalHits(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        return client.search(searchRequest, RequestOptions.DEFAULT);
    }
}
