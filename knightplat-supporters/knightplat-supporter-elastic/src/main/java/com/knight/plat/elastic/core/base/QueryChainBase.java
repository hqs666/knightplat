package com.knight.plat.elastic.core.base;

import com.google.common.collect.Lists;
import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.search.Sort;
import com.knight.plat.commons.JsonObject;
import com.knight.plat.elastic.core.ElasticSearchManager;
import com.knight.plat.elastic.core.QueryChains;
import com.knight.plat.elastic.enums.ParentChildBoolMenu;
import com.knight.plat.elastic.exception.ValidateException;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.InnerHitBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.join.query.JoinQueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class QueryChainBase {

    private final Integer WILD_CARD_LIMIT = 50;

    public RestHighLevelClient client;

    /**
     * 有参构造
     *
     * @param client
     */
    public QueryChainBase(RestHighLevelClient client) {
        this.client = client;
    }

    public final BoolQueryBuilder builder = new BoolQueryBuilder();
    public final List<FieldSortBuilder> sortBuilder = Lists.newArrayList();

    /**
     * RangeQuery范围must查询
     *
     * @param field 字段名称
     * @param from  起始值
     * @param to    结束值
     * @return 查询链
     */
    public QueryChainBase rangeAnd(String field, Object from, Object to) {
        builder.must(QueryBuilders.rangeQuery(field).from(from).to(to));
        return this;
    }

    /**
     * RangeQuery范围should查询
     *
     * @param field 字段名称
     * @param from  起始值
     * @param to    结束值
     * @return 查询链
     */
    public QueryChainBase rangeOr(String field, Object from, Object to) {
        builder.should(QueryBuilders.rangeQuery(field).from(from).to(to));
        return this;
    }

    /**
     * wildcard方式模糊must查询
     *
     * @param field 字段名
     * @param value 字段值 长度要在50以内,超过50后不生效
     * @return 查询链
     */
    public QueryChainBase wildcardAnd(String field, String value) {
        if (value != null && value.length() > WILD_CARD_LIMIT){
            throw new ValidateException("wild_card 类型字段限制50字以内");
        }
        builder.must(QueryBuilders.wildcardQuery(field, value));
        return this;
    }

    /**
     * wildcard方式模糊should查询
     *
     * @param field 字段名
     * @param value 字段值 长度要在50以内
     * @return 查询链
     */
    public QueryChainBase wildcardOr(String field, String value) {
        if (value != null && value.length() > WILD_CARD_LIMIT){
            throw new ValidateException("wild_card 类型字段限制50字以内");
        }
        builder.should(QueryBuilders.wildcardQuery(field, value));
        return this;
    }

    /**
     * FuzzyQuery方式模糊must查询
     *
     * @param field 字段名
     * @param value 字段值
     * @return 查询链
     */
    public <T> QueryChainBase fuzzyAnd(String field, T value) {
        builder.must(QueryBuilders.fuzzyQuery(field, value));
        return this;
    }

    /**
     * FuzzyQuery方式模糊should查询
     *
     * @param field 字段名
     * @param value 字段值
     * @return 查询链
     */
    public <T> QueryChainBase fuzzyOr(String field, T value) {
        builder.should(QueryBuilders.fuzzyQuery(field, value));
        return this;
    }

    /**
     * Prefix方式表达式must查询
     *
     * @param field 字段名
     * @param value 表达式字符串
     * @return 查询链
     */
    public <T> QueryChainBase prefixAnd(String field, T value) {
        builder.must(QueryBuilders.prefixQuery(field, (String) value));
        return this;
    }

    /**
     * Prefix方式表达式should查询
     *
     * @param field 字段名
     * @param value 表达式字符串
     * @return 查询链
     */
    public <T> QueryChainBase prefixOr(String field, T value) {
        builder.should(QueryBuilders.prefixQuery(field, (String) value));
        return this;
    }

    /**
     * TermQuery方式精确must查询
     *
     * @param field 字段名
     * @param value 字段值
     * @return 查询链
     */
    public <T> QueryChainBase termAnd(String field, T value) {
        builder.must(QueryBuilders.termQuery(field, value));
        return this;
    }

    /**
     * MatchQuery方式匹配must查询
     *
     * @param field 字段名
     * @param value 字段值
     * @return 查询链
     */
    public <T> QueryChainBase matchAnd(String field, T value) {
        builder.must(QueryBuilders.matchQuery(field, value));
        return this;
    }

    /**
     * MatchQuery方式匹配should查询
     *
     * @param field 字段名
     * @param value 字段值
     * @return 查询链
     */
    public <T> QueryChainBase matchOr(String field, T value) {
        builder.should(QueryBuilders.matchQuery(field, value));
        return this;
    }

    /**
     * TermQuery方式精确should查询
     *
     * @param field 字段名
     * @param value 字段值
     * @return 查询链
     */
    public <T> QueryChainBase termOr(String field, T value) {
        builder.should(QueryBuilders.termQuery(field, value));
        return this;
    }

    /**
     * 权重方法
     *
     * @param boost
     * @return
     */
    public QueryChainBase boost(float boost) {
        builder.boost(boost);
        return this;
    }

    /**
     * StringQuery方式should查询
     *
     * @param expression 查询条件
     * @return 查询链
     */
    public QueryChainBase stringQueryAnd(String expression) {
        builder.must(QueryBuilders.queryStringQuery(expression));
        return this;
    }

    /**
     * StringQuery方式must查询
     *
     * @param expression 查询条件
     * @return 查询链
     */
    public QueryChainBase stringQueryOr(String expression) {
        builder.should(QueryBuilders.queryStringQuery(expression));
        return this;
    }

    /**
     * MatchPhraseQuery 方式 must 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @return 查询链
     */
    public <T> QueryChainBase matchPhraseAnd(String field, T value, Integer slop) {
        builder.must(QueryBuilders.matchPhraseQuery(field, value).slop(slop));
        return this;
    }

    /**
     * MatchPhraseQuery 方式 should 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @return 查询链
     */
    public <T> QueryChainBase matchPhraseOr(String field, T value, Integer slop) {
        builder.should(QueryBuilders.matchPhraseQuery(field, value).slop(slop));
        return this;
    }

    /**
     * MatchPhrasePrefixQuery 方式 must 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @return 查询链
     */
    public <T> QueryChainBase matchPhrasePrefixAnd(String field, T value, Integer slop) {
        builder.must(QueryBuilders.matchPhrasePrefixQuery(field, value).slop(slop));
        return this;
    }

    /**
     * MatchPhrasePrefixQuery 方式 should 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @return 查询链
     */
    public <T> QueryChainBase matchPhrasePrefixOr(String field, T value, Integer slop) {
        builder.should(QueryBuilders.matchPhrasePrefixQuery(field, value).slop(slop));
        return this;
    }

    /**
     * RangeQuery范围must查询
     *
     * @param fields 字段名称数组
     * @param from   起始值
     * @param to     结束值
     * @return 查询链
     */
    public QueryChainBase rangeAnd(List<String> fields, Object from, Object to) {
        fields.forEach(field -> builder.must(QueryBuilders.rangeQuery(field).from(from).to(to)));
        return this;
    }

    /**
     * RangeQuery范围should查询
     *
     * @param fields 字段名称数组
     * @param from   起始值
     * @param to     结束值
     * @return 查询链
     */
    public QueryChainBase rangeOr(List<String> fields, Object from, Object to) {
        fields.forEach(field -> builder.should(QueryBuilders.rangeQuery(field).from(from).to(to)));
        return this;
    }

    /**
     * FuzzyQuery方式批量模糊must查询
     *
     * @param batchQueryMap 批量查询Map
     * @return 查询链
     */
    public <T> QueryChainBase fuzzyAnd(Map<String, T> batchQueryMap) {
        for (Map.Entry<String, T> queryMap : batchQueryMap.entrySet()) {
            builder.must(QueryBuilders.fuzzyQuery(queryMap.getKey(), queryMap.getValue()));
        }
        return this;
    }

    /**
     * FuzzyQuery方式批量模糊should查询
     *
     * @param batchQueryMap 批量查询Map
     * @return 查询链
     */
    public <T> QueryChainBase fuzzyOr(Map<String, T> batchQueryMap) {
        batchQueryMap.forEach((key, value) -> builder.should(QueryBuilders.fuzzyQuery(key, value)));
        return this;
    }

    /**
     * Prefix方式表达式must批量查询
     *
     * @param batchQueryMap 批量查询Map
     * @return 查询链
     */
    public <T> QueryChainBase prefixAnd(Map<String, T> batchQueryMap) {
        batchQueryMap.forEach((key, value) -> builder.must(QueryBuilders.fuzzyQuery(key, value)));
        return this;
    }

    /**
     * Prefix方式表达式should 批量查询
     *
     * @param batchQueryMap 查询参数
     * @return 查询链
     */
    public <T> QueryChainBase prefixOr(Map<String, T> batchQueryMap) {
        batchQueryMap.forEach((key, value) -> builder.should(QueryBuilders.prefixQuery(key, (String) value)));
        return this;
    }

    /**
     * TermQuery方式精确must查询
     *
     * @param batchQueryMap 批量查询Map
     * @return 查询链
     */
    public <T> QueryChainBase termAnd(Map<String, T> batchQueryMap) {
        batchQueryMap.forEach((key, value) -> builder.must(QueryBuilders.termQuery(key, value)));
        return this;
    }

    /**
     * MatchQuery方式匹配must批量查询查询
     *
     * @param batchQueryMap 批量查询Map
     * @return 查询链
     */
    public <T> QueryChainBase matchAnd(Map<String, T> batchQueryMap) {
        batchQueryMap.forEach((key, value) -> builder.must(QueryBuilders.matchQuery(key, value)));
        return this;
    }

    /**
     * MatchQuery方式匹配should查询
     *
     * @param batchQueryMap 批量查询Map
     * @return 查询链
     */
    public <T> QueryChainBase matchOr(Map<String, T> batchQueryMap) {
        batchQueryMap.forEach((key, value) -> builder.should(QueryBuilders.matchQuery(key, value)));
        return this;
    }

    /**
     * TermQuery方式精确should查询
     *
     * @param batchQueryMap 批量查询Map
     * @return 查询链
     */
    public <T> QueryChainBase termOr(Map<String, T> batchQueryMap) {
        batchQueryMap.forEach((key, value) -> builder.should(QueryBuilders.termQuery(key, value)));
        return this;
    }

    /**
     * MatchPhraseQuery 方式 must 查询
     *
     * @param batchQueryMap 批量查询Map
     * @param slopMap       批量查询对应slop
     * @return 查询链
     */
    public <T> QueryChainBase matchPhraseAnd(Map<String, T> batchQueryMap, Map<String, Integer> slopMap) {
        batchQueryMap.forEach((key, value) -> builder.must(QueryBuilders.matchPhraseQuery(key, value).slop(slopMap.get(key))));
        return this;
    }

    /**
     * MatchPhraseQuery 方式 should 查询
     *
     * @param batchQueryMap 批量查询Map
     * @param slopMap       批量查询对应slop
     * @return 查询链
     */
    public <T> QueryChainBase matchPhraseOr(Map<String, T> batchQueryMap, Map<String, Integer> slopMap) {
        batchQueryMap.forEach((key, value) -> builder.should(QueryBuilders.matchPhraseQuery(key, value).slop(slopMap.get(key))));
        return this;
    }

    /**
     * MatchPhrasePrefixQuery 方式 must 查询
     *
     * @param batchQueryMap 批量查询Map
     * @param slopMap       批量查询对应slop
     * @return 查询链
     */
    public <T> QueryChainBase matchPhrasePrefixAnd(Map<String, T> batchQueryMap, Map<String, Integer> slopMap) {
        batchQueryMap.forEach((key, value) -> builder.must(QueryBuilders.matchPhrasePrefixQuery(key, value).slop(slopMap.get(key))));
        return this;
    }

    /**
     * MatchPhrasePrefixQuery 方式 should 查询
     *
     * @param batchQueryMap 批量查询Map
     * @param slopMap       批量查询对应slop
     * @return 查询链
     */
    public <T> QueryChainBase matchPhrasePrefixOr(Map<String, T> batchQueryMap, Map<String, Integer> slopMap) {
        batchQueryMap.forEach((key, value) -> builder.should(QueryBuilders.matchPhrasePrefixQuery(key, value).slop(slopMap.get(key))));
        return this;
    }

    /**
     * TermsQuery 方式 IN MUST查询
     *
     * @param field  字段名称
     * @param values 包含的的数据
     * @return 查询链
     */
    public <T> QueryChainBase inTerms(String field, List<T> values) {
        builder.must(QueryBuilders.termsQuery(field, values));
        return this;
    }


    /**
     * TermsQuery 方式 NOT IN mustNot 查询
     *
     * @param field  字段名称
     * @param values 包含的的数据
     * @return 查询链
     */
    public <T> QueryChainBase notInTerms(String field, List<T> values) {
        builder.mustNot(QueryBuilders.termsQuery(field, values));
        return this;
    }

    /**
     * nested方式精确must查询
     *
     * @param path          要搜索的嵌套对象的字段
     * @param batchQueryMap 批量查询条件map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedTermAnd(String path, Map<String, T> batchQueryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        batchQueryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.termQuery(k, v)));
        builder.must(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }

    /**
     * nested方式精确批量must查询
     *
     * @param path          要搜索的嵌套对象的字段
     * @param batchQueryMap 批量查询条件map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedTermsAnd(String path, Map<String, List<T>> batchQueryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        batchQueryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.termsQuery(k, v)));
        builder.must(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }

    /**
     * nested方式精确should查询
     *
     * @param path          要搜索的嵌套对象的字段
     * @param batchQueryMap 批量查询条件map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedTermOr(String path, Map<String, T> batchQueryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        batchQueryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.termQuery(k, v)));
        builder.should(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }

    /**
     * nested方式精确should批量查询
     *
     * @param path          要搜索的嵌套对象的字段
     * @param batchQueryMap 批量查询条件map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedTermsOr(String path, Map<String, List<T>> batchQueryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        batchQueryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.termsQuery(k, v)));
        builder.should(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }

    /**
     * nested方式匹配and批量查询
     *
     * @param path          要搜索的嵌套对象的字段
     * @param batchQueryMap 批量查询条件map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedMatchAnd(String path, Map<String, T> batchQueryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        batchQueryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.matchQuery(k, v)));
        builder.must(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }

    /**
     * nested方式匹配should批量查询
     *
     * @param path          要搜索的嵌套对象的字段
     * @param batchQueryMap 批量查询条件map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedMatchOr(String path, Map<String, T> batchQueryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        batchQueryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.matchQuery(k, v)));
        builder.should(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }


    /**
     * nested方式匹配Prefix must查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 前缀匹配查询Map
     * @return QueryChainBase
     */
    public QueryChainBase nestedPrefixAnd(String path, Map<String, String> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.prefixQuery(k, v)));
        return buildNestedMustQuery(path,nestedBuilder);
    }

    /**
     * nested方式匹配Prefix should批量查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 查询条件map
     * @return QueryChainBase
     */
    public QueryChainBase nestedPrefixOr(String path, Map<String, String> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.prefixQuery(k, v)));
        return buildNestedShouldQuery(path,nestedBuilder);
    }

    /**
     * nested方式匹配Regexp must查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 正则匹配查询Map
     * @return QueryChainBase
     */
    public QueryChainBase nestedRegexpAnd(String path, Map<String, String> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.regexpQuery(k, v)));
        return buildNestedMustQuery(path,nestedBuilder);
    }

    /**
     * nested方式匹配Regexp should批量查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 正则查询条件map
     * @return QueryChainBase
     */
    public QueryChainBase nestedRegexpOr(String path, Map<String, String> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.regexpQuery(k, v)));
        return buildNestedShouldQuery(path,nestedBuilder);
    }

    /**
     * nested方式匹配Fuzzy must查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 模糊匹配查询Map
     * @return QueryChainBase
     */
    public QueryChainBase nestedFuzzyAnd(String path, Map<String, String> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.fuzzyQuery(k, v)));
        return buildNestedMustQuery(path,nestedBuilder);
    }

    /**
     * nested方式匹配Fuzzy should批量查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 模糊查询条件map
     * @return QueryChainBase
     */
    public QueryChainBase nestedFuzzyOr(String path, Map<String, String> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.fuzzyQuery(k, v)));
        return buildNestedShouldQuery(path,nestedBuilder);
    }

    /**
     * nested方式匹配matchPhrasePrefix must查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 短语查询Map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedMatchPhrasePrefixAnd(String path, Map<String, T> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.matchPhrasePrefixQuery(k, v)));
        return buildNestedMustQuery(path,nestedBuilder);
    }

    /**
     * nested方式匹配nestedMatchPhrasePrefix should批量查询
     *
     * @param path     要搜索的嵌套对象的字段
     * @param queryMap 短语查询条件map
     * @return QueryChainBase
     */
    public <T> QueryChainBase nestedMatchPhrasePrefixOr(String path, Map<String, T> queryMap) {
        BoolQueryBuilder nestedBuilder = QueryBuilders.boolQuery();
        queryMap.forEach((k, v) -> nestedBuilder.must(QueryBuilders.matchPhrasePrefixQuery(k, v)));
        return buildNestedShouldQuery(path,nestedBuilder);
    }

    private QueryChainBase buildNestedShouldQuery(String path,BoolQueryBuilder nestedBuilder){
        builder.should(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }
    private QueryChainBase buildNestedMustQuery(String path,BoolQueryBuilder nestedBuilder){
        builder.must(QueryBuilders.nestedQuery(path, nestedBuilder, ScoreMode.None));
        return this;
    }


    /**
     * RangeQuery范围must查询
     *
     * @param field 字段名称
     * @param from  起始值
     * @param to    结束值
     * @param boost 权重
     * @return 查询链
     */
    public QueryChainBase rangeAnd(String field, Object from, Object to, float boost) {
        builder.must(QueryBuilders.rangeQuery(field).from(from).to(to).boost(boost));
        return this;
    }

    /**
     * RangeQuery范围should查询
     *
     * @param field 字段名称
     * @param from  起始值
     * @param to    结束值
     * @param boost 权重
     * @return 查询链
     */
    public QueryChainBase rangeOr(String field, Object from, Object to, float boost) {
        builder.should(QueryBuilders.rangeQuery(field).from(from).to(to).boost(boost));
        return this;
    }

    /**
     * FuzzyQuery方式模糊must查询
     *
     * @param field 字段名
     * @param value 字段值
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase fuzzyAnd(String field, T value, float boost) {
        builder.must(QueryBuilders.fuzzyQuery(field, value).boost(boost));
        return this;
    }

    /**
     * FuzzyQuery方式模糊should查询
     *
     * @param field 字段名
     * @param value 字段值
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase fuzzyOr(String field, T value, float boost) {
        builder.should(QueryBuilders.fuzzyQuery(field, value).boost(boost));
        return this;
    }

    /**
     * wildcardQuery方式模糊must查询
     *
     * @param field 字段名
     * @param value 字段值 长度要在50以内
     * @param boost 权重
     * @return 查询链
     */
    public QueryChainBase wildcardAnd(String field, String value, float boost) {
        if (value != null && value.length() > WILD_CARD_LIMIT){
            throw new ValidateException("wild_card 类型字段限制50字以内");
        }
        builder.must(QueryBuilders.wildcardQuery(field, value).boost(boost));
        return this;
    }

    /**
     * wildcardQuery方式模糊should查询
     *
     * @param field 字段名
     * @param value 字段值长度要在50以内
     * @param boost 权重
     * @return 查询链
     */
    public QueryChainBase wildcardOr(String field, String value, float boost) {
        if (value != null && value.length() > WILD_CARD_LIMIT){
            throw new ValidateException("wild_card 类型字段限制50字以内");
        }
        builder.should(QueryBuilders.wildcardQuery(field, value).boost(boost));
        return this;
    }

    /**
     * Prefix方式表达式must查询
     *
     * @param field 字段名
     * @param value 表达式字符串
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase prefixAnd(String field, T value, float boost) {
        builder.must(QueryBuilders.prefixQuery(field, (String) value).boost(boost));
        return this;
    }

    /**
     * Prefix方式表达式should查询
     *
     * @param field 字段名
     * @param value 表达式字符串
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase prefixOr(String field, T value, float boost) {
        builder.should(QueryBuilders.prefixQuery(field, (String) value).boost(boost));
        return this;
    }

    /**
     * TermQuery方式精确must查询
     *
     * @param field 字段名
     * @param value 字段值
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase termAnd(String field, T value, float boost) {
        builder.must(QueryBuilders.termQuery(field, value).boost(boost));
        return this;
    }

    /**
     * MatchQuery方式匹配must查询
     *
     * @param field 字段名
     * @param value 字段值
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase matchAnd(String field, T value, float boost) {
        builder.must(QueryBuilders.matchQuery(field, value).boost(boost));
        return this;
    }

    /**
     * MatchQuery方式匹配should查询
     *
     * @param field 字段名
     * @param value 字段值
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase matchOr(String field, T value, float boost) {
        builder.should(QueryBuilders.matchQuery(field, value).boost(boost));
        return this;
    }

    /**
     * TermQuery方式精确should查询
     *
     * @param field 字段名
     * @param value 字段值
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase termOr(String field, T value, float boost) {
        builder.should(QueryBuilders.termQuery(field, value).boost(boost));
        return this;
    }

    /**
     * StringQuery方式should查询
     *
     * @param expression 查询条件
     * @param boost      权重
     * @return 查询链
     */
    public QueryChainBase stringQueryAnd(String expression, float boost) {
        builder.must(QueryBuilders.queryStringQuery(expression).boost(boost));
        return this;
    }

    /**
     * StringQuery方式must查询
     *
     * @param expression 查询条件
     * @param boost      权重
     * @return 查询链
     */
    public QueryChainBase stringQueryOr(String expression, float boost) {
        builder.should(QueryBuilders.queryStringQuery(expression).boost(boost));
        return this;
    }

    /**
     * TermsQuery 方式 IN MUST查询
     *
     * @param field  字段名称
     * @param values 包含的的数据
     * @param boost  权重
     * @return 查询链
     */
    public <T> QueryChainBase inTerms(String field, List<T> values, float boost) {
        builder.must(QueryBuilders.termsQuery(field, values).boost(boost));
        return this;
    }


    /**
     * TermsQuery 方式 NOT IN mustNot 查询
     *
     * @param field  字段名称
     * @param values 包含的的数据
     * @param boost  权重
     * @return 查询链
     */
    public <T> QueryChainBase notInTerms(String field, List<T> values, float boost) {
        builder.mustNot(QueryBuilders.termsQuery(field, values).boost(boost));
        return this;
    }


    /**
     * MatchPhraseQuery 方式 must 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase matchPhraseAnd(String field, T value, Integer slop, float boost) {
        builder.must(QueryBuilders.matchPhraseQuery(field, value).slop(slop).boost(boost));
        return this;
    }

    /**
     * MatchPhraseQuery 方式 should 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase matchPhraseOr(String field, T value, Integer slop, float boost) {
        builder.should(QueryBuilders.matchPhraseQuery(field, value).slop(slop).boost(boost));
        return this;
    }

    /**
     * MatchPhrasePrefixQuery 方式 must 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase matchPhrasePrefixAnd(String field, T value, Integer slop, float boost) {
        builder.must(QueryBuilders.matchPhrasePrefixQuery(field, value).slop(slop).boost(boost));
        return this;
    }

    /**
     * MatchPhrasePrefixQuery 方式 should 查询
     *
     * @param field 字段名称
     * @param value 包含的的数据
     * @param boost 权重
     * @return 查询链
     */
    public <T> QueryChainBase matchPhrasePrefixOr(String field, T value, Integer slop, float boost) {
        builder.should(QueryBuilders.matchPhrasePrefixQuery(field, value).slop(slop).boost(boost));
        return this;
    }

    /**
     * bool方式嵌套should查询 嵌套层数最多两层，如果想要嵌套多层请使用
     *
     * @param builders BoolQueryBuilder，可以配合
     * @return QueryChainBase
     * @see #boolQuery
     * @see QueryChains#builderMusts() 内嵌使用
     */
    public QueryChainBase shouldNestMatch(QueryBuilder... builders) {
        Lists.newArrayList(builders).forEach(builder::should);
        return this;
    }

    public BoolQueryBuilder boolQuery() {
        return new BoolQueryBuilder();
    }

    /**
     * 多条件查询排序方法
     *
     * @param field 字段名称
     * @param sort  排序方式
     * @return 查询链
     */
    public QueryChainBase sort(String field, Sort sort) {
        FieldSortBuilder sortItem = SortBuilders.fieldSort(field);
        switch (sort) {
            case ASC:
                sortItem.order(SortOrder.ASC);
                break;
            case DESC:
                sortItem.order(SortOrder.DESC);
                break;
            default:
                break;
        }
        this.sortBuilder.add(sortItem);
        return this;
    }


    /**
     * 多条件查询执行方法
     *
     * @param index 索引
     * @param from  其实位置
     * @param size  查询数量
     * @return ResultDTO
     */
    public ResultDTO<List<JsonObject>> execute(String index, @NotNull Integer from, @NotNull Integer size)
            throws IOException {
        return execute(index, from, size, null, null);
    }

    public ResultDTO<List<JsonObject>> execute(String index, @NotNull Integer from, @NotNull Integer size, String[] includes, String[] excludes) throws IOException {
        SearchRequest searchRequest;
        searchRequest = (index == null ? new SearchRequest() : new SearchRequest(index));
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(builder);
        if (includes != null || excludes != null) {
            searchSourceBuilder.fetchSource(includes, excludes);
        }
        for (FieldSortBuilder fieldSortBuilder : sortBuilder) {
            searchSourceBuilder.sort(fieldSortBuilder);
        }
        searchSourceBuilder.trackTotalHits(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(searchResponse.getHits().getTotalHits().value);
        resultDTO.setData(QueryChains.printHitsInJsonRootList(searchResponse));
        return resultDTO;
    }

    public <T> ResultDTO<List<T>> execute(String index, @NotNull Integer from, @NotNull Integer size, Class<T> clazz)
            throws IOException {
        return execute(index, from, size, null, null, clazz);
    }

    public <T> ResultDTO<List<T>> execute(String index, @NotNull Integer from, @NotNull Integer size,
                                          String[] includes, String[] excludes, Class<T> clazz) throws IOException {
        SearchRequest searchRequest;
        searchRequest = (index == null ? new SearchRequest() : new SearchRequest(index));
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        searchSourceBuilder.query(builder);
        if (includes != null || excludes != null) {
            searchSourceBuilder.fetchSource(includes, excludes);
        }
        for (FieldSortBuilder fieldSortBuilder : sortBuilder) {
            searchSourceBuilder.sort(fieldSortBuilder);
        }
        searchSourceBuilder.trackTotalHits(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(searchResponse.getHits().getTotalHits().value);
        List<T> jsonObjects = QueryChains.printHitsInJsonRootList(searchResponse, clazz);
        resultDTO.setData(jsonObjects);
        return resultDTO;
    }

    /**
     * parent-child多条件查询执行方法
     *
     * @param index       索引
     * @param from        起始位置
     * @param size        查询数量
     * @param queryType   parent-child查询join值
     * @param isParent    true-通过父文档获取子文档, false-通过子文档获取父文档
     * @param isInnerJoin true-join当前父文档/子文档数据, false-不join父文档/子文档数据
     * @param base        QueryChainBase 子查询语句封装
     * @param p           bool查询类型
     * @return ResultDTO
     */
    public ResultDTO<List<JsonObject>> execute(String index, @NotNull Integer from, @NotNull Integer size,
                                               String queryType, boolean isParent, boolean isInnerJoin, QueryChainBase base,
                                               ParentChildBoolMenu p) throws IOException {
        SearchRequest searchRequest;
        searchRequest = (index == null ? new SearchRequest() : new SearchRequest(index));
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        InnerHitBuilder innerHitBuilder = new InnerHitBuilder();
        innerHitBuilder.setSize(60);

        if (isParent) {
            QueryBuilder qb = JoinQueryBuilders.hasParentQuery(
                    queryType,
                    builder,
                    false
            ).innerHit(innerHitBuilder);

            buildParentChildQuery(p, base, qb);
        } else {
            QueryBuilder qb = JoinQueryBuilders.hasChildQuery(
                    queryType,
                    builder,
                    ScoreMode.None
            ).innerHit(innerHitBuilder);

            buildParentChildQuery(p, base, qb);
        }

        searchSourceBuilder.query(base.builder);
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        for (FieldSortBuilder fieldSortBuilder : sortBuilder) {
            searchSourceBuilder.sort(fieldSortBuilder);
        }
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<JsonObject>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(searchResponse.getHits().getTotalHits().value);

        if (isInnerJoin) {
            resultDTO.setData(ElasticSearchManager.printInnerHitsInJsonRootList(searchResponse));
        } else {
            resultDTO.setData(ElasticSearchManager.printHitsInJsonRootList(searchResponse));
        }
        return resultDTO;
    }

    /**
     * parent-child多条件查询执行方法
     *
     * @param index       索引
     * @param from        起始位置
     * @param size        查询数量
     * @param queryType   parent-child查询join值
     * @param isParent    true-通过父文档获取子文档, false-通过子文档获取父文档
     * @param isInnerJoin true-join当前父文档/子文档数据, false-不join父文档/子文档数据
     * @param base        QueryChainBase 子查询语句封装
     * @param p           bool查询类型
     * @param clazz       指定类型
     * @return ResultDTO
     */
    public <T> ResultDTO<List<T>> execute(String index, @NotNull Integer from, @NotNull Integer size,
                                          String queryType, boolean isParent, boolean isInnerJoin, QueryChainBase base,
                                          ParentChildBoolMenu p, Class<T> clazz) throws IOException {
        SearchRequest searchRequest;
        searchRequest = (index == null ? new SearchRequest() : new SearchRequest(index));
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        InnerHitBuilder innerHitBuilder = new InnerHitBuilder();
        innerHitBuilder.setSize(60);

        if (isParent) {
            QueryBuilder qb = JoinQueryBuilders.hasParentQuery(
                    queryType,
                    builder,
                    false
            ).innerHit(innerHitBuilder);

            buildParentChildQuery(p, base, qb);
        } else {
            QueryBuilder qb = JoinQueryBuilders.hasChildQuery(
                    queryType,
                    builder,
                    ScoreMode.None
            ).innerHit(innerHitBuilder);

            buildParentChildQuery(p, base, qb);
        }

        searchSourceBuilder.query(base.builder);
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        for (FieldSortBuilder fieldSortBuilder : sortBuilder) {
            searchSourceBuilder.sort(fieldSortBuilder);
        }
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        ResultDTO<List<T>> resultDTO = new ResultDTO<>();
        resultDTO.setTotal(searchResponse.getHits().getTotalHits().value);

        if (isInnerJoin) {
            resultDTO.setData(QueryChains.printInnerHitsInJsonRootList(searchResponse, clazz));
        } else {
            resultDTO.setData(QueryChains.printHitsInJsonRootList(searchResponse, clazz));
        }
        return resultDTO;
    }

    /**
     * 获取索引的配置 Settings
     *
     * @param index 需要获取配置的索引
     * @return
     * @throws IOException
     */
    public Settings getSettings(String index) throws IOException {
        GetIndexRequest request = new GetIndexRequest(index);
        request.includeDefaults(true);
        request.indicesOptions(IndicesOptions.lenientExpandOpen());
        GetIndexResponse getIndexResponse = client.indices().get(request, RequestOptions.DEFAULT);
        return getIndexResponse.getSettings().get(index);
    }


    /**
     * 检查索引只读配置
     *
     * @param index 索引
     * @return true 是只读 false 非只读
     * @throws IOException
     */
    public boolean checkIndexReadOnly(String index) throws IOException {
        GetIndexRequest request = new GetIndexRequest(index);
        request.includeDefaults(true);
        request.indicesOptions(IndicesOptions.lenientExpandOpen());
        GetIndexResponse getIndexResponse = client.indices().get(request, RequestOptions.DEFAULT);
        return QueryChains.newQueryChains().indexBlocksReadOnly(getIndexResponse.getSettings().get(index));
    }


    public long count(String index) throws IOException {
        return count(index, null, null);
    }

    public long count(String index, String[] includes, String[] excludes) throws IOException {
        CountRequest countRequest;
        countRequest = (index == null ? new CountRequest() : new CountRequest(index));
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(builder);
        if (includes != null || excludes != null) {
            searchSourceBuilder.fetchSource(includes, excludes);
        }
        for (FieldSortBuilder fieldSortBuilder : sortBuilder) {
            searchSourceBuilder.sort(fieldSortBuilder);
        }
        searchSourceBuilder.trackTotalHits(true);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        countRequest.source(searchSourceBuilder);
        CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
        return countResponse.getCount();
    }

    /**
     * queryBuilder-bool查询
     *
     * @param p    bool查询类型
     * @param base QueryChainBase 子查询语句封装
     * @param qb   parent-child查询语句
     */
    private void buildParentChildQuery(ParentChildBoolMenu p, QueryChainBase base, QueryBuilder qb) {
        switch (p) {
            case MUST:
                base.builder.must().add(qb);
                break;
            case SHOULD:
                base.builder.should().add(qb);
                break;
            case FILTER:
                base.builder.filter().add(qb);
                break;
            case MUST_NOT:
                base.builder.mustNot().add(qb);
                break;
        }
    }

}
