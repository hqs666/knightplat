package com.knight.plat.elastic.exception;



public class ValidateException extends RuntimeException {
    private static final long serialVersionUID = 6057602589533840889L;

    public ValidateException(String msg) {
        super(msg);
    }

}
