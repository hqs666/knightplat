package com.knight.plat.elastic.core.base;

import com.google.common.collect.Lists;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;

import java.io.IOException;
import java.util.List;


public class SuggestChainBase {

    private RestHighLevelClient client;

    private final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    private final SuggestBuilder suggestBuilder = new SuggestBuilder();


    //无参构造
    private SuggestChainBase() {

    }

    //有参构造
    public SuggestChainBase(RestHighLevelClient client) {
        this.client = client;
    }

    /**
     * 提示查询
     *
     * @param filed          查询字段
     * @param val            查询内容
     * @param suggestName    当前提示查询节点名称
     * @param size           查询数量
     * @param skipDuplicates 去重复
     * @return
     */
    public SuggestChainBase completionSuggestion(String filed, String val, String suggestName, int size, boolean skipDuplicates) {
        suggestBuilder.addSuggestion(suggestName, SuggestBuilders
                .completionSuggestion(filed)
                .text(val)
                .size(size)
                .skipDuplicates(skipDuplicates));
        return this;
    }

    /**
     * 提示查询 --默认去重
     *
     * @param filed       查询字段
     * @param val         查询内容
     * @param suggestName 当前提示查询节点名称
     * @param size        查询数量
     * @return
     */
    public SuggestChainBase completionSuggestion(String filed, String val, String suggestName, int size) {
        suggestBuilder.addSuggestion(suggestName, SuggestBuilders
                .completionSuggestion(filed)
                .prefix(val)
                .size(size)
                .skipDuplicates(true));
        return this;
    }

    /**
     * 构建好的request执行
     *
     * @param indices
     * @return
     * @throws IOException
     */
    public Suggest execute(String... indices) throws IOException {
        SearchResponse response = client.search(
                new SearchRequest(indices).source(searchSourceBuilder.suggest(suggestBuilder)),
                RequestOptions.DEFAULT
        );
        return response.getSuggest();
    }

    /**
     * 获取匹配值
     *
     * @param suggestions 查询结果集
     * @param suggestName 需要获取的suggest名称
     * @return
     */
    public List<String> getSuggestion(Suggest suggestions, String suggestName) {
        List<String> suggests = Lists.newArrayList();
        suggestions.getSuggestion(suggestName).getEntries()
                .forEach(entry -> entry.forEach(option -> suggests.add(option.getText().toString())));
        return suggests;
    }


}
