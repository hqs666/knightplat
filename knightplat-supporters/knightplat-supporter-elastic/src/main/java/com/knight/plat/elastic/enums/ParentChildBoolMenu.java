package com.knight.plat.elastic.enums;

public enum ParentChildBoolMenu {
    /**
     * 匹配的文档必须满足的子句，并会用于计算分数
     */
    MUST(1),
    /**
     * 匹配的文档必须满足的子句，但不会向must一样计算分数，分数被忽略，Filter子句在Filter上下文中运行，这意味着计分被忽略，并且子句被考虑用于缓存
     */
    FILTER(2),
    /**
     * 匹配的文档可能满足的子句，不是一定需要满足
     */
    SHOULD(3),
    /**
     * 匹配的文档必须不满足子句，子句在Filter上下文中执行，计分被忽略，并且会使用缓存，由于计分被忽略，因此所有文档的分数返回均为0
     */
    MUST_NOT(4);

    ParentChildBoolMenu(){

    }

    ParentChildBoolMenu(Integer code){
        this.code = code;
    }
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
