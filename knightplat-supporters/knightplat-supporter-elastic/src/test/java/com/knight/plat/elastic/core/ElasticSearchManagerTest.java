package com.knight.plat.elastic.core;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.commons.JsonObject;
import com.knight.plat.elastic.core.ElasticSearchManager;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// @RunWith(SpringRunner.class)
// @SpringBootTest(classes = { ElasticsearchDataAutoConfiguration.class})
public class ElasticSearchManagerTest {

    static ElasticSearchManager elasticSearchManager;

    //@BeforeAll
    static void before() {
        HttpHost host = new HttpHost("127.0.0.1", 9200, "http");
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(RestClient.builder(host));
        elasticSearchManager = new ElasticSearchManager(restHighLevelClient);
    }

    //@Test
    public void query() throws Exception {
        Map<String, Object> nestedMap = new HashMap<>();

        nestedMap.put("poolMark.poolNo", "P0026");
        ResultDTO<List<JsonObject>> result = elasticSearchManager.query()
                .nestedMatchAnd("poolMark", nestedMap)
                .execute("mytest01", 0, 10);
        System.out.println(result);
    }

}