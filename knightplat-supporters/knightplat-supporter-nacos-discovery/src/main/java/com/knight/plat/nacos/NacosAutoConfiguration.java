package com.knight.plat.nacos;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.cloud.nacos.endpoint.NacosDiscoveryEndpointAutoConfiguration;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.google.common.base.Strings;
import com.knight.plat.api.exceptions.BusinessException;
import com.knight.plat.api.security.annotations.IgnoreAuth;
import io.swagger.v3.oas.annotations.Hidden;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AutoConfiguration(before = NacosDiscoveryEndpointAutoConfiguration.class)
public class NacosAutoConfiguration {

    private static final Logger log = LoggerFactory.getLogger(NacosAutoConfiguration.class);

    @Bean
    public BeanPostProcessor propertiesPostProcessor(@Value("${spring.cloud.nacos.server-path:}") String contextPath) {
        return new BeanPostProcessor() {
            @Override
            public Object postProcessBeforeInitialization(@NotNull Object bean, @NotNull String beanName) throws BeansException {
                if (bean instanceof NacosDiscoveryProperties && !Strings.isNullOrEmpty(contextPath)) {
                    log.info("Register nacos context-path: " + contextPath);
                    System.setProperty("nacos.naming.web.context", contextPath);
                }
                return bean;
            }
        };
    }

    @RestController
    @Hidden
    static class MonitorHandlerController {

        private final NacosServiceManager manager;
        private final NacosDiscoveryProperties nacosDiscoveryProperties;

        MonitorHandlerController(NacosServiceManager manager,
                                 NacosDiscoveryProperties nacosDiscoveryProperties) {
            this.manager = manager;
            this.nacosDiscoveryProperties = nacosDiscoveryProperties;
        }

        private NamingService namingService() {
            return manager.getNamingService();
        }

        @RequestMapping("_plat/deregister")
        @IgnoreAuth
        public String deregister() {
            String serviceName = nacosDiscoveryProperties.getService();
            String groupName = nacosDiscoveryProperties.getGroup();
            String clusterName = nacosDiscoveryProperties.getClusterName();
            String ip = nacosDiscoveryProperties.getIp();
            int port = nacosDiscoveryProperties.getPort();
            try {
                namingService().deregisterInstance(serviceName, groupName, ip, port, clusterName);
            } catch (NacosException e) {
                throw new BusinessException("Deregister error", e);
            }
            return "OK";
        }
    }

}
