package com.knight.plat.fegin;

import feign.opentracing.FeignSpanDecorator;
import io.opentracing.Tracer;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cloud.openfeign.FeignContext;

import java.util.List;

public class FeignContextBeanPostProcessor implements BeanPostProcessor {

    private final Tracer tracer;
    private final BeanFactory beanFactory;
    private final List<FeignSpanDecorator> spanDecorators;

    FeignContextBeanPostProcessor(Tracer tracer, BeanFactory beanFactory,
            List<FeignSpanDecorator> spanDecorators) {
        this.tracer = tracer;
        this.beanFactory = beanFactory;
        this.spanDecorators = spanDecorators;
    }

    @Override
    public Object postProcessBeforeInitialization(@NotNull Object bean, @NotNull String name) throws BeansException {
        if (bean instanceof FeignContext && !(bean instanceof TraceFeignContext)) {
            return new TraceFeignContext(tracer, (FeignContext) bean, beanFactory, spanDecorators);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(@NotNull Object bean, @NotNull String name) throws BeansException {
        return bean;
    }
}
