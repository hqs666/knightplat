package com.knight.plat.fegin;

import feign.Client;
import feign.opentracing.FeignSpanDecorator;
import feign.opentracing.TracingClient;
import io.opentracing.Tracer;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.cloud.client.loadbalancer.LoadBalancedRetryFactory;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.support.LoadBalancerClientFactory;
import org.springframework.cloud.openfeign.loadbalancer.FeignBlockingLoadBalancerClient;
import org.springframework.cloud.openfeign.loadbalancer.RetryableFeignBlockingLoadBalancerClient;
import org.springframework.context.annotation.Lazy;

import java.util.List;

class TracedFeignBeanFactory {

    private final Tracer tracer;
    private final BeanFactory beanFactory;
    private final List<FeignSpanDecorator> spanDecorators;

    public TracedFeignBeanFactory(Tracer tracer, BeanFactory beanFactory, @Lazy List<FeignSpanDecorator> spanDecorators) {
        this.tracer = tracer;
        this.beanFactory = beanFactory;
        this.spanDecorators = spanDecorators;
    }

    public Object from(Object bean) {
        if (bean instanceof TracingClient || bean instanceof LoadBalancedTracedFeign || bean instanceof RetryableLoadBalancedTracedFeign) {
            return bean;
        }

        if (bean instanceof Client) {
            if (bean instanceof FeignBlockingLoadBalancerClient) {
                return new LoadBalancedTracedFeign(
                        buildTracingClient(((FeignBlockingLoadBalancerClient) bean).getDelegate(), tracer),
                        beanFactory.getBean(LoadBalancerClient.class),
                        beanFactory.getBean(LoadBalancerClientFactory.class));
            }
            if (bean instanceof RetryableFeignBlockingLoadBalancerClient) {
                return new RetryableLoadBalancedTracedFeign(
                        buildTracingClient(((RetryableFeignBlockingLoadBalancerClient) bean).getDelegate(), tracer),
                        beanFactory.getBean(LoadBalancerClient.class),
                        beanFactory.getBean(LoadBalancedRetryFactory.class),
                        beanFactory.getBean(LoadBalancerClientFactory.class));
            }
            return buildTracingClient((Client) bean, tracer);
        }

        return bean;
    }


    private TracingClient buildTracingClient(Client delegate, Tracer tracer) {
        return new TracingClientBuilder(delegate, tracer)
                .withFeignSpanDecorators(spanDecorators)
                .build();
    }

    static class LoadBalancedTracedFeign extends FeignBlockingLoadBalancerClient {

        public LoadBalancedTracedFeign(Client delegate, LoadBalancerClient loadBalancerClient, LoadBalancerClientFactory loadBalancerClientFactory) {
            super(delegate, loadBalancerClient, loadBalancerClientFactory);
        }
    }

    static class RetryableLoadBalancedTracedFeign extends RetryableFeignBlockingLoadBalancerClient {

        public RetryableLoadBalancedTracedFeign(Client delegate, LoadBalancerClient loadBalancerClient,
                LoadBalancedRetryFactory loadBalancedRetryFactory,
                LoadBalancerClientFactory loadBalancerClientFactory) {
            super(delegate, loadBalancerClient, loadBalancedRetryFactory, loadBalancerClientFactory);
        }
    }

}
