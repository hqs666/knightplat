package com.knight.plat.fegin;

import feign.Client;
import feign.opentracing.FeignSpanDecorator;
import feign.opentracing.TracingClient;
import io.opentracing.Tracer;

import java.util.List;

class TracingClientBuilder {

    private Client delegate;
    private Tracer tracer;
    private List<FeignSpanDecorator> decorators;

    TracingClientBuilder(Client delegate, Tracer tracer) {
        this.delegate = delegate;
        this.tracer = tracer;
    }

    TracingClientBuilder withFeignSpanDecorators(List<FeignSpanDecorator> decorators) {
        this.decorators = decorators;
        return this;
    }

    TracingClient build() {
        if (decorators == null || decorators.isEmpty()) {
            return new TracingClient(delegate, tracer);
        }
        return new TracingClient(delegate, tracer, decorators);
    }

}
