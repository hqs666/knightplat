package com.knight.plat.fegin;

import feign.Client;
import feign.Request;
import feign.opentracing.FeignSpanDecorator;
import feign.opentracing.TracingClient;
import io.opentracing.Tracer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.loadbalancer.FeignBlockingLoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.util.List;

@AutoConfiguration(beforeName = "org.springframework.cloud.openfeign.FeignAutoConfiguration")
@ConditionalOnClass({Client.class, FeignBlockingLoadBalancerClient.class})
@ConditionalOnBean(Tracer.class)
//@AutoConfigureAfter(TracerAutoConfiguration.class)
@ConditionalOnProperty(name = "opentracing.spring.cloud.feign.enabled", havingValue = "true", matchIfMissing = true)
public class FeignTracingAutoConfiguration {

    @Autowired
    @Lazy
    private Tracer tracer;

    @Autowired(required = false)
    @Lazy
    private List<FeignSpanDecorator> spanDecorators;

    @Bean
    @ConditionalOnClass(name = "org.springframework.cloud.openfeign.FeignContext")
    FeignContextBeanPostProcessor feignContextBeanPostProcessor(BeanFactory beanFactory) {
        return new FeignContextBeanPostProcessor(tracer, beanFactory, spanDecorators);
    }

    @Bean
    public TracingAspect tracingAspect() {
        return new TracingAspect();
    }

    /**
     * Trace feign clients created manually
     */
    @Aspect
    class TracingAspect {

        @Around("execution (* feign.Client.*(..)) && !within(is(FinalType))")
        public Object feignClientWasCalled(final ProceedingJoinPoint pjp) throws Throwable {
            Object bean = pjp.getTarget();
            if (!(bean instanceof TracingClient)) {
                Object[] args = pjp.getArgs();
                return new TracingClientBuilder((Client) bean, tracer)
                        .withFeignSpanDecorators(spanDecorators)
                        .build()
                        .execute((Request) args[0], (Request.Options) args[1]);
            }
            return pjp.proceed();
        }
    }
}
