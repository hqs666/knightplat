package com.knight.plat.s3;

import com.google.common.base.Strings;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.filesystem.FileManager;
import com.knight.plat.api.filesystem.Part;
import com.knight.plat.async.AsyncManager;
import com.knight.plat.utils.KeyGenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CompleteMultipartUploadRequest;
import software.amazon.awssdk.services.s3.model.CompletedMultipartUpload;
import software.amazon.awssdk.services.s3.model.CompletedPart;
import software.amazon.awssdk.services.s3.model.CreateMultipartUploadRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.UploadPartRequest;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class S3FileManager implements FileManager {
    private static final Logger logger = LoggerFactory.getLogger(S3FileManager.class);

    private final S3Properties properties;
    private final BucketManager bucketManager;
    private final AsyncManager asyncManager;

    public S3FileManager(BucketManager bucketManager, S3Properties properties,
                         AsyncManager asyncManager) {
        this.bucketManager = bucketManager;
        this.properties = properties;
        this.asyncManager = asyncManager;
    }

    private Path getTempPath(String... path) {
        if (path == null || path.length == 0) {
            return null;
        }

        if (path.length == 1) {
            return FileSystems.getDefault().getPath(System.getProperty("user.home"), ".knightplat", "s3", "temp", path[0]);
        }

        final String[] finalPaths = new String[path.length + 3];
        finalPaths[0] = ".knightplat";
        finalPaths[1] = "s3";
        finalPaths[2] = "temp";
        System.arraycopy(path, 0, finalPaths, 3, path.length);
        return FileSystems.getDefault().getPath(System.getProperty("user.home"), finalPaths);
    }

    private S3Client determineClient(String bucket) {
        S3Client bucketClient = bucketManager.getClient(bucket);
        if (bucketClient != null) {
            return bucketClient;
        }
        S3Client defaultClient = bucketManager.getDefaultClient();
        if (defaultClient == null) {
            throw new RuntimeException("没有找到可用的S3客户端，请检查配置文件是否正确");
        }
        return defaultClient;
    }

    private S3AsyncClient determineAsyncClient(String bucket) {
        S3AsyncClient bucketAsyncClient = bucketManager.getAsyncClient(bucket);
        if (bucketAsyncClient != null) {
            return bucketAsyncClient;
        }
        S3AsyncClient defaultAsyncClient = bucketManager.getDefaultAsyncClient();
        if (defaultAsyncClient == null) {
            throw new RuntimeException("没有找到可用的S3客户端，请检查配置文件是否正确");
        }
        return defaultAsyncClient;
    }

    @Override
    public String save(InputStream data, String bucket, String path) throws IOException {
        return save(data, -1, bucket, path);
    }

    @Override
    public String save(InputStream data, long contentLength, String bucket, String path) throws IOException {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        final String contentType = Files.probeContentType(Paths.get(path));
        try (InputStream is = data) {
            final PutObjectRequest.Builder requestBuilder = PutObjectRequest.builder()
                    .bucket(bucket)
                    .key(path);
            if (!Strings.isNullOrEmpty(contentType)) {
                requestBuilder.contentType(contentType);
            }
            final PutObjectRequest request = requestBuilder.build();
            final long finalLen = contentLength == -1 ? is.available() : contentLength;
            final RequestBody body = RequestBody.fromInputStream(is, finalLen);
            determineClient(bucket).putObject(request, body);
        }

        return getBaseUrl(bucket, path);
    }

    @Override
    public void saveAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        saveAsync(data, -1, bucket, path, callback);
    }

    @Override
    public void saveAsync(InputStream data, long contentLength, String bucket, String path,
                          Callback<String, IOException> callback) {
        asyncManager.stage(() -> save(data, contentLength, bucket, path)).whenComplete((res, err) -> {
            if (err == null) {
                callback.onSuccess(res);
            } else {
                if (err instanceof IOException) {
                    callback.onException((IOException) err);
                } else {
                    callback.onException(new IOException("Unknown exception when saving file", err));
                }
            }
        });
    }

    @Override
    public String append(InputStream data, String bucket, String path) throws IOException {
        // TODO
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public void appendAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        // TODO
        throw new UnsupportedOperationException("todo");
    }

    @Override
    public byte[] read(String bucket, String path) throws IOException {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        final GetObjectRequest objectRequest = GetObjectRequest
                .builder()
                .key(path)
                .bucket(bucket)
                .build();
        final ResponseBytes<GetObjectResponse> objectBytes;
        try {
            objectBytes = determineClient(bucket).getObjectAsBytes(objectRequest);
        } catch (Exception e) {
            throw new IOException(e);
        }

        return objectBytes.asByteArray();
    }

    @Override
    public long size(String bucket, String path) throws IOException {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        final HeadObjectRequest request = HeadObjectRequest
                .builder()
                .key(path)
                .bucket(bucket)
                .build();
        try {
            return determineClient(bucket).headObject(request).contentLength();
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    @Override
    public InputStream readAsInputStream(String bucket, String path) throws IOException {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        final GetObjectRequest objectRequest = GetObjectRequest
                .builder()
                .key(path)
                .bucket(bucket)
                .build();
        try {
            return determineClient(bucket).getObject(objectRequest);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public InputStream readRange(String bucket, String path, String range) throws IOException {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        final GetObjectRequest objectRequest = GetObjectRequest
                .builder()
                .key(path)
                .bucket(bucket)
                .range(range)
                .build();
        try {
            return determineClient(bucket).getObject(objectRequest);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    @Override
    public void readAsync(String bucket, String path, Callback<byte[], IOException> callback) {
        final String realPath;
        if (path.startsWith("/")) {
            realPath = path.substring(1);
        } else {
            realPath = path;
        }
        GetObjectRequest objectRequest = GetObjectRequest
                .builder()
                .key(realPath)
                .bucket(bucket)
                .build();
        CompletableFuture<ResponseBytes<GetObjectResponse>> future = determineAsyncClient(bucket).getObject(objectRequest,
                AsyncResponseTransformer.toBytes());
        future.whenComplete((res, err) -> {
            if (err == null) {
                callback.onSuccess(res.asByteArray());
            } else {
                if (err instanceof IOException) {
                    callback.onException((IOException) err);
                } else {
                    callback.onException(new IOException(err));
                }
            }
        });
    }

    @Override
    public void readAsInputStreamAsync(String bucket, String path, Callback<InputStream, IOException> callback) {
        final String realPath;
        if (path.startsWith("/")) {
            realPath = path.substring(1);
        } else {
            realPath = path;
        }
        final GetObjectRequest objectRequest = GetObjectRequest
                .builder()
                .key(realPath)
                .bucket(bucket)
                .build();
        final Path tempFile = getTempPath(KeyGenUtils.randomString(10000, 99999, 5));
        final CompletableFuture<GetObjectResponse> future = determineAsyncClient(bucket).getObject(objectRequest,
                AsyncResponseTransformer.toFile(tempFile));
        future.whenComplete((res, err) -> {
            if (err == null) {
                try (InputStream in = AutoDeleteInputStream.of(tempFile)) {
                    callback.onSuccess(in);
                } catch (IOException e) {
                    callback.onException(e);
                }
            } else {
                if (err instanceof IOException) {
                    callback.onException((IOException) err);
                } else {
                    callback.onException(new IOException(err));
                }
            }
        });
    }

    @Override
    public byte[] read(String bucket, String path, String params) throws IOException {
        // TODO: 等待宝之云提供的图片处理接口
        return read(bucket, path);
    }

    @Override
    public void readAsync(String bucket, String path, String params, Callback<byte[], IOException> callback) {
        // TODO: 等待宝之云提供的图片处理接口
        readAsync(bucket, path, callback);
    }

    @Override
    public String createMultipartUpload(String bucket, String path) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        CreateMultipartUploadRequest.Builder builder = CreateMultipartUploadRequest.builder()
                .bucket(bucket)
                .key(path);

        try {
            final String contentType = Files.probeContentType(Paths.get(path));
            if (!Strings.isNullOrEmpty(contentType)) {
                builder.contentType(contentType);
            }
        } catch (IOException e) {
            logger.warn("Something went wrong when generating content-type for path: " + path);
        }

        CreateMultipartUploadRequest createMultipartUploadRequest = builder.build();
        return determineClient(bucket).createMultipartUpload(createMultipartUploadRequest)
                .uploadId();
    }

    @Override
    public Part multipartUpload(String bucket, String path, Integer chunk, InputStream data, String uploadId)
            throws IOException {
        if (chunk == null) chunk = 0;
        int partNum = chunk + 1;

        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        String etag = determineClient(bucket).uploadPart(UploadPartRequest.builder()
                .bucket(bucket)
                .key(path)
                .uploadId(uploadId)
                .partNumber(partNum)
                .build(), RequestBody.fromInputStream(data, data.available())).eTag();
        Part part = new Part();
        part.setEtag(etag);
        part.setPartNumber(partNum);
        return part;
    }

    @Override
    public String completeMultipartUpload(String bucket, String path, List<Part> parts, String uploadId) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        List<CompletedPart> partArray = parts.stream()
                .map(p -> CompletedPart.builder().partNumber(p.getPartNumber()).eTag(p.getEtag()).build())
                .sorted(Comparator.comparing(CompletedPart::partNumber))
                .collect(Collectors.toList());

        CompletedMultipartUpload multipartUpload = CompletedMultipartUpload
                .builder()
                .parts(partArray)
                .build();

        determineClient(bucket).completeMultipartUpload(CompleteMultipartUploadRequest
                .builder()
                .bucket(bucket)
                .key(path)
                .uploadId(uploadId)
                .multipartUpload(multipartUpload)
                .build());

        return getBaseUrl(bucket, path);
    }

    @Override
    public String getExpireUrl(String bucketName, String key, Long expireMillisecond) {
        //TODO
        throw new UnsupportedOperationException("还未实现");
    }

    @Override
    public boolean dropFile(String bucket, String path) throws IOException {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        DeleteObjectRequest request = DeleteObjectRequest.builder()
                .bucket(bucket)
                .key(path)
                .build();
        final Boolean deleteMarker = determineClient(bucket).deleteObject(request).deleteMarker();
        return deleteMarker != null && deleteMarker;
    }

    @Override
    public void dropFileAsync(String bucket, String path, Callback<Boolean, IOException> callback) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        DeleteObjectRequest request = DeleteObjectRequest.builder()
                .bucket(bucket)
                .key(path)
                .build();
        determineAsyncClient(bucket).deleteObject(request).whenComplete((res, err) -> {
            if (err == null) {
                final Boolean deleteMarker = res.deleteMarker();
                callback.onSuccess(deleteMarker);
            } else {
                if (err instanceof IOException) {
                    callback.onException((IOException) err);
                } else {
                    callback.onException(new IOException(err));
                }
            }
        });
    }

    private String getBaseUrl(String bucket, String path) {
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        String baseUrl;
        if (properties.getEndpoint().contains("://")) {
            baseUrl = properties.getEndpoint();
        } else {
            baseUrl = properties.getSchema() + "://" + properties.getEndpoint();
        }
        return baseUrl + "/" + bucket + "/" + path;
    }

    private String setContentType(String path) {
        if (path != null && path.lastIndexOf(".") > -1) {
            return path.substring(path.lastIndexOf("."));
        } else return null;

    }
}
