package com.knight.plat.s3;

import com.google.common.base.Strings;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.http.apache.ProxyConfiguration;
import software.amazon.awssdk.http.nio.netty.NettyNioAsyncHttpClient;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.S3Client;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BucketManager implements AutoCloseable {
    private final S3Client defaultClient;
    private final S3AsyncClient defaultAsyncClient;
    private final Map<String, S3Client> s3Clients = new HashMap<>();
    private final Map<String, S3AsyncClient> s3AsyncClients = new HashMap<>();

    // for test
    BucketManager(S3Client client, S3AsyncClient asyncClient) {
        defaultClient = client;
        defaultAsyncClient = asyncClient;
    }

    public BucketManager(S3Properties properties) {
        final String endpoint = properties.getEndpoint();

        final ApacheHttpClient.Builder httpClientBuilder = ApacheHttpClient.builder();
        final NettyNioAsyncHttpClient.Builder asyncHttpClientBuilder = NettyNioAsyncHttpClient.builder();

        // 处理代理配置
        makeProxyConfig(properties).ifPresent(proxyConfiguration -> {
            httpClientBuilder.proxyConfiguration(proxyConfiguration.apacheProxy);
            asyncHttpClientBuilder.proxyConfiguration(proxyConfiguration.nettyProxy);
        });

        // 处理公共配置
        if (endpoint != null && !endpoint.isEmpty()) {
            URI uri;
            if (endpoint.contains("://")) {
                uri = URI.create(endpoint);
            } else {
                uri = URI.create(properties.getSchema() + "://" + endpoint);
            }
            final AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(AwsBasicCredentials.create(
                    properties.getAccessKeyId(), properties.getSecretAccessKey()));

            defaultClient = S3Client.builder()
                    .httpClientBuilder(httpClientBuilder)
                    .endpointOverride(uri)
                    .region(properties.getRegion())
                    .credentialsProvider(credentialsProvider)
                    .forcePathStyle(true)
                    .build();
            defaultAsyncClient = S3AsyncClient.builder()
                    .httpClientBuilder(asyncHttpClientBuilder)
                    .endpointOverride(uri)
                    .region(properties.getRegion())
                    .credentialsProvider(credentialsProvider)
                    .forcePathStyle(true)
                    .build();
        } else {
            defaultClient = null;
            defaultAsyncClient = null;
        }

        // 处理多bucket配置下每个bucket的独立配置
        final Map<String, S3Properties.BucketConfig> configMap = properties.getBuckets();
        if (configMap != null && !configMap.isEmpty()) {
            configMap.forEach((bucket, config) -> {
                String subEndpoint = properties.getEndpoint();
                if (subEndpoint == null || subEndpoint.isEmpty()) {
                    if (endpoint == null || endpoint.isEmpty()) {
                        throw new RuntimeException("没有找到S3服务的endpoint配置，请检查配置文件");
                    }
                    subEndpoint = endpoint;
                }
                URI configUri;
                if (subEndpoint.contains("://")) {
                    configUri = URI.create(subEndpoint);
                } else {
                    configUri = URI.create(config.getSchema() + "://" + subEndpoint);
                }
                final AwsCredentialsProvider configCredentialsProvider = StaticCredentialsProvider.create(AwsBasicCredentials.create(
                        config.getAccessKeyId(), config.getSecretAccessKey()));
                final S3Client client = S3Client.builder()
                        .httpClientBuilder(httpClientBuilder)
                        .endpointOverride(configUri)
                        .region(config.getRegion())
                        .credentialsProvider(configCredentialsProvider)
                        .forcePathStyle(true)
                        .build();
                final S3AsyncClient asyncClient = S3AsyncClient.builder()
                        .httpClientBuilder(asyncHttpClientBuilder)
                        .endpointOverride(configUri)
                        .region(config.getRegion())
                        .credentialsProvider(configCredentialsProvider)
                        .forcePathStyle(true)
                        .build();
                s3Clients.put(bucket, client);
                s3AsyncClients.put(bucket, asyncClient);
            });
        }
    }

    static class ProxyConfig {
        software.amazon.awssdk.http.apache.ProxyConfiguration apacheProxy;
        software.amazon.awssdk.http.nio.netty.ProxyConfiguration nettyProxy;
    }

    private Optional<ProxyConfig> makeProxyConfig(S3Properties properties) {
        final String endpoint = properties.getProxyServer();
        if (Strings.isNullOrEmpty(endpoint)) {
            return Optional.empty();
        }

        ProxyConfig proxyConfig = new ProxyConfig();
        Integer proxyPort = properties.getProxyPort();

        URI uri;
        String suffix;
        if (proxyPort != null) {
            suffix = ":" + proxyPort;
        } else {
            suffix = "";
        }
        if (endpoint.contains("://")) {
            uri = URI.create(endpoint + suffix);
        } else {
            uri = URI.create(properties.getSchema() + "://" + endpoint + suffix);
        }
        final ProxyConfiguration.Builder builder = software.amazon.awssdk.http.apache.ProxyConfiguration.builder().endpoint(uri);
        final String proxyUserName = properties.getProxyUserName();
        if (!Strings.isNullOrEmpty(proxyUserName)) {
            builder.username(proxyUserName);
        }
        final String proxyPassword = properties.getProxyPassword();
        if (!Strings.isNullOrEmpty(proxyPassword)) {
            builder.password(proxyPassword);
        }
        software.amazon.awssdk.http.nio.netty.ProxyConfiguration.Builder asyncBuilder = software.amazon.awssdk.http.nio.netty.ProxyConfiguration.builder()
                .host(uri.getHost())
                .port(uri.getPort())
                .scheme(uri.getScheme());
        proxyConfig.apacheProxy = builder.build();
        proxyConfig.nettyProxy = asyncBuilder.build();

        return Optional.of(proxyConfig);
    }

    public S3Client getClient(String bucket) {
        return s3Clients.get(bucket);
    }

    public S3AsyncClient getAsyncClient(String bucket) {
        return s3AsyncClients.get(bucket);
    }

    public S3Client getDefaultClient() {
        return defaultClient;
    }

    public S3AsyncClient getDefaultAsyncClient() {
        return defaultAsyncClient;
    }

    @Override
    public void close() {
        if (defaultClient != null) {
            defaultClient.close();
        }

        if (defaultAsyncClient != null) {
            defaultAsyncClient.close();
        }

        if (!s3Clients.isEmpty()) {
            s3Clients.values().forEach(S3Client::close);
        }

        if (!s3AsyncClients.isEmpty()) {
            s3AsyncClients.values().forEach(S3AsyncClient::close);
        }
    }
}
