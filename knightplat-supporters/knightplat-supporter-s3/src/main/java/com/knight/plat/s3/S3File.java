package com.knight.plat.s3;


import com.knight.plat.api.filesystem.FileChannel;

public final class S3File implements FileChannel {

    private S3File() {}

    private static final S3File INSTANCE = new S3File();

    public static S3File channel() {
        return INSTANCE;
    }

    @Override
    public String getName() {
        return "s3";
    }
}
