package com.knight.plat.s3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FilterInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class AutoDeleteInputStream extends FilterInputStream {

    private final Path deleteFile;

    private final Logger log = LoggerFactory.getLogger(AutoDeleteInputStream.class);

    private AutoDeleteInputStream(Path deleteFile) throws IOException {
        super(Files.newInputStream(deleteFile));
        this.deleteFile = deleteFile;
    }

    public static AutoDeleteInputStream of(Path file) throws IOException {
        return new AutoDeleteInputStream(file);
    }

    @Override
    public synchronized void close() throws IOException {
        super.close();
        boolean deleteResult = Files.deleteIfExists(deleteFile);
        if (!deleteResult) {
            log.warn("Fail when deleting " + deleteFile);
        }
    }
}
