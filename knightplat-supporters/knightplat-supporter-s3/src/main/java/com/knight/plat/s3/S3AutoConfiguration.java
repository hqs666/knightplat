/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.s3;


import com.knight.plat.api.filesystem.FileChannelPair;
import com.knight.plat.async.AsyncManager;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;


@AutoConfiguration
@EnableConfigurationProperties(S3Properties.class)
public class S3AutoConfiguration {

    private final S3Properties properties;
    private final AsyncManager asyncManager;

    public S3AutoConfiguration(S3Properties properties, AsyncManager asyncManager) {
        this.properties = properties;
        this.asyncManager = asyncManager;
    }

    @Bean(destroyMethod = "close")
    public BucketManager bucketManager() {
        return new BucketManager(properties);
    }

    @Bean
    public FileChannelPair s3Channel(BucketManager bucketManager) {
        return new FileChannelPair(new S3FileManager(bucketManager, properties, asyncManager), S3File.channel()
                .getName());
    }
}
