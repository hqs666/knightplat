package com.knight.plat.seata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import io.seata.rm.datasource.undo.UndoLogParser;
import io.seata.rm.datasource.undo.UndoLogParserFactory;
import io.seata.rm.datasource.undo.parser.JacksonUndoLogParser;
import io.seata.spring.boot.autoconfigure.SeataAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;

/**
 * 分布式数据源代理初始化
 * Created at 2021/6/15 10:45
 * @author Yongqi Ren
 * @version 1.0
 */
@AutoConfiguration(after = SeataAutoConfiguration.class)
@ConditionalOnProperty(prefix = "seata", name = "enabled", havingValue = "true")
public class SeataConfiguration {

    private static final Logger log = LoggerFactory.getLogger(SeataConfiguration.class);

    @PostConstruct
    public void init() {
        final UndoLogParser logParser = UndoLogParserFactory.getInstance();
        if (logParser instanceof JacksonUndoLogParser) {
            final JacksonUndoLogParser jacksonUndoLogParser = (JacksonUndoLogParser) logParser;
            try {
                final Field mapperField = JacksonUndoLogParser.class.getDeclaredField("mapper");
                mapperField.setAccessible(true);
                final Object mapperObj = mapperField.get(jacksonUndoLogParser);
                if (mapperObj instanceof ObjectMapper) {
                    final ObjectMapper mapper = (ObjectMapper) mapperObj;
                    mapper.registerModule(new ParameterNamesModule())
                            .registerModule(new Jdk8Module())
                            .registerModule(new JavaTimeModule());
                    log.info("rewrite seata undo log jackson parser.");
                } else {
                    throw new RuntimeException("类属性无法对应，请检查seata版本");
                }
                mapperField.setAccessible(false);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new RuntimeException("类属性无法对应，请检查seata版本");
            }
        }
    }
}
