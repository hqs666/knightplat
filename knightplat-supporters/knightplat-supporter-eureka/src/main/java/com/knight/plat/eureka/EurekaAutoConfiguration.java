package com.knight.plat.eureka;


import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.netflix.discovery.EurekaClient;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;

/**
 * Created on 2019/8/29.
 */
@AutoConfiguration
@ConditionalOnClass(EurekaClient.class)
@AutoConfigImportFilter
public class EurekaAutoConfiguration {
    // Nothing right now
}
