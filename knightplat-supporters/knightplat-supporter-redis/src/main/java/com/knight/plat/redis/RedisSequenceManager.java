/*
 * Copyright (c) 2016. Knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.redis;

import com.knight.plat.api.utils.SequenceManager;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Created on 2016/11/8.
 */
public class RedisSequenceManager implements ApplicationContextAware, SequenceManager {

    private static final Logger log = LoggerFactory.getLogger(RedisSequenceManager.class);

    private static ApplicationContext applicationContext;

    private static HashOperations<String, String, Long> getOperations() {
        return Holder.operations;
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        RedisSequenceManager.applicationContext = applicationContext;
    }

    @Override
    public boolean contains(String key) {
        return getOperations().hasKey(SEQUENCE_KEY, key);
    }

    @Override
    public long increment(String key) {
        return getOperations().increment(SEQUENCE_KEY, key, 1);
    }

    @Override
    public void save(String key, long value) {
        getOperations().put(SEQUENCE_KEY, key, value);
    }

    private static class Holder {

        private static final HashOperations<String, String, Long> operations;

        static {
            log.info("Initiate the redis client for RedisSequenceManager.");
            @SuppressWarnings("unchecked")
            RedisTemplate<String, Long> template = (RedisTemplate<String, Long>) applicationContext
                    .getBean("redisTemplate");
            template.setKeySerializer(new StringRedisSerializer());
            template.setHashKeySerializer(new StringRedisSerializer());
            template.setHashValueSerializer(new RedisSerializer<Long>() {
                @Override
                public byte[] serialize(Long aLong) throws SerializationException {
                    return String.valueOf(aLong).getBytes();
                }

                @Override
                public Long deserialize(byte[] bytes) throws SerializationException {
                    return Long.parseLong(new String(bytes));
                }
            });
            operations = template.opsForHash();
        }

    }

}
