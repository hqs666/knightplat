package com.knight.plat.redis.caching;

import java.util.List;
import org.redisson.spring.cache.CacheConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "knightplat.cache")
public class RedisCacheProperties {

    private List<CacheDefinition> redis;

    public List<CacheDefinition> getRedis() {
        return redis;
    }

    public void setRedis(List<CacheDefinition> redis) {
        this.redis = redis;
    }

    static class CacheDefinition {
        private String name;

        private long ttl = 24*60*60*1000;

        private long maxIdleTime = 12*60*1000;

        private int maxSize;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getTtl() {
            return ttl;
        }

        public void setTtl(long ttl) {
            this.ttl = ttl;
        }

        public long getMaxIdleTime() {
            return maxIdleTime;
        }

        public void setMaxIdleTime(long maxIdleTime) {
            this.maxIdleTime = maxIdleTime;
        }

        public int getMaxSize() {
            return maxSize;
        }

        public void setMaxSize(int maxSize) {
            this.maxSize = maxSize;
        }

        public CacheConfig getConfig() {
            CacheConfig config = new CacheConfig(ttl, maxIdleTime);
            config.setMaxSize(maxSize);
            return config;
        }
    }
}
