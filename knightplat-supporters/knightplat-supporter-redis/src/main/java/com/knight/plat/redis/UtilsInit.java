/*
 * Copyright (c) 2016. Knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.redis;


import com.knight.plat.redis.values.impl.CommonValueSetManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created on 2016/12/19. 帮助初始化工具类 Updatedd by AJ.Don 2018/8/24 帮助初始化 ValueSetUtils
 */
@Configuration
@Import({RedisAutoConfiguration.class})
class UtilsInit {

// 方法类不是个好东西，谁有更好的解决方案(#‵′)凸

    @Bean
    Void init() {
        if (!ValueSetUtils.isInited()) {
            ValueSetUtils.init(new CommonValueSetManager());
            // ValueSetUtils.init(new JDRedisValueSetManager());
        }
        return null;
    }

}
