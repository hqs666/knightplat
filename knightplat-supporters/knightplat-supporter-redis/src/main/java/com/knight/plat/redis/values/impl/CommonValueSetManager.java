package com.knight.plat.redis.values.impl;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.knight.plat.api.commons.CodeValue;
import com.knight.plat.api.utils.ValueSetManager;
import com.knight.plat.redis.RedisUtils;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.redisson.api.RMap;
import org.springframework.util.Assert;

/**
 * Created by GHuang on 2016/11/25. Updatedd by AJ.Don 2018/8/24 Implements of valueSetManager in ry-plat-core using BD
 * storage.
 */
public class CommonValueSetManager implements ValueSetManager {

    protected static RMap<String, CodeValue> map = RedisUtils.getRedisson().getMap("_value_code_map");

    @Override
    public void load() {
        // Have a rest :)
    }

    @Override
    public CodeValue getValue(String setGroup, String code) {
        return map.get(setGroup + "." + code);
    }

    @Override
    public CodeValue getValueById(String id) {
        for (Map.Entry<String, CodeValue> e : map.entrySet()) {
            if (e.getValue().getId().equals(id)) {
                return e.getValue();
            }
        }
        return null;
    }

    @Override
    public Map<String, CodeValue> getValuesByGroup(final String setGroup) {
        Assert.notNull(setGroup, "Getting ValueSet by group with a null set group");
        Set<Entry<String, CodeValue>> entries = map.entrySet();
        Set<Entry<String, CodeValue>> filtered = Sets.filter(entries,
                item -> item != null && item.getValue() != null && setGroup.equals(item.getValue().getGroup()));
        TreeSet<Entry<String, CodeValue>> sorted = Sets.newTreeSet(new CodeValueEntityComparator());
        sorted.addAll(filtered);
        return new MapFromEntry<>(sorted);
    }

    @Override
    public Map<String, CodeValue> getValuesByParentId(final String parentId) {
        Assert.notNull(parentId, "Getting ValueSet by parentId with a null parentId");
        Set<Entry<String, CodeValue>> entries = Maps
                .filterValues(map, codeValue -> null != codeValue && parentId.equals(codeValue.getParentId()))
                .entrySet();
        TreeSet<Entry<String, CodeValue>> sorted = Sets.newTreeSet(new CodeValueEntityComparator());
        sorted.addAll(entries);
        return new MapFromEntry<>(sorted);
    }

    @Override
    public void clean() {
        map.clear();
    }

}
