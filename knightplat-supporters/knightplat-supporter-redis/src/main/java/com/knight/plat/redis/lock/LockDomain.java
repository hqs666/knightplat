package com.knight.plat.redis.lock;

public class LockDomain {
    private String value;
    private String key;

    public LockDomain(String value, String key) {
        this.value = value;
        this.key = key;
    }

    public LockDomain(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }

}
