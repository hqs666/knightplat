package com.knight.plat.redis.caching;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.redis.caching.RedisCacheProperties.CacheDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.redisson.api.RedissonClient;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@AutoConfigImportFilter
@AutoConfiguration
@EnableCaching
@EnableConfigurationProperties(RedisCacheProperties.class)
@ConditionalOnClass(CacheManager.class)
@ConditionalOnProperty(prefix = "knightplat.cache.redis", name = "enabled", havingValue = "true", matchIfMissing = true)
public class SpringCachingAutoConfiguration {

    private static final Logger log = LoggerFactory.getLogger("knightplat-cache");

    private final RedisCacheProperties properties;

    @Autowired
    public SpringCachingAutoConfiguration(RedisCacheProperties properties) {
        this.properties = properties;
    }

    @Bean
    @Primary
    CacheManager cacheManager(RedissonClient redissonClient) {
        Map<String, CacheConfig> config = new HashMap<>();

        List<CacheDefinition> definitions = properties.getRedis();
        if (definitions != null && !definitions.isEmpty()) {
            definitions.forEach(d -> {
                String name = d.getName();
                if (name != null && !name.isEmpty()) {
                    log.info("Configure redis cache instance: {}", name);
                    config.put(name, d.getConfig());
                }
            });
        }
        return new RedissonSpringCacheManager(redissonClient, config);
    }
}
