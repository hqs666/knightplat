package com.knight.plat.redis.lock;


import com.knight.plat.redis.lock.annoation.RedisLock;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


@Aspect
@ConditionalOnClass({StringRedisTemplate.class})
@Order(1)
public class RedisLockAspect {
    private static final Logger logger = LoggerFactory.getLogger(RedisLockAspect.class);

    private static StringRedisTemplate stringRedisTemplate;

    public RedisLockAspect(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @Pointcut("@annotation(com.knight.plat.redis.lock.annoation.RedisLock)")
    private void anyMethod() {
    }

    @Around("anyMethod() && @annotation(lockInfo)")
    public Object lock(ProceedingJoinPoint pjp, RedisLock lockInfo) throws Throwable {
        String synKey = "";
        if (lockInfo.isFixedKey()) {
            synKey = lockInfo.synKey();
        } else {
            synKey = this.getSynKey(pjp, lockInfo.synKey());
        }

        Object[] args = pjp.getArgs();
        if (args.length > 0 && args[0] instanceof LockDomain) {
            LockDomain lockDomain = (LockDomain) args[0];
            if (lockDomain.getValue() != null) {
                synKey = synKey + "." + lockDomain.getValue();
            }

            if (lockDomain.getKey() != null) {
                synKey = synKey + "." + lockDomain.getKey();
            }
        }

        boolean lock = false;
        Object obj = null;

        try {
            long maxSleepMills = System.currentTimeMillis() + lockInfo.maxSleepMills();

            while (true) {
                while (!lock) {
                    long keepMills = System.currentTimeMillis() + lockInfo.keepMills();
                    lock = this.setIfAbsent(synKey, keepMills);
                    if (lock) {
                        obj = pjp.proceed();
                    } else if (lockInfo.keepMills() <= 0L) {
                        if (!lockInfo.toWait()) {
                            return obj;
                        }

                        if (lockInfo.maxSleepMills() > 0L && System.currentTimeMillis() > maxSleepMills) {
                            throw new TimeoutException("RedisLockAspect.lock.TimeoutException");
                        }

                        TimeUnit.MILLISECONDS.sleep(lockInfo.sleepMills());
                    } else if (System.currentTimeMillis() > this.getLock(synKey) && System.currentTimeMillis() > this.getSet(synKey, keepMills)) {
                        lock = true;
                        obj = pjp.proceed();
                    } else {
                        if (!lockInfo.toWait()) {
                            return obj;
                        }

                        if (lockInfo.maxSleepMills() > 0L && System.currentTimeMillis() > maxSleepMills) {
                            throw new TimeoutException("RedisLockAspect.lock.TimeoutException");
                        }

                        TimeUnit.MILLISECONDS.sleep(lockInfo.sleepMills());
                    }
                }

                return obj;
            }
        } catch (Exception var14) {
            logger.error("RedisLockAspect.lock.Exception", var14);
            throw var14;
        } finally {
            if (lock) {
                this.releaseLock(synKey);
            }

        }
    }

    private String getSynKey(ProceedingJoinPoint pjp, String synKey) {
        StringBuffer synKeyBuffer = new StringBuffer(pjp.getSignature().getDeclaringTypeName());
        synKeyBuffer.append(".").append(pjp.getSignature().getName());
        if (StringUtils.isNotEmpty(synKey)) {
            synKeyBuffer.append(".").append(synKey);
        }

        return synKeyBuffer.toString();
    }

    public BoundValueOperations<String, String> getOperations(String key) {
        return this.stringRedisTemplate.boundValueOps(key);
    }

    public boolean setIfAbsent(String key, Long value) {
        return this.getOperations(key).setIfAbsent(value.toString());
    }

    public long getLock(String key) {
        String time = (String) this.getOperations(key).get();
        return time == null ? 0L : Long.valueOf(time);
    }

    public long getSet(String key, Long value) {
        String time = (String) this.getOperations(key).getAndSet(value.toString());
        return time == null ? 0L : Long.valueOf(time);
    }

    public void releaseLock(String key) {
        this.stringRedisTemplate.delete(key);
    }
}
