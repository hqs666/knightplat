package com.knight.plat.redis.mq;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.api.eventbus.SchemaStrategyPair;
import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfigImportFilter
@AutoConfiguration
public class RedisMqAutoConfiguration {

    @Bean
    @SuppressWarnings("unused")
    public SchemaStrategyPair redisEventStrategyInfo(RedissonClient redissonClient) {
        RedisMqEventStrategy redisMqEventStrategy = new RedisMqEventStrategy(redissonClient);
        return new SchemaStrategyPair("redis", redisMqEventStrategy);
    }
}
