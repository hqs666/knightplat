/*
 * Copyright (c) 2016. Knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.redis;

import com.google.common.collect.Lists;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.redisson.api.RAtomicLong;
import org.redisson.api.RBucket;
import org.redisson.api.RBuckets;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RSet;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 2016/10/28.
 */
public final class RedisUtils {

    private static final String LOCK_PREFIX = "PLAT_REDIS_UTILS_LOCK_";
    private static final Logger log = LoggerFactory.getLogger(RedisUtils.class);
    private static RedissonClient redissonClient;

    private RedisUtils() {
    }

    static void initRedisson(RedissonClient redissonClient) {
        RedisUtils.redissonClient = redissonClient;
        System.out.println("我被加载了....." + redissonClient != null);
    }

    public static RedissonClient getRedisson() {
        return redissonClient;
    }

    public static <K, V> V get(K key, Class<V> clazz) {
        if (null == key) {
            return null;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        Object result = bucket.get();
        if (!clazz.isInstance(result)) {
            return null;
        }
        return clazz.cast(result);
    }

    public static <K, V> V get(K key) {
        if (null == key) {
            return null;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        return bucket.get();
    }

    public static <K, V> Map<String, V> get(List<K> keys, Class<V> clazz) {
        if (null == keys || keys.isEmpty()) {
            return Collections.emptyMap();
        }
        if (keys.size() > 99) {
            throw new RuntimeException("Getting more than 99(got " + keys.size() + ") keys in one redis operation(get).");
        }
        RBuckets buckets = redissonClient.getBuckets();
        String[] keyArr = keys.stream()
                .filter(Objects::nonNull)
                .map(Object::toString)
                .toArray(String[]::new);
        return buckets.get(keyArr);
    }

    public static <K, V> void put(K key, V value) {
        if (null == key) {
            return;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        bucket.set(value);
    }

    public static <K, V> void put(K key, V value, long expire) {
        if (null == key) {
            return;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        bucket.set(value, expire, TimeUnit.MILLISECONDS);
    }

    public static <K, V> V removeAndGet(K key) {
        if (null == key) {
            return null;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        return bucket.getAndDelete();
    }

    public static <K, V> boolean remove(K key) {
        if (null == key) {
            return false;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        return bucket.delete();
    }

    public static <K> long increaseLong(K key) {
        if (null == key) {
            throw new NullPointerException("Cannot increase value for a null key.");
        }
        RAtomicLong aLong = redissonClient.getAtomicLong(key.toString());
        return aLong.incrementAndGet();
    }

    public static <K> long increaseLong(K key, long delta) {
        if (null == key) {
            throw new NullPointerException("Cannot increase value for a null key.");
        }
        RAtomicLong aLong = redissonClient.getAtomicLong(key.toString());
        return aLong.addAndGet(delta);
    }

    public static <K, V> boolean setContains(K key, V value) {
        if (null == key) {
            return false;
        }
        RSet<V> rSet = redissonClient.getSet(key.toString());
        return rSet.contains(value);
    }

    @SafeVarargs
    public static <K, V> void setAdd(K key, V... value) {
        if (null == key) {
            return;
        }
        RSet<V> rSet = redissonClient.getSet(key.toString());
        rSet.addAll(Lists.newArrayList(value));
    }

    public static <K, V> void setRemove(K key, V value) {
        if (null == key) {
            return;
        }
        RSet<V> rSet = redissonClient.getSet(key.toString());
        rSet.remove(value);
    }

    @Deprecated
    public static <K> Set<Object> setGet(K key) {
        return getSet(key, Object.class);
    }

    public static <K, V> Set<V> getSet(K key, Class<V> clazz) {
        if (null == key) {
            return null;
        }
        return redissonClient.getSet(key.toString());
    }

    public static <K, HK, HV> HV hashGet(K key, HK hashKey) {
        if (null == key) {
            return null;
        }
        RMap<HK, HV> map = redissonClient.getMap(key.toString());
        return map.get(hashKey);
    }

    public static <K, HK, HV> Map<HK, HV> hashMultiGet(K key, Set<HK> hashKeys) {
        if (null == key) {
            return null;
        }
        if (hashKeys.size() > 99) {
            throw new RuntimeException("Getting more than 99(got " + hashKeys.size() + ") hkeys in one redis operation(hget).");
        }
        RMap<HK, HV> map = redissonClient.getMap(key.toString());
        return map.getAll(hashKeys);
    }

    public static <K, HK, HV> Map<HK, HV> hashGetAll(K key) {
        if (null == key) {
            return null;
        }
        RMap<HK, HV> map = redissonClient.getMap(key.toString());
        final int size = map.size();
        if (size > 999) {
            throw new RuntimeException("Getting more than 999(got " + size + ") values in one redis operation(hgetall).");
        }
        return map.readAllMap();
    }

    public static <K, HK, HV> Iterator<Entry<HK, HV>> hashIterator(K key) {
        if (null == key) {
            return null;
        }
        RMap<HK, HV> map = redissonClient.getMap(key.toString());
        return map.entrySet().iterator();
    }

    public static <K, HK> long hashIncreaseLong(K key, HK hkey) {
        if (null == key) {
            throw new NullPointerException("Cannot increase value with a null key.");
        }
        if (null == hkey) {
            throw new NullPointerException("Cannot increase value with a null hkey.");
        }
        RMap<HK, Long> map = redissonClient.getMap(key.toString());
        return map.addAndGet(hkey, 1);
    }

    public static <K, HK> long hashIncreaseLong(K key, HK hkey, long delta) {
        if (null == key) {
            throw new NullPointerException("Cannot increase value with a null key.");
        }
        if (null == hkey) {
            throw new NullPointerException("Cannot increase value with a null hkey.");
        }
        RMap<HK, Long> map = redissonClient.getMap(key.toString());
        return map.addAndGet(hkey, delta);
    }

    public static <K, HK, HV> int hashLength(K key) {
        if (null == key) {
            throw new NullPointerException("Cannot get hash length with a null key.");
        }
        RMap<HK, HV> map = redissonClient.getMap(key.toString());
        return map.size();
    }

    public static <K, HK, HV> void hashSave(K key, HK hashKey, HV hashValue) {
        if (null == key) {
            return;
        }
        RMap<HK, HV> map = redissonClient.getMap(key.toString());
        map.fastPut(hashKey, hashValue);
    }

    public static <K, HK, HV> void hashMultiSave(K key, Map<HK, HV> values) {
        if (null == key) {
            return;
        }
        RMap<HK, HV> map = redissonClient.getMap(key.toString());
        map.putAll(values);
    }

    @SafeVarargs
    public static <K, HK> void hashRemove(K key, HK... hashKey) {
        if (null == key) {
            return;
        }
        RMap<HK, ?> map = redissonClient.getMap(key.toString());
        map.fastRemove(hashKey);
    }

    public static long lock(String key) {
        String realKey = LOCK_PREFIX + key;
        RLock lock = redissonClient.getFairLock(realKey);
        boolean result;
        try {
            result = lock.tryLock(10, 10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.error("Redis Lock error.", e);
            Thread.currentThread().interrupt();
            return 0;
        }
        return result ? 1 : 0;
    }

    public static boolean tryLock(String key, long waitTime, long leaseTime, TimeUnit unit) {
        String realKey = LOCK_PREFIX + key;
        RLock lock = redissonClient.getFairLock(realKey);
        try {
            return lock.tryLock(waitTime, leaseTime, unit);
        } catch (InterruptedException e) {
            log.error("Redis Lock error.", e);
            Thread.currentThread().interrupt();
            return false;
        }
    }

    public static boolean tryLock(String key, long leaseTime, TimeUnit unit) {
        String realKey = LOCK_PREFIX + key;
        RLock lock = redissonClient.getFairLock(realKey);
        try {
            return lock.tryLock(leaseTime, unit);
        } catch (InterruptedException e) {
            log.error("Redis Lock error.", e);
            Thread.currentThread().interrupt();
            return false;
        }
    }

    public static long unlock(String key) {
        String realKey = LOCK_PREFIX + key;
        RLock lock = redissonClient.getFairLock(realKey);
        if (null != lock && lock.isLocked()) {
            lock.unlock();
            return 0;
        }
        return -1;
    }

    public static boolean isLocked(String key) {
        String realKey = LOCK_PREFIX + key;
        RLock lock = redissonClient.getFairLock(realKey);
        return lock.isLocked();
    }

    public static <K, V> boolean setIfAbsent(K key, V value) {
        if (null == key) {
            return false;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        return bucket.setIfAbsent(value);
    }

    public static <K, V> boolean setIfAbsent(K key, V value, long expire) {
        if (null == key) {
            return false;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        return bucket.setIfAbsent(value, Duration.of(expire, ChronoUnit.MILLIS));
    }

    /**
     * 设置过期时间
     *
     * @param key    目标key
     * @param expire 过期时间毫秒数
     * @param <K>    key类型
     * @param <V>    value类型
     * @return 是否设置成功
     * @since 1.0.18.19
     */
    public static <K, V> boolean expire(K key, long expire) {
        if (null == key) {
            return false;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        return bucket.expire(Duration.of(expire, ChronoUnit.MILLIS));
    }

    /**
     * 查询过期时间
     *
     * @param key 目标key
     * @param <K> key类型
     * @param <V> value类型
     * @return 过期时间毫秒数
     * @since 1.0.18.19
     */
    public static <K, V> long remainTimeToLive(K key) {
        if (null == key) {
            return -1;
        }
        RBucket<V> bucket = redissonClient.getBucket(key.toString());
        return bucket.remainTimeToLive();
    }

}