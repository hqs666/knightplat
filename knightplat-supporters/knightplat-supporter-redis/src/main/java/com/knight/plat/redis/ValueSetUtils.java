/*
 * Copyright (c) 2016. Runyi Co., Ltd. All rights reserved.
 */

package com.knight.plat.redis;

import com.knight.plat.api.commons.CodeValue;
import com.knight.plat.api.utils.ValueSetManager;

import java.util.Map;

/**
 * Created by GHuang on 2016/11/17. Utils help to get and handle value set.
 */
public class ValueSetUtils {

    private static ValueSetManager valueSetManager;

    private ValueSetUtils() {
    }

    static void init(ValueSetManager valueSetManager) {
        ValueSetUtils.valueSetManager = valueSetManager;
        // valueSetManager.load();
    }

    static boolean isInited() {
        return valueSetManager != null;
    }

    private static ValueSetManager getValueSetManager() {
        if (null == valueSetManager) {
            throw new IllegalStateException("ValueSetUtils is not bean initiated.");
        }
        return valueSetManager;
    }

    public static void refresh() {
        getValueSetManager().load();
    }

    public static CodeValue getValue(String setGroup, String code) {
        return getValueSetManager().getValue(setGroup, code);
    }

    public static CodeValue getValueById(String id) {
        return getValueSetManager().getValueById(id);
    }

    public static Map<String, CodeValue> getValuesByGroup(String setGroup) {
        return getValueSetManager().getValuesByGroup(setGroup);
    }

    public static Map<String, CodeValue> getValuesByParentId(String parentId) {
        return getValueSetManager().getValuesByParentId(parentId);
    }

}
