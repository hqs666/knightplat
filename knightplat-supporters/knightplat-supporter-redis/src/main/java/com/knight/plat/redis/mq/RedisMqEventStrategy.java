package com.knight.plat.redis.mq;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.eventbus.Actions;
import com.knight.plat.api.eventbus.ConsumerHandler;
import com.knight.plat.api.eventbus.ErrorHandler;
import com.knight.plat.api.eventbus.EventStrategy;
import java.util.Set;
import org.redisson.api.RFuture;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedisMqEventStrategy implements EventStrategy {

    private final RedissonClient redisson;

    private static final Logger log = LoggerFactory.getLogger("knightplat-eventbus-redis");

    private final Set<Actions> actions = Sets.newEnumSet(Lists.newArrayList(Actions.PUBLISH, Actions.CONSUME), Actions.class);

    private final Set<String> listenerTopics = Sets.newHashSet();

    public RedisMqEventStrategy(RedissonClient redisson) {
        this.redisson = redisson;
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumer(router, clazz, handler, e -> log.error("Got message from redis topic: " + router + ", handling exception with error.", e));
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumerSync(router, clazz, handler, e -> log.error("Got message from redis topic: " + router + ", handling exception with error.", e));
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        RTopic topic = redisson.getTopic(router);
        synchronized (listenerTopics) {
            topic.addListener(clazz, (channel, message) -> {
                try {
                    handler.handle(message);
                } catch (Throwable e) {
                    errorHandler.handle(e);
                }
            });
            listenerTopics.add(router);
        }
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        // TODO:区分同步异步逻辑
        consumer(router, clazz, handler, errorHandler);
    }

    @Override
    public <T> void send(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        throw new RuntimeException("Redis channel send logic not implemented, please use publish() method");
    }

    @Override
    public <T> void publish(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        RTopic topic = redisson.getTopic(router);
        RFuture<Long> publishAsync = topic.publishAsync(payload);
        publishAsync.whenComplete((r, e) -> {
            if (e != null) {
                result.onException(e);
            } else {
                result.onSuccess(payload);
            }
        });
    }

    @Override
    public Set<Actions> supportedActions() {
        return actions;
    }

    @Override
    public void close() {
        synchronized (listenerTopics) {
            listenerTopics.forEach(topic -> redisson.getTopic(topic).removeAllListeners());
        }
    }
}
