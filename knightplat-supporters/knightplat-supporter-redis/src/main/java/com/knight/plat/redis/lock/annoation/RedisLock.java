package com.knight.plat.redis.lock.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RedisLock {

    boolean isFixedKey() default false;

    String synKey() default "";

    long keepMills() default 20000L;

    boolean toWait() default true;

    long sleepMills() default 10L;

    long maxSleepMills() default 10000L;

}
