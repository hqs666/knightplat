/*
 * Copyright (c) 2016. Knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.redis;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.redis.core.ValueOperations;

/**
 * Created on 16/3/8.
 */
@ConfigurationProperties(prefix = "shiro")
public class RedisSessionDAO extends AbstractSessionDAO {

    private static final Logger logger = LoggerFactory.getLogger(RedisSessionDAO.class);

    private ValueOperations<Serializable, Object> redisManager;

    private int globalSessionTimeout = 1800000;

    private String keyPrefix = "shiro_session:";

    @Override
    public void update(Session session) throws UnknownSessionException {
        this.saveSession(session);
    }

    private void saveSession(Session session) throws UnknownSessionException {
        if (session == null || session.getId() == null) {
            logger.error("session or session id is null");
            return;
        }

        Serializable key = session.getId();
        session.setTimeout(globalSessionTimeout);
        this.redisManager.set(key, session, globalSessionTimeout, TimeUnit.MILLISECONDS);
    }

    @Override
    public void delete(Session session) {
        if (session == null || session.getId() == null) {
            logger.error("session or session id deleted is null");
            return;
        }
        redisManager.getOperations().delete(session.getId());

    }

    @Override
    public Collection<Session> getActiveSessions() {
        Set<Session> sessions = new HashSet<Session>();

        Set<Serializable> keys = redisManager.getOperations().keys(this.keyPrefix + "*");
        if (keys != null && !keys.isEmpty()) {
            for (Serializable key : keys) {
                Session s = (Session) redisManager.get(key);
                sessions.add(s);
            }
        }

        return sessions;
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        this.saveSession(session);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        if (sessionId == null) {
            logger.error("session id is null.");
            return null;
        }

        return (Session) redisManager.get(sessionId);
    }

    public ValueOperations<Serializable, Object> getRedisManager() {
        return redisManager;
    }

    public void setRedisManager(ValueOperations<Serializable, Object> redisManager) {
        this.redisManager = redisManager;
    }

    /**
     * Returns the Redis session keys prefix.
     *
     * @return The prefix
     */
    public String getKeyPrefix() {
        return keyPrefix;
    }

    /**
     * Sets the Redis sessions key prefix.
     *
     * @param keyPrefix The prefix
     */
    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    public int getGlobalSessionTimeout() {
        return globalSessionTimeout;
    }

    public void setGlobalSessionTimeout(int globalSessionTimeout) {
        this.globalSessionTimeout = globalSessionTimeout;
    }

}
