/*
 * Copyright (c) 2016. Knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.redis;

import com.knight.plat.api.annotations.AutoConfigImportFilter;
import com.knight.plat.api.utils.SequenceManager;
import com.knight.plat.redis.lock.RedisLockAspect;
import com.knight.plat.timer.delayed.TimerAutoConfiguration;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.Codec;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.spring.starter.RedissonAutoConfigurationCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.io.Serializable;

/**
 * Created on 16/7/22.
 */
@AutoConfiguration(
        value = "RedisAutoConfiguration",
        after = {
                org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration.class,
                org.redisson.spring.starter.RedissonAutoConfiguration.class
        },
        beforeName = "com.knight.plat.autoconfiguration.UtilsAutoConfiguration.SequenceConfig"
)
@AutoConfigImportFilter
public class RedisAutoConfiguration {

    @Autowired
    public RedisAutoConfiguration(RedissonClient redissonClient) {
        RedisUtils.initRedisson(redissonClient);
    }

    @Bean
    public RedisTemplate<Serializable, Object> serializableRedisTemplate(
            @SuppressWarnings("SpringJavaAutowiringInspection") RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Serializable, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }

    @Bean
    @ConditionalOnClass(StringRedisTemplate.class)
    public RedisLockAspect redisLockAspect(StringRedisTemplate stringRedisTemplate) {
        return new RedisLockAspect(stringRedisTemplate);
    }

    @Configuration(proxyBeanMethods = false)
    @AutoConfigureBefore(name = {"com.knight.plat.shiro.ShiroAutoConfiguration"})
    @ConditionalOnClass({SessionDAO.class, CacheManager.class})
    static class RedisShiroConfiguration {

        @Bean
        @ConditionalOnMissingBean
        public SessionDAO sessionDAO(RedisTemplate<Serializable, Object> redisTemplate) {
            RedisSessionDAO sessionDAO = new RedisSessionDAO();
            sessionDAO.setRedisManager(redisTemplate.opsForValue());
            return sessionDAO;
        }

        @Bean
        @ConditionalOnMissingBean
        public CacheManager cacheManager(RedissonClient redissonClient) {
            RedisCacheManager cacheManager = new RedisCacheManager();
            cacheManager.setRedissonClient(redissonClient);
            return cacheManager;
        }

    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnClass(SequenceManager.class)
    @AutoConfigureBefore(name = "com.knight.plat.autoconfiguration.UtilsAutoConfiguration.SequenceConfig")
    static class RedisSequenceConfig {

        @Bean
        @ConditionalOnProperty(prefix = "knightplat.sequence", name = "provider", havingValue = "redis")
        SequenceManager redisSequenceManager() {
            return new RedisSequenceManager();
        }

    }

    @Configuration
    @ConditionalOnClass(RedissonClient.class)
    static class RedissonConfig {

        private static final Logger logger = LoggerFactory.getLogger(RedissonConfig.class);

        @Bean
        public Codec jsonCodec() {
            try {
                Class.forName("com.knight.plat.commons.JsonCustomCodec", false, this.getClass().getClassLoader());
                return new JsonJacksonCodec(com.knight.plat.commons.JsonCustomCodec.mapper());
            } catch (Exception e) {
                logger.warn("knightplat-core not found.", e);
                return new JsonJacksonCodec();
            }
        }

        @Bean
        public RedissonAutoConfigurationCustomizer redissonCodecConfig(Codec codec) {
            return configuration -> configuration.setCodec(codec);
        }

    }

}
