/*
 * Copyright (c) 2016. Knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.redis;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.CollectionUtils;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 16/3/8.
 */
public class RedisCache<K, V> implements Cache<K, V> {

    private static final Logger logger = LoggerFactory.getLogger(RedisCache.class);

    private final TimeUnit timeUnit = TimeUnit.MILLISECONDS;

    private final RMapCache<K, V> cache;

    private String keyPrefix = "shiro_session:";

    public RedisCache(RedissonClient redissonClient) {
        if (null == redissonClient) {
            throw new IllegalArgumentException("redissonClient argument cannot be null.");
        }
        this.cache = redissonClient.getMapCache("shiro_session");
    }

    public RedisCache(RedissonClient redissonClient, String prefix) {
        this(redissonClient);
        // set the prefix
        this.keyPrefix = prefix;
    }

    public String getKeyPrefix() {
        return keyPrefix;
    }

    public void setKeyPrefix(String keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    @Override
    public V get(K key) throws CacheException {
        if (logger.isDebugEnabled()) {
            logger.debug("Get object from redis server: key [" + key + "]");
        }
        try {
            if (key == null) {
                return null;
            } else {
                return cache.get(key);
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }

    }

    @Override
    public V put(K key, V value) throws CacheException {
        if (logger.isDebugEnabled()) {
            logger.debug("Put object to redis: key [" + key + "]");
        }
        try {
            cache.put(key, value);
            return value;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    public V put(K key, V value, long expire) throws CacheException {
        if (logger.isDebugEnabled()) {
            logger.debug("Put object to redis: key [" + key + "] with expire: " + expire + "millisecond.");
        }
        try {
            cache.put(key, value, expire, timeUnit);
            return value;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public V remove(K key) throws CacheException {
        if (logger.isDebugEnabled()) {
            logger.debug("Remove from redis key [" + key + "]");
        }
        try {
            return cache.remove(key);
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public void clear() throws CacheException {
        if (logger.isDebugEnabled()) {
            logger.debug("Remove all object from redis");
        }
        try {
            cache.clear();
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public int size() {
        try {
            return cache.size();
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public Set<K> keys() {
        try {
            return cache.keySet();
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public Collection<V> values() {
        try {
            Set<K> keys = keys();
            if (!CollectionUtils.isEmpty(keys)) {
                return cache.values();
            } else {
                return Collections.emptySet();
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

}