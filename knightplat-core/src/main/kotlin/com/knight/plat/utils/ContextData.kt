@file:JvmName("ContextData")

package com.knight.plat.utils

import com.knight.plat.api.commons.Constants
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.context.request.RequestContextHolder
import javax.servlet.http.HttpServletRequest

internal fun getHeader(name: String): Any? {
    return try {
        (RequestContextHolder
            .currentRequestAttributes()
            .getAttribute(Constants.RY_REQUEST_KEY, RequestAttributes.SCOPE_REQUEST) as? HttpServletRequest)
            ?.getHeader(name)
    } catch (e: IllegalStateException) {
        null
    }
}