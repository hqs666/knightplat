/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */
@file:JvmName("Json")

package com.knight.plat.utils

import com.fasterxml.jackson.core.type.TypeReference
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

private fun <T> makeListType(clazz: Class<T>): TypeReference<List<T>> {
    return object : TypeReference<List<T>>() {
        override fun getType(): Type {
            return object : ParameterizedType {
                override fun getActualTypeArguments(): Array<Type> = arrayOf(clazz)
                override fun getRawType(): Type = List::class.java
                override fun getOwnerType(): Type? = null
            }
        }
    }
}

internal fun <T> encode(src: T) = jsonSerialize(src)

internal fun <T> encodeToBytes(src: T) = jsonSerializeToBytes(src)

internal fun <T> decode(src: String, clazz: Class<T>) = jsonDeserialize(src, clazz)

internal fun <T> decode(src: String, type: TypeReference<T>) = jsonDeserialize(src, type)

internal fun <T> decodeToList(src: String, clazz: Class<T>) : List<T?>? = jsonDeserialize(src, makeListType(clazz))

internal fun <T> decode(src: ByteArray, clazz: Class<T>) = jsonDeserializeFromBytes(src, clazz)

internal fun <T> decode(src: ByteArray, type: TypeReference<T>) = jsonDeserializeFromBytes(src, type)

internal fun <T> decodeToList(src: ByteArray, clazz: Class<T>) : List<T?>? = jsonDeserializeFromBytes(src, makeListType(clazz))