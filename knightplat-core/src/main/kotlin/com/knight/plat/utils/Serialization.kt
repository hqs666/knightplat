/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

/**
 * Created on 2016/10/25. Serialization utils relay on kryo(for bytes) or jackson(for json).
 */
@file:JvmName("Serialization")

package com.knight.plat.utils

import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.Input
import com.esotericsoftware.kryo.io.Output
import com.esotericsoftware.kryo.util.Pool
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.knight.plat.autoconfiguration.GuavaRandomAccessListSerializer
import com.knight.plat.autoconfiguration.GuavaSequentialListSerializer
import com.knight.plat.autoconfiguration.MapInterfaceSerializer
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException

private val log = LoggerFactory.getLogger("knightplat - SerializationUtils")

private var kryoPool: Pool<Kryo>? = null
private var objectMapper: ObjectMapper? = null

internal fun createKryo(): Kryo {
    val kryo = Kryo()
    try {
        kryo.register(
            MutableList::class.java, GuavaRandomAccessListSerializer()
        )
        kryo.register(
            Class.forName("com.google.common.collect.Lists\$TransformingRandomAccessList"),
            GuavaRandomAccessListSerializer()
        )
        kryo.register(
            Class.forName("com.google.common.collect.Lists\$TransformingSequentialList"),
            GuavaSequentialListSerializer()
        )
        kryo.register(
            MutableMap::class.java, MapInterfaceSerializer()
        )
    } catch (e: ClassNotFoundException) {
        log.error("Cannot find guava lib.", e)
    }
    return kryo
}

internal fun getKryoPool(): Pool<Kryo> {
    if (kryoPool == null) {
        kryoPool = object : Pool<Kryo>(true, true, 16) {
            override fun create(): Kryo = createKryo()
        }
    }
    return kryoPool as Pool<Kryo>
}

internal fun getObjectMapper(): ObjectMapper {
    if (objectMapper == null) {
        objectMapper = SpringContextUtils.getBean(ObjectMapper::class.java)
    }
    return objectMapper ?: ObjectMapper()
}

internal fun <T> serialize(src: T): ByteArray {
    return serialize(src, false)
}

internal fun <T> serializeWithClass(src: T): ByteArray {
    return serialize(src, true)
}

// core method

@Suppress("UNCHECKED_CAST")
private fun <T> deserialize(src: ByteArray, clazz: Class<T>, withClass: Boolean): T {
    if (clazz == ByteArray::class.java) {
        return src as T
    }
    if (clazz == String::class.java) {
        return String(src) as T
    }
    val pool = getKryoPool()
    val kryo = pool.obtain()
    kryo.isRegistrationRequired = false
    val input = Input(ByteArrayInputStream(src))
    val result: T = if (withClass) {
        val obj = kryo.readClassAndObject(input)
        clazz.cast(obj)
    } else {
        kryo.readObject(input, clazz)
    }
    input.close()
    pool.free(kryo)
    return result
}

private fun <T> serialize(src: T, withClass: Boolean): ByteArray {
    if (src is ByteArray) {
        return src
    }
    if (src is String) {
        return src.toByteArray()
    }
    val pool = getKryoPool()
    val kryo = pool.obtain()
    kryo.isRegistrationRequired = false
    val output = Output(ByteArrayOutputStream(1024))
    if (withClass) {
        kryo.writeClassAndObject(output, src)
    } else {
        kryo.writeObject(output, src)
    }
    val result = output.toBytes()
    output.close()
    pool.free(kryo)
    return result
}

internal fun <T> deserialize(src: ByteArray, clazz: Class<T>): T {
    return deserialize(src, clazz, false)
}

internal fun deserializeWithClass(src: ByteArray): Any {
    return deserialize(src, Any::class.java, true)
}

internal fun <T> jsonSerialize(src: T): String {
    return try {
        getObjectMapper().writeValueAsString(src)
    } catch (e: JsonProcessingException) {
        if (log.isWarnEnabled) {
            log.warn("jsonSerialize fail because of a wrong format", e)
        }
        ""
    }
}

internal fun <T> jsonSerializeToBytes(src: T): ByteArray {
    return try {
        getObjectMapper().writeValueAsBytes(src)
    } catch (e: JsonProcessingException) {
        if (log.isWarnEnabled) {
            log.warn("jsonSerialize fail because of a wrong format", e)
        }
        ByteArray(0)
    }

}

internal fun <T> jsonDeserialize(src: String, clazz: Class<T>): T? {
    return try {
        getObjectMapper().readValue(src, clazz)
    } catch (e: IOException) {
        if (log.isWarnEnabled) {
            log.warn("jsonDeSerialize fail because of a wrong format", e)
        }
        null
    }
}

internal fun <T> jsonDeserialize(src: String, type: TypeReference<T>): T? {
    return try {
        getObjectMapper().readValue(src, type)
    } catch (e: IOException) {
        if (log.isWarnEnabled) {
            log.warn("jsonDeSerialize fail because of a wrong format", e)
        }
        null
    }
}

internal fun <T> jsonDeserializeFromBytes(src: ByteArray, clazz: Class<T>): T? {
    return try {
        getObjectMapper().readValue(src, clazz)
    } catch (e: IOException) {
        if (log.isWarnEnabled) {
            log.warn("jsonDeSerialize fail because of a wrong format", e)
        }
        null
    }
}

internal fun <T> jsonDeserializeFromBytes(src: ByteArray, type: TypeReference<T>): T? {
    return try {
        getObjectMapper().readValue(src, type)
    } catch (e: IOException) {
        if (log.isWarnEnabled) {
            log.warn("jsonDeSerialize fail because of a wrong format", e)
        }
        null
    }
}
