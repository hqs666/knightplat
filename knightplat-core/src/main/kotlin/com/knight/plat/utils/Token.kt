@file:JvmName("Token")
package com.knight.plat.utils

import com.knight.plat.token.TokenManager
import io.jsonwebtoken.Claims
import javax.crypto.SecretKey

private val tokenManager = TokenManager()

internal fun create(tokenId: String?, userId: String?, data: Map<String?, Any?>?): String? {
    return tokenManager.create(tokenId, userId, data)
}

internal fun create(tokenId: String?, userId: String?, data: Map<String?, Any?>?, key: SecretKey?): String? {
    return tokenManager.create(tokenId, userId, data, key)
}

internal fun create(tokenId: String?, userId: String?, data: Map<String?, Any?>?, expMillis: Long): String? {
    return tokenManager.create(tokenId, userId, data, expMillis)
}

internal fun create(tokenId: String?, userId: String?, data: Map<String?, Any?>?, key: SecretKey?, expMillis: Long): String? {
    return tokenManager.create(tokenId, userId, data, key, expMillis)
}

internal fun checkAndRead(token: String?): Claims? {
    return tokenManager.checkAndRead(token)
}

internal fun checkAndRead(token: String?, key: SecretKey?): Claims? {
    return tokenManager.checkAndRead(token, key)
}

internal fun readWithoutCheck(token: String?): Claims? {
    return tokenManager.readWithoutCheck(token)
}