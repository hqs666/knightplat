@file:JvmName("HttpClient")

package com.knight.plat.utils

import com.google.common.base.Charsets
import com.knight.plat.autoconfiguration.HttpClientProperties
import com.knight.plat.autoconfiguration.HttpProxyProperties
import okhttp3.*
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URLEncoder
import java.util.concurrent.TimeUnit

private const val DEFAULT_TIMEOUT_SECONDS = 5 * 1000L
private const val DEFAULT_POOL_SIZE = 50
private const val DEFAULT_ALIVE_TIME = 60 * 1000L

private var client: OkHttpClient? = null
private var proxyClient: OkHttpClient? = null

fun getProperties(): HttpClientProperties {
    val proxyProperties = SpringContextUtils.getBean(HttpProxyProperties::class.java)
    var clientProperties = SpringContextUtils.getBean(HttpClientProperties::class.java)
    if (clientProperties == null) {
        clientProperties = HttpClientProperties()
    }
    if (proxyProperties != null) {
        if (proxyProperties.proxyServer != null && proxyProperties.proxyServer.isNotEmpty()) {
            clientProperties.proxyServer = proxyProperties.proxyServer
        }
        if (proxyProperties.port != null) {
            clientProperties.proxyPort = proxyProperties.port
        }
    }
    if (clientProperties.readTimeout == null) {
        clientProperties.readTimeout = DEFAULT_TIMEOUT_SECONDS
    }
    if (clientProperties.connectTimeout == null) {
        clientProperties.connectTimeout = DEFAULT_TIMEOUT_SECONDS
    }
    if (clientProperties.writeTimeout == null) {
        clientProperties.writeTimeout = DEFAULT_TIMEOUT_SECONDS
    }
    if (clientProperties.poolSize == null) {
        clientProperties.poolSize = DEFAULT_POOL_SIZE
    }
    if (clientProperties.keepAliveDuration == null) {
        clientProperties.keepAliveDuration = DEFAULT_ALIVE_TIME
    }
    return clientProperties
}

private fun getClient(): OkHttpClient = if (client != null) {
    client!!
} else {
    val properties = getProperties()
    client = OkHttpClient.Builder()
        .connectTimeout(properties.connectTimeout, TimeUnit.MILLISECONDS)
        .readTimeout(properties.readTimeout, TimeUnit.MILLISECONDS)
        .connectionPool(ConnectionPool(properties.poolSize, properties.keepAliveDuration, TimeUnit.MILLISECONDS))
        .build()
    client!!
}

private fun getProxyClient(): OkHttpClient = if (proxyClient != null) {
    proxyClient!!
} else {
    val properties = getProperties()
    if (properties.proxyServer != null) {
        proxyClient = OkHttpClient.Builder()
            .connectTimeout(properties.connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(properties.readTimeout, TimeUnit.MILLISECONDS)
            .connectionPool(ConnectionPool(properties.poolSize, properties.keepAliveDuration, TimeUnit.MILLISECONDS))
            .proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress(properties.proxyServer, properties.proxyPort ?: 80)))
            .build()
        proxyClient!!
    } else {
        getClient()
    }
}

private fun urlEncode(original: String): String {
    return try {
        URLEncoder.encode(original, Charsets.UTF_8.name())
    } catch (e: UnsupportedEncodingException) {
        throw Error("Your environment must support utf-8 encode!", e)
    }
}

internal fun doGet(urlPath: String, params: Map<String, Any>, headers: Map<String, String?>?): Call {
    return getClient().newCall(getRequestBuild(urlPath, params, headers))
}

internal fun doGetRes(urlPath: String, params: Map<String, Any>, headers: Map<String, String?>?): Call {
    return getClient().newCall(getRequestBuild(urlPath, params, headers))
}

internal fun doGetProxy(urlPath: String, params: Map<String, Any>, headers: Map<String, String?>?): Call {
    return getProxyClient().newCall(getRequestBuild(urlPath, params, headers))
}

internal fun doGetProxyRes(urlPath: String, params: Map<String, Any>, headers: Map<String, String?>?): Call {
    return getProxyClient().newCall(getRequestBuild(urlPath, params, headers))
}

internal fun doPost(urlPath: String, body: String, contentType: MediaType?, headers: Map<String, String?>?): Call {
    return getClient().newCall(postRequestBuild(urlPath, body, contentType, headers))
}

internal fun doPostRes(urlPath: String, body: String, contentType: MediaType?, headers: Map<String, String?>?): Call {
    return getClient().newCall(postRequestBuild(urlPath, body, contentType, headers))
}

internal fun doPostProxy(urlPath: String, body: String, contentType: MediaType?, headers: Map<String, String?>?): Call {
    return getProxyClient().newCall(postRequestBuild(urlPath, body, contentType, headers))
}

internal fun doPostProxyRes(urlPath: String, body: String, contentType: MediaType?, headers: Map<String, String?>?): Call {
    return getProxyClient().newCall(postRequestBuild(urlPath, body, contentType, headers))
}

@Throws(IOException::class)
internal fun response(response: Response): String? {
    response.use {
        return if (it.isSuccessful) {
            it.body.use { b ->
                b?.string()
            }
        } else {
            throw IOException("Request fail with code: ${it.code}, message: ${it.message}")
        }
    }
}

private fun getRequestBuild(urlPath: String, params: Map<String, Any>, headers: Map<String, String?>?): Request {
    val queryStr = StringBuilder()
    if (params.isNotEmpty()) {
        queryStr.append('?')
        params.forEach { (k, v) ->
            val encodedValue = urlEncode(v.toString())
            queryStr.append(k).append('=').append(encodedValue).append('&')
        }
        queryStr.deleteCharAt(queryStr.length - 1)
    }
    val requestBuilder = Request.Builder()
        .url(urlPath + queryStr.toString())
    if (!headers.isNullOrEmpty()) {
        headers.forEach { requestBuilder.addHeader(it.key, it.value ?: "") }
    }
    return requestBuilder.get().build()
}

private fun postRequestBuild(
    urlPath: String,
    body: String,
    contentType: MediaType?,
    headers: Map<String, String?>?
): Request {
    val requestBuilder = Request.Builder()
        .url(urlPath)
    if (!headers.isNullOrEmpty()) {
        headers.forEach { requestBuilder.addHeader(it.key, it.value ?: "") }
    }
    return requestBuilder
        .post(body.toRequestBody(contentType)) //post请求
        .build()
}