package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static com.knight.plat.utils.helper.HttpClientHelper.bodyToJson;
import static com.knight.plat.utils.helper.HttpClientHelper.getServiceUrl;

@Utils("asyncHttpClient")
public class AsyncHttpClientUtils {

    private static final MediaType JSON_MEDIA_TYPE = MediaType.get("application/json");

    // 以下为get请求的各种重载方法

    public static Mono<String> doGet(String urlPath, Map<String, ?> params, Map<String, String> headers) {
        Call call = HttpClient.doGet(urlPath, params, headers);
        return makeStringMono(call);
    }

    public static Mono<String> doGet(String urlPath, Map<String, String> headers) {
        return doGet(urlPath, Collections.emptyMap(), headers);
    }

    public static Mono<String> doGet(String urlPath) {
        return doGet(urlPath, Collections.emptyMap(), Collections.emptyMap());
    }

    public static Mono<okhttp3.Response> doGetRes(String urlPath, Map<String, ?> params, Map<String, String> headers) {
        Call call = HttpClient.doGetRes(urlPath, params, headers);
        return makeResponseMono(call);
    }

    public static Mono<okhttp3.Response> doGetRes(String urlPath, Map<String, String> headers) {
        return doGetRes(urlPath, Collections.emptyMap(), headers);
    }

    public static Mono<okhttp3.Response> doGetRes(String urlPath) {
        return doGetRes(urlPath, Collections.emptyMap(), Collections.emptyMap());
    }

    public static Mono<String> doGetProxy(String urlPath, Map<String, ?> params, Map<String, String> headers) {
        Call call = HttpClient.doGetProxy(urlPath, params, headers);
        return makeStringMono(call);
    }

    public static Mono<String> doGetProxy(String urlPath, Map<String, String> headers) {
        return doGetProxy(urlPath, Collections.emptyMap(), headers);
    }

    public static Mono<String> doGetProxy(String urlPath) {
        return doGetProxy(urlPath, Collections.emptyMap(), Collections.emptyMap());
    }

    public static Mono<okhttp3.Response> doGetProxyRes(String urlPath, Map<String, ?> params, Map<String, String> headers) {
        Call call = HttpClient.doGetProxyRes(urlPath, params, headers);
        return makeResponseMono(call);
    }

    public static Mono<okhttp3.Response> doGetProxyRes(String urlPath, Map<String, String> headers) {
        Call call = HttpClient.doGetProxyRes(urlPath, Collections.emptyMap(), headers);
        return makeResponseMono(call);
    }

    public static Mono<okhttp3.Response> doGetProxyRes(String urlPath) {
        Call call =  HttpClient.doGetProxyRes(urlPath, Collections.emptyMap(), Collections.emptyMap());
        return makeResponseMono(call);
    }

    public static Mono<String> doServiceGet(String serviceId, String path, Map<String, ?> params, Map<String, String> headers) {
        return doGet(getServiceUrl(serviceId, path), params, headers);
    }

    public static Mono<String> doServiceGet(String serviceId, String path, Map<String, String> headers) {
        return doGet(getServiceUrl(serviceId, path), Collections.emptyMap(), headers);
    }

    public static Mono<String> doServiceGet(String serviceId, String path) {
        return doGet(getServiceUrl(serviceId, path), Collections.emptyMap(), Collections.emptyMap());
    }

    public static Mono<okhttp3.Response> doServiceGetRes(String serviceId, String path, Map<String, ?> params, Map<String, String> headers) {
        return doGetRes(getServiceUrl(serviceId, path), params, headers);
    }

    public static Mono<okhttp3.Response> doServiceGetRes(String serviceId, String path, Map<String, String> headers) {
        return doServiceGetRes(serviceId, path, Collections.emptyMap(), headers);
    }

    public static Mono<okhttp3.Response> doServiceGetRes(String serviceId, String path) {
        return doServiceGetRes(serviceId, path, Collections.emptyMap(), Collections.emptyMap());
    }


    // 以下为post请求的各种重载方法

    public static Mono<String> doPost(String urlPath, String body, MediaType contentType, Map<String, String> headers) {
        Call call = HttpClient.doPost(urlPath, body, contentType, headers);
        return makeStringMono(call);
    }

    public static Mono<String> doPost(String urlPath, String body, MediaType contentType) {
        return doPost(urlPath, body, contentType, Collections.emptyMap());
    }

    public static Mono<okhttp3.Response> doPostRes(String urlPath, String body, MediaType contentType, Map<String, String> headers) {
        Call call =  HttpClient.doPostRes(urlPath, body, contentType, headers);
        return makeResponseMono(call);
    }

    public static Mono<okhttp3.Response> doPostRes(String urlPath, String body, MediaType contentType) {
        return doPostRes(urlPath, body, contentType, Collections.emptyMap());
    }

    public static Mono<String> doPostProxy(String urlPath, String body, MediaType contentType, Map<String, String> headers) {
        Call call = HttpClient.doPostProxy(urlPath, body, contentType, headers);
        return makeStringMono(call);
    }

    public static Mono<String> doPostProxy(String urlPath, String body, MediaType contentType) {
        return doPostProxy(urlPath, body, contentType, Collections.emptyMap());
    }

    public static Mono<String> doServicePost(String serviceId, String path, String body, MediaType contentType, Map<String, String> headers) {
        return doPost(getServiceUrl(serviceId, path), body, contentType, headers);
    }

    public static Mono<String> doServicePost(String serviceId, String path, String body, MediaType contentType) {
        return doServicePost(serviceId, path, body, contentType, Collections.emptyMap());
    }

    public static Mono<okhttp3.Response> doPostProxyRes(String urlPath, String body, MediaType contentType, Map<String, String> headers) {
        Call call = HttpClient.doPostProxyRes(urlPath, body, contentType, headers);
        return makeResponseMono(call);
    }

    public static Mono<okhttp3.Response> doPostProxyRes(String urlPath, String body, MediaType contentType) {
        return doPostProxyRes(urlPath, body, contentType, Collections.emptyMap());
    }

    public static Mono<okhttp3.Response> doServicePostRes(String serviceId, String path, String body, MediaType contentType, Map<String, String> headers) {
        return doPostProxyRes(getServiceUrl(serviceId, path), body, contentType, headers);
    }

    public static Mono<okhttp3.Response> doServicePostRes(String serviceId, String path, String body, MediaType contentType) {
        return doServicePostRes(serviceId, path, body, contentType, Collections.emptyMap());
    }


    // 以下为参数为json格式的post请求的各种重载方法

    public static <T> Mono<String> doPostJson(String urlPath, T body, Map<String, String> headers) {
        return doPost(urlPath, bodyToJson(body), JSON_MEDIA_TYPE, headers);
    }

    public static <T> Mono<String> doPostJson(String urlPath, T body) {
        return doPostJson(urlPath, body, Collections.emptyMap());
    }

    public static <T> Mono<String> doServicePostJson(String serviceId, String path, T body, Map<String, String> headers) {
        return doPostJson(getServiceUrl(serviceId, path), body, headers);
    }

    public static <T> Mono<String> doServicePostJson(String serviceId, String path, T body) {
        return doServicePostJson(serviceId, path, body, Collections.emptyMap());
    }

    public static <T> Mono<okhttp3.Response> doPostJsonRes(String urlPath, T body, Map<String, String> headers) {
        return doPostRes(urlPath, bodyToJson(body), JSON_MEDIA_TYPE, headers);
    }

    public static <T> Mono<okhttp3.Response> doPostJsonRes(String urlPath, T body) {
        return doPostJsonRes(urlPath, body, Collections.emptyMap());
    }

    public static <T> Mono<okhttp3.Response> doServicePostJsonRes(String serviceId, String path, T body, Map<String, String> headers) {
        return doPostJsonRes(getServiceUrl(serviceId, path), body, headers);
    }

    public static <T> Mono<okhttp3.Response> doServicePostJsonRes(String serviceId, String path, T body) {
        return doPostJsonRes(getServiceUrl(serviceId, path), body, Collections.emptyMap());
    }

    private static Mono<okhttp3.Response> makeResponseMono(Call call) {
        return Mono.create(sink -> call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                sink.error(e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                sink.success(response);
            }
        }));
    }

    private static Mono<String> makeStringMono(Call call) {
        return Mono.create(sink -> call.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                sink.error(e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                try {
                    String resStr = HttpClient.response(response);
                    sink.success(resStr);
                } catch (IOException e) {
                    sink.error(e);
                }
            }
        }));
    }
}
