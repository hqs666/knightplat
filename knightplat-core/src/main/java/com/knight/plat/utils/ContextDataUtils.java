package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;

@Utils("context")
public final class ContextDataUtils {
    public static Object getHeader(String name) {
        return ContextData.getHeader(name);
    }
}
