package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import okhttp3.Call;
import okhttp3.MediaType;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static com.knight.plat.utils.helper.HttpClientHelper.bodyToJson;
import static com.knight.plat.utils.helper.HttpClientHelper.getServiceUrl;

@Utils("httpClient")
public final class HttpClientUtils {

    private static final MediaType JSON_MEDIA_TYPE = MediaType.get("application/json");

    // 以下为get请求的各种重载方法

    @Utils.ThrowException({IOException.class})
    public static String doGet(String urlPath, Map<String, ?> params, Map<String, String> headers) throws IOException {
        Call call = HttpClient.doGet(urlPath, params, headers);
        return HttpClient.response(call.execute());
    }

    @Utils.ThrowException({IOException.class})
    public static String doGet(String urlPath, Map<String, String> headers) throws IOException {
        return doGet(urlPath, Collections.emptyMap(), headers);
    }

    @Utils.ThrowException({IOException.class})
    public static String doGet(String urlPath) throws IOException {
        return doGet(urlPath, Collections.emptyMap(), Collections.emptyMap());
    }

    @Utils.ThrowException({IOException.class})
    public static okhttp3.Response doGetRes(String urlPath, Map<String, ?> params, Map<String, String> headers) throws IOException {
        return HttpClient.doGetRes(urlPath, params, headers).execute();
    }

    @Utils.ThrowException({IOException.class})
    public static okhttp3.Response doGetRes(String urlPath, Map<String, String> headers) throws IOException {
        return doGetRes(urlPath, Collections.emptyMap(), headers);
    }

    @Utils.ThrowException({IOException.class})
    public static okhttp3.Response doGetRes(String urlPath) throws IOException {
        return doGetRes(urlPath, Collections.emptyMap(), Collections.emptyMap());
    }

    @Utils.ThrowException({IOException.class})
    public static String doGetProxy(String urlPath, Map<String, ?> params, Map<String, String> headers) throws IOException {
        Call call = HttpClient.doGetProxy(urlPath, params, headers);
        return HttpClient.response(call.execute());
    }

    @Utils.ThrowException({IOException.class})
    public static String doGetProxy(String urlPath, Map<String, String> headers) throws IOException {
        return doGetProxy(urlPath, Collections.emptyMap(), headers);
    }

    @Utils.ThrowException({IOException.class})
    public static String doGetProxy(String urlPath) throws IOException {
        return doGetProxy(urlPath, Collections.emptyMap(), Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doGetProxyRes(String urlPath, Map<String, ?> params, Map<String, String> headers) throws IOException {
        return HttpClient.doGetProxyRes(urlPath, params, headers).execute();
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doGetProxyRes(String urlPath, Map<String, String> headers) throws IOException {
        return HttpClient.doGetProxyRes(urlPath, Collections.emptyMap(), headers).execute();
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doGetProxyRes(String urlPath) throws IOException {
        return HttpClient.doGetProxyRes(urlPath, Collections.emptyMap(), Collections.emptyMap()).execute();
    }

    @Utils.ThrowException({IOException.class})
    public static String doServiceGet(String serviceId, String path, Map<String, ?> params, Map<String, String> headers) throws IOException {
        return doGet(getServiceUrl(serviceId, path), params, headers);
    }

    @Utils.ThrowException({IOException.class})
    public static String doServiceGet(String serviceId, String path, Map<String, String> headers) throws IOException {
        return doGet(getServiceUrl(serviceId, path), Collections.emptyMap(), headers);
    }

    @Utils.ThrowException({IOException.class})
    public static String doServiceGet(String serviceId, String path) throws IOException {
        return doGet(getServiceUrl(serviceId, path), Collections.emptyMap(), Collections.emptyMap());
    }

    @Utils.ThrowException({IOException.class})
    public static okhttp3.Response doServiceGetRes(String serviceId, String path, Map<String, ?> params, Map<String, String> headers) throws IOException {
        return doGetRes(getServiceUrl(serviceId, path), params, headers);
    }

    @Utils.ThrowException({IOException.class})
    public static okhttp3.Response doServiceGetRes(String serviceId, String path, Map<String, String> headers) throws IOException {
        return doServiceGetRes(serviceId, path, Collections.emptyMap(), headers);
    }

    @Utils.ThrowException({IOException.class})
    public static okhttp3.Response doServiceGetRes(String serviceId, String path) throws IOException {
        return doServiceGetRes(serviceId, path, Collections.emptyMap(), Collections.emptyMap());
    }


    // 以下为post请求的各种重载方法

    @Utils.ThrowException(IOException.class)
    public static String doPost(String urlPath, String body, MediaType contentType, Map<String, String> headers) throws IOException {
        Call call = HttpClient.doPost(urlPath, body, contentType, headers);
        return HttpClient.response(call.execute());
    }

    @Utils.ThrowException(IOException.class)
    public static String doPost(String urlPath, String body, MediaType contentType) throws IOException {
        return doPost(urlPath, body, contentType, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doPostRes(String urlPath, String body, MediaType contentType, Map<String, String> headers) throws IOException {
        return HttpClient.doPostRes(urlPath, body, contentType, headers).execute();
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doPostRes(String urlPath, String body, MediaType contentType) throws IOException {
        return doPostRes(urlPath, body, contentType, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static String doPostProxy(String urlPath, String body, MediaType contentType, Map<String, String> headers) throws IOException {
        Call call = HttpClient.doPostProxy(urlPath, body, contentType, headers);
        return HttpClient.response(call.execute());
    }

    @Utils.ThrowException(IOException.class)
    public static String doPostProxy(String urlPath, String body, MediaType contentType) throws IOException {
        return doPostProxy(urlPath, body, contentType, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static String doServicePost(String serviceId, String path, String body, MediaType contentType, Map<String, String> headers) throws IOException {
        return doPost(getServiceUrl(serviceId, path), body, contentType, headers);
    }

    @Utils.ThrowException(IOException.class)
    public static String doServicePost(String serviceId, String path, String body, MediaType contentType) throws IOException {
        return doServicePost(serviceId, path, body, contentType, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doPostProxyRes(String urlPath, String body, MediaType contentType, Map<String, String> headers) throws IOException {
        return HttpClient.doPostProxyRes(urlPath, body, contentType, headers).execute();
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doPostProxyRes(String urlPath, String body, MediaType contentType) throws IOException {
        return doPostProxyRes(urlPath, body, contentType, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doServicePostRes(String serviceId, String path, String body, MediaType contentType, Map<String, String> headers) throws IOException {
        return doPostProxyRes(getServiceUrl(serviceId, path), body, contentType, headers);
    }

    @Utils.ThrowException(IOException.class)
    public static okhttp3.Response doServicePostRes(String serviceId, String path, String body, MediaType contentType) throws IOException {
        return doServicePostRes(serviceId, path, body, contentType, Collections.emptyMap());
    }


    // 以下为参数为json格式的post请求的各种重载方法

    @Utils.ThrowException(IOException.class)
    public static <T> String doPostJson(String urlPath, T body, Map<String, String> headers) throws IOException {
        return doPost(urlPath, bodyToJson(body), JSON_MEDIA_TYPE, headers);
    }

    @Utils.ThrowException(IOException.class)
    public static <T> String doPostJson(String urlPath, T body) throws IOException {
        return doPostJson(urlPath, body, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static <T> String doServicePostJson(String serviceId, String path, T body, Map<String, String> headers) throws IOException {
        return doPostJson(getServiceUrl(serviceId, path), body, headers);
    }

    @Utils.ThrowException(IOException.class)
    public static <T> String doServicePostJson(String serviceId, String path, T body) throws IOException {
        return doServicePostJson(serviceId, path, body, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static <T> okhttp3.Response doPostJsonRes(String urlPath, T body, Map<String, String> headers) throws IOException {
        return doPostRes(urlPath, bodyToJson(body), JSON_MEDIA_TYPE, headers);
    }

    @Utils.ThrowException(IOException.class)
    public static <T> okhttp3.Response doPostJsonRes(String urlPath, T body) throws IOException {
        return doPostJsonRes(urlPath, body, Collections.emptyMap());
    }

    @Utils.ThrowException(IOException.class)
    public static <T> okhttp3.Response doServicePostJsonRes(String serviceId, String path, T body, Map<String, String> headers) throws IOException {
        return doPostJsonRes(getServiceUrl(serviceId, path), body, headers);
    }

    @Utils.ThrowException(IOException.class)
    public static <T> okhttp3.Response doServicePostJsonRes(String serviceId, String path, T body) throws IOException {
        return doPostJsonRes(getServiceUrl(serviceId, path), body, Collections.emptyMap());
    }
}
