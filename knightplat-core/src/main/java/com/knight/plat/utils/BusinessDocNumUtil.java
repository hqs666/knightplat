package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.keyword.docnum.BusinessDocManager;

import java.util.List;

/**
 * 项目名: obei-commons
 * 文件名: BusinessDocNumUtil
 * 模块说明:
 * 修改历史:
 * 2022/4/6 - Knight - 创建
 */
@Utils("businessDocFilter")
public class BusinessDocNumUtil {

    private static BusinessDocManager businessDocManager;

    BusinessDocNumUtil(){

    }
    private static BusinessDocManager getBusinessDocManager(){
        if (null == businessDocManager){
            BusinessDocManager manager = SpringContextUtils.getBean(BusinessDocManager.class);
            if (null == manager){
                throw new IllegalStateException("No <BusinessDocManager> instance found.");
            }
            businessDocManager =manager;
        }
        return businessDocManager;
    }

    public static boolean isFilter(String businessDocNum){
        if (null == businessDocManager){
            businessDocManager = getBusinessDocManager();
        }
        return businessDocManager.isFilter(businessDocNum);
    }

    public void reload(List<String> keywords){
        if (null == businessDocManager){
            businessDocManager = getBusinessDocManager();
        }
        businessDocManager.load(keywords);
    }


}
