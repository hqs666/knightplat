/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.utils.helper.bean2bean.BeanConverter;
import com.knight.plat.utils.helper.map2bean.MapConverter;
import io.vulcan.bean.BeanUtils;

import java.util.List;
import java.util.Map;

/**
 * Created on 16/8/10. Bean and Map convert utils.
 */
@Utils("bean")
public final class BeanConvertUtils {

    private BeanConvertUtils() {
        // prevent to create new instance
    }

    /**
     * Convert map to java bean instance, a new instance will be returned. <b>This method implements a glib proxy to
     * improve performance, but could influence serializing process.</b>
     *
     * @param map   map to be converted
     * @param clazz class of the instance
     * @param <T>   type parameter
     * @return the result instance
     */
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public static <T> T mapToBeanEffective(final Map<String, Object> map, final Class<T> clazz) {
        return BeanUtils.mapToBean(map, clazz);
    }

    /**
     * Convert map to java bean instance with a instance but not a class. <b>instance will not be changed, a new
     * instance will be returned</b>
     *
     * @param map      map to be converted
     * @param instance an instance of the class to be converted
     * @param <T>      type parameter
     * @return the result instance with type T
     */
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public static <T> T mapToBeanEffective(final Map<String, Object> map, final T instance) {
        return BeanUtils.mapToBean(map, instance);
    }

    /**
     * Copy properties from `src` instance to a new `distClass`'s instance <b>This method implements a glib proxy to
     * improve performance, but could influence serializing process.</b>
     *
     * @param src       instance that copy from
     * @param distClass instance's class that copy to
     * @param <D>       dist instance type
     * @param <S>       src instance type
     * @return the result instance with type D
     */
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public static <S, D> D beanToBeanEffective(final S src, final Class<D> distClass) {
        return BeanUtils.beanToBean(src, distClass);
    }

    /**
     * Copy properties from `src` instance to a new `distClass`'s instance <b>instance will not be changed, a new
     * instance will be returned</b>
     *
     * @param src  instance that copy from
     * @param dist an instance of the class to be converted
     * @param <D>  dist instance type
     * @param <S>  src instance type
     * @return the result instance with type D
     */
    @Deprecated
    @SuppressWarnings("DeprecatedIsStillUsed")
    public static <S, D> D beanToBeanEffective(final S src, final D dist) {
        return beanToBean(src, dist);
    }

    public static <T> void register(Class<T> distClass, MapConverter<T> converter) {
        BeanUtils.register(distClass, converter);
    }

    public static <T> void speedup(Class<T> distClass) {
        BeanUtils.speedup(distClass);
    }

    public static <S, D> void register(Class<S> srcClass, Class<D> distClass, BeanConverter<S, D> converter) {
        BeanUtils.register(srcClass, distClass, converter);
    }

    public static <S, D> void speedup(Class<S> srcClass, Class<D> distClass) {
        BeanUtils.speedup(srcClass, distClass);
    }

    /**
     * Convert map to java bean instance, a new instance will be returned. <b>This method is friendly with the
     * serializing process but has a bad performance</b>
     *
     * @param map   map to be converted
     * @param clazz class of the instance
     * @param <T>   type parameter
     * @return the result instance with type T
     */
    public static <T> T mapToBean(final Map<String, Object> map, final Class<T> clazz) {
        return BeanUtils.mapToBean(map, clazz);
    }

    /**
     * Convert map to java bean instance with a instance but not a class.
     *
     * @param map      map to be converted
     * @param instance an instance of the class to be converted
     * @param <T>      type parameter
     * @return the result instance with type T
     */
    public static <T> T mapToBean(final Map<String, Object> map, final T instance) {
        return BeanUtils.mapToBean(map, instance);
    }

    /**
     * Copy properties from `src` instance to a new `distClass`'s instance <b>This method is friendly with the
     * serializing process but has a bad performance</b>
     *
     * @param src       instance that copy from
     * @param distClass instance's class that copy to
     * @param <D>       dist instance type
     * @param <S>       src instance type
     * @return the result instance with type D
     */
    public static <D, S> D beanToBean(final S src, final Class<D> distClass) {
        return BeanUtils.beanToBean(src, distClass);
    }

    /**
     * Copy properties from `src` instance to a new `distClass`'s instance <b>instance will not be changed, a new
     * instance will be returned</b>
     *
     * @param src  instance that copy from
     * @param dist an instance of the class to be converted
     * @param <D>  dist instance type
     * @param <S>  src instance type
     * @return the result instance with type D
     */
    public static <D, S> D beanToBean(final S src, final D dist) {
        return BeanUtils.beanToBean(src, dist);
    }

    /**
     * Convert a java bean to a map.
     *
     * @param bean bean to be converted
     * @param <T>  bean type
     * @return result map
     */
    public static <T> Map<String, Object> beanToMap(final T bean) {
        return BeanUtils.beanToMap(bean);
    }

    public static <T> List<T> mapToBeanInList(final List<Map<String, Object>> mapList, final Class<T> clazz) {
        return mapToBeanInList(mapList, clazz, false);
    }

    public static <T> List<T> mapToBeanInList(final List<Map<String, Object>> mapList, final Class<T> clazz, boolean ignoreNull) {
        return BeanUtils.mapToBeanInList(mapList, clazz, ignoreNull);
    }

    public static <T> List<Map<String, Object>> beanToMapInList(final List<T> beanList) {
        return beanToMapInList(beanList, false);
    }

    public static <T> List<Map<String, Object>> beanToMapInList(final List<T> beanList, boolean ignoreNull) {
        return BeanUtils.beanToMapInList(beanList, ignoreNull);
    }

    public static <D, S> List<D> beanToBeanInList(final List<S> srcList, final Class<D> distClass) {
        return beanToBeanInList(srcList, distClass, false);
    }

    public static <D, S> List<D> beanToBeanInList(final List<S> srcList, final Class<D> distClass, boolean ignoreNull) {
        return BeanUtils.beanToBeanInList(srcList, distClass, ignoreNull);
    }
}
