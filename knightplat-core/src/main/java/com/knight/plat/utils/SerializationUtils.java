package com.knight.plat.utils;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.util.Pool;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knight.plat.api.annotations.Utils;

@Utils("serialize")
public class SerializationUtils {
    public static Kryo createKryo() {
        return Serialization.createKryo();
    }

    public static Pool<Kryo> getKryoPool() {
        return Serialization.getKryoPool();
    }

    public static ObjectMapper getObjectMapper() {
        return Serialization.getObjectMapper();
    }

    public static <T> byte[] serialize(T src) {
        return Serialization.serialize(src);
    }

    public static <T> byte[] serializeWithClass(T src) {
        return Serialization.serializeWithClass(src);
    }

    public static <T> T deserialize(byte[] src, Class<T> clazz) {
        return Serialization.deserialize(src, clazz);
    }

    public static Object deserializeWithClass(byte[] src) {
        return Serialization.deserializeWithClass(src);
    }

    public static <T> String jsonSerialize(T src) {
        return Serialization.jsonSerialize(src);
    }

    public static <T> byte[] jsonSerializeToBytes(T src) {
        return Serialization.jsonSerializeToBytes(src);
    }

    public static <T> T jsonDeserialize(String src, Class<T> clazz) {
        return Serialization.jsonDeserialize(src, clazz);
    }

    public static <T> T jsonDeserialize(String src, TypeReference<T> type) {
        return Serialization.jsonDeserialize(src, type);
    }

    public static <T> T jsonDeserializeFromBytes(byte[] src, Class<T> clazz) {
        return Serialization.jsonDeserializeFromBytes(src, clazz);
    }

    public static <T> T jsonDeserializeFromBytes(byte[] src, TypeReference<T> type) {
        return Serialization.jsonDeserializeFromBytes(src, type);
    }
}
