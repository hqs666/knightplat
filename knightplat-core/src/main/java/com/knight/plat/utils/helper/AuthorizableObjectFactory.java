/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils.helper;

import com.knight.plat.api.security.Authorizable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 16/9/5. 创建一个Authorizable的实例对象
 */
public final class AuthorizableObjectFactory {

    private static final Object[] NULL_ARGS = new Object[0];
    private static final Log log = LogFactory.getLog(AuthorizableObjectFactory.class);
    private final String userNameField;
    private final String passwordField;
    private final String[] fields;

    public AuthorizableObjectFactory(String userNameField, String passwordField, String... fields) {
        this.userNameField = userNameField;
        this.passwordField = passwordField;
        this.fields = fields;
    }

    public Authorizable create(Object src) {

        final Object userName = getFieldValue(src, userNameField);
        final Object password = getFieldValue(src, passwordField);
        final Authorizable instance = createBase(userName, password);
        for (String field : fields) {
            instance.putMetaValue(field, getFieldValue(src, field));
        }
        return instance;
    }

    public Authorizable create(Map<String, Object> src) {
        final Object userName = src.get(userNameField);
        final Object password = src.get(passwordField);
        final Authorizable instance = createBase(userName, password);
        for (String field : fields) {
            instance.putMetaValue(field, src.get(field));
        }
        return instance;
    }

    private Authorizable createBase(Object userName, Object password) {
        final SimpleUser instance = new SimpleUser();
        Assert.notNull(userName, "User name must not be null.");
        instance.setUserName(userName.toString());
        Assert.notNull(password, "Please provide a password.");
        instance.setPassword(password.toString());
        return instance;
    }

    private Object getFieldValue(Object src, String field) {
        try {
            final Method fieldMethod = getMethod(src, "get", field);
            return fieldMethod.invoke(src, NULL_ARGS);
        } catch (NoSuchMethodException e) {
            if (log.isWarnEnabled()) {
                log.warn(e.getLocalizedMessage());
            }
            return null;
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new UserCreationException(e);
        }
    }

    private Method getMethod(Object src, String prefix, String field) throws NoSuchMethodException {
        return src.getClass().getDeclaredMethod(prefix + upperCaseFirstLetter(field));
    }

    private String upperCaseFirstLetter(String src) {
        final char[] arr = src.toCharArray();
        if (arr[0] >= 'a' && arr[0] <= 'z') {
            arr[0] -= 'a' - 'A';
        }
        return new String(arr);
    }

    /**
     * Authorizable的简单实现类
     */
    private static class SimpleUser implements Authorizable {

        private String userName;
        private String password;
        private final Map<String, Object> metaData = new HashMap<String, Object>();

        @Override
        public String getUserName() {
            return this.userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        @Override
        public String getPassword() {
            return this.password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public void cleanPassword() {
            setPassword("***");
        }

        @Override
        public boolean passwordValidate() {
            return false;
        }

        @Override
        public Map<String, Object> getMetaData() {
            return this.metaData;
        }

        @Override
        public Object getMetaValue(String key) {
            return this.metaData.get(key);
        }

        @Override
        public void putMetaValue(String key, Object value) {
            this.metaData.put(key, value);
        }
    }

    private static class UserCreationException extends RuntimeException {
        UserCreationException(Throwable cause) {
            super(cause);
        }
    }
}
