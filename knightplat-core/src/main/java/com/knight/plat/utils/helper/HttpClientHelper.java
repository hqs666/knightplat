package com.knight.plat.utils.helper;

import com.knight.plat.utils.JsonUtils;
import com.knight.plat.utils.SpringContextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;

public final class HttpClientHelper {

    private static final Logger log = LoggerFactory.getLogger("knightplat - HttpClient");

    public static <T> String bodyToJson(T body) {
        if (body instanceof String) {
            return (String) body;
        }
        return JsonUtils.encode(body);
    }

    public static String getServiceUrl(String serviceId, String path) {
        final LoadBalancerClient loadBalancerClient = SpringContextUtils.getBean(LoadBalancerClient.class);
        if (loadBalancerClient == null) {
            log.warn("LoadBalancerClient is null, serviceId[{}] will be use for hostname", serviceId);
            return prefixUrl(serviceId, path);
        }
        final ServiceInstance instance = loadBalancerClient.choose(serviceId);
        if (null == instance) {
            log.warn("ServiceInstance is null, serviceId[{}] will be use for hostname", serviceId);
            return prefixUrl(serviceId, path);
        }
        return concatUrl(instance.getUri().toString(), path);
    }

    public static String concatUrl(String host, String path) {
        if (host.endsWith("/") || path.startsWith("/")) {
            return host + path;
        }
        return host + '/' + path;
    }

    public static String prefixUrl(String serviceId, String path) {
        if (serviceId.startsWith("http://") || serviceId.startsWith("https://")) {
            return concatUrl(serviceId, path);
        }
        return "http://" + concatUrl(serviceId, path);
    }
}
