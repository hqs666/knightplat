/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Created on 16/8/9.
 */
@Utils("spring")
public final class SpringContextUtils {

    private static ApplicationContext applicationContext;

    private SpringContextUtils() {
    }

    static void init(ApplicationContext applicationContext) {
        SpringContextUtils.applicationContext = applicationContext;
    }

    public static Object getBean(String name) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(String name, Class<T> aClass) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getBean(name, aClass);
    }

    public static <T> T getBean(Class<T> type) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getBean(type);
    }

    public static Object getBean(String name, Object... objects) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getBean(name, objects);
    }

    public static <T> T getBean(Class<T> type, Object... objects) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getBean(type, objects);
    }

    public static <T> Map<String, T> getBeanMap(Class<T> type) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getBeansOfType(type);
    }

    public static <T> Collection<T> getBeans(Class<T> type) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getBeansOfType(type).values();
    }

    public static boolean containsBean(String name) {
        if (applicationContext == null) {
            throw new RuntimeException("Spring context not loaded");
        }
        return applicationContext.containsBean(name);
    }

    public static boolean isSingleton(String name) {
        if (applicationContext == null) {
            throw new RuntimeException("Spring context not loaded");
        }
        return applicationContext.isSingleton(name);
    }

    public static Class<?> getType(String name) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getType(name);
    }

    public static String[] getAliases(String name) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getAliases(name);
    }

    public static Resource getResource(String name) {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getResource(name);
    }

    public static ClassLoader getClassLoader() {
        if (applicationContext == null) {
            return null;
        }
        return applicationContext.getClassLoader();
    }

    public static Resource[] getResources(String name) throws IOException {
        if (applicationContext == null) {
            throw new IOException("Spring context not loaded");
        }
        return applicationContext.getResources(name);
    }

    public static long getRunTime() {
        if (applicationContext == null) {
            return 0L;
        }
        long startupTime = System.currentTimeMillis() - applicationContext.getStartupDate();
        return startupTime;
    }
}
