package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.keyword.AbstractWhiteListManager;

@Utils("whiteListKeyword")
public class WhiteListKeywordUtils {

    private static AbstractWhiteListManager manager;

    private WhiteListKeywordUtils() { }

    private static AbstractWhiteListManager getKeywordManager() {
        if (null == manager) {
            AbstractWhiteListManager keywordManager = SpringContextUtils.getBean(AbstractWhiteListManager.class);
            if (keywordManager == null) {
                throw new IllegalStateException("No <AbstractWhiteListManager> instance found.");
            }
            manager = keywordManager;
            manager.load();
        }
        return manager;
    }


    public static boolean belongWhiteList(String str) {
        if (manager == null) {
            getKeywordManager();
        }

        return manager.belong(str);
    }
}
