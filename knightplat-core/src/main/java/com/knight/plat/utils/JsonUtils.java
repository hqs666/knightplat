package com.knight.plat.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.knight.plat.api.annotations.Utils;

import java.util.List;

@Utils("json")
public final class JsonUtils {
    public static <T> String encode(T src) {
        return Json.encode(src);
    }

    public static <T> byte[] encodeToBytes(T src) {
        return Json.encodeToBytes(src);
    }

    public static <T> T decode(String src, Class<T> clazz) {
        return Json.decode(src, clazz);
    }

    public static <T> T decode(String src, TypeReference<T> type) {
        return Json.decode(src, type);
    }

    public static <T> List<T> decodeToList(String src, Class<T> clazz) {
        return Json.decodeToList(src, clazz);
    }

    public static <T> T decode(byte[] src, Class<T> clazz) {
        return Json.decode(src, clazz);
    }

    public static <T> T decode(byte[] src, TypeReference<T> type) {
        return Json.decode(src, type);
    }

    public static <T> List<T> decodeToList(byte[] src,  Class<T> clazz) {
        return Json.decodeToList(src, clazz);
    }
}
