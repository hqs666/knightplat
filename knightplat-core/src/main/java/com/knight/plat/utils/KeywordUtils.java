/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import com.knight.plat.api.utils.config.Config;
import com.knight.plat.keyword.AbstractKeywordManager;

import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created on 2016/10/24. 敏感词过滤工具类
 */
@Utils("wordFilter")
public class KeywordUtils {

    private static AbstractKeywordManager manager;

    private KeywordUtils() { }

    private static AbstractKeywordManager getKeywordManager() {
        if (null == manager) {
            AbstractKeywordManager keywordManager = SpringContextUtils.getBean(AbstractKeywordManager.class);
            if (keywordManager == null) {
                throw new IllegalStateException("No <AbstractKeywordManager> instance found.");
            }
            manager = keywordManager;
            manager.load();
        }
        return manager;
    }

    // for test
    public static void setAndLoadManager(AbstractKeywordManager manager) {
        KeywordUtils.manager = manager;
        manager.load();
    }

    // for test
    public static void resetKeyword(String... keywords) {
        if (null == manager){
            manager = getKeywordManager();
        }
        manager.resetTo(keywords);
    }

    // for test
    public static void addKeywords(String... keywords) {
        if (null == manager){
            manager = getKeywordManager();
        }
        manager.loadAndAdd(keywords);
    }

    /**
     * 重新载入敏感词库
     */
    public static void reload() {
        if (null == manager){
            manager = getKeywordManager();
        }
        manager.load();
    }

    /**
     * 敏感词替换为指定的字符
     *
     * @param src     需替换的字符串
     * @param replace 替换字符
     * @param traceId 追踪id  用于跟踪消息处理结果
     * @return 替换结果
     */
    public static String filter(String src, char replace, String traceId) {
        if (null == manager){
            manager = getKeywordManager();
        }
       return manager.filter(src, replace, traceId);
    }
    /**
     * 敏感词替换为指定的字符
     *
     * @param src     需替换的字符串
     * @param replace 替换字符
     * @return 替换结果
     */
    public static String filter(String src, char replace) {
        if (null == manager){
            manager = getKeywordManager();
        }
       return manager.filter(src, replace, UUID.randomUUID().toString().replace("-",""));
    }

    /**
     * 按照提供的设置处理字符串敏感词
     *
     * @param src    需处理的字符串
     * @param config 设置对象
     * @return 处理结果
     */
    public static String filter(String src, Config config) {
        if (null == manager){
            manager = getKeywordManager();
        }
        return manager.filter(src, config);
    }

    /**
     * 判断是否包含敏感词
     *
     * @param src 待判断字符串
     * @return 判断结果
     */
    public static boolean isSensitive(String src) {
        if (null == manager){
            manager = getKeywordManager();
        }
        return manager.isSensitive(src);
    }

    /**
     * 统计给出的字符串包含的敏感词级别
     *
     * @param src 给出的字符串
     * @return 敏感词级别列表
     */
    public static List<String> keyWordLevels(String src) {
        if (null == manager){
            manager = getKeywordManager();
        }
        return manager.keyWordLevels(src);
    }

    public static Set<String> countKeyWords(String src) {
        if (null == manager){
            manager = getKeywordManager();
        }
        return manager.countKeyWords(src);
    }
}
