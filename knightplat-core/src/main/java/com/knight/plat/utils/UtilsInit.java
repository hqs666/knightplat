/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.utils;

import com.knight.plat.api.log.LogProvider;
import com.knight.plat.api.utils.HolidayManager;
import com.knight.plat.api.utils.SequenceManager;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Created on 2016/12/19. 帮助初始化工具类
 */
@Configuration
public class UtilsInit implements ApplicationContextAware { // 方法类不是个好东西，谁有更好的解决方案(#‵′)凸

    private LogProvider logProvider;

    private SequenceManager sequenceManager;

    private HolidayManager holidayManager;

    @Autowired
    public void setLogProvider(LogProvider logProvider) {
        this.logProvider = logProvider;
    }

    @Autowired
    public void setSequenceManager(SequenceManager sequenceManager) {
        this.sequenceManager = sequenceManager;
    }

    @Autowired
    public void setHolidayManager(HolidayManager holidayManager) {
        this.holidayManager = holidayManager;
    }

    @PostConstruct
    void init() {
        LogUtils.init(logProvider);
        SequenceUtils.init(sequenceManager);
        WorkdayUtils.init(holidayManager);
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) {
        SpringContextUtils.init(applicationContext);
    }
}
