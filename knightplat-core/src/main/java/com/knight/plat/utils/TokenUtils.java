package com.knight.plat.utils;

import com.knight.plat.api.annotations.Utils;
import io.jsonwebtoken.Claims;

import javax.crypto.SecretKey;
import java.util.Map;

@Utils("token")
public final class TokenUtils {
    public static String create(String tokenId, String userId, Map<String, ?> data) {
        return Token.create(tokenId, userId, data);
    }

    public static String create(String tokenId, String userId, Map<String, ?> data, SecretKey key) {
        return Token.create(tokenId, userId, data, key);
    }

    public static String create(String tokenId, String userId, Map<String, ?> data, long expMillis) {
        return Token.create(tokenId, userId, data, expMillis);
    }

    public static String create(String tokenId, String userId, Map<String, ?> data, SecretKey key, long expMillis) {
        return Token.create(tokenId, userId, data, key, expMillis);
    }

    public static Claims checkAndRead(String token) {
        return Token.checkAndRead(token);
    }

    public static Claims checkAndRead(String token, SecretKey key) {
        return Token.checkAndRead(token, key);
    }

    public static Claims readWithoutCheck(String token) {
        return Token.readWithoutCheck(token);
    }
}
