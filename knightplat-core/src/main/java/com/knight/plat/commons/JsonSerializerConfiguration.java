package com.knight.plat.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfiguration;

@AutoConfiguration
public class JsonSerializerConfiguration implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(@NotNull Object bean, @NotNull String beanName) throws BeansException {
        if (bean instanceof ObjectMapper) {
            final ObjectMapper mapper = (ObjectMapper) bean;
            JsonTypeTools.initialize(mapper, null);
        }
        return bean;
    }
}
