package com.knight.plat.commons;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.WritableTypeId;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import io.vertx.core.buffer.Buffer;

import javax.annotation.Nullable;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import static io.vertx.core.json.impl.JsonUtil.BASE64_DECODER;
import static io.vertx.core.json.impl.JsonUtil.BASE64_ENCODER;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;

public class JsonTypeTools {

    static void initialize(ObjectMapper mapper, @Nullable ObjectMapper prettyMapper) {
        // Non-standard JSON but we allow C style comments in our JSON
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        mapper.configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true);
        mapper.configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        if (prettyMapper != null) {
            prettyMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
            prettyMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            prettyMapper.configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true);
            prettyMapper.configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, true);
            prettyMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }

        SimpleModule module = new SimpleModule();
        // custom types
        module.addSerializer(JsonRoot.class, new JsonRootSerializer());
        module.addDeserializer(JsonRoot.class, new JsonRootDeserializer());
        module.addSerializer(JsonObject.class, new JsonObjectSerializer());
        module.addDeserializer(JsonObject.class, new JsonObjectDeserializer());
        module.addSerializer(JsonArray.class, new JsonArraySerializer());
        module.addDeserializer(JsonArray.class, new JsonArrayDeserializer());
        // he have 2 extensions: RFC-7493
        module.addSerializer(Instant.class, new InstantSerializer());
        module.addDeserializer(Instant.class, new InstantDeserializer());
        module.addSerializer(byte[].class, new ByteArraySerializer());
        module.addDeserializer(byte[].class, new ByteArrayDeserializer());
        module.addSerializer(Buffer.class, new BufferSerializer());
        module.addDeserializer(Buffer.class, new BufferDeserializer());

        mapper.registerModule(module)
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());
        if (prettyMapper != null) {
            prettyMapper.registerModule(module)
                    .registerModule(new ParameterNamesModule())
                    .registerModule(new Jdk8Module())
                    .registerModule(new JavaTimeModule());
        }
    }

    public static class JsonRootSerializer extends JsonSerializer<JsonRoot> {

        @Override
        public void serialize(JsonRoot value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            if (value.isArray()) {
                jgen.writeObject(value.getJsonArray().getList());
            } else {
                jgen.writeObject(value.getJsonObject().getMap());
            }
        }

        @Override
        public void serializeWithType(JsonRoot value, JsonGenerator gen, SerializerProvider serializers,
                TypeSerializer typeSer) throws IOException {
            WritableTypeId typeId = typeSer.writeTypePrefix(gen,typeSer.typeId(value, JsonToken.VALUE_STRING));
            serialize(value, gen, serializers);
            typeSer.writeTypeSuffix(gen, typeId);
        }
    }

    public static class JsonObjectSerializer extends JsonSerializer<JsonObject> {

        @Override
        public void serialize(JsonObject value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeObject(value.getMap());
        }

        @Override
        public void serializeWithType(JsonObject value, JsonGenerator gen, SerializerProvider serializers,
                TypeSerializer typeSer) throws IOException {
            WritableTypeId typeId = typeSer.writeTypePrefix(gen,typeSer.typeId(value, JsonToken.VALUE_STRING));
            serialize(value, gen, serializers);
            typeSer.writeTypeSuffix(gen, typeId);
        }
    }

    public static class JsonArraySerializer extends JsonSerializer<JsonArray> {

        @Override
        public void serialize(JsonArray value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeObject(value.getList());
        }

        @Override
        public void serializeWithType(JsonArray value, JsonGenerator gen, SerializerProvider serializers,
                TypeSerializer typeSer) throws IOException {
            WritableTypeId typeId = typeSer.writeTypePrefix(gen,typeSer.typeId(value, JsonToken.VALUE_STRING));
            serialize(value, gen, serializers);
            typeSer.writeTypeSuffix(gen, typeId);
        }
    }

    public static class JsonRootDeserializer extends JsonDeserializer<JsonRoot> {

        @Override
        @SuppressWarnings({"unchecked", "rawtypes"})
        public JsonRoot deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException, JsonProcessingException {
            if (jsonParser.isExpectedStartArrayToken()) {
                List list = jsonParser.getCodec().readValue(jsonParser, List.class);
                return new JsonRoot(new JsonArray(list));
            } else {
                Map<String, Object> map = (Map<String, Object>) jsonParser.getCodec().readValue(jsonParser, Map.class);
                return new JsonRoot(new JsonObject(map));
            }
        }
    }

    public static class JsonObjectDeserializer extends JsonDeserializer<JsonObject> {

        @Override
        @SuppressWarnings("unchecked")
        public JsonObject deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException, JsonProcessingException {
            Map<String, Object> map = (Map<String, Object>) jsonParser.getCodec().readValue(jsonParser, Map.class);
            return new JsonObject(map);
        }
    }

    public static class JsonArrayDeserializer extends JsonDeserializer<JsonArray> {

        @Override
        @SuppressWarnings("rawtypes")
        public JsonArray deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
                throws IOException, JsonProcessingException {
            List list = jsonParser.getCodec().readValue(jsonParser, List.class);
            return new JsonArray(list);
        }
    }

    public static class InstantSerializer extends JsonSerializer<Instant> {

        @Override
        public void serialize(Instant value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(ISO_INSTANT.format(value));
        }

        @Override
        public void serializeWithType(Instant value, JsonGenerator gen, SerializerProvider serializers,
                TypeSerializer typeSer) throws IOException {
            WritableTypeId typeId = typeSer.writeTypePrefix(gen,typeSer.typeId(value, JsonToken.VALUE_STRING));
            serialize(value, gen, serializers);
            typeSer.writeTypeSuffix(gen, typeId);
        }
    }

    public static class InstantDeserializer extends JsonDeserializer<Instant> {

        @Override
        public Instant deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            String text = p.getText();
            try {
                return Instant.from(ISO_INSTANT.parse(text));
            } catch (DateTimeException e) {
                throw new InvalidFormatException(p, "Expected an ISO 8601 formatted date time", text, Instant.class);
            }
        }
    }

    public static class ByteArraySerializer extends JsonSerializer<byte[]> {

        @Override
        public void serialize(byte[] value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeString(BASE64_ENCODER.encodeToString(value));
        }

        @Override
        public void serializeWithType(byte[] value, JsonGenerator gen, SerializerProvider serializers,
                TypeSerializer typeSer) throws IOException {
            WritableTypeId typeId = typeSer.writeTypePrefix(gen,typeSer.typeId(value, JsonToken.VALUE_STRING));
            serialize(value, gen, serializers);
            typeSer.writeTypeSuffix(gen, typeId);
        }
    }

    public static class ByteArrayDeserializer extends JsonDeserializer<byte[]> {

        @Override
        public byte[] deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            String text = p.getText();
            try {
                return BASE64_DECODER.decode(text);
            } catch (IllegalArgumentException e) {
                throw new InvalidFormatException(p, "Expected a base64 encoded byte array", text, Instant.class);
            }
        }
    }

    public static class BufferSerializer extends JsonSerializer<Buffer> {

        @Override
        public void serialize(Buffer value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeString(BASE64_ENCODER.encodeToString(value.getBytes()));
        }

        @Override
        public void serializeWithType(Buffer value, JsonGenerator gen, SerializerProvider serializers,
                TypeSerializer typeSer) throws IOException {
            WritableTypeId typeId = typeSer.writeTypePrefix(gen,typeSer.typeId(value, JsonToken.VALUE_STRING));
            serialize(value, gen, serializers);
            typeSer.writeTypeSuffix(gen, typeId);
        }
    }

    public static class BufferDeserializer extends JsonDeserializer<Buffer> {

        @Override
        public Buffer deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            String text = p.getText();
            try {
                return Buffer.buffer(BASE64_DECODER.decode(text));
            } catch (IllegalArgumentException e) {
                throw new InvalidFormatException(p, "Expected a base64 encoded byte array", text, Instant.class);
            }
        }
    }
}
