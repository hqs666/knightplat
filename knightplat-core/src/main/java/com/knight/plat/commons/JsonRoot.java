package com.knight.plat.commons;

import java.util.List;
import java.util.Map;

public class JsonRoot {
    private final JsonObject jsonObject;
    private final JsonArray jsonArray;

    private final boolean array;

    public JsonRoot(String source) {
        if (source.trim().startsWith("[")) {
            this.jsonArray = new JsonArray(source);
            this.jsonObject = null;
            this.array = true;
        } else {
            this.jsonObject = new JsonObject(source);
            this.jsonArray = null;
            this.array = false;
        }
    }

    public JsonRoot(Map<String, Object> map) {
        this.jsonObject = new JsonObject(map);
        this.jsonArray = null;
        this.array = false;
    }

    public JsonRoot(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
        this.jsonArray = null;
        this.array = false;
    }

    @SuppressWarnings("rawtypes")
    public JsonRoot(List list) {
        this.jsonArray = new JsonArray(list);
        this.jsonObject = null;
        this.array = true;
    }

    public JsonRoot(JsonArray jsonArray) {
        this.jsonArray = jsonArray;
        this.jsonObject = null;
        this.array = true;
    }

    public JsonObject getJsonObject() {
        if (jsonObject == null) {
            throw new UnsupportedOperationException("Not a json object string");
        }
        return jsonObject;
    }

    public Map<String, Object> getMap() {
        return getJsonObject().getMap();
    }

    public JsonArray getJsonArray() {
        if (jsonArray == null) {
            throw new UnsupportedOperationException("Not a json array string");
        }
        return jsonArray;
    }

    @SuppressWarnings("rawtypes")
    public List getList() {
        return getJsonArray().getList();
    }

    public boolean isArray() {
        return array;
    }

    public String encode() {
        if (array && jsonArray != null) {
            return jsonArray.encode();
        }
        if (!array && jsonObject != null) {
            return jsonObject.encode();
        }
        throw new NullPointerException("JsonRoot has null value");
    }

    @Override
    public String toString() {
        return encode();
    }
}
