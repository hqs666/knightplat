package com.knight.plat.image;

import com.knight.plat.api.image.ThumbnailsManager;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;
import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * @author Yongqi Ren
 * @version 1.0
 */
public class LocalThumbnailsManager implements ThumbnailsManager<LocalThumbnailsManager> {

    private static Thumbnails.Builder of;


    public static LocalThumbnailsManager manager(String... files) {
        return new LocalThumbnailsManager(files);
    }

    public static LocalThumbnailsManager manager(File... files) {
        return new LocalThumbnailsManager(files);
    }

    public static LocalThumbnailsManager manager(URL... urls) {
        return new LocalThumbnailsManager(urls);
    }

    public static LocalThumbnailsManager manager(InputStream... inputStreams) {
        return new LocalThumbnailsManager(inputStreams);
    }

    private LocalThumbnailsManager() {
    }

    private LocalThumbnailsManager(String... files) {
        of = Thumbnails.of(files);
    }

    private LocalThumbnailsManager(File... files) {
        of = Thumbnails.of(files);
    }

    private LocalThumbnailsManager(URL... urls) {
        of = Thumbnails.of(urls);
    }

    private LocalThumbnailsManager(InputStream... inputStreams) {
        of = Thumbnails.of(inputStreams);
    }

    @Override
    public LocalThumbnailsManager rotate(double angle) {
        of.rotate(angle);
        return this;
    }

    @Override
    public LocalThumbnailsManager size(int width, int height) {
        of.size(width, height);
        return this;
    }

    @Override
    public LocalThumbnailsManager scale(double scale) {
        of.scale(scale);
        return this;
    }

    @Override
    public LocalThumbnailsManager scale(double scaleWidth, double scaleHeight) {
        of.scale(scaleWidth, scaleHeight);
        return this;
    }

    @Override
    public LocalThumbnailsManager watermark(String file, float opacity) throws IOException {
        of.watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File(file)), opacity);
        return this;
    }

    @Override
    public LocalThumbnailsManager watermark(File file, float opacity) throws IOException {
        of.watermark(Positions.BOTTOM_RIGHT, ImageIO.read(file), opacity);
        return this;
    }

    @Override
    public LocalThumbnailsManager outputQuality(float quality) {
        of.outputQuality(quality);
        return this;
    }

    @Override
    public LocalThumbnailsManager outputFormat(String format) {
        of.outputFormat(format);
        return this;
    }


    @Override
    public void toFile(String path) throws IOException {
        of.toFile(path);
    }

    /**
     * 获取源图像，并将缩略图作为文件写入destinationDir
     *
     * @param destinationDir
     * @param rename
     * @throws IOException
     */
    public void toFiles(File destinationDir, Rename rename) throws IOException {
        of.toFiles(destinationDir, rename);
    }

    @Override
    public void toOutputStream(OutputStream stream) throws IOException {
        of.toOutputStream(stream);
    }

    public String toBase64() throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        of.toOutputStream(os);
        byte[] bytes = os.toByteArray();
        return Base64.encodeBase64String(bytes);
    }
}
