package com.knight.plat.async;

import com.knight.plat.api.commons.Callable;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.executor.RunnableFilter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AsyncManagerImpl implements AsyncManager {

    private final Executor executor;
    private final List<RunnableFilter> runnableFilters;

    AsyncManagerImpl(Executor executor, List<RunnableFilter> runnableFilters) {
        this.executor = executor;
        this.runnableFilters = runnableFilters;
    }

    public <T> Mono<T> run(Callable<T> callable) {
        return Mono.create(sink -> execute(callable, makeCallback(sink)));
    }

    public Mono<Void> run(Runnable runnable) {
        return Mono.create(sink -> execute(runnable, makeCallback(sink)));
    }

    @Override
    public Flux<Object> runMultiple(Callable<?>... callables) {
        if (callables == null || callables.length == 0) {
            return Flux.empty();
        }

        return Flux.mergeSequential(Stream.of(callables).map(this::run).collect(Collectors.toList()));
    }

    @Override
    public <T> Flux<T> runMultiple(Collection<Callable<T>> callables) {
        if (callables == null || callables.isEmpty()) {
            return Flux.empty();
        }

        return Flux.mergeSequential(callables.stream().map(this::run).collect(Collectors.toList()));
    }

    @Override
    public <T> CompletionStage<T> stage(Callable<T> callable) {
        return execute(callable);
    }

    @Override
    public CompletionStage<Void> stage(Runnable runnable) {
        return execute(runnable);
    }

    @Override
    public <T> Future<T> future(Callable<T> callable) {
        return execute(callable).toCompletableFuture();
    }

    @Override
    public Future<Void> future(Runnable runnable) {
        return execute(runnable).toCompletableFuture();
    }

    private void execute(Runnable runnable, Callback<Void, Throwable> callback) {
        try {
            executor.execute(filterTask(() -> {
                try {
                    runnable.run();
                    callback.onSuccess(null);
                } catch (Throwable t) {
                    callback.onException(t);
                }
            }));
        } catch (Throwable t) {
            callback.onException(t);
        }
    }

    private <R> void execute(Callable<R> callable, Callback<R, Throwable> callback) {
        try {
            executor.execute(filterTask(() -> {
                try {
                    R result = callable.call();
                    callback.onSuccess(result);
                } catch (Throwable t) {
                    callback.onException(t);
                }
            }));
        } catch (Throwable t) {
            callback.onException(t);
        }
    }

    private CompletableFuture<Void> execute(Runnable runnable) {
        final CompletableFuture<Void> future = new CompletableFuture<>();
        execute(runnable, Callback.make(future));
        return future;
    }

    private  <R> CompletableFuture<R> execute(Callable<R> callable) {
        final CompletableFuture<R> future = new CompletableFuture<>();
        execute(callable, Callback.make(future));
        return future;
    }

    private <R> Callback<R, Throwable> makeCallback(MonoSink<R> sink) {
        return new Callback<R, Throwable>() {
            @Override
            public void onSuccess(R sendResult) {
                sink.success(sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                sink.error(throwable);
            }
        };
    }

    private Runnable filterTask(Runnable task) {
        if (runnableFilters == null || runnableFilters.isEmpty()) {
            return task;
        }
        Runnable result = task;
        for (RunnableFilter filter : runnableFilters) {
            result = filter.filter(result);
        }
        return result;
    }
}
