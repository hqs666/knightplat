package com.knight.plat.async;

import com.knight.plat.api.commons.Callable;
import com.knight.plat.api.executor.RunnableFilter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

public interface AsyncManager {

    static AsyncManager create(Executor executor, List<RunnableFilter> runnableFilters) {
        return new AsyncManagerImpl(executor, runnableFilters);
    }

    <T> Mono<T> run(Callable<T> callable);

    Mono<Void> run(Runnable runnable);

    Flux<Object> runMultiple(Callable<?>... callables);

    <T> Flux<T> runMultiple(Collection<Callable<T>> callables);

    <T> CompletionStage<T> stage(Callable<T> callable);

    CompletionStage<Void> stage(Runnable runnable);

    <T> Future<T> future(Callable<T> callable);

    Future<Void> future(Runnable runnable);

}
