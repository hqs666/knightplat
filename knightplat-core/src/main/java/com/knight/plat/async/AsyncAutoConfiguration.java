package com.knight.plat.async;

import com.knight.plat.api.executor.RunnableFilter;
import com.knight.plat.vertx.VertxAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

@AutoConfiguration(after = VertxAutoConfiguration.class)
public class AsyncAutoConfiguration {

    @Lazy
    @Bean
    public AsyncManager asyncManager(List<Executor> executors, List<RunnableFilter> runnableFilters) {
        if (executors == null || executors.isEmpty()) {
            throw new RuntimeException("没有找到Executor实现类，请注入实现类");
        }
        if (runnableFilters == null || runnableFilters.isEmpty()) {
            return AsyncManager.create(executors.get(0), Collections.emptyList());
        }
        return AsyncManager.create(executors.get(0), runnableFilters);
    }
}
