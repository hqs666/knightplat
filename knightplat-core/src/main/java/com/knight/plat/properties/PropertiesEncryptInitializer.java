package com.knight.plat.properties;

import com.google.common.base.Strings;
import com.knight.plat.utils.EncryptUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;

import java.security.spec.InvalidKeySpecException;
import java.util.Properties;

public class PropertiesEncryptInitializer implements
        ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
        final ConfigurableEnvironment environment = applicationContext.getEnvironment();
        final MutablePropertySources propertySources = environment.getPropertySources();

        String key = "knightplat_properties_salt_1984_to32";

        // 寻找加解密密钥配置
        label_top:
        for (PropertySource<?> propertySource : propertySources) {
            if (!(propertySource instanceof EnumerablePropertySource)) {
                continue;
            }
            final EnumerablePropertySource<?> source = (EnumerablePropertySource<?>) propertySource;
            for (String propertyName : source.getPropertyNames()) {
                if ("knightplat.properties.enc.key".equalsIgnoreCase(propertyName)) {
                    Object keyObj = source.getProperty(propertyName);
                    if (keyObj != null) {
                        key = keyObj.toString();
                        break label_top;
                    }
                }
            }
        }

        // 寻找加密内容并修改为解密后内容
        for (PropertySource<?> propertySource : propertySources) {
            if (!(propertySource instanceof EnumerablePropertySource)) {
                continue;
            }
            final EnumerablePropertySource<?> source = (EnumerablePropertySource<?>) propertySource;
            final Properties properties = new Properties();
            for (String propertyName : source.getPropertyNames()) {
                final Object valueObj = source.getProperty(propertyName);
                if (valueObj instanceof String) {
                    final String valueStr = (String) valueObj;
                    if (valueStr.startsWith("ENC(") && valueStr.endsWith(")")) {
                        final String encValue = valueStr.substring(4, valueStr.length() - 1);
                        final String realValue;
                        try {
                            realValue = EncryptUtils.aesDecryptToString(encValue, key);
                        } catch (InvalidKeySpecException e) {
                            throw new RuntimeException("Property decrypt error: (" + propertyName + ": " + valueStr + ").", e);
                        }

                        if (Strings.isNullOrEmpty(realValue)) {
                            throw new RuntimeException("Do not encrypt a null property.");
                        }
                        properties.setProperty(propertyName, realValue);
                    }
                }
            }
            PropertySource<?> newSource = new PropertiesPropertySource("encryptor" + source.getName(), properties);
            propertySources.addBefore(source.getName(), newSource);
        }

    }
}
