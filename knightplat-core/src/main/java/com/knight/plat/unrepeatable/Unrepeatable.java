/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.unrepeatable;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created on 2016/12/2. Mark a request method as an unrepeatable request.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Unrepeatable {

    @AliasFor("unrepeatable")
    boolean value() default true;

    @AliasFor("value")
    boolean unrepeatable() default true;

    String key() default "";
}
