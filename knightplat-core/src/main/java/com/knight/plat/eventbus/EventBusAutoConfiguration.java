package com.knight.plat.eventbus;

import com.knight.plat.api.eventbus.EventBus;
import com.knight.plat.api.eventbus.EventStrategy;
import com.knight.plat.api.eventbus.SchemaStrategyPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.util.List;

/**
 * 异步事件总线自动配置
 * Add on 2021/01/20
 *
 * @author Huang.Wj
 * @since 1.0.12
 */
@AutoConfiguration
public class EventBusAutoConfiguration {

    private final Logger log = LoggerFactory.getLogger(EventBusAutoConfiguration.class);

    @Lazy
    @Bean(destroyMethod = "close")
    @SuppressWarnings("unused")
    public EventBus eventBus(final List<SchemaStrategyPair> pairs) {
        final EventBus eventBus = new EventBusImpl();
        pairs.forEach(pair -> {
            final String schema = pair.getSchema();
            final EventStrategy strategy = pair.getStrategy();
            eventBus.registerEventStrategy(schema, strategy);
            log.info("Register event strategy for schema: {} using {}", schema, strategy.getClass().getName());
        });
        return eventBus;
    }
}
