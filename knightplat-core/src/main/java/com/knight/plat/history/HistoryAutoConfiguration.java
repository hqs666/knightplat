package com.knight.plat.history;

import com.knight.plat.api.history.History;
import com.knight.plat.api.history.HistoryRecorder;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.util.List;

@AutoConfiguration
public class HistoryAutoConfiguration implements ApplicationContextAware {
    private final Logger log = LoggerFactory.getLogger("knightplat-history-recorder");

    private ApplicationContext applicationContext;

    @Lazy
    @Bean
    @SuppressWarnings("unused")
    public History history(List<HistoryRecorder> recorderList) {
        String appName = applicationContext.getApplicationName();
        History history = new History(appName.isEmpty() ? "default" : appName);
        if (recorderList != null && !recorderList.isEmpty()) {
            recorderList.forEach(history::registerRecorder);
        }
        return history;
    }

    @Lazy
    @Bean
    @SuppressWarnings("unused")
    public HistoryRecorder localDebugRecorder() {
        return history -> {
            if (log.isDebugEnabled()) {
                log.debug(history.serialize());
            }
        };
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
