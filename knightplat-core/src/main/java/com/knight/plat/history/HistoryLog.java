package com.knight.plat.history;

import com.google.common.collect.Maps;
import com.knight.plat.api.history.BaseHistoryLog;
import com.knight.plat.utils.JsonUtils;
import com.knight.plat.utils.KeyGenUtils;

import java.util.Date;
import java.util.Map;

public class HistoryLog implements BaseHistoryLog {

    private String app;
    private String uuid;
    private Date dateTime;
    private String userId;
    private String userName;
    private String bizType;
    private String bizId;
    private String module;
    private String system;
    private String device;
    private String company;
    private String organization;
    private String event;
    private String requestUrl;
    private String requestMethod;
    private Map<String, Object> data;

    private HistoryLog() {
    }

    public static class Builder {

        private Date dateTime;
        private String userId;
        private String userName;
        private String bizType;
        private String bizId;
        private String module;
        private String system;
        private String device;
        private String company;
        private String event;
        private String organization;
        private String requestUrl;
        private String requestMethod;
        private Map<String, Object> data;

        public Builder dateTime(Date dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder userName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder bizType(String bizType) {
            this.bizType = bizType;
            return this;
        }

        public Builder bizId(String bizId) {
            this.bizId = bizId;
            return this;
        }

        public Builder module(String module) {
            this.module = module;
            return this;
        }

        public Builder system(String system) {
            this.system = system;
            return this;
        }

        public Builder device(String device) {
            this.device = device;
            return this;
        }

        public Builder company(String company) {
            this.company = company;
            return this;
        }

        public Builder event(String event) {
            this.event = event;
            return this;
        }

        public Builder organization(String organization) {
            this.organization = organization;
            return this;
        }

        public Builder requestUrl(String requestUrl) {
            this.requestUrl = requestUrl;
            return this;
        }

        public Builder requestMethod(String requestMethod) {
            this.requestMethod = requestMethod;
            return this;
        }

        public Builder data(Map<String, Object> data) {
            this.data = data;
            return this;
        }

        public Builder data(String key, Object value) {
            if (this.data == null) {
                this.data = Maps.newHashMap();
            }
            this.data.put(key, value);
            return this;
        }

        public HistoryLog build() {
            HistoryLog log = new HistoryLog();
            log.dateTime = dateTime != null ? dateTime : new Date();
            log.userId = userId;
            log.userName = userName;
            log.bizType = bizType;
            log.bizId = bizId;
            log.module = module;
            log.system = system;
            log.device = device;
            log.company = company;
            log.event = event;
            log.organization = organization;
            log.requestMethod = requestMethod;
            log.requestUrl = requestUrl;
            log.data = data == null ? Maps.newHashMap() : data;
            log.uuid = KeyGenUtils.newUuid();
            return log;
        }
    }

    public static Builder builder() {
        return new Builder();
    }


    @Override
    public String getApp() {
        return app;
    }

    @Override
    public void setApp(String app) {
        this.app = app;
    }

    @Override
    public String getModule() {
        return module;
    }

    @Override
    public Map<String, Object> getData() {
        return data;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public Date getDateTime() {
        return dateTime;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getBizType() {
        return bizType;
    }

    @Override
    public String getBizId() {
        return bizId;
    }

    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public String getDevice() {
        return device;
    }

    @Override
    public String getCompany() {
        return company;
    }

    @Override
    public String getEvent() {
        return event;
    }

    @Override
    public String getOrganization() {
        return organization;
    }

    @Override
    public String getRequestUrl() {
        return requestUrl;
    }

    @Override
    public String getRequestMethod() {
        return requestMethod;
    }

    public String json() {
        return JsonUtils.encode(this);
    }

    @Override
    public String serialize() {
        return json();
    }
}
