package com.knight.plat.vertx;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.task.TaskExecutorCustomizer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Vertx integration setup
 * Created  on 2021/1/15.
 * @author Knight
 */
@AutoConfiguration
public class ThreadAutoConfiguration implements TaskExecutorCustomizer {

    @Override
    public void customize(ThreadPoolTaskExecutor taskExecutor) {
        taskExecutor.setThreadNamePrefix("knight-worker-pool-");
    }
}
