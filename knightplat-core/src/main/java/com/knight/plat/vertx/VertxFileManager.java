package com.knight.plat.vertx;

import com.google.common.base.Strings;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.filesystem.FileManager;
import com.knight.plat.api.filesystem.Part;
import com.knight.plat.utils.IoUtils;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.FileSystem;
import io.vertx.core.file.OpenOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

public class VertxFileManager implements FileManager {

    private final FileSystem vertxFs;
    private final Vertx vertx;

    private final Log log = LogFactory.getLog(VertxFileManager.class);

    public VertxFileManager(Vertx vertx) {
        this.vertxFs = vertx.fileSystem();
        this.vertx = vertx;
    }

    @Override
    public String save(InputStream data, String bucket, String path) throws IOException {
        return save(data, -1, bucket, path);
    }

    @Override
    public String save(InputStream data, long contentLength, String bucket, String path) throws IOException {
        final String finalPath = finalPath(bucket, path);
        createParentDirs(finalPath);
        try (final FileOutputStream os = new FileOutputStream(finalPath, false)) {
            IoUtils.copy(data, os, contentLength);
        }
        return finalPath;
    }

    @Override
    public void saveAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        saveAsync(data, -1, bucket, path, callback);
    }

    @Override
    public void saveAsync(InputStream data, long contentLength, String bucket, String path,
            Callback<String, IOException> callback) {
        vertx.<String>executeBlocking(promise -> {
            final String finalPath = finalPath(bucket, path);
            FileOutputStream os = null;
            try {
                createParentDirs(finalPath);
                os = new FileOutputStream(finalPath, false);
                IoUtils.copy(data, os, contentLength);
                promise.complete(finalPath);
            } catch (IOException e) {
                promise.fail(e);
            } finally {
                IoUtils.closeQuietly(os);
            }
        }, saveRes -> {
            if (saveRes.succeeded()) {
                callback.onSuccess(saveRes.result());
            } else {
                final Throwable cause = saveRes.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    @Override
    public String append(InputStream data, String bucket, String path) throws IOException {
        final String finalPath = finalPath(bucket, path);
        createParentDirs(finalPath);
        try (final FileOutputStream os = new FileOutputStream(finalPath, true)) {
            IoUtils.copy(data, os);
        }
        return finalPath;
    }

    @Override
    public void appendAsync(InputStream data, String bucket, String path, Callback<String, IOException> callback) {
        vertx.<String>executeBlocking(promise -> {
            final String finalPath = finalPath(bucket, path);
            FileOutputStream os = null;
            try {
                createParentDirs(finalPath);
                os = new FileOutputStream(finalPath, true);
                IoUtils.copy(data, os);
                promise.complete(finalPath);
            } catch (IOException e) {
                promise.fail(e);
            } finally {
                IoUtils.closeQuietly(os);
            }
        }, saveRes -> {
            if (saveRes.succeeded()) {
                callback.onSuccess(saveRes.result());
            } else {
                final Throwable cause = saveRes.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    @Override
    public byte[] read(String bucket, String path) throws IOException {
        final String finalPath = finalPath(bucket, path);
        final Buffer buffer = vertxFs.readFileBlocking(finalPath);
        return buffer.getBytes();
    }

    @Override
    public long size(String bucket, String path) throws IOException {
        final String finalPath = finalPath(bucket, path);
        return new File(finalPath).length();
    }

    @Override
    public InputStream readAsInputStream(String bucket, String path) throws IOException {
        final String finalPath = finalPath(bucket, path);
        return Files.newInputStream(FileSystems.getDefault().getPath(finalPath));
    }

    @Override
    public void readAsync(String bucket, String path, Callback<byte[], IOException> callback) {
        final String finalPath = finalPath(bucket, path);
        vertxFs.readFile(finalPath, res -> {
            if (res.succeeded()) {
                callback.onSuccess(res.result().getBytes());
            } else {
                final Throwable cause = res.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    @Override
    public void readAsInputStreamAsync(String bucket, String path, Callback<InputStream, IOException> callback) {
        vertx.<InputStream>executeBlocking(promise -> {
            final String finalPath = finalPath(bucket, path);
            try {
                final InputStream is = Files.newInputStream(FileSystems.getDefault().getPath(finalPath));
                promise.complete(is);
            } catch (IOException e) {
                promise.fail(e);
            }
        }, ar -> {
            if (ar.succeeded()) {
                final InputStream resultInputStream = ar.result();
                if (resultInputStream != null) {
                    try (InputStream in = resultInputStream) {
                        callback.onSuccess(in);
                    } catch (IOException e) {
                        callback.onException(e);
                    }
                }
            } else {
                final Throwable cause = ar.cause();
                if (cause instanceof IOException) {
                    callback.onException((IOException) cause);
                } else {
                    callback.onException(new IOException(cause));
                }
            }
        });
    }

    @Override
    public byte[] read(String bucket, String path, String params) throws IOException {
        log.warn("Local file manager not support read with params");
        return read(bucket, path);
    }

    @Override
    public void readAsync(String bucket, String path, String params, Callback<byte[], IOException> callback) {
        log.warn("Local file manager not support read with params");
        readAsync(bucket, path, callback);
    }

    @Override
    public String createMultipartUpload(String bucket, String path) {
        return bucket + "TEMP";
    }

    @Override
    public Part multipartUpload(String bucket, String path, Integer chunk, InputStream data, String uploadId)
            throws IOException {
        //分片上传
        final String key = chunk + path;
        String save = "";
        if (!vertxFs.existsBlocking(uploadId + key)) {
            save = save(data, uploadId, key);
        }
        final Part part = new Part();
        part.setPartNumber(chunk);
        part.setEtag(save);
        return part;
    }

    @Override
    public String completeMultipartUpload(String bucket, String path, List<Part> parts, String uploadId)
            throws IOException {
        final String finalPath = finalPath(bucket, path);
        createParentDirs(finalPath);

        //排序
        final List<Part> sorted = parts.stream()
                .sorted(Comparator.comparing(Part::getPartNumber))
                .collect(Collectors.toList());

        //文件合并
        final AsyncFile asyncFile = vertxFs.openBlocking(finalPath, new OpenOptions().setAppend(true).setDsync(true));
        sorted.forEach(part -> asyncFile.write(vertxFs.readFileBlocking(part.getEtag())));

        //关闭流
        asyncFile.end();

        //删除临时文件目录
        vertxFs.deleteRecursiveBlocking(uploadId, true);
        return finalPath;
    }

    @Override
    public String getExpireUrl(String bucketName, String key, Long expireMillisecond) {
        throw new UnsupportedOperationException("本地文件不支持获取url操作");
    }


    @Override
    public boolean dropFile(String bucket, String path) throws IOException {
        final String finalPath = finalPath(bucket, path);
        vertxFs.delete(finalPath);
        return true;
    }

    @Override
    public void dropFileAsync(String bucket, String path, Callback<Boolean, IOException> callback) {
            vertx.<Boolean>executeBlocking(promise-> CompletableFuture.supplyAsync(()-> {
                boolean b = false;
                try {
                    b = dropFile(bucket, path);
                } catch (IOException e) {
                    promise.fail(e);
                }
                return b;
            }).whenComplete((r, err)-> {
               if (r != null) {
                   promise.complete(r);
               } else {
                   promise.fail(err);
               }
            }), res-> {
                if (res.succeeded()) {
                    callback.onSuccess(res.result());
                } else {
                    if (res.cause() instanceof  IOException){
                        callback.onException((IOException) res.cause());
                    } else {
                        throw new RuntimeException("vertx file 删除异常", res.cause());
                    }
                }
            });
    }

    private String finalPath(String bucket, String path) {
        String finalPath;
        if (Strings.isNullOrEmpty(bucket) || path.startsWith(File.separator)) {
            finalPath = path;
        } else {
            finalPath = bucket + File.separator + path;
        }
        return finalPath;
    }

    private void createParentDirs(String path) throws IOException {
        checkNotNull(path);
        final File file = new File(path);
        final File parent = file.getCanonicalFile().getParentFile();
        if (parent == null) {
            /*
             * The given directory is a filesystem root. All zero of its ancestors exist. This doesn't
             * mean that the root itself exists -- consider x:\ on a Windows machine without such a drive
             * -- or even that the caller can create it, but this method makes no such guarantees even for
             * non-root files.
             */
            return;
        }
        boolean ignored = parent.mkdirs();
        if (!parent.isDirectory()) {
            throw new IOException("Unable to create parent directories of " + file);
        }
    }
}
