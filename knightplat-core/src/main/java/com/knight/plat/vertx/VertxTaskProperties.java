package com.knight.plat.vertx;

import io.vulcan.worker.PoolProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("knightplat.task")
@SuppressWarnings("unused")
public class VertxTaskProperties {
    private PoolProperties pool = new PoolProperties();

    public PoolProperties getPool() {
        return pool;
    }

    public void setPool(PoolProperties pool) {
        this.pool = pool;
    }
}
