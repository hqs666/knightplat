package com.knight.plat.vertx;

import com.knight.plat.api.executor.RunnableFilter;
import com.knight.plat.api.task.Task;
import com.knight.plat.api.task.TaskStatus;
import com.knight.plat.api.task.TaskStrategy;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import io.vulcan.worker.PoolProperties;
import io.vulcan.worker.WorkerPool;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class VertxTaskStrategy implements TaskStrategy {

    private final Vertx vertx;
    private final WorkerPool workerPool;
    private final List<RunnableFilter> runnableFilters;
    private final Map<String, Task<?>> taskMap = new ConcurrentHashMap<>();

    private final Executor syncExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {

        private final ThreadGroup group = System.getSecurityManager() != null ? System.getSecurityManager().getThreadGroup() : Thread.currentThread().getThreadGroup();

        @Override
        public Thread newThread(@NotNull Runnable r) {
            Thread t = new Thread(group, r, "knight-sync-task-1", 0);
            if (t.isDaemon())
                t.setDaemon(false);
            if (t.getPriority() != Thread.NORM_PRIORITY)
                t.setPriority(Thread.NORM_PRIORITY);
            return t;
        }
    });

    private static final String SYNC_TASK_TOPIC = "_knightplat_sync_task_topic";

    public VertxTaskStrategy(Vertx vertx, VertxTaskProperties taskProperties, List<RunnableFilter> filters) {
        final PoolProperties poolProperties = taskProperties.getPool();
        this.vertx = vertx;
        this.workerPool = WorkerPool.create("async-task", poolProperties.getCoreSize(), poolProperties.getMaxSize(),
                poolProperties.getKeepAlive().toMillis(), TimeUnit.MILLISECONDS, poolProperties.getQueueCapacity());
        this.runnableFilters = filters;
        this.workerPool.setFilters(filters);
        final MessageConsumer<String> consumer = vertx.eventBus().consumer(SYNC_TASK_TOPIC);
        consumer.handler(msg -> {
            final String taskId = msg.body();
            final Task<?> task = taskMap.get(taskId);
            if (task != null) {
                syncExecutor.execute(filterTask(() -> {
                    try {
                        task.run();
                    } catch (Throwable e) {
                        task.forceFail(e);
                    }
                }));
            }
        });
    }

    @Override
    public void start(final Task<?> task) {
        if (task == null) {
            throw new RuntimeException("待执行的task为null");
        }
        taskMap.put(task.getId(), task);
        final CompletionStage<Void> stage = workerPool.execute(task::run);
        stage.whenComplete((r, e) -> {
            if (e != null) {
                task.forceFail(e);
            }
        });
    }

    @Override
    public void syncStart(final Task<?> task) {
        if (task == null) {
            throw new RuntimeException("待执行的task为null");
        }
        taskMap.put(task.getId(), task);
        vertx.eventBus().send(SYNC_TASK_TOPIC, task.getId());
    }

    @Override
    public void loop(final Task<?> task) {
        if (task == null) {
            throw new RuntimeException("待执行的task为null");
        }
        taskMap.put(task.getId(), task);
        new Thread(filterTask(() -> {
            while (TaskStatus.RUNNING.equals(task.getStatus())) {
                task.run();
            }
        })).start();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <R> Task<R> getTask(String id) {
        return (Task<R>) taskMap.get(id);
    }

    @Override
    public synchronized void remove(String id) {
        final Task<?> task = taskMap.remove(id);
        if (task != null) {
            task.cancel();
        }
    }

    @Override
    public synchronized void clear() {
        taskMap.values().forEach(Task::cancel);
        taskMap.clear();
    }

    private Runnable filterTask(Runnable task) {
        if (runnableFilters == null || runnableFilters.isEmpty()) {
            return task;
        }
        Runnable result = task;
        for (RunnableFilter filter : runnableFilters) {
            result = filter.filter(result);
        }
        return result;
    }
}
