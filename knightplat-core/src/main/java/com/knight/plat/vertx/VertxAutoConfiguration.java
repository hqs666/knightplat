package com.knight.plat.vertx;

import com.knight.plat.api.eventbus.SchemaStrategyPair;
import com.knight.plat.api.executor.RunnableFilter;
import com.knight.plat.api.filesystem.FileChannelPair;
import com.knight.plat.api.task.TaskStrategy;
import com.knight.plat.async.AsyncManager;
import com.knight.plat.eventbus.EventBusAutoConfiguration;
import com.knight.plat.filesystem.LocalFile;
import com.knight.plat.task.TaskAutoConfiguration;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.task.TaskExecutionAutoConfiguration;
import org.springframework.boot.autoconfigure.task.TaskExecutionProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.util.List;

/**
 * Vertx integration setup
 * Created  on 2021/1/15.
 * @author Knight
 */
@AutoConfiguration(before = {EventBusAutoConfiguration.class, TaskAutoConfiguration.class},
        after = TaskExecutionAutoConfiguration.class)
@EnableConfigurationProperties({VertxProperties.class, VertxTaskProperties.class})
public class VertxAutoConfiguration {

    private final Logger log = LoggerFactory.getLogger(VertxAutoConfiguration.class);

    private final VertxProperties options;

    private final TaskExecutionProperties springTaskProperties;

    public VertxAutoConfiguration(VertxProperties options,
            TaskExecutionProperties springTaskProperties) {
        this.options = options;
        this.springTaskProperties = springTaskProperties;
    }

    @Lazy
    @Bean(destroyMethod = "close")
    @SuppressWarnings("unused")
    public Vertx vertx() {
        log.info("Initializing vertx instance.");
        if (options.getWorkerPoolSize() == -1) {
            options.setWorkerPoolSize(springTaskProperties.getPool().getCoreSize());
        }
        if (options.getInternalBlockingPoolSize() == -1) {
            options.setInternalBlockingPoolSize(springTaskProperties.getPool().getCoreSize());
        }
        final VertxOptions vertxOptions = options.toVertxOption();
        handleTracer(vertxOptions);
        return Vertx.vertx(vertxOptions);
    }

    @Lazy
    @Bean
    @SuppressWarnings("unused")
    public SchemaStrategyPair vertxEventStrategyInfo(Vertx vertx, AsyncManager asyncManager) {
        return new SchemaStrategyPair("local", new VertxEventStrategy(vertx, asyncManager));
    }

    @Lazy
    @Bean
    @SuppressWarnings("unused")
    public TaskStrategy vertxTaskStrategy(Vertx vertx, VertxTaskProperties taskProperties, List<RunnableFilter> filters) {
        return new VertxTaskStrategy(vertx, taskProperties, filters);
    }

    @Lazy
    @Bean
    @SuppressWarnings("unused")
    public FileChannelPair vertxFileChannelInfo(Vertx vertx) {
        return new FileChannelPair(new VertxFileManager(vertx), LocalFile.channel().getName());
    }

    private void handleTracer(VertxOptions vertxOptions) {
        // TODO 实现vertx的链路跟踪
//        final ServiceLoader<Tracer> loader = ServiceLoader.load(Tracer.class);
//        boolean loaded = false;
//        Tracer another = null;
//        for (Tracer tracer : loader) {
//            if ("org.apache.skywalking.apm.toolkit.opentracing.SkywalkingTracer".equals(tracer.getClass().getName())) {
//                vertxOptions.setTracingOptions(new OpenTracingOptions(tracer));
//                log.info("SkywalkingTracer loaded for vertx");
//                loaded = true;
//                break;
//            }
//            another = tracer;
//        }
//        if (!loaded && another != null) {
//            vertxOptions.setTracingOptions(new OpenTracingOptions(another));
//            log.info("{} loaded for vertx", another.getClass());
//            loaded = true;
//        }
//        if (!loaded) {
//            log.info("No Tracer implementation found");
//        }
    }
}
