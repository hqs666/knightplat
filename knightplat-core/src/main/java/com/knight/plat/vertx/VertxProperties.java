package com.knight.plat.vertx;

import io.vertx.core.VertxOptions;
import io.vertx.core.dns.AddressResolverOptions;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.file.FileSystemOptions;
import io.vertx.core.metrics.MetricsOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.core.tracing.TracingOptions;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.concurrent.TimeUnit;

@ConfigurationProperties("knightplat.vertx")
@SuppressWarnings("unused")
public class VertxProperties {

    private int eventLoopPoolSize = VertxOptions.DEFAULT_EVENT_LOOP_POOL_SIZE;
    private int workerPoolSize = -1;
    private int internalBlockingPoolSize = -1;
    private long blockedThreadCheckInterval = VertxOptions.DEFAULT_BLOCKED_THREAD_CHECK_INTERVAL;
    private long maxEventLoopExecuteTime = VertxOptions.DEFAULT_MAX_EVENT_LOOP_EXECUTE_TIME;
    private long maxWorkerExecuteTime = VertxOptions.DEFAULT_MAX_WORKER_EXECUTE_TIME;
    private ClusterManager clusterManager;
    private boolean haEnabled = VertxOptions.DEFAULT_HA_ENABLED;
    private int quorumSize = VertxOptions.DEFAULT_QUORUM_SIZE;
    private String haGroup = VertxOptions.DEFAULT_HA_GROUP;
    private MetricsOptions metricsOptions = new MetricsOptions();
    private TracingOptions tracingOptions;
    private FileSystemOptions fileSystemOptions = new FileSystemOptions();
    private long warningExceptionTime = TimeUnit.SECONDS.toNanos(5);
    private EventBusOptions eventBusOptions = new EventBusOptions();
    private AddressResolverOptions addressResolverOptions = new AddressResolverOptions();
    private boolean preferNativeTransport = VertxOptions.DEFAULT_PREFER_NATIVE_TRANSPORT;
    private TimeUnit maxEventLoopExecuteTimeUnit = VertxOptions.DEFAULT_MAX_EVENT_LOOP_EXECUTE_TIME_UNIT;
    private TimeUnit maxWorkerExecuteTimeUnit = VertxOptions.DEFAULT_MAX_WORKER_EXECUTE_TIME_UNIT;
    private TimeUnit warningExceptionTimeUnit = VertxOptions.DEFAULT_WARNING_EXCEPTION_TIME_UNIT;
    private TimeUnit blockedThreadCheckIntervalUnit = VertxOptions.DEFAULT_BLOCKED_THREAD_CHECK_INTERVAL_UNIT;

    public VertxOptions toVertxOption() {
        VertxOptions result = new VertxOptions();
        result.setEventLoopPoolSize(this.eventLoopPoolSize);
        result.setWorkerPoolSize(this.workerPoolSize);
        result.setInternalBlockingPoolSize(this.internalBlockingPoolSize);
        result.setBlockedThreadCheckInterval(this.blockedThreadCheckInterval);
        result.setMaxEventLoopExecuteTime(this.maxEventLoopExecuteTime);
        result.setMaxWorkerExecuteTime(this.maxWorkerExecuteTime);
        result.setClusterManager(this.clusterManager);
        result.setHAEnabled(this.haEnabled);
        result.setQuorumSize(this.quorumSize);
        result.setHAGroup(this.haGroup);
        result.setMetricsOptions(this.metricsOptions);
        result.setTracingOptions(this.tracingOptions);
        result.setFileSystemOptions(this.fileSystemOptions);
        result.setWarningExceptionTime(this.warningExceptionTime);
        result.setEventBusOptions(this.eventBusOptions);
        result.setAddressResolverOptions(this.addressResolverOptions);
        result.setPreferNativeTransport(this.preferNativeTransport);
        result.setMaxEventLoopExecuteTimeUnit(this.maxEventLoopExecuteTimeUnit);
        result.setMaxWorkerExecuteTimeUnit(this.maxWorkerExecuteTimeUnit);
        result.setWarningExceptionTimeUnit(this.warningExceptionTimeUnit);

        return result;
    }

    public int getEventLoopPoolSize() {
        return eventLoopPoolSize;
    }

    public void setEventLoopPoolSize(int eventLoopPoolSize) {
        this.eventLoopPoolSize = eventLoopPoolSize;
    }

    public int getWorkerPoolSize() {
        return workerPoolSize;
    }

    public void setWorkerPoolSize(int workerPoolSize) {
        this.workerPoolSize = workerPoolSize;
    }

    public int getInternalBlockingPoolSize() {
        return internalBlockingPoolSize;
    }

    public void setInternalBlockingPoolSize(int internalBlockingPoolSize) {
        this.internalBlockingPoolSize = internalBlockingPoolSize;
    }

    public long getBlockedThreadCheckInterval() {
        return blockedThreadCheckInterval;
    }

    public void setBlockedThreadCheckInterval(long blockedThreadCheckInterval) {
        this.blockedThreadCheckInterval = blockedThreadCheckInterval;
    }

    public long getMaxEventLoopExecuteTime() {
        return maxEventLoopExecuteTime;
    }

    public void setMaxEventLoopExecuteTime(long maxEventLoopExecuteTime) {
        this.maxEventLoopExecuteTime = maxEventLoopExecuteTime;
    }

    public long getMaxWorkerExecuteTime() {
        return maxWorkerExecuteTime;
    }

    public void setMaxWorkerExecuteTime(long maxWorkerExecuteTime) {
        this.maxWorkerExecuteTime = maxWorkerExecuteTime;
    }

    public ClusterManager getClusterManager() {
        return clusterManager;
    }

    public void setClusterManager(ClusterManager clusterManager) {
        this.clusterManager = clusterManager;
    }

    public boolean isHaEnabled() {
        return haEnabled;
    }

    public void setHaEnabled(boolean haEnabled) {
        this.haEnabled = haEnabled;
    }

    public int getQuorumSize() {
        return quorumSize;
    }

    public void setQuorumSize(int quorumSize) {
        this.quorumSize = quorumSize;
    }

    public String getHaGroup() {
        return haGroup;
    }

    public void setHaGroup(String haGroup) {
        this.haGroup = haGroup;
    }

    public MetricsOptions getMetricsOptions() {
        return metricsOptions;
    }

    public void setMetricsOptions(MetricsOptions metricsOptions) {
        this.metricsOptions = metricsOptions;
    }

    public TracingOptions getTracingOptions() {
        return tracingOptions;
    }

    public void setTracingOptions(TracingOptions tracingOptions) {
        this.tracingOptions = tracingOptions;
    }

    public FileSystemOptions getFileSystemOptions() {
        return fileSystemOptions;
    }

    public void setFileSystemOptions(FileSystemOptions fileSystemOptions) {
        this.fileSystemOptions = fileSystemOptions;
    }

    public long getWarningExceptionTime() {
        return warningExceptionTime;
    }

    public void setWarningExceptionTime(long warningExceptionTime) {
        this.warningExceptionTime = warningExceptionTime;
    }

    public EventBusOptions getEventBusOptions() {
        return eventBusOptions;
    }

    public void setEventBusOptions(EventBusOptions eventBusOptions) {
        this.eventBusOptions = eventBusOptions;
    }

    public AddressResolverOptions getAddressResolverOptions() {
        return addressResolverOptions;
    }

    public void setAddressResolverOptions(AddressResolverOptions addressResolverOptions) {
        this.addressResolverOptions = addressResolverOptions;
    }

    public boolean isPreferNativeTransport() {
        return preferNativeTransport;
    }

    public void setPreferNativeTransport(boolean preferNativeTransport) {
        this.preferNativeTransport = preferNativeTransport;
    }

    public TimeUnit getMaxEventLoopExecuteTimeUnit() {
        return maxEventLoopExecuteTimeUnit;
    }

    public void setMaxEventLoopExecuteTimeUnit(TimeUnit maxEventLoopExecuteTimeUnit) {
        this.maxEventLoopExecuteTimeUnit = maxEventLoopExecuteTimeUnit;
    }

    public TimeUnit getMaxWorkerExecuteTimeUnit() {
        return maxWorkerExecuteTimeUnit;
    }

    public void setMaxWorkerExecuteTimeUnit(TimeUnit maxWorkerExecuteTimeUnit) {
        this.maxWorkerExecuteTimeUnit = maxWorkerExecuteTimeUnit;
    }

    public TimeUnit getWarningExceptionTimeUnit() {
        return warningExceptionTimeUnit;
    }

    public void setWarningExceptionTimeUnit(TimeUnit warningExceptionTimeUnit) {
        this.warningExceptionTimeUnit = warningExceptionTimeUnit;
    }

    public TimeUnit getBlockedThreadCheckIntervalUnit() {
        return blockedThreadCheckIntervalUnit;
    }

    public void setBlockedThreadCheckIntervalUnit(TimeUnit blockedThreadCheckIntervalUnit) {
        this.blockedThreadCheckIntervalUnit = blockedThreadCheckIntervalUnit;
    }
}
