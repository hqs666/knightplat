package com.knight.plat.vertx;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.knight.plat.api.commons.Callback;
import com.knight.plat.api.eventbus.Actions;
import com.knight.plat.api.eventbus.ConsumerHandler;
import com.knight.plat.api.eventbus.ErrorHandler;
import com.knight.plat.api.eventbus.EventStrategy;
import com.knight.plat.async.AsyncManager;
import com.knight.plat.utils.JsonUtils;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 基于Vertx的eventbus实现事件处理
 * Add on 2021/1/20.
 * @author Knight
 */
public class VertxEventStrategy implements EventStrategy {

    private final Logger log = LoggerFactory.getLogger("knightplat-eventbus-local");

    private final Vertx vertx;
    private final AsyncManager asyncManager;

    @SuppressWarnings("rawtypes")
    private final List<MessageConsumer> consumerList = new CopyOnWriteArrayList<>();
    private final Set<Actions> actions = Sets.newEnumSet(Lists.newArrayList(Actions.SEND, Actions.PUBLISH, Actions.CONSUME), Actions.class);

    public VertxEventStrategy(Vertx vertx, AsyncManager asyncManager) {
        this.vertx = vertx;
        this.asyncManager = asyncManager;
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumer(router, clazz, handler, e -> log.error("Received message from local topic: " + router + ", but fail on handling", e));
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler) {
        consumerSync(router, clazz, handler, e -> log.error("Received message from local topic: " + router + ", but fail on handling", e));
    }

    @Override
    public <T> void consumer(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        MessageConsumer<byte[]> vertxConsumer = vertx.eventBus().consumer(router);
        vertxConsumer.handler(msg -> asyncManager.stage(() -> {
            try {
                handler.handle(decodeValue(msg.body(), clazz, JsonUtils::decode));
            } catch (Throwable e) {
                errorHandler.handle(e);
            }
        }));
        consumerList.add(vertxConsumer);
    }

    @Override
    public <T> void consumerSync(String router, Class<T> clazz, ConsumerHandler<T> handler, ErrorHandler errorHandler) {
        MessageConsumer<byte[]> vertxConsumer = vertx.eventBus().consumer(router);
        vertxConsumer.handler(msg -> {
            try {
                handler.handle(decodeValue(msg.body(), clazz, JsonUtils::decode));
            } catch (Throwable e) {
                errorHandler.handle(e);
            }
        });
        consumerList.add(vertxConsumer);
    }

    @Override
    public <T> void send(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        try {
            vertx.eventBus().send(router, encodeValue(payload, clazz, JsonUtils::encodeToBytes));
            result.onSuccess(payload);
        } catch (Throwable e) {
            result.onException(e);
        }
    }

    @Override
    public <T> void publish(String router, Class<T> clazz, T payload, Callback<T, Throwable> result) {
        try {
            vertx.eventBus().publish(router, encodeValue(payload, clazz, JsonUtils::encodeToBytes));
            result.onSuccess(payload);
        } catch (Throwable e) {
            result.onException(e);
        }
    }

    @Override
    public Set<Actions> supportedActions() {
        return actions;
    }

    @Override
    public void close() {
        consumerList.forEach(MessageConsumer::unregister);
    }
}
