/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.autoconfiguration;

import com.knight.plat.api.log.LogProvider;
import com.knight.plat.api.utils.HolidayManager;
import com.knight.plat.api.utils.KeywordManager;
import com.knight.plat.api.utils.SequenceManager;
import com.knight.plat.utils.BeanConvertUtils;
import com.knight.plat.utils.SpringContextUtils;
import com.knight.plat.utils.UtilsInit;
import com.knight.plat.utils.helper.SimpleHolidayManager;
import com.knight.plat.utils.helper.SimpleLogProvider;
import com.knight.plat.utils.helper.SimpleSequenceManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;

/**
 * Created on 16/8/9. 关于工具类的一些配置
 */
@AutoConfiguration
@Import({SpringContextUtils.class, BeanConvertUtils.class, UtilsInit.class})
// Import to active the spring init process.
public class UtilsAutoConfiguration {

    private static final Log log = LogFactory.getLog(UtilsAutoConfiguration.class);

    // TODO: 2016/12/10 工具类初始化方式需要全面优化来增加健壮度以及减少环境依赖, 使逻辑更加严谨

    // 默认实现部分

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnClass(LogProvider.class)
    @Order
    static class LoggerConfig { // Logger config.

        @Bean
        @ConditionalOnMissingBean
        LogProvider logProvider() {
            return new SimpleLogProvider();
        }
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnClass(HolidayManager.class)
    @Order
    static class WorkdayConfig { // Work day utils config.

        @Bean
        @ConditionalOnMissingBean
        HolidayManager holidayManager() {
            return new SimpleHolidayManager();
        }
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnClass(KeywordManager.class)
    @Order
    static public class SequenceConfig { // Sequence generator config.

        @Bean
        @ConditionalOnMissingBean
        SequenceManager sequenceManager() {
            return new SimpleSequenceManager();
        }
    }

}
