/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.autoconfiguration;

import com.knight.plat.api.utils.ValueSetManager;
import com.knight.plat.autoconfiguration.defaults.InMemoryValueSetManager;

/**
 * Created on 2016/11/17. Value set config.
 */
//@Configuration
//@Import(ValueSetUtils.class)
public class ValueSetAutoConfiguration {

    //    @Bean(initMethod = "load", destroyMethod = "clean")
//    @ConditionalOnMissingBean
//    @AutoConfigureOrder(Ordered.LOWEST_PRECEDENCE)
    public ValueSetManager valueSetManager() {
        return new InMemoryValueSetManager();
    }
}
