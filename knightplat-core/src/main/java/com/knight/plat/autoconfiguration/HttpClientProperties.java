package com.knight.plat.autoconfiguration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.StringJoiner;

@ConfigurationProperties("knightplat.http.client")
public class HttpClientProperties {
    private String proxyServer;
    private Integer proxyPort;
    private Long connectTimeout;
    private Long readTimeout;

    private Long writeTimeout;

    private Integer poolSize;

    private Long keepAliveDuration;

    public String getProxyServer() {
        return proxyServer;
    }

    public void setProxyServer(String proxyServer) {
        this.proxyServer = proxyServer;
    }

    public Integer getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(Integer proxyPort) {
        this.proxyPort = proxyPort;
    }

    public Long getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Long connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public Long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public Long getWriteTimeout() {
        return writeTimeout;
    }

    public void setWriteTimeout(Long writeTimeout) {
        this.writeTimeout = writeTimeout;
    }

    public Integer getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(Integer poolSize) {
        this.poolSize = poolSize;
    }

    public Long getKeepAliveDuration() {
        return keepAliveDuration;
    }

    public void setKeepAliveDuration(Long keepAliveDuration) {
        this.keepAliveDuration = keepAliveDuration;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", HttpClientProperties.class.getSimpleName() + "[", "]")
                .add("proxyServer='" + proxyServer + "'")
                .add("proxyPort=" + proxyPort)
                .add("connectTimeout=" + connectTimeout)
                .add("readTimeout=" + readTimeout)
                .toString();
    }
}
