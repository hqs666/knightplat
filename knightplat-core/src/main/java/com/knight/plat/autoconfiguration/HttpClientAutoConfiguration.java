/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.autoconfiguration;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Created on 2016/10/17.
 */
@AutoConfiguration
@EnableConfigurationProperties({HttpProxyProperties.class, HttpClientProperties.class})
public class HttpClientAutoConfiguration {

}
