/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.autoconfiguration.defaults;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.knight.plat.api.commons.CodeValue;
import com.knight.plat.api.utils.ValueSetManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 2016/11/17. Default in memory implement of ValueSetManager, load from values.dict
 *
 * @see com.knight.plat.api.utils.ValueSetManager
 */
public class InMemoryValueSetManager implements ValueSetManager {

    private static final Map<String, CodeValue> map = Collections.synchronizedMap(new LinkedHashMap<String, CodeValue>());
    private final Log log = LogFactory.getLog(InMemoryValueSetManager.class);

    @Override
    public void load() {
        try {
            Enumeration<URL> resources
                    = Thread.currentThread().getContextClassLoader().getResources("values.json");
            while (resources.hasMoreElements()) {
                URL element = resources.nextElement();
                InputStream is = element.openStream();
                ObjectMapper objectMapper = new ObjectMapper();
                List<CodeValue> values = objectMapper.readValue(
                        is, objectMapper.getTypeFactory()
                                .constructCollectionType(List.class, CodeValue.class));
                for (CodeValue value : values) {
                    map.put(value.getGroup() + "." + value.getCode(), value);
                }
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Load keyword error.", e);
            }
        }
    }

    @Override
    public CodeValue getValue(String setGroup, String code) {
        return map.get(setGroup + "." + code);
    }

    @Override
    public CodeValue getValueById(String id) {
        for (Map.Entry<String, CodeValue> e : map.entrySet()) {
            if (e.getValue().getId().equals(id)) {
                return e.getValue();
            }
        }
        return null;
    }

    @Override
    public Map<String, CodeValue> getValuesByGroup(final String setGroup) {
        return Maps.filterValues(map, new Predicate<CodeValue>() {
            @Override
            public boolean apply(CodeValue codeValue) {
                return codeValue.getGroup().equals(setGroup);
            }
        });
    }

    @Override
    public Map<String, CodeValue> getValuesByParentId(final String parentId) {
        return Maps.filterValues(map, new Predicate<CodeValue>() {
            @Override
            public boolean apply(CodeValue codeValue) {
                return codeValue.getParentId().equals(parentId);
            }
        });
    }

    @Override
    public void clean() {
        map.clear();
    }
}
