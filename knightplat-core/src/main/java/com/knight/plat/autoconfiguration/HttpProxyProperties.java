package com.knight.plat.autoconfiguration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 项目名: knightplat
 * 文件名: ProxyProperties
 * 模块说明:
 * 修改历史:
 * 2021/6/3 - Knight - 创建
 */
@ConfigurationProperties("knightplat.proxy")
public class HttpProxyProperties {
    private String proxyServer;
    private Integer port;

    public String getProxyServer() {
        return proxyServer;
    }

    public void setProxyServer(String proxyServer) {
        this.proxyServer = proxyServer;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
