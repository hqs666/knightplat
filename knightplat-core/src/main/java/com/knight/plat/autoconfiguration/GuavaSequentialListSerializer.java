/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.autoconfiguration;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.serializers.CollectionSerializer;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created on 2016/11/7.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class GuavaSequentialListSerializer extends CollectionSerializer {

    @Override
    public Collection read(Kryo kryo, Input input, Class type) {
        Class<Collection> subType = (Class<Collection>) LinkedList.class.asSubclass(Collection.class);
        return super.read(kryo, input, subType);
    }

}
