/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.autoconfiguration;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.serializers.MapSerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 2021/01/20.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class MapInterfaceSerializer extends MapSerializer {

    @Override
    public Map read(Kryo kryo, Input input, Class type) {
        Class<Map> subType = (Class<Map>) HashMap.class.asSubclass(Map.class);
        return super.read(kryo, input, subType);
    }

}
