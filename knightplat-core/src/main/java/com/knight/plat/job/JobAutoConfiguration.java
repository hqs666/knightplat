package com.knight.plat.job;

import com.knight.plat.api.job.JobManager;
import com.knight.plat.api.job.JobStrategyPair;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.List;

@AutoConfiguration
public class JobAutoConfiguration {

    @Bean
    public JobManager jobManager(List<JobStrategyPair> strategyPairs) {
        final JobManager jobManager = new JobManagerImpl();
        if (strategyPairs != null && !strategyPairs.isEmpty()) {
            strategyPairs.forEach(pairs -> jobManager.registerStrategy(pairs.getSchema(), pairs.getStrategy(), pairs.getLogger()));
        }
        return jobManager;
    }
}
