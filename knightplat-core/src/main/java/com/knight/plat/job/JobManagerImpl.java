package com.knight.plat.job;

import com.knight.plat.api.job.JobHandler;
import com.knight.plat.api.job.JobLogger;
import com.knight.plat.api.job.JobManager;
import com.knight.plat.api.job.JobStrategy;

import java.util.LinkedHashMap;
import java.util.Map;

public final class JobManagerImpl implements JobManager {
    private final Map<String, JobStrategy> strategyRouter = new LinkedHashMap<>();
    private final Map<String, JobLogger> loggerRouter = new LinkedHashMap<>();

    JobManagerImpl() {}

    @Override
    public void registerStrategy(final String schema, final JobStrategy strategy, final JobLogger logger) {
        strategyRouter.put(schema, strategy);
        loggerRouter.put(schema, logger);
    }

    @Override
    public void registerJob(String executor, String name, JobHandler jobHandler) {
        final JobStrategy strategy = strategyRouter.get(executor);
        if (strategy == null) {
            throw new RuntimeException("Job executor: '" + executor + "' not exists.");
        }
        final JobLogger logger = loggerRouter.get(executor);
        strategy.registerJob(name, jobHandler, logger);
    }

    @Override
    public boolean hasExecutor(String executor) {
        return strategyRouter.containsKey(executor);
    }
}
