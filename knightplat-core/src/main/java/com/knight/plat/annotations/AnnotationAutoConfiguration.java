package com.knight.plat.annotations;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;

@AutoConfiguration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class AnnotationAutoConfiguration implements BeanPostProcessor, EnvironmentAware {

    private final List<AnnotationHandler> handlers = new ArrayList<>();

    @Override
    public void setEnvironment(@NotNull Environment environment) {
        handlers.add(new EventListenerHandler(environment));
        handlers.add(new JobRegisterHandler(environment));
    }

    @Override
    public Object postProcessAfterInitialization(@NotNull Object bean, @NotNull String beanName) throws BeansException {
        for (AnnotationHandler handler : handlers) {
            handler.handle(bean);
        }
        return bean;
    }
}
