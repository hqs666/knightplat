package com.knight.plat.annotations;

import com.knight.plat.api.annotations.Job;
import com.knight.plat.api.annotations.Job.Param;
import com.knight.plat.api.job.JobLogger;
import com.knight.plat.api.job.JobManager;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class JobRegisterHandler implements AnnotationHandler {

    private static final Logger log = LoggerFactory.getLogger(JobRegisterHandler.class);

    private JobManager jobManager;

    private final Environment environment;

    private final List<HandlerMeta> tempList = new ArrayList<>();

    JobRegisterHandler(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void handle(@NotNull Object bean) {
        if (jobManager == null && bean instanceof JobManager) {
            this.jobManager = (JobManager) bean;
            registerTempList(this.jobManager, tempList);
            return;
        }

        final Method[] methods = bean.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (!Modifier.isPublic(method.getModifiers())) {
                continue; // 跳过非public方法
            }
            method.setAccessible(true);
            final Job job = AnnotationUtils.findAnnotation(method, Job.class);
            if (job == null || job.executor().length == 0) {
                continue; // 跳过无注解方法
            }
            final Parameter[] parameters = method.getParameters();
            int argIndex = 0;
            int loggerIndex = -1;
            int paramIndex = -1;
            while (argIndex < parameters.length) {
                if (loggerIndex == -1 && parameters[argIndex].getType().isAssignableFrom(JobLogger.class)) {
                    loggerIndex = argIndex;
                } else if (paramIndex == -1 && AnnotationUtils.getAnnotation(parameters[argIndex], Param.class) != null) {
                    paramIndex = argIndex;
                }
                argIndex++;
            }
            if (jobManager == null) {
                tempList.add(HandlerMeta.of(job.executor(), job.name(), parameters.length, loggerIndex, paramIndex, bean, method));
            } else {
                registerTempList(this.jobManager, tempList);
                registerJob(jobManager, job.executor(), job.name(), parameters.length, loggerIndex, paramIndex, bean, method);
            }
        }
    }

    private void registerJob(JobManager jobManager, String[] executors, String nameOrigin, int argLen, int loggerIndex, int paramIndex, @NotNull Object bean, @NotNull Method method) {
        final String name = environment.resolvePlaceholders(nameOrigin);
        for (String executorOrigin : executors) {
            final String executor = environment.resolvePlaceholders(executorOrigin);
            if (!jobManager.hasExecutor(executor)) {
                log.warn("Job executor: " + executor + "not found, job:<" + name + "> ignored");
            }
            jobManager.registerJob(executor, name, ((params, logger) -> {
                if (argLen == 0) {
                    method.invoke(bean);
                    return;
                }
                final Object[] args = new Object[argLen];
                if (loggerIndex != -1) {
                    args[loggerIndex] = logger;
                }
                if (paramIndex != -1) {
                    args[paramIndex] = params;
                }
                method.invoke(bean, args);
            }));
            log.info("Bind job:<" + name + "> to method:<" + method.getName() + "> using executor: " + executor);
        }
    }

    private void registerTempList(JobManager jobManager, List<HandlerMeta> tempList) {
        if (!tempList.isEmpty()) {
            for (HandlerMeta meta : tempList) {
                registerJob(jobManager, meta.executors, meta.name, meta.argLen, meta.loggerIndex, meta.paramIndex, meta.bean, meta.method);
            }
            tempList.clear();
        }
    }

    private static class HandlerMeta {
        String[] executors;
        String name;
        int argLen;
        int loggerIndex;
        int paramIndex;
        Object bean;
        Method method;

        static HandlerMeta of(String[] executors, String name, int argLen, int loggerIndex, int paramIndex, @NotNull Object bean, @NotNull Method method) {
            HandlerMeta meta = new HandlerMeta();
            meta.executors = executors;
            meta.name = name;
            meta.argLen = argLen;
            meta.loggerIndex = loggerIndex;
            meta.paramIndex = paramIndex;
            meta.bean = bean;
            meta.method = method;
            return meta;
        }
    }

}
