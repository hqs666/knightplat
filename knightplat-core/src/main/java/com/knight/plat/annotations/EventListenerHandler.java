package com.knight.plat.annotations;

import com.knight.plat.api.annotations.EventListener;
import com.knight.plat.api.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySourcesPropertyResolver;
import org.springframework.util.PropertyPlaceholderHelper;
import org.springframework.util.StringValueResolver;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class EventListenerHandler implements AnnotationHandler {

    private static final Logger log = LoggerFactory.getLogger(EventListenerHandler.class);

    private EventBus eventBus;

    private final Environment environment;

    private final List<HandlerMeta<?>> tempList = new ArrayList<>();

    EventListenerHandler(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void handle(@NotNull Object bean) {
        if (this.eventBus == null && bean instanceof EventBus) {
            this.eventBus = (EventBus) bean;
            registerTempList(this.eventBus, tempList);
            return;
        }

        final Method[] methods = bean.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (!Modifier.isPublic(method.getModifiers())) {
                continue; // 跳过非public方法
            }
            method.setAccessible(true);
            final EventListener eventListener = AnnotationUtils.findAnnotation(method, EventListener.class);
            if (eventListener == null || eventListener.router().length == 0) {
                continue; // 跳过无注解方法
            }
            final Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length == 0) {
                continue; // 跳过无参数方法
            }
            final Class<?> argType = parameterTypes[0];
            if (eventBus == null) {
                tempList.add(HandlerMeta.of(eventListener.router(), argType, parameterTypes.length, bean, method));
            } else {
                registerTempList(eventBus, tempList);
                register(eventBus, eventListener.router(), argType, parameterTypes.length, bean, method);
            }
        }
    }

    private <T> void register(@NotNull EventBus eventBus, String[] routers,
                              Class<T> clazz, int argLen, @NotNull Object bean, @NotNull Method method) {
        for (String routerOrigin : routers) {
            //解析spel表达式
            String router = environment.resolvePlaceholders(routerOrigin);
            eventBus.register(router, clazz, msg -> {
                final Object[] args = new Object[argLen];
                args[0] = msg;
                method.invoke(bean, args);
            });
            log.info("Bind event router:<" + router + "> consumer to method:<" + method.getName() + ">");
        }
    }

    private void registerTempList(EventBus eventBus, List<HandlerMeta<?>> tempList) {
        if (!tempList.isEmpty()) {
            for (HandlerMeta<?> meta : tempList) {
                register(eventBus, meta.routers, meta.clazz, meta.argLen, meta.bean, meta.method);
            }
            tempList.clear();
        }
    }

    private static class HandlerMeta<T> {
        String[] routers;
        Class<T> clazz;
        int argLen;
        Object bean;
        Method method;

        static <ET> HandlerMeta<ET> of(String[] routers,
                                       Class<ET> clazz, int argLen, @NotNull Object bean, @NotNull Method method) {
            HandlerMeta<ET> meta = new HandlerMeta<>();
            meta.routers = routers;
            meta.clazz = clazz;
            meta.argLen = argLen;
            meta.bean = bean;
            meta.method = method;
            return meta;
        }
    }
}
