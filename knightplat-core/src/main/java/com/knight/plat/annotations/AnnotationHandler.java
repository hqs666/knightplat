package com.knight.plat.annotations;

import org.jetbrains.annotations.NotNull;

public interface AnnotationHandler {

    void handle(@NotNull Object bean);

}
