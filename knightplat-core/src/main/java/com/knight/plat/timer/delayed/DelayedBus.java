package com.knight.plat.timer.delayed;

import com.knight.plat.async.AsyncManager;
import com.knight.plat.timer.delayed.DelayBusPersistenceHandler.TaskMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * 延迟事件推送
 * EventBus作为核心库
 * 持久化延迟队列需要自行实现DelayBusPersistenceHandler进行持久化选择存储
 */
public abstract class DelayedBus {
    protected final AsyncManager asyncManager;
    protected final TimerManager timerManager;
    protected final List<DelayBusPersistenceHandler> persistenceHandlerList;

    private final static Logger log = LoggerFactory.getLogger(DelayedBus.class);

    protected DelayedBus(AsyncManager asyncManager, TimerManager timerManager,
                         List<DelayBusPersistenceHandler> persistenceHandlerList) {
        this.asyncManager = asyncManager;
        this.timerManager = timerManager;
        this.persistenceHandlerList = persistenceHandlerList;
    }

    protected abstract CompletableFuture<Boolean> containsTask(String id);

    protected abstract void cleanTask(String id);

    protected abstract void cleanAll();

    protected abstract <T> CompletableFuture<String> saveTask(TaskMeta<T> meta);

    private <T> void handleTaskDelivery(final boolean isPublish, final long delay, final TimeUnit timeUnit, TaskMeta<T> taskMeta, CompletableFuture<String> resultFuture) {
        final TimerFuture<T> timerFuture;
        if (isPublish) {
            timerFuture = timerManager.delayPublish(delay, timeUnit, taskMeta.getTopic(), taskMeta.getClazz(),
                    taskMeta.getData());
        } else {
            timerFuture = timerManager.delaySend(delay, timeUnit, taskMeta.getTopic(), taskMeta.getClazz(),
                    taskMeta.getData());
        }
        timerFuture.whenComplete((d, ex) -> {
            if (persistenceHandlerList != null && !persistenceHandlerList.isEmpty()) {
                if (ex != null) {
                    persistenceHandlerList.forEach(handler -> handler.onTaskFail(taskMeta, ex));
                } else {
                    persistenceHandlerList.forEach(handler -> handler.onTaskFinish(taskMeta));
                }
            }
            cleanTask(timerFuture.getId());
        });
        resultFuture.complete(timerFuture.getId());
    }

    protected <T> CompletableFuture<String> delayDeliveryInner(final boolean isPublish, final long delay, final TimeUnit timeUnit, TaskMeta<T> taskMeta) {
        final String existId = taskMeta.getId();

        final CompletableFuture<String> resultFuture = new CompletableFuture<>();
        if (existId == null || existId.isEmpty()) {
            // 实际分发延迟任务
            handleTaskDelivery(isPublish, delay, timeUnit, taskMeta, resultFuture);
            return resultFuture;
        }

        containsTask(existId).whenComplete((isExist, e) -> {
            // 异常了
            if (e != null) {
                resultFuture.completeExceptionally(e);
                return;
            }

            // 存在了
            if (isExist) {
                resultFuture.complete(existId);
                return;
            }

            // 实际分发延迟任务
            handleTaskDelivery(isPublish, delay, timeUnit, taskMeta, resultFuture);
        });
        return resultFuture;
    }

    // 不调用持久化
    protected <T> CompletableFuture<String> delayPublishInner(long delay, TimeUnit timeUnit, TaskMeta<T> taskMeta) {
        return delayDeliveryInner(true, delay, timeUnit, taskMeta);
    }

    // 不调用持久化
    protected <T> CompletableFuture<String> delaySendInner(long delay, TimeUnit timeUnit, TaskMeta<T> taskMeta) {
        return delayDeliveryInner(false, delay, timeUnit, taskMeta);
    }

    private <T> CompletableFuture<String> delivery(boolean isPublish, long delay, TimeUnit timeUnit, final String router,
                                                   final Class<T> clazz, final T payload) {
        final CompletableFuture<String> resultFuture = new CompletableFuture<>();
        final long expectedTimeMillis = System.currentTimeMillis() + timeUnit.toMillis(delay);
        final TaskMeta<T> meta = DelayBusPersistenceHandler.createMeta(null, expectedTimeMillis, router, payload, clazz,
                false);
        meta.setPublish(isPublish);
        final CompletableFuture<String> idFuture = delayDeliveryInner(isPublish, delay, timeUnit, meta);
        idFuture.whenComplete((id, e) -> {
            if (e != null) {
                resultFuture.completeExceptionally(e);
                return;
            }

            meta.setId(id);
            saveTask(meta).whenComplete((saveId, saveEx) -> {
                if (saveEx != null) {
                    resultFuture.completeExceptionally(saveEx);
                    return;
                }
                resultFuture.complete(saveId);
            });
        });
        return resultFuture;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected int loadTask() {
        if (persistenceHandlerList != null && !persistenceHandlerList.isEmpty()) {
            final List<TaskMeta<?>> metaList = new ArrayList<>();
            for (DelayBusPersistenceHandler handler : persistenceHandlerList) {
                try {
                    final List<TaskMeta<?>> taskMetas = handler.loadTaskMeta();
                    if (taskMetas != null && !taskMetas.isEmpty()) {
                        metaList.addAll(taskMetas);
                    }
                } catch (Exception e) {
                    log.error("从持久化接口载入数据时发生异常", e);
                }
            }
            for (TaskMeta meta : metaList) {
                containsTask(meta.getId()).whenComplete((exists, e) -> {
                    if (e == null && !exists) {
                        long delayMillis = meta.getExpectedTimeMillis() - System.currentTimeMillis();
                        if (meta.isPublish()) {
                            delayPublishInner(delayMillis, TimeUnit.MILLISECONDS, meta);
                        } else {
                            delaySendInner(delayMillis, TimeUnit.MILLISECONDS, meta);
                        }
                    } else if (e != null) {
                        log.error("从持久化接口载入数据时发生异常", e);
                    }
                });
            }
            return metaList.size();
        }
        return 0;
    }

    /**
     * 延迟发送消息到EventBus队列中
     *
     * @param delay    延迟时间
     * @param timeUnit 延迟时间单位
     * @param router   队列router
     * @param clazz    发送数据类型的类对象
     * @param payload  发送数据
     * @param <T>      发送数据类型
     * @return 表示发送完成的Future
     */
    public <T> CompletableFuture<String> delaySend(long delay, TimeUnit timeUnit, final String router,
                                                   final Class<T> clazz, final T payload) {
        return delivery(false, delay, timeUnit, router, clazz, payload);
    }

    /**
     * 延迟发送广播消息到EventBus队列中
     *
     * @param delay    延迟时间
     * @param timeUnit 延迟时间单位
     * @param router   队列router
     * @param clazz    发送数据类型的类对象
     * @param payload  发送数据
     * @param <T>      发送数据类型
     * @return 表示发送完成的Future
     */
    public <T> CompletableFuture<String> delayPublish(long delay, TimeUnit timeUnit, final String router,
                                                      final Class<T> clazz, final T payload) {
        return delivery(true, delay, timeUnit, router, clazz, payload);
    }

    public boolean cancelMessage(String id) {
        cleanTask(id);
        return timerManager.cancel(id);
    }
}
