package com.knight.plat.timer.delayed;

import com.knight.plat.async.AsyncManager;
import com.knight.plat.timer.delayed.DelayBusPersistenceHandler.TaskMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class StandaloneDelayedBus extends DelayedBus {

    private final ConcurrentMap<String, TaskMeta<?>> taskMap = new ConcurrentHashMap<>();

    private final static Logger log = LoggerFactory.getLogger(StandaloneDelayedBus.class);

    public StandaloneDelayedBus(TimerManager timerManager,
            AsyncManager asyncManager,
            List<DelayBusPersistenceHandler> persistenceHandlerList) {
        super(asyncManager, timerManager, persistenceHandlerList);
        int loaded = loadTask();
        log.info("从持久化接口中恢复载入了 {} 条队列任务数据", loaded);
    }

    @Override
    protected CompletableFuture<Boolean> containsTask(String id) {
        return CompletableFuture.completedFuture(taskMap.containsKey(id));
    }

    @Override
    protected void cleanTask(String id) {
        taskMap.remove(id);
    }

    @Override
    protected void cleanAll() {
        taskMap.clear();
    }

    @Override
    protected <T> CompletableFuture<String> saveTask(TaskMeta<T> meta) {
        taskMap.put(meta.getId(), meta);
        final CompletableFuture<String> resultFuture = new CompletableFuture<>();
        if (persistenceHandlerList != null && !persistenceHandlerList.isEmpty()) {
            asyncManager.stage(() -> {
                for (DelayBusPersistenceHandler handler : persistenceHandlerList) {
                    final boolean result = handler.saveTaskMeta(meta);
                    if (!result) {
                        throw new IllegalStateException("延时任务持久化失败");
                    }
                }
                return true;
            }).whenComplete((r, e) -> {
                if (e != null) {
                    taskMap.remove(meta.getId());
                    resultFuture.completeExceptionally(e);
                } else {
                    resultFuture.complete(meta.getId());
                }
            });
        } else {
            resultFuture.complete(meta.getId());
        }
        return resultFuture;
    }
}
