package com.knight.plat.timer.delayed;

import com.knight.plat.api.eventbus.EventBus;
import com.knight.plat.async.AsyncManager;
import com.knight.plat.utils.KeyGenUtils;
import com.knight.plat.utils.SpringContextUtils;
import io.netty.util.internal.PlatformDependent;
import org.jctools.queues.MpscUnboundedArrayQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

public final class WheelTimer {

    private final ConcurrentMap<String, Timeout<?>> taskMap = new ConcurrentHashMap<>();

    private final static Logger log = LoggerFactory.getLogger(WheelTimer.class);

    // 小时轮，每格一分钟，1440格，共24小时一圈
    private final Wheel hourWheel = new Wheel(1, TimeUnit.MINUTES, 1440);

    // 分钟轮，每格5秒钟，720格，共60分钟一圈
    private final Wheel minuteWheel = new Wheel(5, TimeUnit.SECONDS, 720);

    // 秒轮，每格200毫秒，300格，共60秒
    private final Wheel secondWheel = new Wheel(200, TimeUnit.MILLISECONDS, 300);

    private final static long ONE_HOUR_NANO = TimeUnit.HOURS.toNanos(1); // 1小时纳秒数
    private final static long ONE_MINUTE_NANO = TimeUnit.MINUTES.toNanos(1); // 1分钟纳秒数
    private final static long ONE_SECONDS_NANO = TimeUnit.SECONDS.toNanos(1); // 1秒纳秒数

    private final static long DELAY_RESTART_TIME = 60 * 1000; //延迟一分钟执行

    private final EventBus eventBus;
    private final AsyncManager asyncManager;

    public WheelTimer(AsyncManager asyncManager, EventBus eventBus) {
        this.eventBus = eventBus;
        this.asyncManager = asyncManager;
    }

    /**
     * 延迟执行任务
     *
     * @param delay    延迟时间
     * @param timeUnit 延迟时间单位
     * @param task     执行的任务
     * @param <R>      任务返回值类型
     * @return 表示任务执行的Future对象
     */
    public <R> TimerFuture<R> delay(final long delay, final TimeUnit timeUnit, final TimerTask<R> task) {
        final String id = KeyGenUtils.newUuid();
        long realDelay = delay;

        //如果EventListener还没全部扫描并注册完成 这里立即执行持久化恢复队列时会找不到处理器 延迟一分钟执行
        long runTime = SpringContextUtils.getRunTime();
        if (runTime == 0) {
            realDelay = DELAY_RESTART_TIME;
        }

        // 过期任务立即执行
        if (realDelay <= 0) {
            return TimerFuture.immediate(id, task);
        }
        final long delayNano = timeUnit.toNanos(realDelay);

        // 少于一分钟的延迟，只启用秒轮
        if (delayNano < ONE_MINUTE_NANO) {
            final TimerFuture<R> resultFuture = TimerFuture.fromTimeout(id, runSeconds(delayNano, task));
            registerToTaskMap(id, resultFuture);
            return resultFuture;
        }

        // 少于一小时的延迟，启用分钟轮和秒轮
        if (delayNano < ONE_HOUR_NANO) {
            final long minutesNano = TimeUnit.MINUTES.toNanos(timeUnit.toMinutes(realDelay));
            if (log.isDebugEnabled()) {
                log.debug("minutesNano: {}", TimeUnit.NANOSECONDS.toSeconds(minutesNano));
            }
            final long secondsNano = delayNano - minutesNano;
            if (log.isDebugEnabled()) {
                log.debug("secondsNano: {}", TimeUnit.NANOSECONDS.toSeconds(secondsNano));
            }

            // 分钟轮到期将任务转移到秒轮
            final long startTime = System.nanoTime();
            final TimerFuture<R> resultFuture = TimerFuture.fromTimeout(id, runMinutes(startTime, minutesNano, secondsNano, task));
            registerToTaskMap(id, resultFuture);
            return resultFuture;
        }

        // 执行到此处说明任务延迟超过一小时，三级时间轮同时启用

        final long hoursNano = TimeUnit.HOURS.toNanos(timeUnit.toHours(realDelay));
        if (log.isDebugEnabled()) {
            log.debug("hoursNano: {}", TimeUnit.NANOSECONDS.toSeconds(hoursNano));
        }
        final long minutesNano = TimeUnit.MINUTES.toNanos(timeUnit.toMinutes(realDelay)) - hoursNano;
        if (log.isDebugEnabled()) {
            log.debug("minutesNano: {}", TimeUnit.NANOSECONDS.toSeconds(minutesNano));
        }
        final long secondsNano = delayNano - minutesNano - hoursNano;
        if (log.isDebugEnabled()) {
            log.debug("secondsNano: {}", TimeUnit.NANOSECONDS.toSeconds(secondsNano));
        }
        final long hourStart = System.nanoTime();
        final TimerFuture<R> resultFuture = TimerFuture.fromTimeout(id, runHour(hourStart, hoursNano, minutesNano, secondsNano, task));
        registerToTaskMap(id, resultFuture);
        return resultFuture;
    }

    public boolean cancel(String id) {
        Timeout<?> timeout = taskMap.remove(id);
        if (timeout == null) {
            return false;
        }
        return timeout.cancel();
    }

    /**
     * 延迟发送消息到EventBus队列中
     *
     * @param delay    延迟时间
     * @param timeUnit 延迟时间单位
     * @param router   队列router
     * @param clazz    发送数据类型的类对象
     * @param payload  发送数据
     * @param <T>      发送数据类型
     * @return 表示发送完成的Future
     */
    public <T> TimerFuture<T> delaySend(long delay, TimeUnit timeUnit, final String router, final Class<T> clazz, final T payload) {
        return delay(delay, timeUnit, timeout -> eventBus.send(router, clazz, payload).get());
    }

    /**
     * 延迟发送广播消息到EventBus队列中
     *
     * @param delay    延迟时间
     * @param timeUnit 延迟时间单位
     * @param router   队列router
     * @param clazz    发送数据类型的类对象
     * @param payload  发送数据
     * @param <T>      发送数据类型
     * @return 表示发送完成的Future
     */
    public <T> TimerFuture<T> delayPublish(long delay, TimeUnit timeUnit, final String router, final Class<T> clazz, final T payload) {
        return delay(delay, timeUnit, timeout -> eventBus.publish(router, clazz, payload).get());
    }

    private <R> void registerToTaskMap(String id, TimerFuture<R> resultFuture) {
        taskMap.put(id, resultFuture.getTimeout());
        resultFuture.whenComplete((d, e) -> taskMap.remove(id));
    }

    private <R> WheelTimeout<R> runHour(long hourStart, long hoursNano, long originMinutesNano, long originSecondsNano, TimerTask<R> task) {
        hourWheel.start();
        long hourDeadline = System.nanoTime() + hoursNano - hourWheel.startTime;
        if (hourDeadline < 0) {
            hourDeadline = Long.MAX_VALUE;
        }
        AsyncWheelTimeout<R> timeout = new AsyncWheelTimeout<>(asyncManager, hourWheel, hourDeadline, () -> {
            long minuteStart = System.nanoTime();
            long deltaMinutesNano = System.nanoTime() - hourStart - hoursNano; // 小时轮实际执行带来的误差
            if (log.isDebugEnabled()) {
                log.debug("deltaMinutes: {}", TimeUnit.NANOSECONDS.toSeconds(deltaMinutesNano));
            }
            long realMinutesNano = Math.max(deltaMinutesNano > 0 ? originMinutesNano - deltaMinutesNano : originMinutesNano, 0);
            if (log.isDebugEnabled()) {
                log.debug("realMinutes: {}", (realMinutesNano / ONE_MINUTE_NANO));
            }
            // 小时轮到期将任务转移到分钟轮
            return runMinutes(minuteStart, realMinutesNano, originSecondsNano, task).toFuture();
        });
        hourWheel.timeouts.add(timeout);
        return timeout;
    }

    private <R> WheelTimeout<R> runMinutes(long minuteStart, long realMinutesNano, long originSecondsNano, TimerTask<R> task) {
        minuteWheel.start();
        long minutesDeadline = System.nanoTime() + realMinutesNano - minuteWheel.startTime;
        if (minutesDeadline < 0) {
            minutesDeadline = Long.MAX_VALUE;
        }
        AsyncWheelTimeout<R> timeout = new AsyncWheelTimeout<>(asyncManager, minuteWheel, minutesDeadline, () -> {
            long deltaSecondsNano = System.nanoTime() - minuteStart - realMinutesNano; // 分钟轮实际执行带来的误差
            if (log.isDebugEnabled()) {
                log.debug("deltaSeconds: {}", TimeUnit.NANOSECONDS.toSeconds(deltaSecondsNano));
            }
            long realSecondsNano = Math.max(deltaSecondsNano > 0 ? originSecondsNano - deltaSecondsNano : originSecondsNano, 0);
            if (log.isDebugEnabled()) {
                log.debug("realSeconds: {}", (realSecondsNano / ONE_SECONDS_NANO));
            }
            // 分钟轮到期将任务转移到秒轮
            return runSeconds(realSecondsNano, task).toFuture();
        });
        minuteWheel.timeouts.add(timeout);
        return timeout;
    }

    private <R> WheelTimeout<R> runSeconds(long realSecondsNano, TimerTask<R> task) {
        secondWheel.start();
        long secondsDeadline = System.nanoTime() + realSecondsNano - secondWheel.startTime;
        if (secondsDeadline < 0) {
            secondsDeadline = Long.MAX_VALUE;
        }
        WheelTimeout<R> timeout = new WheelTimeout<>(asyncManager, secondWheel, secondsDeadline, task);
        secondWheel.timeouts.add(timeout);
        return timeout;
    }

    @SuppressWarnings("unused")
    public void stop() {
        secondWheel.stop();
        minuteWheel.stop();
        hourWheel.stop();
        taskMap.clear();
    }

    private static class Wheel {
        private long tickDuration;
        private final WheelBucket[] buckets;
        private final int bucketCount;

        public static final int WORKER_STATE_INIT = 0;
        public static final int WORKER_STATE_STARTED = 1;
        public static final int WORKER_STATE_SHUTDOWN = 2;
        private static final AtomicIntegerFieldUpdater<Wheel> WORKER_STATE_UPDATER =
                AtomicIntegerFieldUpdater.newUpdater(Wheel.class, "workerState");

        private volatile long startTime;
        private volatile int workerState; // 0 - init, 1 - started, 2 - shut down

        private final Worker worker = new Worker();
        private final Thread workerThread;
        private final CountDownLatch startTimeInitialized = new CountDownLatch(1);

        private final Queue<WheelTimeout<?>> timeouts = new MpscUnboundedArrayQueue<>(1024);
        private final Queue<WheelTimeout<?>> cancelledTimeouts = new MpscUnboundedArrayQueue<>(1024);
        private final AtomicLong pendingTimeouts = new AtomicLong(0);

        public Wheel(long tickDuration, TimeUnit timeUnit, int bucketCount) {

            if (tickDuration <= 0) {
                throw new IllegalArgumentException("tickDuration must grater than 0");
            }
            if (bucketCount <= 0) {
                throw new IllegalArgumentException("bucketCount must grater than 0");
            }

            this.tickDuration = tickDuration;
            this.bucketCount = bucketCount;

            this.buckets = new WheelBucket[bucketCount];

            for (int i = 0; i < this.buckets.length; i++) {
                this.buckets[i] = new WheelBucket();
            }

            // 转换为纳秒
            long duration = timeUnit.toNanos(tickDuration);

            // 防止数值溢出
            if (duration >= Long.MAX_VALUE / buckets.length) {
                throw new IllegalArgumentException(String.format(
                        "tickDuration: %d (expected: 0 < tickDuration in nanos < %d",
                        tickDuration, Long.MAX_VALUE / buckets.length));
            }

            long MILLISECOND_NANOS = TimeUnit.MILLISECONDS.toNanos(1);

            if (duration < MILLISECOND_NANOS) {
                log.warn("Configured tickDuration {} smaller then {}, using 1ms.",
                        tickDuration, MILLISECOND_NANOS);
                this.tickDuration = MILLISECOND_NANOS;
            } else {
                this.tickDuration = duration;
            }

            ThreadFactory threadFactory = Executors.defaultThreadFactory();
            workerThread = threadFactory.newThread(worker);
        }

        public void start() {
            switch (WORKER_STATE_UPDATER.get(this)) {
                case WORKER_STATE_INIT:
                    if (WORKER_STATE_UPDATER.compareAndSet(this, WORKER_STATE_INIT, WORKER_STATE_STARTED)) {
                        workerThread.start();
                    }
                    break;
                case WORKER_STATE_STARTED:
                    break;
                case WORKER_STATE_SHUTDOWN:
                    throw new IllegalStateException("cannot be started once stopped");
                default:
                    throw new Error("Invalid WorkerState");
            }

            // startTime会在worker线程中初始化，此处等待其完成
            while (startTime == 0) {
                try {
                    startTimeInitialized.await();
                } catch (InterruptedException ignore) {
                    // Ignore.
                }
            }
        }

        @SuppressWarnings("UnusedReturnValue")
        public Set<Timeout<?>> stop() {
            if (Thread.currentThread() == workerThread) {
                throw new IllegalStateException(
                        WheelTimer.class.getSimpleName() +
                                ".stop() cannot be called from " +
                                TimerTask.class.getSimpleName());
            }

            if (!WORKER_STATE_UPDATER.compareAndSet(this, WORKER_STATE_STARTED, WORKER_STATE_SHUTDOWN)) {
                return Collections.emptySet();
            }

            boolean interrupted = false;
            while (workerThread.isAlive()) {
                workerThread.interrupt();
                try {
                    workerThread.join(100);
                } catch (InterruptedException ignored) {
                    interrupted = true;
                }
            }

            if (interrupted) {
                Thread.currentThread().interrupt();
            }
            return worker.unprocessedTimeouts();
        }

        private final class Worker implements Runnable {

            private long tick;
            private final Set<Timeout<?>> unprocessedTimeouts = new HashSet<>();
            private final Logger logger = LoggerFactory.getLogger(Worker.class);

            @Override
            public void run() {
                // 初始化startTime
                startTime = System.nanoTime();
                if (startTime == 0) {
                    // 0值作为未初始化标识，此处保证不为0.
                    startTime = 1;
                }

                // 通知start()方法初始化完成
                startTimeInitialized.countDown();

                do {
                    final long deadline = waitForNextTick();
                    if (deadline > 0) {
                        int idx = (int) (tick % bucketCount);
                        processCancelledTasks();
                        WheelBucket bucket = buckets[idx];
                        transferTimeoutsToBuckets();
                        bucket.expireTimeouts(deadline);
                        tick++;
                    }
                } while (WORKER_STATE_UPDATER.get(Wheel.this) == WORKER_STATE_STARTED);

                // 填充unprocessedTimeouts集合，方便在stop()中返回
                for (WheelBucket bucket : buckets) {
                    bucket.clearTimeouts(unprocessedTimeouts);
                }
                for (; ; ) {
                    WheelTimeout<?> timeout = timeouts.poll();
                    if (timeout == null) {
                        break;
                    }
                    if (!timeout.isCancelled()) {
                        unprocessedTimeouts.add(timeout);
                    }
                }
                processCancelledTasks();
            }

            private void processCancelledTasks() {
                for (; ; ) {
                    WheelTimeout<?> timeout = cancelledTimeouts.poll();
                    if (timeout == null) {
                        // 处理完成
                        break;
                    }
                    try {
                        timeout.remove();
                    } catch (Throwable t) {
                        if (logger.isWarnEnabled()) {
                            logger.warn("An exception was thrown while process a cancellation task", t);
                        }
                    }
                }
            }

            private void transferTimeoutsToBuckets() {
                // 每个tick最多从队列中取100000个timeout对像发送到buckets，防止在循环中添加新timeout对象时污染worker线程
                for (int i = 0; i < 100000; i++) {
                    WheelTimeout<?> timeout = timeouts.poll();
                    if (timeout == null) {
                        // all processed
                        break;
                    }
                    if (timeout.state() == WheelTimeout.ST_CANCELLED) {
                        // Was cancelled in the meantime.
                        continue;
                    }

                    long calculated = timeout.deadline / tickDuration;
                    timeout.remainingRounds = (calculated - tick) / buckets.length;

                    final long ticks = Math.max(calculated, tick); // Ensure we don't schedule for past.
                    int stopIndex = (int) (ticks % bucketCount);

                    WheelBucket bucket = buckets[stopIndex];
                    bucket.addTimeout(timeout);
                }
            }

            private long waitForNextTick() {
                long deadline = tickDuration * (tick + 1);

                for (; ; ) {
                    final long currentTime = System.nanoTime() - startTime;
                    long sleepTimeMs = (deadline - currentTime + 999999) / 1000000;

                    if (sleepTimeMs <= 0) {
                        if (currentTime == Long.MIN_VALUE) {
                            return -Long.MAX_VALUE;
                        } else {
                            return currentTime;
                        }
                    }

                    if (PlatformDependent.isWindows()) {
                        sleepTimeMs = sleepTimeMs / 10 * 10;
                        if (sleepTimeMs == 0) {
                            sleepTimeMs = 1;
                        }
                    }

                    try {
                        //noinspection BusyWait
                        Thread.sleep(sleepTimeMs);
                    } catch (InterruptedException ignored) {
                        if (WORKER_STATE_UPDATER.get(Wheel.this) == WORKER_STATE_SHUTDOWN) {
                            return Long.MIN_VALUE;
                        }
                    }
                }
            }

            public Set<Timeout<?>> unprocessedTimeouts() {
                return Collections.unmodifiableSet(unprocessedTimeouts);
            }
        }

    }

    private static class WheelBucket {
        WheelTimeout<?> head;
        WheelTimeout<?> tail;

        public void addTimeout(WheelTimeout<?> timeout) {
            assert timeout.bucket == null;
            timeout.bucket = this;
            if (head == null) {
                head = tail = timeout;
            } else {
                tail.next = timeout;
                timeout.prev = tail;
                tail = timeout;
            }
        }

        public void expireTimeouts(long deadline) {
            WheelTimeout<?> timeout = head;
            // process all timeouts
            while (timeout != null) {
                WheelTimeout<?> next = timeout.next;
                if (timeout.remainingRounds <= 0) {
                    next = remove(timeout);
                    if (timeout.deadline <= deadline) {
                        timeout.expire();
                    } else {
                        // timeout对象出现在错误的时间槽中，正常状态下不应该发生
                        throw new IllegalStateException(String.format(
                                "timeout.deadline (%d) > deadline (%d)", timeout.deadline, deadline));
                    }
                } else if (timeout.isCancelled()) {
                    next = remove(timeout);
                } else {
                    timeout.remainingRounds--;
                }
                timeout = next;
            }
        }

        public WheelTimeout<?> remove(WheelTimeout<?> timeout) {
            WheelTimeout<?> next = timeout.next;
            // remove timeout that was either processed or cancelled by updating the linked-list
            if (timeout.prev != null) {
                timeout.prev.next = next;
            }
            if (timeout.next != null) {
                timeout.next.prev = timeout.prev;
            }

            if (timeout == head) {
                // 如果头指针指向的timeout同时也是尾指针指向的对象，两个指针都需要处理
                if (timeout == tail) {
                    tail = null;
                    head = null;
                } else {
                    head = next;
                }
            } else if (timeout == tail) {
                // 如果当前timeout是尾指针指向的对象，将尾指针指向timeout的prev节点
                tail = timeout.prev;
            }
            // 指针置空保证GC执行.
            timeout.prev = null;
            timeout.next = null;
            timeout.bucket = null;
            timeout.wheel.pendingTimeouts.decrementAndGet();
            return next;
        }

        public void clearTimeouts(Set<Timeout<?>> set) {
            for (; ; ) {
                WheelTimeout<?> timeout = pollTimeout();
                if (timeout == null) {
                    return;
                }
                if (timeout.isExpired() || timeout.isCancelled()) {
                    continue;
                }
                set.add(timeout);
            }
        }

        private WheelTimeout<?> pollTimeout() {
            WheelTimeout<?> head = this.head;
            if (head == null) {
                return null;
            }
            WheelTimeout<?> next = head.next;
            if (next == null) {
                tail = this.head = null;
            } else {
                this.head = next;
                next.prev = null;
            }

            // 指针置空保证GC执行.
            head.next = null;
            head.prev = null;
            head.bucket = null;
            return head;
        }
    }

    private static class WheelTimeout<R> implements Timeout<R> {
        WheelTimeout<?> prev;
        WheelTimeout<?> next;
        WheelBucket bucket;

        private final Logger logger = LoggerFactory.getLogger(WheelTimeout.class);

        long remainingRounds;
        private volatile int state = ST_INIT;

        private final AsyncManager asyncManager;
        private final Wheel wheel;
        private final long deadline;

        private final TimerTask<R> task;
        protected final CompletableFuture<R> taskFuture;

        public WheelTimeout(AsyncManager asyncManager, Wheel wheel, long deadline, TimerTask<R> task) {
            this.asyncManager = asyncManager;
            this.wheel = wheel;
            this.deadline = deadline;
            this.task = task;
            this.taskFuture = new CompletableFuture<>();
        }

        private static final int ST_INIT = 0;
        private static final int ST_CANCELLED = 1;
        private static final int ST_EXPIRED = 2;
        @SuppressWarnings("rawtypes")
        private static final AtomicIntegerFieldUpdater<WheelTimeout> STATE_UPDATER =
                AtomicIntegerFieldUpdater.newUpdater(WheelTimeout.class, "state");

        public int state() {
            return state;
        }

        @Override
        public boolean cancel() {
            // 只更新下一个tick会被移除的任务状态
            if (!compareAndSetState(ST_INIT, ST_CANCELLED)) {
                return false;
            }
            // 如果任务需要被取消，我们将其放入另一个队列，该队列每一个tick都会被处理，此时可以得到最高的GC处理效率（延迟一个tick）
            wheel.cancelledTimeouts.add(this);
            return true;
        }

        @Override
        public CompletableFuture<R> toFuture() {
            return taskFuture;
        }

        @Override
        public boolean isCancelled() {
            return state() == ST_CANCELLED;
        }

        @Override
        public TimerTask<R> task() {
            return task;
        }

        @Override
        public boolean isExpired() {
            return state() == ST_EXPIRED;
        }

        @SuppressWarnings("BooleanMethodIsAlwaysInverted")
        public boolean compareAndSetState(int expected, int state) {
            return STATE_UPDATER.compareAndSet(this, expected, state);
        }

        public void expire() {
            if (!compareAndSetState(ST_INIT, ST_EXPIRED)) {
                return;
            }

            final CompletionStage<R> stage = asyncManager.stage(() -> task.run(this));
            stage.whenComplete((r, e) -> {
                if (e != null) {
                    if (logger.isWarnEnabled()) {
                        logger.warn("An exception was thrown by " + TimerTask.class.getSimpleName() + '.', e);
                    }
                    taskFuture.completeExceptionally(e);
                } else {
                    taskFuture.complete(r);
                }
            });
        }

        void remove() {
            WheelBucket bucket = this.bucket;
            if (bucket != null) {
                bucket.remove(this);
            } else {
                wheel.pendingTimeouts.decrementAndGet();
            }
        }
    }

    private static class AsyncWheelTimeout<R> extends WheelTimeout<R> {

        private final Supplier<CompletableFuture<R>> asyncTask;

        public AsyncWheelTimeout(AsyncManager asyncManager, Wheel wheel, long deadline, Supplier<CompletableFuture<R>> task) {
            super(asyncManager, wheel, deadline, null);
            this.asyncTask = task;
        }

        @Override
        public void expire() {
            if (!compareAndSetState(WheelTimeout.ST_INIT, WheelTimeout.ST_EXPIRED)) {
                return;
            }
            try {
                CompletableFuture<R> result = asyncTask.get();
                result.whenComplete((value, err) -> {
                    if (err != null) {
                        taskFuture.completeExceptionally(err);
                    } else {
                        taskFuture.complete(value);
                    }
                });
            } catch (Throwable t) {
                if (super.logger.isWarnEnabled()) {
                    super.logger.warn("An exception was thrown by " + TimerTask.class.getSimpleName() + '.', t);
                }
            }
        }

        @Override
        public TimerTask<R> task() {
            throw new UnsupportedOperationException("Cannot not get task in this inner class");
        }
    }
}
