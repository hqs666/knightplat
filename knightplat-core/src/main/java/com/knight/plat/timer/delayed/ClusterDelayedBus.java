package com.knight.plat.timer.delayed;

import com.knight.plat.async.AsyncManager;
import com.knight.plat.cluster.ClusterManager;
import com.knight.plat.timer.delayed.DelayBusPersistenceHandler.TaskMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public final class ClusterDelayedBus extends DelayedBus {

    private final ClusterManager clusterManager;

    private final static Logger log = LoggerFactory.getLogger(ClusterDelayedBus.class);

    private static final String TASK_BUCKET = "@delayed_timer_task";
    private static final String ID_BUCKET = "@delayed_timer_id_task";

    public ClusterDelayedBus(String serviceName, TimerManager timerManager, AsyncManager asyncManager, List<DelayBusPersistenceHandler> persistenceHandlerList, ClusterManager clusterManager) {
        super(asyncManager, timerManager, persistenceHandlerList);
        this.clusterManager = clusterManager;
        clusterManager.onLeaderStart(sig -> {
            int loaded = loadTask();
            log.info("从持久化接口中恢复载入了 {} 条队列任务数据", loaded);
        });
        clusterManager.subscribe(message -> {
            if (!serviceName.equals(message.getServiceName()) // 不是当前服务注册的定时任务
                    || !TASK_BUCKET.equals(message.getBucket())) { // 不是任务对象的消息
                return;
            }
            if (clusterManager.isLeader()) {
                final Class<?> clazz = message.getClazz();
                if (clazz == TaskMeta.class) {
                    @SuppressWarnings("rawtypes")
                    final TaskMeta meta = (TaskMeta) message.getData();
                    if (persistenceHandlerList != null && !persistenceHandlerList.isEmpty()) {
                        asyncManager.stage(() -> {
                            for (DelayBusPersistenceHandler handler : persistenceHandlerList) {
                                @SuppressWarnings("unchecked")
                                final boolean result = handler.saveTaskMeta(meta);
                                if (!result) {
                                    throw new IllegalStateException("延时任务持久化失败");
                                }
                            }
                            return true;
                        }).whenComplete((r, e) -> {
                            if (e != null) {
                                clusterManager.removeData(ID_BUCKET, meta.getId());
                                clusterManager.removeData(TASK_BUCKET, meta);
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    protected CompletableFuture<Boolean> containsTask(String id) {
        return clusterManager.contains(ID_BUCKET, id);
    }

    @Override
    protected void cleanTask(String id) {
        clusterManager.removeData(ID_BUCKET, id);
    }

    @Override
    protected void cleanAll() {
        // TODO
    }

    @Override
    protected <T> CompletableFuture<String> saveTask(TaskMeta<T> meta) {
        final CompletableFuture<String> resultFuture = new CompletableFuture<>();
        clusterManager.publish(TASK_BUCKET, meta, TaskMeta.class);
        clusterManager.publish(ID_BUCKET, meta.getId(), String.class);
        resultFuture.complete(meta.getId());
        return resultFuture;
    }
}
