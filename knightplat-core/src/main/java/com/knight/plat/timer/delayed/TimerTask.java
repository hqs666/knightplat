package com.knight.plat.timer.delayed;

public interface TimerTask<R> {
    R run(Timeout<R> timeout) throws Exception;
}
