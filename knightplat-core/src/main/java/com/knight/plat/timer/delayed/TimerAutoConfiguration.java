package com.knight.plat.timer.delayed;

import com.knight.plat.api.eventbus.EventBus;
import com.knight.plat.async.AsyncManager;
import com.knight.plat.cluster.ClusterAutoConfiguration;
import com.knight.plat.cluster.ClusterManager;
import org.jetbrains.annotations.NotNull;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.util.List;

@AutoConfiguration(after = ClusterAutoConfiguration.class)
public class TimerAutoConfiguration implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private final AsyncManager asyncManager;

    private final static Logger log = LoggerFactory.getLogger(TimerAutoConfiguration.class);

    @Autowired
    public TimerAutoConfiguration(AsyncManager asyncManager) {
        this.asyncManager = asyncManager;
    }

    @Bean(destroyMethod = "close")
    public TimerManager timerManager(EventBus eventBus) {
        return new TimerManager(new WheelTimer(asyncManager, eventBus));
    }


    @Bean(destroyMethod = "cleanAll")
    @DependsOn({"RedisAutoConfiguration"})
    public DelayedBus delayedBus(@Value("${spring.application.name:knight-default}") String serviceName, TimerManager timerManager, List<DelayBusPersistenceHandler> persistenceHandlerList) {
        if (applicationContext == null) {
            return new StandaloneDelayedBus(timerManager, asyncManager, persistenceHandlerList);
        }

        // 依赖检测 - 有cluster依赖优先使用cluster模式
        ClusterManager clusterManager = null;
        try {
            clusterManager = applicationContext.getBean(ClusterManager.class);
        } catch (BeansException e) {
            // do nothing.
        }
        if (clusterManager != null) {
            log.info("Create ClusterDelayedBus instance");
            return new ClusterDelayedBus(serviceName, timerManager, asyncManager, persistenceHandlerList, clusterManager);
        }

        // 依赖检测 - 如果没有cluster依赖，检测redis依赖
        try {
            Class.forName("org.redisson.api.RedissonClient");
            Class.forName("org.springframework.cloud.client.discovery.DiscoveryClient");
            Class.forName("org.springframework.cloud.client.serviceregistry.Registration");
            RedissonClient redissonClient = applicationContext.getBean(RedissonClient.class);
            DiscoveryClient discoveryClient = applicationContext.getBean(DiscoveryClient.class);
            Registration registration = applicationContext.getBean(Registration.class);
            log.info("Create RedisDelayedBus instance");
            return new RedisDelayedBus(asyncManager, timerManager, persistenceHandlerList, redissonClient, discoveryClient, registration, serviceName);
        } catch (BeansException | ClassNotFoundException e) {
            // do nothing.
        }

        // 依赖检测 - 上面的依赖都没有则使用本地实现
        log.info("Create StandaloneDelayedBus instance");
        return new StandaloneDelayedBus(timerManager, asyncManager, persistenceHandlerList);
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
