package com.knight.plat.timer.delayed;

import java.util.concurrent.CompletableFuture;

public class TimerFuture<T> extends CompletableFuture<T> {

    private final String id;
    private final Timeout<T> timeout;

    public TimerFuture(String id, Timeout<T> timeout) {
        super();
        this.id = id;
        this.timeout = timeout;
    }

    public String getId() {
        return id;
    }

    public static <T> TimerFuture<T> fromTimeout(String id, Timeout<T> timeout) {
        final TimerFuture<T> resultFuture = new TimerFuture<>(id, timeout);
        timeout.toFuture().whenComplete((d, e) -> {
            if (e != null) {
                resultFuture.completeExceptionally(e);
            } else {
                resultFuture.complete(d);
            }
        });
        return resultFuture;
    }

    public static <T> TimerFuture<T> immediate(String id, TimerTask<T> task) {
        final TimerFuture<T> resultFuture = new TimerFuture<>(id, null);
        try {
            final T result = task.run(null);
            resultFuture.complete(result);
        } catch (Exception e) {
            e.printStackTrace();
            resultFuture.completeExceptionally(e);
        }
        return resultFuture;
    }

    public Timeout<T> getTimeout() {
        return timeout;
    }
}
