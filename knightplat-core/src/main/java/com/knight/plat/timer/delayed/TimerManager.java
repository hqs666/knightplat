package com.knight.plat.timer.delayed;

import java.util.concurrent.TimeUnit;

public class TimerManager {
    private final WheelTimer wheelTimer;

    public TimerManager(WheelTimer wheelTimer) {
        this.wheelTimer = wheelTimer;
    }

    public void close() {
        if (this.wheelTimer != null) {
            this.wheelTimer.stop();
        }
    }

    /**
     * 延迟执行任务
     * @param delay 延迟时间
     * @param timeUnit 延迟时间单位
     * @param task 执行的任务
     * @param <R> 任务返回值类型
     * @return 表示任务执行的Future对象
     */
    public <R> TimerFuture<R> delayRun(final long delay, final TimeUnit timeUnit, final TimerTask<R> task) {
        return wheelTimer.delay(delay, timeUnit, task);
    }

    /**
     * 取消任务
     * @param id 任务id
     * @return 取消结果
     */
    public boolean cancel(String id) {
        return wheelTimer.cancel(id);
    }

    /**
     * 延迟发送消息到EventBus队列中
     * @param delay 延迟时间
     * @param timeUnit 延迟时间单位
     * @param router 队列router
     * @param clazz 发送数据类型的类对象
     * @param payload 发送数据
     * @param <T> 发送数据类型
     * @return 表示发送完成的Future
     */
    <T> TimerFuture<T> delaySend(long delay, TimeUnit timeUnit, final String router, final Class<T> clazz, final T payload) {
        return wheelTimer.delaySend(delay, timeUnit, router, clazz, payload);
    }

    /**
     * 延迟发送广播消息到EventBus队列中
     * @param delay 延迟时间
     * @param timeUnit 延迟时间单位
     * @param router 队列router
     * @param clazz 发送数据类型的类对象
     * @param payload 发送数据
     * @param <T> 发送数据类型
     * @return 表示发送完成的Future
     */
    <T> TimerFuture<T> delayPublish(long delay, TimeUnit timeUnit, final String router, final Class<T> clazz, final T payload) {
        return wheelTimer.delayPublish(delay, timeUnit, router, clazz, payload);
    }
}
