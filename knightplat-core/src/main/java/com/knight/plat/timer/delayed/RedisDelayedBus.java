package com.knight.plat.timer.delayed;

import com.knight.plat.async.AsyncManager;
import com.knight.plat.timer.delayed.DelayBusPersistenceHandler.TaskMeta;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class RedisDelayedBus extends DelayedBus {

    private static final String REDIS_MAP_KEY = "_knightplat_delayed_task_map";
    private static final String REDIS_LOCK_KEY = "_knightplat_delayed_task_lock";

    private final RedissonClient redissonClient;
    private final DiscoveryClient discoveryClient;
    private final Registration registration;

    private final String serviceName;

    private final static Logger log = LoggerFactory.getLogger(RedisDelayedBus.class);

    protected RedisDelayedBus(AsyncManager asyncManager, TimerManager timerManager,
            List<DelayBusPersistenceHandler> persistenceHandlerList, RedissonClient redissonClient,
            DiscoveryClient discoveryClient, Registration registration, String serviceName) {
        super(asyncManager, timerManager, persistenceHandlerList);
        this.redissonClient = redissonClient;
        this.discoveryClient = discoveryClient;
        this.registration = registration;
        this.serviceName = serviceName;
        int loaded = loadTask();
        log.info("从持久化接口中恢复载入了 {} 条队列任务数据", loaded);
    }

    @Override
    protected CompletableFuture<Boolean> containsTask(String id) {
        final List<ServiceInstance> instances = discoveryClient.getInstances(serviceName);
        boolean exists = false;

        final String selfUri = registration.getUri().toString();
        if (redissonClient.getMap(REDIS_MAP_KEY + "_" + selfUri).containsKey(id)) {
            return CompletableFuture.completedFuture(true);
        }

        if (instances != null && !instances.isEmpty()) {
            for (ServiceInstance instance : instances) {
                final RMap<String, TaskMeta<?>> map = redissonClient.getMap(REDIS_MAP_KEY + "_" + instance.getUri().toString());
                final boolean instExists = map.containsKey(id);
                if (instExists) {
                    exists = true;
                    break;
                }
            }
        }
        return CompletableFuture.completedFuture(exists);
    }

    @Override
    protected void cleanTask(String id) {
        final String selfUri = registration.getUri().toString();
        final RMap<String, TaskMeta<?>> map = redissonClient.getMap(REDIS_MAP_KEY + "_" + selfUri);
        map.remove(id);
    }

    @Override
    protected void cleanAll() {
        final String selfUri = registration.getUri().toString();
        final RMap<String, TaskMeta<?>> map = redissonClient.getMap(REDIS_MAP_KEY + "_" + selfUri);
        map.clear();
    }

    @Override
    protected int loadTask() {
        final RLock lock = redissonClient.getFairLock(REDIS_LOCK_KEY);
        if (lock == null) {
            throw new RuntimeException("创建分布式锁失败，为避免异常，取消读取数据");
        }
        try {
            boolean locked = lock.tryLock(20, 40, TimeUnit.SECONDS);
            if (!locked) {
                throw new RuntimeException("分布式锁获取失败，为避免异常，取消读取数据");
            }
            return super.loadTask();
        } catch (InterruptedException e) {
            throw new RuntimeException("分布式锁获取失败，为避免异常，取消读取数据", e);
        } finally {
            lock.unlock();
        }
    }

    @Override
    protected <T> CompletableFuture<String> saveTask(TaskMeta<T> meta) {
        final String selfUri = registration.getUri().toString();
        final RMap<String, TaskMeta<?>> map = redissonClient.getMap(REDIS_MAP_KEY + "_" + selfUri);
        map.put(meta.getId(), meta);
        final CompletableFuture<String> resultFuture = new CompletableFuture<>();
        if (persistenceHandlerList != null && !persistenceHandlerList.isEmpty()) {
            asyncManager.stage(() -> {
                for (DelayBusPersistenceHandler handler : persistenceHandlerList) {
                    final boolean result = handler.saveTaskMeta(meta);
                    if (!result) {
                        throw new IllegalStateException("延时任务持久化失败");
                    }
                }
                return true;
            }).whenComplete((r, e) -> {
                if (e != null) {
                    map.remove(meta.getId());
                    resultFuture.completeExceptionally(e);
                } else {
                    resultFuture.complete(meta.getId());
                }
            });
        } else {
            resultFuture.complete(meta.getId());
        }
        return resultFuture;
    }
}
