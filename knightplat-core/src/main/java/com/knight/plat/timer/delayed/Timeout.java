package com.knight.plat.timer.delayed;

import java.util.concurrent.CompletableFuture;

public interface Timeout<R> {

    TimerTask<R> task();

    boolean isExpired();

    boolean isCancelled();

    boolean cancel();

    CompletableFuture<R> toFuture();
}
