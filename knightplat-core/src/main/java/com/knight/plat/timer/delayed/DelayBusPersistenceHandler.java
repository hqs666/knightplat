package com.knight.plat.timer.delayed;

import java.util.List;

/**
 * 延迟队列 持久化处理
 * @author Knight
 */
public interface DelayBusPersistenceHandler {

    class TaskMeta<T> {
        private String id;
        private long expectedTimeMillis;
        private T data;
        private Class<T> clazz;
        private String topic;
        private boolean isPublish;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public long getExpectedTimeMillis() {
            return expectedTimeMillis;
        }

        public void setExpectedTimeMillis(long expectedTimeMillis) {
            this.expectedTimeMillis = expectedTimeMillis;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Class<T> getClazz() {
            return clazz;
        }

        public void setClazz(Class<T> clazz) {
            this.clazz = clazz;
        }

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public boolean isPublish() {
            return isPublish;
        }

        public void setPublish(boolean publish) {
            isPublish = publish;
        }
    }

    static <T> TaskMeta<T> createMeta(String id, long expectedTimeMillis, String topic, T data, Class<T> clazz, boolean isPublish) {
        final TaskMeta<T> meta = new TaskMeta<>();
        meta.setId(id);
        meta.setExpectedTimeMillis(expectedTimeMillis);
        meta.setData(data);
        meta.setClazz(clazz);
        meta.setTopic(topic);
        meta.setPublish(isPublish);
        return meta;
    }

    <T> boolean saveTaskMeta(TaskMeta<T> meta);

    <T> void onTaskFinish(TaskMeta<T> meta);

    <T> void onTaskFail(TaskMeta<T> meta, Throwable e);

    <T> boolean onTaskCancel(TaskMeta<T> meta);

    List<TaskMeta<?>> loadTaskMeta();
}
