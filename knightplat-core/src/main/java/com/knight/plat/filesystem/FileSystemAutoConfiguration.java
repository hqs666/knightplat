package com.knight.plat.filesystem;

import com.knight.plat.api.filesystem.FileChannelPair;
import com.knight.plat.api.filesystem.FileManager;
import com.knight.plat.api.filesystem.FileSystem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

import java.util.List;

@AutoConfiguration
public class FileSystemAutoConfiguration {
    private final Log log = LogFactory.getLog("knightplat-filesystem");

    @Lazy
    @Bean
    @SuppressWarnings("unused")
    public FileSystem fileSystem(final List<FileChannelPair> pairs) {
        FileSystem fs = new FileSystemImpl();
        pairs.forEach(pair -> {
            String channel = pair.getChannel();
            FileManager manager = pair.getFileManager();
            fs.registerFileManager(channel, manager);
            log.info("Register file channel: " + channel + ", using " + manager.getClass().getName());
        });
        return fs;
    }
}
