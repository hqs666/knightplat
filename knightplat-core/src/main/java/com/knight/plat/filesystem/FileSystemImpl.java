package com.knight.plat.filesystem;

import com.knight.plat.api.filesystem.FileChannel;
import com.knight.plat.api.filesystem.FileManager;
import com.knight.plat.api.filesystem.FileSystem;

import java.util.HashMap;
import java.util.Map;

public class FileSystemImpl implements FileSystem {

    FileSystemImpl() {}

    private final Map<String, FileManager> fileChannels = new HashMap<>();

    @Override
    public void registerFileManager(String channel, FileManager fileManager) {
        fileChannels.put(channel, fileManager);
    }

    @Override
    public FileManager channel(String channel) {
        return fileChannels.get(channel);
    }

    @Override
    public FileManager channel(FileChannel channel) {
        return fileChannels.get(channel.getName());
    }
}
