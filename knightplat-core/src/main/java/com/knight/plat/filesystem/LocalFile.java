package com.knight.plat.filesystem;

import com.knight.plat.api.filesystem.FileChannel;

public class LocalFile implements FileChannel {

    private LocalFile() {}

    private static final LocalFile INSTANCE = new LocalFile();

    public static LocalFile channel() {
        return INSTANCE;
    }

    @Override
    public String getName() {
        return "local";
    }
}
