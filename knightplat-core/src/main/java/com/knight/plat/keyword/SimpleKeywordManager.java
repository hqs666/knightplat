/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.keyword;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

/**
 * Created on 2016/11/17. Simple implement of KeywordManager, read keywords from keyword.dic
 *
 * @see com.knight.plat.api.utils.KeywordManager
 */
public class SimpleKeywordManager extends AbstractKeywordManager {

    private final static Logger log = LoggerFactory.getLogger("knightplat-sensitive-keywords-simple");

    @Override
    public Collection<String> loadKeywords() {
        Collection<String> keywords = Lists.newLinkedList();
        try {
            Enumeration<URL> resources
                    = Thread.currentThread().getContextClassLoader().getResources("keyword.dict");
            while (resources.hasMoreElements()) {
                final URL element = resources.nextElement();
                final InputStream is = element.openStream();
                final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null) {
                    List<String> split = Splitter.on(',').omitEmptyStrings().trimResults().splitToList(line);
                    keywords.addAll(split);
                }
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Load keyword error.");
            }
        }
        return keywords;
    }
}
