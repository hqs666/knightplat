/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.keyword;

import com.knight.plat.api.utils.WhiteListWordManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractWhiteListManager implements WhiteListWordManager {

    private static final Logger log = LoggerFactory.getLogger("knightplat-white-list-keywords");


    private Collection<String> whiteList = new ArrayList<>();


    @Override
    public void load() {
        this.whiteList = loadKeywords();
    }


    @Override
    public boolean belong(String src) {
        return whiteList.contains(src);
    }


}
