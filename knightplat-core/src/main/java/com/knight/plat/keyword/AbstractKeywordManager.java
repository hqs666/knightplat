/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.keyword;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.knight.plat.api.utils.KeywordManager;
import com.knight.plat.api.utils.config.Config;
import com.knight.plat.keyword.config.FilterHandler;
import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 2016/12/19. Core logic for sensitive words handler
 */
public abstract class AbstractKeywordManager implements KeywordManager {

    private static final Logger log = LoggerFactory.getLogger("knightplat-sensitive-keywords");

    private Set<String> keywords = new HashSet<>();

    private Trie trie;

    private Map<String, Trie> priorityTries = Maps.newHashMap();

    @Override
    public void load() {
        Collection<String> strings = loadKeywords();
        load(strings);
        this.keywords = dealKeywords(strings);
    }

    @Override
    public String filter(String src, char replace, String traceId) {
        StringBuilder sb = new StringBuilder(src);
        Map<String, Trie> priorityTries = getPriorityTries();
        if (null != priorityTries && priorityTries.size() > 0) {
            for (Map.Entry<String, Trie> e : priorityTries.entrySet()) {
                if (null != e.getValue()) {
                    replaceEmits(sb, e.getValue(), replace);
                }
            }
        }
        if (null != getTrie()) {
            replaceEmits(sb, getTrie(), replace);
        }
        return sb.toString();
    }

    @Override
    public String filter(String src, Config config) {
        FilterHandler handler = new FilterHandler(config);
        return handler.handle(
                src, getPriorityTries(), getTrie());
    }

    @Override
    public boolean isSensitive(String src) {
        Map<String, Trie> priorityTries = getPriorityTries();
        if (null != priorityTries && !priorityTries.isEmpty()) {
            for (Map.Entry<String, Trie> e : priorityTries.entrySet()) {
                if (null != e.getValue() && e.getValue().containsMatch(src)) {
                    return true;
                }
            }
        }
        return null != getTrie()
                && getTrie().containsMatch(src);
    }

    @Override
    public List<String> keyWordLevels(String src) {
        List<String> levels = Lists.newArrayListWithCapacity(5);

        Map<String, Trie> priorityTries = getPriorityTries();
        if (null != priorityTries && !priorityTries.isEmpty()) {
            for (Map.Entry<String, Trie> e : priorityTries.entrySet()) {
                if (null != e.getValue() && e.getValue().containsMatch(src)) {
                    levels.add(e.getKey());
                }
            }
        }

        return levels;
    }

    @Override
    public Set<String> countKeyWords(String src) {
        Set<String> words = Sets.newHashSet();

        Map<String, Trie> priorityTries = getPriorityTries();
        if (null != priorityTries && !priorityTries.isEmpty()) {
            for (Map.Entry<String, Trie> e : priorityTries.entrySet()) {
                if (null != e.getValue()) {
                    addToSet(words, e.getValue().parseText(src));
                }
            }
        }
        if (null != getTrie()) {
            Collection<Emit> emits = getTrie().parseText(src);
            addToSet(words, emits);
        }

        return words;
    }

    public Trie getTrie() {
        return trie;
    }

    public void setTrie(Trie trie) {
        this.trie = trie;
    }

    public Map<String, Trie> getPriorityTries() {
        return priorityTries;
    }

    public void setPriorityTries(Map<String, Trie> priorityTries) {
        this.priorityTries = priorityTries;
    }

    public void resetTo(String... keywords) {
        load(Sets.newHashSet(keywords));
    }

    public void loadAndAdd(String... keywords) {
        Collection<String> words = loadKeywords();
        words.addAll(Arrays.asList(keywords));
        load(words);
    }

    protected void load(Collection<String> keywords) {
        Trie.TrieBuilder localBuilder = Trie.builder();
        localBuilder.ignoreCase();
        Map<String, Trie.TrieBuilder> priorityBuilders = Maps.newHashMap();
        if (null != keywords) {
            for (String keyword : keywords) {
                if (keyword.startsWith("#")) {

                    // 增加优先级处理 - 2017-01-13 - HuangWj
                    String[] array = keyword.split("#");
                    if (array.length > 2) {
                        String priority = array[1];
                        String word = array[2];
                        if (priorityBuilders.containsKey(priority)) {
                            priorityBuilders.get(priority).addKeyword(word);
                        } else {
                            Trie.TrieBuilder pb = Trie.builder();
                            pb.addKeyword(word);
                            priorityBuilders.put(priority, pb);
                        }
                    }
                } else {
                    localBuilder.addKeyword(keyword);
                }
            }
        }
        // 按优先级分别生成Trie树
        for (Map.Entry<String, Trie.TrieBuilder> e : priorityBuilders.entrySet()) {
            if (null != e.getValue()) {
                priorityTries.put(e.getKey(), e.getValue().ignoreOverlaps().build());
            }
        }
        // 没有优先级的关键字进入默认Trie树
        trie = localBuilder.ignoreOverlaps().build();
        log.info("Loaded {} sensitive words.", null != keywords ? keywords.size() : 0);
    }

    public static void replaceEmits(StringBuilder sb, Trie trie, char replace) {
        Collection<Emit> emits = trie.parseText(sb);
        for (Emit emit : emits) {
            int start = emit.getStart();
            int end = emit.getEnd();
            for (int i = start; i <= end; i++) {
                sb.setCharAt(i, replace);
            }
        }
    }

    public static String replaceToChar(Integer length,char replace) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0;i<length;i++) {
            sb.append(replace);
        }
        return sb.toString();
    }

    public static void addToSet(Set<String> words, Collection<Emit> emits) {
        words.addAll(Collections2.transform(emits, Emit::getKeyword));
    }

    public Set<String> dealKeywords(Collection<String> collection){
        Set<String> sets = new HashSet<>();
        collection.forEach(w->{
            sets.add(w.replaceAll("#+[0-9]+#", ""));
        });
        return sets;
    }
}
