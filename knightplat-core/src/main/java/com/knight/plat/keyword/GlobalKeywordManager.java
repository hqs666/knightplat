package com.knight.plat.keyword;

import com.beust.jcommander.internal.Sets;
import com.knight.plat.api.utils.KeywordManager;

import java.util.Collection;
import java.util.Set;

public class GlobalKeywordManager extends AbstractKeywordManager {

    private final Collection<KeywordManager> managers;

    public GlobalKeywordManager(Collection<KeywordManager> managers) {
        this.managers = managers;
    }

    @Override
    public Collection<String> loadKeywords() {
        if (managers.isEmpty()) {
            // 空集合不处理
            return null;
        }
        if (managers.size() == 1) {
            // 单对象直接调用
            return managers.iterator().next().loadKeywords();
        }
        // 多对象重写载入方法
        Set<String> words = Sets.newHashSet();
        managers.forEach(manager -> words.addAll(manager.loadKeywords()));
        return words;
    }
}
