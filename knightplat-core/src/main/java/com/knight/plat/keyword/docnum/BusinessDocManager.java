package com.knight.plat.keyword.docnum;

import java.util.ArrayList;
import java.util.List;

/**
 * 项目名: obei-commons
 * 文件名: BusinessDocManager
 * 模块说明:
 * 修改历史:
 * 2022/4/6 - Knight - 创建
 */
public class BusinessDocManager {



    private String sysCode;
    private List<String> docList = new ArrayList<>();

    public BusinessDocManager(String sysCode) {
        this.sysCode = sysCode;
        load();
    }

    public void load(List<String> keywords) {
        docList = keywords;
    }

    public List<String> load() {
        return docList;
    }



    public boolean isFilter(String businessDocNum){
        if (docList.contains(businessDocNum)){
            return true;
        }else return false;
    }
}
