package com.knight.plat.keyword;

import com.knight.plat.api.utils.KeywordManager;
import org.springframework.boot.autoconfigure.AutoConfiguration;

@AutoConfiguration
public class KeywordAutoConfiguration {

    //@Bean
    KeywordManager localFileKeywordManager() {
        return new SimpleKeywordManager();
    }
}
