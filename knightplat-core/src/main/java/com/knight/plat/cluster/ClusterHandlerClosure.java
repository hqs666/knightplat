package com.knight.plat.cluster;

import com.alipay.sofa.jraft.Closure;
import com.alipay.sofa.jraft.Status;

public class ClusterHandlerClosure implements Closure {

    private ClusterMessage<?> data;

    public ClusterHandlerClosure(ClusterMessage<?> data) {
        this.data = data;
    }

    @Override
    public void run(Status status) {
        if (!status.isOk()) {
            System.out.println("回调 失败！！！！ msg: " + status.getErrorMsg());
        }
    }

    void setData(ClusterMessage<?> result) {
        this.data = result;
    }

    public ClusterMessage<?> getData() {
        return data;
    }
}
