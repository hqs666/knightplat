package com.knight.plat.cluster;

import com.alipay.sofa.jraft.Closure;
import com.alipay.sofa.jraft.Node;
import com.alipay.sofa.jraft.RaftGroupService;
import com.alipay.sofa.jraft.StateMachine;
import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.closure.ReadIndexClosure;
import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.entity.Task;
import com.alipay.sofa.jraft.option.CliOptions;
import com.alipay.sofa.jraft.rpc.RpcResponseClosureAdapter;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;
import com.google.protobuf.ByteString;
import com.google.protobuf.BytesValue;
import com.google.protobuf.InvalidProtocolBufferException;
import com.knight.plat.api.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ClusterManager {

    private final RaftGroupService raftGroupService;
    private final EventBus eventBus;
    private final PeerId serviceId;
    private final String serviceName;

    public static final String CLUSTER_TOPIC = "@cluster_local_topic";
    public static final String CLUSTER_OP_TOPIC = "@cluster_op_local_topic_";

    static final String SIG_RECOVER = "recover";
    static final String SIG_LEADER_START = "leader_start";
    static final String SIG_LEADER_STOP = "leader_stop";

    public static final Logger log = LoggerFactory.getLogger(ClusterManager.class);

    public ClusterManager(RaftGroupService raftGroupService, EventBus eventBus, PeerId serviceId, String serviceName) {
        this.raftGroupService = raftGroupService;
        this.serviceId = serviceId;
        this.eventBus = eventBus;
        this.serviceName = serviceName;
    }

    void recover() {
        if (raftGroupService == null || !raftGroupService.isStarted()) {
            log.warn("Raft服务未启动");
            return;
        }
        log.info("收到raft节点恢复信号");
        Node gotNode = raftGroupService.getRaftNode();
        if (!gotNode.listPeers().contains(serviceId)) {
            final Closure recoverCallback = status -> {
                log.info("恢复raft节点<{}:{}>状态: {}", serviceId.getIp(), serviceId.getPort(), status);
                sendClusterOperation(SIG_RECOVER);
            };
            gotNode.addPeer(serviceId, recoverCallback);
        }
    }

    public void close() {
        if (raftGroupService != null && raftGroupService.isStarted()) {
            raftGroupService.shutdown();
        }
    }

    public boolean isLeader() {
        return raftGroupService != null && raftGroupService.isStarted() && raftGroupService.getRaftNode().isLeader();
    }

    public <T> CompletableFuture<List<T>> getData(String bucket, Class<T> clazz) {
        final ClusterStateMachine fsm = fsm();
        final Node node = raftGroupService.getRaftNode();
        final CompletableFuture<List<T>> future = new CompletableFuture<>();
        node.readIndex(bucket.getBytes(StandardCharsets.UTF_8), new ReadIndexClosure() {
            @Override
            public void run(Status status, long index, byte[] reqCtx) {
                if (status.isOk()) {
                    final String bucket = new String(reqCtx, StandardCharsets.UTF_8);
                    Set<byte[]> data = fsm.getData(bucket);
                    if (data == null || data.isEmpty()) {
                        future.complete(Collections.emptyList());
                        return;
                    }
                    future.complete(data.stream().map(bytes -> ClusterMessage.convertToInstance(bytes, clazz)).collect(Collectors.toList()));
                } else {
                    future.completeExceptionally(new IllegalStateException(status.getErrorMsg()));
                }
            }
        });
        return future;
    }

    public <T> CompletableFuture<Boolean> contains(String bucket, T data) {
        final ClusterStateMachine fsm = fsm();
        final Node node = raftGroupService.getRaftNode();
        final CompletableFuture<Boolean> future = new CompletableFuture<>();
        node.readIndex(bucket.getBytes(StandardCharsets.UTF_8), new ReadIndexClosure() {
            @Override
            public void run(Status status, long index, byte[] reqCtx) {
                if (status.isOk()) {
                    final String bucket = new String(reqCtx, StandardCharsets.UTF_8);
                    @SuppressWarnings("unchecked")
                    boolean containsData = fsm.containsData(bucket, ClusterMessage.convertToBytes(data, (Class<? super T>) data.getClass()));
                    future.complete(containsData);
                } else {
                    future.completeExceptionally(new IllegalStateException(status.getErrorMsg()));
                }
            }
        });
        return future;
    }

    @SuppressWarnings("unchecked")
    public <T> void removeData(String bucket, T data) {
        final CleanSignal signal = new CleanSignal();
        signal.setData(ClusterMessage.convertToBytes(data, (Class<? super T>) data.getClass()));
        publish(bucket, signal, CleanSignal.class);
    }

    public <T> void publish(String bucket, T data, Class<T> clazz) {
        if (raftGroupService == null || !raftGroupService.isStarted()) {
            log.warn("Raft服务未启动");
            return;
        }
        Task task = new Task();
        final ClusterMessage<T> message = new ClusterMessage<>(bucket, clazz, data, serviceName, serviceId.getIp(), serviceId.getPort());
        byte[] transportData = message.toJson().encode().getBytes(StandardCharsets.UTF_8);
        task.setData(ByteBuffer.wrap(transportData));
        task.setDone(new ClusterHandlerClosure(message));
        if (raftGroupService.getRaftNode().isLeader()) {
            raftGroupService.getRaftNode().apply(task);
        } else {
            try {
                transportData(transportData);
            } catch (InterruptedException e) {
                log.error("转发信息到leader节点出现中断异常", e);
            } catch (TimeoutException e) {
                log.error("转发信息到leader节点超时", e);
            } catch (InvalidProtocolBufferException e) {
                log.error("转发信息到leader节点，消息转换失败", e);
            }
        }
    }

    public <T> void subscribe(Consumer<ClusterMessage<T>> consumer) {
        eventBus.register("local://" + CLUSTER_TOPIC, ClusterMessage.class, consumer::accept);
    }

    public void onLeaderStart(Consumer<String> consumer) {
        if (raftGroupService != null && raftGroupService.isStarted() && raftGroupService.getRaftNode().isLeader()) {
            consumer.accept(SIG_LEADER_START);
        }
        subscribeOperation(SIG_LEADER_START, consumer);
    }

    public void onLeaderStop(Consumer<String> consumer) {
        subscribeOperation(SIG_LEADER_STOP, consumer);
    }

    public void onRecover(Consumer<String> consumer) {
        subscribeOperation(SIG_RECOVER, consumer);
    }

    void sendClusterData(ClusterMessage<?> data) {
        eventBus.send("local://" + CLUSTER_TOPIC, ClusterMessage.class, data);
    }

    void sendClusterOperation(String sig) {
        eventBus.send("local://" + CLUSTER_OP_TOPIC + sig, String.class, sig);
    }

    private void subscribeOperation(String sig, Consumer<String> consumer) {
        eventBus.register("local://" + CLUSTER_OP_TOPIC + sig, String.class,  consumer::accept);
    }

    private ClusterStateMachine fsm() {
        if (raftGroupService == null || !raftGroupService.isStarted()) {
            throw new RuntimeException("Raft服务未启动");
        }
        final StateMachine fsm = raftGroupService.getRaftNode().getOptions().getFsm();
        if (fsm instanceof ClusterStateMachine) {
            return (ClusterStateMachine) fsm;
        }
        throw new RuntimeException("没有注册ClusterStateMachine");
    }

    private void transportData(byte[] transportData)
            throws InterruptedException, TimeoutException, InvalidProtocolBufferException {
        if (raftGroupService == null || !raftGroupService.isStarted()) {
            log.warn("Raft服务未启动");
            return;
        }
        final CliClientServiceImpl cliService = new CliClientServiceImpl();
        cliService.init(new CliOptions());

        final PeerId leader = raftGroupService.getRaftNode().getLeaderId();
        final BytesValue message = BytesValue.newBuilder().setValue(ByteString.copyFrom(transportData)).build();
        cliService.invokeWithDone(leader.getEndpoint(), message, new RpcResponseClosureAdapter<BytesValue>() {
            @Override
            public void run(Status status) {
                if (status.isOk()) {
                    log.info("请求转到leader节点成功");
                } else {
                    log.error("请求转到leader节点失败: " + status.getErrorMsg());
                }
            }
        }, 5000);
    }

}
