package com.knight.plat.cluster;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alipay.sofa.jraft.Node;
import com.alipay.sofa.jraft.RaftGroupService;
import com.alipay.sofa.jraft.StateMachine;
import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.entity.Task;
import com.alipay.sofa.jraft.option.NodeOptions;
import com.alipay.sofa.jraft.rpc.RaftRpcServerFactory;
import com.alipay.sofa.jraft.rpc.RpcRequestClosure;
import com.alipay.sofa.jraft.rpc.RpcRequestProcessor;
import com.alipay.sofa.jraft.rpc.RpcServer;
import com.alipay.sofa.jraft.rpc.impl.BoltRpcServer;
import com.google.protobuf.ByteString;
import com.google.protobuf.BytesValue;
import com.google.protobuf.Message;
import com.knight.plat.api.eventbus.EventBus;
import com.knight.plat.commons.JsonObject;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@AutoConfiguration
@ConditionalOnClass({RaftGroupService.class, Message.class})
@EnableConfigurationProperties(ClusterProperties.class)
public class ClusterAutoConfiguration {

    public static final Logger log = LoggerFactory.getLogger(ClusterAutoConfiguration.class);

    private final String serviceName;
    private final ClusterProperties clusterProperties;

    public ClusterAutoConfiguration(@Value("${spring.application.name:knight-default}") String serviceName,
            ClusterProperties clusterProperties) {
        this.serviceName = serviceName;
        this.clusterProperties = clusterProperties;
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnBean({NacosServiceManager.class, NacosDiscoveryProperties.class})
    public class NacosConfig { // TODO: 改为使用通用的DiscoveryClient

        private final NacosDiscoveryProperties nacosProperties;
        private final RaftGroupService raftGroupService;
        private final ClusterManager clusterManager;

        @Autowired
        @SuppressWarnings("all")
        public NacosConfig(NacosServiceManager manager, NacosDiscoveryProperties properties,
                EventBus eventBus) {
            this.nacosProperties = properties;
            final Properties innerNacosProperties = properties.getNacosProperties();
            final String groupName = properties.getGroup();
            final String clusterName = properties.getClusterName();
            final String ip = properties.getIp();

            PeerId thisPeerId = new PeerId(ip, clusterProperties.getPort());
            final NodeOptions nodeOptions;
            final ClusterStateMachine fsm = new ClusterStateMachine();
            try {
                nodeOptions = initNodeOptions(fsm);
            } catch (IOException e) {
                throw new RuntimeException("无法初始化raft数据目录", e);
            }
            final com.alipay.remoting.rpc.RpcServer innerRpcServer = new com.alipay.remoting.rpc.RpcServer(thisPeerId.getPort());
            final RpcServer rpcServer = new BoltRpcServer(innerRpcServer);
            RaftRpcServerFactory.addRaftRequestProcessors(rpcServer);
            rpcServer.registerProcessor(new FollowerProcessor(null));
            raftGroupService = new RaftGroupService(clusterProperties.getClusterName(), thisPeerId, nodeOptions, rpcServer);

            final NamingService namingService = manager.getNamingService(innerNacosProperties);
            try {
                final List<Instance> initInstances = namingService.getAllInstances(clusterProperties.getClusterName());
                if (initInstances != null) {
                    final List<PeerId> peerIds = new ArrayList<>(initInstances.size() + 1);
                    peerIds.add(new PeerId(nacosProperties.getIp(), clusterProperties.getPort()));
                    initInstances.forEach(instance -> {
                        PeerId thisPeer = new PeerId(instance.getIp(), instance.getPort());
                        if (!peerIds.contains(thisPeer)) {
                            peerIds.add(thisPeer);
                        }
                    });
                    nodeOptions.setInitialConf(new com.alipay.sofa.jraft.conf.Configuration(peerIds));
                }
                namingService.registerInstance(clusterProperties.getClusterName(), groupName, ip, clusterProperties.getPort(), clusterName);
                namingService.subscribe(clusterProperties.getClusterName(), event -> {
                    try {
                        nodeEventHandler(thisPeerId, raftGroupService, namingService);
                    } catch (Exception e) {
                        log.error("处理节点变更时发生异常", e);
                    }
                });
            } catch (NacosException e) {
                log.error("向nacos服务注册raft节点信息时发生错误，集群功能将部分失效", e);
            }

            this.clusterManager = new ClusterManager(raftGroupService, eventBus, thisPeerId, serviceName);
            fsm.setClusterManager(this.clusterManager);
            raftGroupService.start();
        }

        @Bean(destroyMethod = "close")
        public ClusterManager clusterManager() {
            return this.clusterManager;
        }

        private NodeOptions initNodeOptions(StateMachine stateMachine) throws IOException {
            // 配置raft node option
            final NodeOptions nodeOptions = new NodeOptions();

            final FileSystem fs = FileSystems.getDefault();
            final String subFold = nacosProperties.getIp() + "_" + clusterProperties.getPort();
            File baseDir = fs.getPath(System.getProperty("user.home"), ".knightplat", "raft", subFold).toFile();
            FileUtils.forceMkdir(baseDir);
            FileUtils.cleanDirectory(baseDir);

            final String logUri = fs.getPath(System.getProperty("user.home"), ".knightplat", "raft", subFold, "log").toAbsolutePath().toString();
            log.info("正在设置raft的log路径: {}", logUri);
            nodeOptions.setLogUri(logUri);

            final String metaUri = fs.getPath(System.getProperty("user.home"), ".knightplat", "raft", subFold, "meta").toAbsolutePath().toString();
            log.info("正在设置raft的meta路径: {}", metaUri);
            nodeOptions.setRaftMetaUri(metaUri);

            final String snapshotUri = fs.getPath(System.getProperty("user.home"), ".knightplat", "raft", subFold, "snapshot").toAbsolutePath().toString();
            log.info("正在设置raft的snapshot路径: {}", snapshotUri);
            nodeOptions.setSnapshotUri(snapshotUri);

            nodeOptions.setFsm(stateMachine);

            return nodeOptions;
        }

        private void nodeEventHandler(PeerId self, RaftGroupService raftGroupService, NamingService namingService)
                throws NacosException {
            final Node gotNode = raftGroupService.getRaftNode();
            if (gotNode == null) {
                log.info("节点有变动，但本节点没有已启动的raft服务，不做处理");
                return;
            }
            final List<Instance> nacosInstances = namingService.getAllInstances(clusterProperties.getClusterName());
            if (nacosInstances != null && !nacosInstances.isEmpty()) {
                final List<PeerId> peerIds = nacosInstances.stream()
                        .map(instance -> new PeerId(instance.getIp(), clusterProperties.getPort()))
                        .collect(Collectors.toList());
                if (!peerIds.contains(self)) {
                    peerIds.add(self);
                }
                log.info("发生集群节点变更，新的节点列表为: {}", peerIds);
                final com.alipay.sofa.jraft.conf.Configuration newConf = new com.alipay.sofa.jraft.conf.Configuration(peerIds);
                if (raftGroupService.isStarted()) {
                    final Status status = gotNode.resetPeers(newConf);
                    log.info("raft节点变更结果状态: {}", status);
                } else {
                    final NodeOptions options = (NodeOptions) raftGroupService.getNodeOptions();
                    options.setInitialConf(newConf);
                }
            }
        }

        private class FollowerProcessor extends RpcRequestProcessor<BytesValue> {

            public FollowerProcessor(Executor executor) {
                super(executor, BytesValue.of(ByteString.copyFromUtf8("UNDEFINED")));
            }

            @Override
            public String interest() {
                return BytesValue.class.getName();
            }

            @Override
            public Message processRequest(BytesValue request, RpcRequestClosure done) {
                ByteString byteString = request.getValue();
                byte[] bytes = byteString.toByteArray();
                final String jsonStr = new String(bytes, StandardCharsets.UTF_8);
                final JsonObject jsonObject = new JsonObject(jsonStr);
                final ClusterMessage<?> message = ClusterMessage.fromJson(jsonObject);
                Task task = new Task();
                task.setData(ByteBuffer.wrap(bytes));
                task.setDone(new ClusterHandlerClosure(message));
                raftGroupService.getRaftNode().apply(task);
                return BytesValue.of(ByteString.copyFromUtf8("OK"));
            }
        }
    }

}
