package com.knight.plat.cluster;

import com.alipay.sofa.jraft.Iterator;
import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.core.StateMachineAdapter;
import com.alipay.sofa.jraft.error.RaftException;
import com.google.common.collect.Sets;
import com.knight.plat.commons.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ClusterStateMachine extends StateMachineAdapter {

    public static final Logger log = LoggerFactory.getLogger(ClusterStateMachine.class);

    private ClusterManager clusterManager;

    private final ConcurrentMap<String, Set<byte[]>> dataMap = new ConcurrentHashMap<>();

    public void addData(String bucket, byte[] data) {
        if (bucket == null) {
            return;
        }
        dataMap.merge(bucket, Sets.newHashSet(data), (k, set) -> {
            set.add(data);
            return set;
        });
    }

    public void addData(String bucket, Collection<byte[]> data) {
        if (bucket == null) {
            return;
        }
        dataMap.merge(bucket, new HashSet<>(data), (k, set) -> {
            set.addAll(data);
            return set;
        });
    }

    public Set<byte[]> getData(String bucket) {
        return dataMap.get(bucket);
    }

    public boolean containsData(String bucket, byte[] data) {
        if (bucket == null) {
            return false;
        }
        return dataMap.containsKey(bucket) && dataMap.get(bucket).contains(data);
    }

    public void removeData(String bucket, byte[] data) {
        if (bucket == null || data == null) {
            return;
        }
        dataMap.computeIfPresent(bucket, (k, set) -> {
            set.remove(data);
            return set;
        });
    }

    @Override
    public void onApply(Iterator iter) {
        try {
            while (iter.hasNext()) {
                final ByteBuffer data = iter.getData();
                final ClusterMessage<?> message;
                if (iter.done() instanceof ClusterHandlerClosure) {
                    final ClusterHandlerClosure handler = (ClusterHandlerClosure) iter.done();
                    handler.run(Status.OK());
                    message = handler.getData();
                } else {
                    final String jsonStr = new String(data.array(), StandardCharsets.UTF_8);
                    final JsonObject jsonObject = new JsonObject(jsonStr);
                    message = ClusterMessage.fromJson(jsonObject);
                }
                if (message.getClazz() == CleanSignal.class) {
                    final CleanSignal signal = (CleanSignal) message.getData();
                    removeData(message.getBucket(), signal.getData());
                } else {
                    addData(message.getBucket(), message.getDataBytes());
                    if (clusterManager != null) {
                        clusterManager.sendClusterData(message);
                    }
                }
                iter.next();
            }
        } catch (Exception e) {
            log.error("集群状态机执行失败", e);
        }
    }

    @Override
    public void onError(RaftException e) {
        log.error("集群状态机发生错误", e);
        if (clusterManager != null) {
            clusterManager.recover();
        }
    }

    @Override
    public void onLeaderStart(long term) {
        super.onLeaderStart(term);
        clusterManager.sendClusterOperation(ClusterManager.SIG_LEADER_START);
    }

    @Override
    public void onLeaderStop(Status status) {
        super.onLeaderStop(status);
        clusterManager.sendClusterOperation(ClusterManager.SIG_LEADER_STOP);
    }

    public void setClusterManager(ClusterManager clusterManager) {
        this.clusterManager = clusterManager;
    }
}
