package com.knight.plat.cluster;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "knightplat.cluster")
public class ClusterProperties {
    private int port = 5555;
    private String clusterName = "knightplat-cluster-nodes";

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }
}
