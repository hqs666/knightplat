package com.knight.plat.cluster;

import com.knight.plat.commons.JsonObject;
import com.knight.plat.utils.JsonUtils;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

public class ClusterMessage<T> implements Serializable {

    public static final String BUCKET_KEY = "@cluster_bucket_key";
    public static final String CLASS_KEY = "@cluster_class_key";
    public static final String DATA_KEY = "@cluster_data_key";
    public static final String SERVICE_KEY = "@cluster_service_key";
    public static final String IP_KEY = "@cluster_ip_key";
    public static final String PORT_KEY = "@cluster_port_key";

    private String bucket;
    private Class<?> clazz;
    private byte[] data;
    private String serviceName;
    private String ip;
    private int port;

    public ClusterMessage() {}

    public ClusterMessage(String bucket, Class<T> clazz, T data, String serviceName, String ip, int port) {
        this.bucket = bucket;
        this.clazz = clazz;
        this.serviceName = serviceName;
        this.ip = ip;
        this.port = port;
        setData(data);
    }

    @SuppressWarnings("unchecked")
    static <T> T convertToInstance(byte[] bytes, Class<T> clazz) {
        if (bytes == null) {
            return null;
        }

        Object value;
        if (byte[].class == clazz) {
            value = bytes;
        } else if (String.class == clazz) {
            value = new String(bytes, StandardCharsets.UTF_8);
        } else {
            value = JsonUtils.decode(bytes, clazz);
        }
        return (T) value;
    }

    static <T> byte[] convertToBytes(T data, Class<T> clazz) {
        if (data == null) {
            return null;
        }
        if (byte[].class == clazz) {
            return (byte[]) data;
        }
        if (String.class == clazz) {
            return ((String) data).getBytes(StandardCharsets.UTF_8);
        }
        return JsonUtils.encodeToBytes(data);
    }

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    public void setData(T data) {
        this.data = convertToBytes(data, getClazz());
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @SuppressWarnings("unchecked")
    public Class<T> getClazz() {
        return (Class<T>) clazz;
    }

    public T getData() {
        return ClusterMessage.convertToInstance(data, getClazz());
    }

    public String getBucket() {
        return bucket;
    }

    byte[] getDataBytes() {
        return data;
    }

    void setDataBytes(byte[] data) {
        this.data = data;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public JsonObject toJson() {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.put(BUCKET_KEY, bucket);
        jsonObject.put(CLASS_KEY, clazz.getName());
        jsonObject.put(DATA_KEY, data);
        jsonObject.put(SERVICE_KEY, serviceName);
        jsonObject.put(IP_KEY, ip);
        jsonObject.put(PORT_KEY, port);
        return jsonObject;
    }

    public static ClusterMessage<?> fromJson(JsonObject jsonObject) {
        final String className = jsonObject.getString(CLASS_KEY);
        Class<?> clazz;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("无法获取类定义: " + className, e);
        }
        final String bucket = jsonObject.getString(BUCKET_KEY);
        final byte[] dataBytes = jsonObject.getBinary(DATA_KEY);
        final String serviceName = jsonObject.getString(SERVICE_KEY);
        final String ip = jsonObject.getString(IP_KEY);
        final int port = jsonObject.getInteger(PORT_KEY);

        final ClusterMessage<?> result = new ClusterMessage<>();
        result.bucket = bucket;
        result.clazz = clazz;
        result.data = dataBytes;
        result.serviceName = serviceName;
        result.ip = ip;
        result.port = port;

        return result;
    }
}
