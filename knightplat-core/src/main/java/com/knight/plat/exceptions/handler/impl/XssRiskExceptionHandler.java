package com.knight.plat.exceptions.handler.impl;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.api.exceptions.XssRiskException;
import com.knight.plat.exceptions.handler.ExceptionCollector;
import com.knight.plat.exceptions.handler.ExceptionHandler;
import com.knight.plat.exceptions.handler.ExceptionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class XssRiskExceptionHandler implements ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(XssRiskExceptionHandler.class);

    @Override
    public ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        XssRiskException xre = (XssRiskException) ex;
        if (!collectors.isEmpty()) {
            if (request != null) {
                collectors.forEach(c -> c.onCatch(request, xre));
            }
            if (exchange != null) {
                collectors.forEach(c -> c.onCatch(exchange, xre));
            }
        }
        log.error("疑似xss注入代码，已阻止此请求", xre);
        String code = xre.getCode();
        String msg = xre.getLocalizedMessage();
        if (code == null || code.isEmpty()) {
            code = properties.getXssCode();
        }
        if (msg == null || msg.isEmpty()) {
            msg = properties.getDefaultXssMessage();
        }
        final ResultDTO<Object> resultDTO = new ResultDTO<>();
        resultDTO.setCode(code);
        resultDTO.setMessage(msg);
        resultDTO.setSuccess(Boolean.FALSE);

        return resultDTO;
    }

    @Override
    public boolean isMatch(Throwable th) {
        return th instanceof XssRiskException;
    }
}
