package com.knight.plat.exceptions.handler.impl;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.api.runtime.FunctionException;
import com.knight.plat.exceptions.handler.ExceptionCollector;
import com.knight.plat.exceptions.handler.ExceptionHandler;
import com.knight.plat.exceptions.handler.ExceptionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class FunctionExceptionHandler implements ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(FunctionExceptionHandler.class);

    @Override
    public ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        FunctionException fe = (FunctionException) ex;
        if (!collectors.isEmpty()) {
            if (request != null) {
                collectors.forEach(c -> c.onCatch(request, fe));
            }
            if (exchange != null) {
                collectors.forEach(c -> c.onCatch(exchange, fe));
            }
        }
        final String msg = fe.getLocalizedMessage();
        log.error("Runtime Function执行异常：" + fe.getStack(), fe);
        final ResultDTO<Object> resultDTO = new ResultDTO<>();
        resultDTO.setCode(properties.getFunctionCode());
        resultDTO.setMessage(msg);
        resultDTO.setSuccess(Boolean.FALSE);
        return resultDTO;
    }

    @Override
    public boolean isMatch(Throwable th) {
        return th instanceof FunctionException;
    }
}
