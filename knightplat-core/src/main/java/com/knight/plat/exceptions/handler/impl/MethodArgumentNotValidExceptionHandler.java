package com.knight.plat.exceptions.handler.impl;

import com.knight.plat.api.commons.FieldErrorInfo;
import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.api.utils.Tools;
import com.knight.plat.exceptions.handler.ExceptionCollector;
import com.knight.plat.exceptions.handler.ExceptionHandler;
import com.knight.plat.exceptions.handler.ExceptionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MethodArgumentNotValidExceptionHandler implements ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(MethodArgumentNotValidExceptionHandler.class);

    @Override
    public ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        MethodArgumentNotValidException ve = (MethodArgumentNotValidException) ex;
        if (!collectors.isEmpty()) {
            if (request != null) {
                collectors.forEach(c -> c.onCatch(request, ve));
            }
            if (exchange != null) {
                collectors.forEach(c -> c.onCatch(exchange, ve));
            }
        }
        final BindingResult result = ve.getBindingResult();
        final List<FieldErrorInfo> errorInfoList;
        if (result.hasErrors()) {
            final List<ObjectError> errors = result.getAllErrors();

            errorInfoList = errors.stream()
                    .filter(p -> p instanceof FieldError)
                    .map(p -> (FieldError) p)
                    .peek(p -> log.error("数据字段检查错误 : object{" + p.getObjectName() + "}, field{" + p.getField()
                            + "}, errorMessage{" + p.getDefaultMessage() + "}"))
                    .map(p -> new FieldErrorInfo(p.getField(), p.getDefaultMessage()))
                    .collect(Collectors.toList());
        } else {
            errorInfoList = Collections.emptyList();
        }
        final ResultDTO<Object> resultDTO = new ResultDTO<>();
        resultDTO.setCode(properties.getValidationCode());
        resultDTO.setMessage(properties.getDefaultValidateMessageHeader() + fieldErrorsToString(errorInfoList, properties));
        resultDTO.setFieldErrors(errorInfoList);
        resultDTO.setSuccess(Boolean.FALSE);

        return resultDTO;
    }

    @Override
    public boolean isMatch(Throwable th) {
        return th instanceof MethodArgumentNotValidException;
    }

    private String fieldErrorsToString(List<FieldErrorInfo> fieldErrors, ExceptionProperties properties) {
        if (fieldErrors == null || fieldErrors.isEmpty()) {
            return "";
        }
        return fieldErrors.stream()
                .map(info -> formatValidateListMessage(properties.getDefaultValidateMessageList(), info.getField(), info.getMessage()))
                .collect(Collectors.joining(", "));
    }

    private String formatValidateListMessage(String pattern, String field, String message) {
        if (pattern == null || pattern.isEmpty()) {
            return "";
        }
        return Tools.patternReplace(pattern, Pattern.compile("\\{\\{(.*?)}}"), key -> {
            if (key.equals("field")) {
                return field;
            }
            if (key.equals("message")) {
                return message;
            }
            return key;
        });
    }
}
