package com.knight.plat.exceptions.handler.impl;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.exceptions.handler.ExceptionCollector;
import com.knight.plat.exceptions.handler.ExceptionHandler;
import com.knight.plat.exceptions.handler.ExceptionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class NullPointerExceptionHandler implements ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(NullPointerExceptionHandler.class);

    @Override
    public ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        NullPointerException npe = (NullPointerException) ex;
        if (!collectors.isEmpty()) {
            if (request != null) {
                collectors.forEach(c -> c.onCatch(request, npe));
            }
            if (exchange != null) {
                collectors.forEach(c -> c.onCatch(exchange, npe));
            }
        }
        log.error("发生空指针异常，请检查代码！", npe);
        final ResultDTO<Object> resultDTO = new ResultDTO<>();
        resultDTO.setCode(properties.getNullPointerCode());
        if (properties.isHideMessage()) {
            resultDTO.setMessage(properties.getDefaultMessage());
        } else {
            resultDTO.setMessage(npe.getLocalizedMessage() != null ? npe.getLocalizedMessage() : npe.getMessage());
        }
        resultDTO.setSuccess(Boolean.FALSE);

        return resultDTO;
    }

    @Override
    public boolean isMatch(Throwable th) {
        return th instanceof NullPointerException;
    }
}
