/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.exceptions.handler;

import com.google.common.collect.Maps;
import com.knight.plat.api.exceptions.ExceptionCodeManager;

import java.util.Map;

/**
 * Created on 2016/11/27.
 */
public class InMemoryExceptionCodeManager implements ExceptionCodeManager {

    private final Map<String, Object> map = Maps.newConcurrentMap();

    @Override
    public void registerCode(String code, Object message) {
        map.put(code, message);
    }

    @Override
    public Object getMessage(String code) {
        if (code == null) {
            return null;
        }
        return map.get(code);
    }
}
