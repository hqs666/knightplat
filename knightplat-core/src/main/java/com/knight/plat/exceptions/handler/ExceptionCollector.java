package com.knight.plat.exceptions.handler;

import com.knight.plat.api.exceptions.BusinessException;
import com.knight.plat.api.exceptions.XssRiskException;
import com.knight.plat.api.runtime.FunctionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;

public interface ExceptionCollector {

    Logger log = LoggerFactory.getLogger(ExceptionCollector.class);

    default void onCatch(HttpServletRequest request, BusinessException businessException) {
        this.onCatch(request, (Throwable) businessException);
    }

    default void onCatch(ServerWebExchange exchange, BusinessException businessException) {
        this.onCatch(exchange, (Throwable) businessException);
    }

    default void onCatch(HttpServletRequest request, MethodArgumentNotValidException methodArgumentNotValidException) {
        this.onCatch(request, (Throwable) methodArgumentNotValidException);
    }

    default void onCatch(ServerWebExchange exchange, MethodArgumentNotValidException methodArgumentNotValidException) {
        this.onCatch(exchange, (Throwable) methodArgumentNotValidException);
    }

    default void onCatch(HttpServletRequest request, HttpRequestMethodNotSupportedException httpRequestMethodNotSupportedException) {
        this.onCatch(request, (Throwable) httpRequestMethodNotSupportedException);
    }

    default void onCatch(ServerWebExchange exchange, HttpRequestMethodNotSupportedException httpRequestMethodNotSupportedException) {
        this.onCatch(exchange, (Throwable) httpRequestMethodNotSupportedException);
    }

    default void onCatch(HttpServletRequest request, NullPointerException nullPointerException) {
        this.onCatch(request, (Throwable) nullPointerException);
    }

    default void onCatch(ServerWebExchange exchange, NullPointerException nullPointerException) {
        this.onCatch(exchange, (Throwable) nullPointerException);
    }

    default void onCatch(HttpServletRequest request, XssRiskException xssRiskException) {
        this.onCatch(request, (Throwable) xssRiskException);
    }

    default void onCatch(ServerWebExchange exchange, XssRiskException xssRiskException) {
        this.onCatch(exchange, (Throwable) xssRiskException);
    }

    default void onCatch(HttpServletRequest request, FunctionException functionException) {
        this.onCatch(request, (Throwable) functionException);
    }

    default void onCatch(ServerWebExchange exchange, FunctionException functionException) {
        this.onCatch(exchange, (Throwable) functionException);
    }

    default void onCatch(HttpServletRequest request, Throwable throwable) {
        final String uri = request.getRequestURI();
        log.error("Error from request: " + uri, throwable);
    }

    default void onCatch(ServerWebExchange exchange, Throwable throwable) {
        final ServerHttpRequest request = exchange.getRequest();
        final String uri = request.getURI().getPath();
        log.error("Error from request: " + uri, throwable);
    }
}
