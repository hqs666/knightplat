package com.knight.plat.exceptions.handler.impl;

import com.google.common.base.Strings;
import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.BusinessException;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.exceptions.handler.ExceptionCollector;
import com.knight.plat.exceptions.handler.ExceptionHandler;
import com.knight.plat.exceptions.handler.ExceptionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class BusinessExceptionHandler implements ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(BusinessExceptionHandler.class);

    @Override
    public ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        BusinessException be = (BusinessException) ex;
        if (!collectors.isEmpty()) {
            if (request != null) {
                collectors.forEach(c -> c.onCatch(request, be));
            }
            if (exchange != null) {
                collectors.forEach(c -> c.onCatch(exchange, be));
            }
        }
        final String code = be.getCode();
        Object message = exceptionCodeManager.getMessage(be.getCode());
        if (null == message) {
            message = be.getLocalizedMessage() != null ? be.getLocalizedMessage() : be.getMessage();
        }
        if (message == null) {
            message = "";
        }
        final String path = selectPath(request, exchange);
        if (!Strings.isNullOrEmpty(message.toString())) {
            log.info("请求服务：{}, 发生业务异常：{}", path, message);
        } else {
            log.info("请求服务：{}, 发生业务异常，并且没有提供message", path);
        }
        if (log.isDebugEnabled()) {
            ex.printStackTrace();
        }
        final ResultDTO<Object> resultDTO = new ResultDTO<>();
        resultDTO.setCode(code);
        resultDTO.setMessage(message.toString());
        resultDTO.setSuccess(Boolean.FALSE);

        return resultDTO;
    }

    @Override
    public boolean isMatch(Throwable th) {
        return th instanceof BusinessException;
    }
}
