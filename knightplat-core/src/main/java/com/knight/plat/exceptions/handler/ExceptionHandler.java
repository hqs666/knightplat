package com.knight.plat.exceptions.handler;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ExceptionHandler {

    default String selectPath(HttpServletRequest request, ServerWebExchange exchange) {
        return request != null
                ? request.getRequestURI()
                : exchange != null ? exchange.getRequest().getURI().getPath() : "unknown";
    }

    ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex);

    boolean isMatch(Throwable th);
}
