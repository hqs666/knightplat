/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.exceptions.handler;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.utils.ThreadLocalUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * Created on 2016/10/17. Exception event config.
 */
@AutoConfiguration(before = ErrorMvcAutoConfiguration.class)
@ConditionalOnClass(ExceptionCodeManager.class)
@EnableConfigurationProperties(ExceptionProperties.class)
public class ExceptionAutoConfiguration {

    private final ExceptionProperties properties;
    private final Logger log = LoggerFactory.getLogger(ExceptionAutoConfiguration.class);

    @Autowired
    public ExceptionAutoConfiguration(ExceptionProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean
    @AutoConfigureOrder(Ordered.LOWEST_PRECEDENCE)
    public ExceptionCodeManager exceptionCodeManager() {
        return new InMemoryExceptionCodeManager();
    }

    @ControllerAdvice
    @SuppressWarnings("unused")
    @ConditionalOnWebApplication(type = Type.REACTIVE)
    class WebFluxExceptionHandlerAdvice {

        private final ExceptionCodeManager exceptionCodeManager;
        private final List<ExceptionCollector> collectors;

        @Autowired
        public WebFluxExceptionHandlerAdvice(ExceptionCodeManager exceptionCodeManager,
                List<ExceptionCollector> collectors) {
            this.exceptionCodeManager = exceptionCodeManager;
            this.collectors = collectors != null ? collectors : Collections.emptyList();
        }

        @ExceptionHandler(Throwable.class)
        @ResponseBody
        @ResponseStatus(HttpStatus.OK)
        ResultDTO<Object> handle(ServerWebExchange exchange, Throwable e) {
            exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
            return internalHandle(exceptionCodeManager, collectors, null, exchange, e);
        }
    }

    @ControllerAdvice
    @SuppressWarnings("unused")
    @ConditionalOnWebApplication(type = Type.SERVLET)
    class ExceptionHandlerAdvice {

        private final ExceptionCodeManager exceptionCodeManager;
        private final List<ExceptionCollector> collectors;

        @Autowired
        public ExceptionHandlerAdvice(ExceptionCodeManager exceptionCodeManager,
                List<ExceptionCollector> collectors) {
            this.exceptionCodeManager = exceptionCodeManager;
            this.collectors = collectors != null ? collectors : Collections.emptyList();
        }

        @ExceptionHandler(Throwable.class)
        @ResponseBody
        @ResponseStatus(HttpStatus.OK)
        ResultDTO<Object> handle(HttpServletRequest request, HttpServletResponse response, Throwable e) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            return internalHandle(exceptionCodeManager, collectors, request, null, e);
        }
    }

    private ResultDTO<Object> internalHandle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            HttpServletRequest request, ServerWebExchange exchange, Throwable e) {
        try {
            // handle by type
            for (ExceptionHandlerCollection handler : ExceptionHandlerCollection.values()) {
                if (handler.isMatch(e)) {
                    return handler.handle(exceptionCodeManager, collectors, properties, request, exchange, e);
                }
            }

            // default
            if (!collectors.isEmpty()) {
                if (request != null) {
                    collectors.forEach(c -> c.onCatch(request, e));
                }
                if (exchange != null) {
                    collectors.forEach(c -> c.onCatch(exchange, e));
                }
            }
            final ResultDTO<Object> resultDTO = new ResultDTO<>();
            resultDTO.setCode(properties.getDefaultCode());
            if (properties.isHideMessage()) {
                resultDTO.setMessage(properties.getDefaultMessage());
            } else {
                resultDTO.setMessage(e.getLocalizedMessage() != null ? e.getLocalizedMessage() : e.getMessage());
            }
            resultDTO.setSuccess(Boolean.FALSE);

            log.error("捕获到非业务异常，请检查代码", e);

            //链路追踪获取调用状态
//            scopeManager.activeSpan().setBaggageItem("_status", -1);
            ThreadLocalUtils.setValue("_span_status", "false");
            return resultDTO;
        } catch (Throwable throwable) {
            log.error("异常捕获处理过程中发生新的异常，请检查<ExceptionCollector>的实现代码", throwable);
            final ResultDTO<Object> resultDTO = new ResultDTO<>();
            resultDTO.setCode(properties.getDefaultCode());
            resultDTO.setMessage(properties.getDefaultMessage());
            resultDTO.setSuccess(Boolean.FALSE);
            return resultDTO;
        }
    }
}
