package com.knight.plat.exceptions.handler;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.exceptions.handler.impl.*;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public enum ExceptionHandlerCollection {

    BUSINESS(new BusinessExceptionHandler()),
    ARGUMENT_VALID(new MethodArgumentNotValidExceptionHandler()),
    NOT_SUPPORTED(new HttpRequestMethodNotSupportedExceptionHandler()),
    NULL_POINTER(new NullPointerExceptionHandler()),
    XSS_RISK(new XssRiskExceptionHandler()),
    FUNCTION(new FunctionExceptionHandler()),
    BLOCK(new BlockedExceptionHandler());

    private final ExceptionHandler handler;

    ExceptionHandlerCollection(ExceptionHandler handler) {
        this.handler = handler;
    }

    ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        return handler.handle(exceptionCodeManager, collectors, properties, request, exchange, ex);
    }

    boolean isMatch(Throwable th) {
        return handler.isMatch(th);
    }
}
