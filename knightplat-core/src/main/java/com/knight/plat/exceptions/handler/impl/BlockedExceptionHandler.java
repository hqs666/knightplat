package com.knight.plat.exceptions.handler.impl;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.exceptions.handler.ExceptionCollector;
import com.knight.plat.exceptions.handler.ExceptionHandler;
import com.knight.plat.exceptions.handler.ExceptionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.List;

/**
 * 项目名: knightplat
 * 文件名: BlockExceptionHandler
 * 模块说明:
 * 修改历史:
 * 2023/2/9 - Knight - 创建
 */
public class BlockedExceptionHandler implements ExceptionHandler {

    private final static String[] names = {
            "com.alibaba.csp.sentinel.slots.block.BlockException",
            "com.alibaba.csp.sentinel.slots.block.authority.AuthorityException",
            "com.alibaba.csp.sentinel.slots.block.degrade.DegradeException",
            "com.alibaba.csp.sentinel.slots.block.flow.FlowException",
            "com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException",
            "com.alibaba.csp.sentinel.slots.system.SystemBlockException",
    };

    private final Logger log = LoggerFactory.getLogger(BlockedExceptionHandler.class);

    private final Class<?>[] types;
    private final Class<?> ruleType;
    private final boolean hasSentinel;

    public BlockedExceptionHandler() {
        boolean tempValue = true;
        Class<?> tempRuleType = null;
        int n = names.length;
        types = new Class<?>[n];
        try {
            for (int i = 0; i < n; i++) {
                types[i] = Class.forName(names[i]);
            }
            tempRuleType = Class.forName("com.alibaba.csp.sentinel.slots.block.AbstractRule");
        } catch (ClassNotFoundException e) {
            tempValue = false;
        }
        hasSentinel = tempValue;
        ruleType = tempRuleType;
    }

    @Override
    public ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors, ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        if (!hasSentinel) {
            throw new IllegalStateException("No BlockException class defined");
        }
        if (!collectors.isEmpty()) {
            if (request != null) {
                collectors.forEach(c -> c.onCatch(request, ex));
            }
            if (exchange != null) {
                collectors.forEach(c -> c.onCatch(exchange, ex));
            }
        }
        final Method getRuleLimitApp = getMethod(getBlockException(), "getRuleLimitApp");
        final String app = (String) ReflectionUtils.invokeMethod(getRuleLimitApp, ex);
        final Method getRule = getMethod(getBlockException(), "getRule");
        final Object rule = ReflectionUtils.invokeMethod(getRule, ex);
        if (rule != null){
            final Method getResource = getMethod(ruleType, "getResource");
            final String resource = (String) ReflectionUtils.invokeMethod(getResource, rule);
            log.error("熔断限流异常：{}-{}",app,resource);
        }else {
            log.error("熔断限流异常：{}",app);
        }
        String msg = app + "资源发生熔断限流异常，请稍后重试!";
        if (getFlowException().isInstance(ex)){
            msg = "接口限流，请稍后重试!";
        }else if (getDegradeException().isInstance(ex)){
            msg = "服务降级，请稍后重试";
        }else if(getParamFlowException().isInstance(ex)){
            msg = "参数限流，请稍后重试";
        }else if (getAuthorityException().isInstance(ex)){
            msg = "权限规则不通过，请稍后重试";
        }else if (getSystemBlockException().isInstance(ex)){
            msg = "系统保护，请稍后重试";
        }
        final ResultDTO<Object> resultDTO = new ResultDTO<>();
        resultDTO.setCode(properties.getBlockCode());
        resultDTO.setMessage(msg);
        resultDTO.setSuccess(Boolean.FALSE);
        return resultDTO;
    }

    @Override
    public boolean isMatch(Throwable th) {
        if (!hasSentinel) {
            return false;
        }
        final Method isBlockException = getMethod(getBlockException(), "isBlockException", Throwable.class);
        final Boolean booleanValue = (Boolean) ReflectionUtils.invokeMethod(isBlockException, null, th);
        return booleanValue != null && booleanValue;
    }

    private Class<?> getBlockException() {
        return types[0];
    }

    private Class<?> getAuthorityException() {
        return types[1];
    }

    private Class<?> getDegradeException() {
        return types[2];
    }

    private Class<?> getFlowException() {
        return types[3];
    }

    private Class<?> getParamFlowException() {
        return types[4];
    }

    private Class<?> getSystemBlockException() {
        return types[5];
    }

    private Method getMethod(Class<?> obj, String methodName, Class<?>... parameterTypes) {
        try {
            return obj.getDeclaredMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
    }

}
