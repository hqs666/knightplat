package com.knight.plat.exceptions.handler.impl;

import com.knight.plat.api.commons.ResultDTO;
import com.knight.plat.api.exceptions.ExceptionCodeManager;
import com.knight.plat.exceptions.handler.ExceptionCollector;
import com.knight.plat.exceptions.handler.ExceptionHandler;
import com.knight.plat.exceptions.handler.ExceptionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.server.ServerWebExchange;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class HttpRequestMethodNotSupportedExceptionHandler implements ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(HttpRequestMethodNotSupportedExceptionHandler.class);

    @Override
    public ResultDTO<Object> handle(ExceptionCodeManager exceptionCodeManager, List<ExceptionCollector> collectors,
            ExceptionProperties properties, HttpServletRequest request, ServerWebExchange exchange, Throwable ex) {
        HttpRequestMethodNotSupportedException mne = (HttpRequestMethodNotSupportedException) ex;
        if (!collectors.isEmpty()) {
            if (request != null) {
                collectors.forEach(c -> c.onCatch(request, mne));
            }
            if (exchange != null) {
                collectors.forEach(c -> c.onCatch(exchange, mne));
            }
        }
        final String causeMethod = mne.getMethod();
        final String[] methodArr = mne.getSupportedMethods();
        final String supportedMethods = methodArr != null ? String.join(", ", methodArr) : "";

        final String message = "不支持: [" + causeMethod + "] 方法, 支持的请求方式为: [" + supportedMethods + "]";
        log.error("请求的路径: " + selectPath(request, exchange) + ", " + message);

        final ResultDTO<Object> resultDTO = new ResultDTO<>();
        resultDTO.setCode(properties.getMethodNotSupportedCode());
        resultDTO.setMessage(message);
        resultDTO.setSuccess(Boolean.FALSE);

        return resultDTO;
    }

    @Override
    public boolean isMatch(Throwable th) {
        return th instanceof HttpRequestMethodNotSupportedException;
    }
}
