/*
 * Copyright (c) 2016. knight Co., Ltd. All rights reserved.
 */

package com.knight.plat.exceptions.handler;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created on 2016/11/23.
 */
@ConfigurationProperties("knightplat.exception")
public class ExceptionProperties {

    private String defaultCode = "RY00";
    private String validationCode = "RY01";
    private String methodNotSupportedCode = "RY02";
    private String nullPointerCode = "RY03";
    private String xssCode = "RY04";
    private String functionCode = "RY05";
    private String blockCode = "RY06";
    private String defaultMessage = "请稍后重试";
    private String defaultXssMessage = "输入的内容含有非法格式";
    private String defaultValidateMessageHeader = "参数异常; ";
    private String defaultValidateMessageList = "字段<{{field}}>: {{message}}";
    private boolean hideMessage = true;

    public String getDefaultCode() {
        return defaultCode;
    }

    public void setDefaultCode(String defaultCode) {
        this.defaultCode = defaultCode;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    public void setDefaultMessage(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public boolean isHideMessage() {
        return hideMessage;
    }

    public void setHideMessage(boolean hideMessage) {
        this.hideMessage = hideMessage;
    }

    public String getMethodNotSupportedCode() {
        return methodNotSupportedCode;
    }

    public void setMethodNotSupportedCode(String methodNotSupportedCode) {
        this.methodNotSupportedCode = methodNotSupportedCode;
    }

    public String getNullPointerCode() {
        return nullPointerCode;
    }

    public void setNullPointerCode(String nullPointerCode) {
        this.nullPointerCode = nullPointerCode;
    }

    public String getXssCode() {
        return xssCode;
    }

    public void setXssCode(String xssCode) {
        this.xssCode = xssCode;
    }

    public String getDefaultXssMessage() {
        return defaultXssMessage;
    }

    public void setDefaultXssMessage(String defaultXssMessage) {
        this.defaultXssMessage = defaultXssMessage;
    }

    public String getDefaultValidateMessageHeader() {
        return defaultValidateMessageHeader;
    }

    public void setDefaultValidateMessageHeader(String defaultValidateMessageHeader) {
        this.defaultValidateMessageHeader = defaultValidateMessageHeader;
    }

    public String getDefaultValidateMessageList() {
        return defaultValidateMessageList;
    }

    public void setDefaultValidateMessageList(String defaultValidateMessageList) {
        this.defaultValidateMessageList = defaultValidateMessageList;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getBlockCode() {
        return blockCode;
    }

    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }
}
