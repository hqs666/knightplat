package com.knight.plat.task;

import com.knight.plat.api.task.TaskManager;
import com.knight.plat.api.task.TaskStrategy;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

@AutoConfiguration
public class TaskAutoConfiguration {

    @Lazy
    @Bean(destroyMethod = "close")
    public TaskManager taskManager(TaskStrategy taskStrategy) {
        return new TaskManagerImpl(taskStrategy);
    }
}
