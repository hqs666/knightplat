use std::cmp::min;
use std::slice;
use jni::JNIEnv;
use jni::objects::JByteBuffer;
use rsa::pkcs8::{DecodePrivateKey, DecodePublicKey};
use rsa::{Pkcs1v15Encrypt, RsaPrivateKey, RsaPublicKey};

use crate::base64_internal::{base64_decode, base64_encode};
use crate::errors::Result;

pub fn rsa_encode<'local>(
    mut env: JNIEnv<'local>,
    data_buf: JByteBuffer<'local>,
    pub_key_buf: JByteBuffer<'local>,
    base64: bool,
) -> Result<JByteBuffer<'local>> {
    let pub_key_bytes = buffer_to_bytes(pub_key_buf, &env)?;
    let data_bytes = buffer_to_bytes(data_buf, &env)?;
    let pk = RsaPublicKey::from_public_key_der(pub_key_bytes)?;

    let mut rng = rand::thread_rng();
    let len = data_bytes.len();
    let encoded = if len > 117 {
        let mut encoded_vec = Vec::new();
        let mut i = 0;
        while i < len {
            let next_index = i + 117;
            let end = min(len, next_index);
            let mut part = pk.encrypt(&mut rng, Pkcs1v15Encrypt, &data_bytes[i..end])?;
            encoded_vec.append(&mut part);
            i = next_index;
        }
        encoded_vec
    } else {
        pk.encrypt(&mut rng, Pkcs1v15Encrypt, data_bytes)?
    };
    let encoded = if base64 {
        base64_encode(encoded, false)?
    } else {
        encoded
    };
    let len = encoded.len();
    let result_buf = unsafe { env.new_direct_byte_buffer(encoded.leak().as_mut_ptr(), len) }?;
    Ok(result_buf)
}

pub fn rsa_decode<'local>(
    mut env: JNIEnv<'local>,
    data_buf: JByteBuffer<'local>,
    pri_key_buf: JByteBuffer<'local>,
    base64: bool,
) -> Result<JByteBuffer<'local>> {
    let pri_key_bytes = buffer_to_bytes(pri_key_buf, &env)?;
    let data_bytes = buffer_to_bytes(data_buf, &env)?;
    let data_vec: Vec<u8>;
    let data_bytes = if base64 {
        data_vec = base64_decode(data_bytes, false)?;
        data_vec.as_slice()
    } else {
        data_bytes
    };
    let pk = RsaPrivateKey::from_pkcs8_der(pri_key_bytes)?;

    let len = data_bytes.len();
    let decoded = if len > 128 {
        let mut decoded_vec = Vec::new();
        let mut i = 0;
        while i < len {
            let next_index = i + 128;
            let end = min(len, next_index);
            let mut part = pk.decrypt(Pkcs1v15Encrypt, &data_bytes[i..end])?;
            decoded_vec.append(&mut part);
            i = next_index;
        }
        decoded_vec
    } else {
        pk.decrypt(Pkcs1v15Encrypt, data_bytes)?
    };
    let len = decoded.len();
    let result_buf = unsafe { env.new_direct_byte_buffer(decoded.leak().as_mut_ptr(), len) }?;
    Ok(result_buf)
}

pub(crate) fn buffer_to_bytes<'a>(buf: JByteBuffer<'a>, env: &'a JNIEnv<'a>) -> Result<&'a [u8]> {
    let data_bytes = env.get_direct_buffer_address(&buf)?;
    let data_len = env.get_direct_buffer_capacity(&buf)?;
    Ok(unsafe { slice::from_raw_parts(data_bytes, data_len) })
}