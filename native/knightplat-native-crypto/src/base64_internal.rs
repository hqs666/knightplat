use base64::{alphabet, engine, Engine};

use crate::errors::Result;

const ENGINE: engine::GeneralPurpose = engine::GeneralPurpose::new(&alphabet::STANDARD, engine::general_purpose::PAD);
const ENGINE_URL: engine::GeneralPurpose = engine::GeneralPurpose::new(&alphabet::URL_SAFE, engine::general_purpose::PAD);

pub fn base64_encode<T: AsRef<[u8]>>(bytes: T, is_url: bool) -> Result<Vec<u8>> {
    let engine = if is_url { ENGINE_URL } else { ENGINE };
    Ok(engine.encode(bytes).into_bytes())
}

pub fn base64_decode<T: AsRef<[u8]>>(bytes: T, is_url: bool) -> Result<Vec<u8>> {
    let engine = if is_url { ENGINE_URL } else { ENGINE };
    Ok(engine.decode(bytes)?)
}