mod rsa_internal;
mod errors;
mod base64_internal;

use jni::JNIEnv;
use jni::objects::{JByteBuffer, JObject};
use jni::sys::{jboolean, JNI_TRUE};

#[no_mangle]
pub extern "system" fn Java_com_knight_plat_crypto_CryptoNativeManager_nativeTest<'local>(
    env: JNIEnv<'local>,
    _object: JObject<'local>,
    data: JByteBuffer<'local>,
) -> JByteBuffer<'local> {
    let data_len = env.get_direct_buffer_capacity(&data).unwrap();
    let data_slice = env.get_direct_buffer_address(&data).unwrap();
    let mut date_string = unsafe { String::from_raw_parts(data_slice, data_len, data_len) };
    date_string.insert_str(0, "Hello ");
    date_string.push_str(" & Rust, 我们都是好朋友😄");
    make_buffer(date_string, env)
}

#[no_mangle]
pub extern "system" fn Java_com_knight_plat_crypto_CryptoNativeManager_rsaEncode<'local>(
    env: JNIEnv<'local>,
    _object: JObject<'local>,
    data_buf: JByteBuffer<'local>,
    pub_key_buf: JByteBuffer<'local>,
    flag_buf: JByteBuffer<'local>,
    base64: jboolean,
) -> JByteBuffer<'local> {
    let env_clone = unsafe { env.unsafe_clone() };
    match rsa_internal::rsa_encode(env_clone, data_buf, pub_key_buf, base64 == JNI_TRUE) {
        Ok(result) => unsafe {
            set_ok_flag(flag_buf, &env);
            result
        }
        Err(err) => unsafe {
            match err {
                errors::Error::InvalidKey { cause: _ } => set_err_flag(flag_buf, &env, 2),
                _ => set_err_flag(flag_buf, &env, 1),
            };
            make_buffer(err.to_string(), env)
        }
    }
}

#[no_mangle]
pub extern "system" fn Java_com_knight_plat_crypto_CryptoNativeManager_rsaDecode<'local>(
    env: JNIEnv<'local>,
    _object: JObject<'local>,
    data_buf: JByteBuffer<'local>,
    pri_key_buf: JByteBuffer<'local>,
    flag_buf: JByteBuffer<'local>,
    base64: jboolean,
) -> JByteBuffer<'local> {
    let env_clone = unsafe { env.unsafe_clone() };
    match rsa_internal::rsa_decode(env_clone, data_buf, pri_key_buf, base64 == JNI_TRUE) {
        Ok(result) => unsafe {
            set_ok_flag(flag_buf, &env);
            result
        }
        Err(err) => unsafe {
            match err {
                errors::Error::InvalidKey { cause: _ } => set_err_flag(flag_buf, &env, 2),
                _ => set_err_flag(flag_buf, &env, 1),
            };
            make_buffer(err.to_string(), env)
        }
    }
}

fn make_buffer(err: String, mut env: JNIEnv) -> JByteBuffer {
    let x = err.into_bytes();
    let len = x.len();
    unsafe { env.new_direct_byte_buffer(x.leak().as_mut_ptr(), len) }.unwrap()
}

unsafe fn set_ok_flag(flag_buf: JByteBuffer, env: &JNIEnv) {
    env.get_direct_buffer_address(&flag_buf).unwrap().write(0);
}

unsafe fn set_err_flag(flag_buf: JByteBuffer, env: &JNIEnv, val: u8) {
    env.get_direct_buffer_address(&flag_buf).unwrap().write(val);
}