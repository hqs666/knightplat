use rsa::pkcs8::spki;
use thiserror::Error;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Error, Debug)]
pub enum Error {

    #[error("JNI error, cause: {cause}")]
    Jni {cause: String},

    #[error("Invalid key, cause: {cause}")]
    InvalidKey {cause: String},

    #[error("RSA error, cause: {cause}")]
    Rsa {cause: String},

    #[error("Base64 {kind} error, cause: {cause}")]
    Base64 { kind: &'static str, cause: String },
}

impl From<jni::errors::Error> for Error {
    fn from(value: jni::errors::Error) -> Self {
        Error::Jni { cause: value.to_string() }
    }
}

impl From<spki::Error> for Error {
    fn from(value: spki::Error) -> Self {
        Error::InvalidKey { cause: value.to_string() }
    }
}

impl From<rsa::pkcs8::Error> for Error {
    fn from(value: rsa::pkcs8::Error) -> Self {
        Error::InvalidKey { cause: value.to_string() }
    }
}

impl From<rsa::Error> for Error {
    fn from(value: rsa::Error) -> Self {
        Error::Rsa { cause: value.to_string() }
    }
}

impl From<base64::DecodeError> for Error {
    fn from(value: base64::DecodeError) -> Self {
        Error::Base64 { kind: "decode", cause: value.to_string() }
    }
}